<?php

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2011 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: luofei614 <weibo.com/luofei614>　
// +----------------------------------------------------------------------

namespace Think;

/**
 * 权限认证类
 * 功能特性：
 * 1，是对规则进行认证，不是对节点进行认证。用户可以把节点当作规则名称实现对节点进行认证。
 *      $auth=new Auth();  $auth->check('规则名称','用户id')
 * 2，可以同时对多条规则进行认证，并设置多条规则的关系（or或者and）
 *      $auth=new Auth();  $auth->check('规则1,规则2','用户id','and') 
 *      第三个参数为and时表示，用户需要同时具有规则1和规则2的权限。 当第三个参数为or时，表示用户值需要具备其中一个条件即可。默认为or
 * 3，一个用户可以属于多个用户组(think_auth_group_access表 定义了用户所属用户组)。我们需要设置每个用户组拥有哪些规则(think_auth_group 定义了用户组权限)
 * 
 * 4，支持规则表达式。
 *      在think_auth_rule 表中定义一条规则时，如果type为1， condition字段就可以定义规则表达式。 如定义{score}>5  and {score}<100  表示用户的分数在5-100之间时这条规则才会通过。
 */
//数据库
/*
  -- ----------------------------
  -- think_auth_rule，规则表，
  -- id:主键，name：规则唯一标识, title：规则中文名称 status 状态：为1正常，为0禁用，condition：规则表达式，为空表示存在就验证，不为空表示按照条件验证
  -- ----------------------------
  DROP TABLE IF EXISTS `think_auth_rule`;
  <org>
  CREATE TABLE `think_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',  # 规则附件条件,满足附加条件的规则,才认为是有效的规则
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
  ) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
  </org>
  <hack author="SoChishun" date="2015-9-22">
  CREATE TABLE IF NOT EXISTS think_auth_rule (
  `id` int auto_increment primary key comment '主键编号',
  `name` varchar(20) not null default '' comment '权限名称(模块、控制器或操作的名称)',
  `title` varchar(50) not null comment '权限标题',
  `code` varchar(32) not null default '' comment '菜单代码(必须加厂商代码前缀,如：P10000_M1_YH,P10000_YH_M2_YHGL,P10000_YH_M3_GLYGL,P10000_YH_GLYGL_XZGLY)',
  `pid` smallint(6) unsigned NOT NULL COMMENT '父级编号(模块pid默认是0)',
  `addon` varchar(32) not null default '' comment '插件名称(应用模块名称,用于卸载应用模块时索引)',
  `level` tinyint(1) unsigned NOT NULL COMMENT '节点等级(1:模块,2:控制器,3:操作)',
  `type` varchar(16) not null default '' comment '类型(M(Menu)=菜单,O(Operate)=操作,F(File)=文件,E(Element)=页面元素)',
  `url` varchar(64) not null default '' comment '链接URL',
  `condition` varchar(128)) not null default '',  # 规则附件条件,满足附加条件的规则,才认为是有效的规则
  `remark` varchar(32) not null default '' comment '备注',
  `sort` smallint not null default 0 comment '排列次序',
  `status` smallint not null default '0' comment '状态(0=禁用,1=可用)',
  `create_time` timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
  INDEX ix_code (code),
  INDEX ix_pid (pid),
  INDEX ix_addon (addon)
  ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='权限规则(节点)表\r\n
  @since 1.0 <2014-6-28> SoChishun <14507247@qq.com> Added.\r\n
  @since 1.1 <2015-8-28> SoChishun 修改:parent_id 改为 parent_code.\r\n
  @since 1.2 <2015-8-29> SoChishun 新增:vendor_code,appmodule_name.\r\n
  @since 2.0 <2015-9-19> SoChishun 重构以适合RBAC的自动拦截';
  </hack>
  -- ----------------------------
  -- think_auth_group 用户组表，
  -- id：主键， title:用户组中文名称， rules：用户组拥有的规则id， 多个规则","隔开，status 状态：为1正常，为0禁用
  -- ----------------------------
  DROP TABLE IF EXISTS `think_auth_group`;
  CREATE TABLE `think_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` varchar(800) NOT NULL DEFAULT '',
  `code` varchar(32) not null default '' comment '角色代码',
  `remark` varchar(32) not null default '' comment '备注',
  `sort` smallint not null default 0 comment '排列次序',
  `create_time` timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
  PRIMARY KEY (`id`)
  ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户组(角色)表';
  -- ----------------------------
  -- think_auth_group_access 用户组明细表
  -- uid:用户id，group_id：用户组id
  -- ----------------------------
  DROP TABLE IF EXISTS `think_auth_group_access`;
  CREATE TABLE `think_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户和用户组(角色)的对应表';
 */

/**
 * 权限认证类
 * @since 1.0 <2015-9-23> SoChishun <14507247@qq.com> Hack.
 */
class AuthX {

    //默认配置
    protected $_config = array(
        'AUTH_ON' => true, // 认证开关
        'AUTH_TYPE' => 1, // 认证方式，1为实时认证；2为登录认证。
        'AUTH_GROUP' => 'think_auth_group', // 用户组数据表名
        'AUTH_GROUP_ACCESS' => 'think_auth_group_access', // 用户-用户组关系表
        'AUTH_RULE' => 'think_auth_rule', // 权限规则表
        'AUTH_USER' => 'think_user'             // 用户信息表
    );

    public function __construct() {
        $prefix = C('DB_PREFIX');
        $this->_config['AUTH_GROUP'] = $prefix . $this->_config['AUTH_GROUP'];
        $this->_config['AUTH_RULE'] = $prefix . $this->_config['AUTH_RULE'];
        $this->_config['AUTH_USER'] = $prefix . $this->_config['AUTH_USER'];
        $this->_config['AUTH_GROUP_ACCESS'] = $prefix . $this->_config['AUTH_GROUP_ACCESS'];
        if (C('AUTH_CONFIG')) {
            //可设置配置项 AUTH_CONFIG, 此配置项为数组。
            $this->_config = array_merge($this->_config, C('AUTH_CONFIG'));
        }
    }

    /**
     * 检查权限
     * @param string|array $name  需要验证的规则列表,支持逗号分隔的权限规则或索引数组
     * @param int $uid           认证用户的id
     * @param int $type 认证方式,1为实时认证,2为登录认证
     * @param string $mode        执行check的模式
     * @param string $relation    如果为 'or' 表示满足任一条规则即通过验证;如果为 'and'则表示需满足所有规则才能通过验证
     * @return boolean           通过验证返回true;失败返回false
     */
    public function check($name, $uid, $type = 1, $mode = 'url', $relation = 'or') {
        if (!$this->_config['AUTH_ON'])
            return true;
        $authList = $this->getAuthList($uid, $type); //获取用户需要验证的所有有效规则列表
        if (is_string($name)) {
            $name = strtolower($name);
            if (strpos($name, ',') !== false) {
                $name = explode(',', $name);
            } else {
                $name = array($name);
            }
        }
        $list = array(); //保存验证通过的规则名
        if ($mode == 'url') {
            $REQUEST = unserialize(strtolower(serialize($_REQUEST)));
        }
        foreach ($authList as $auth) {
            $query = preg_replace('/^.+\?/U', '', $auth);
            if ($mode == 'url' && $query != $auth) {
                parse_str($query, $param); //解析规则中的param
                $intersect = array_intersect_assoc($REQUEST, $param);
                $auth = preg_replace('/\?.*$/U', '', $auth);
                if (in_array($auth, $name) && $intersect == $param) {  //如果节点相符且url参数满足
                    $list[] = $auth;
                }
            } else if (in_array($auth, $name)) {
                $list[] = $auth;
            }
        }
        if ($relation == 'or' and ! empty($list)) {
            return true;
        }
        $diff = array_diff($name, $list);
        if ($relation == 'and' and empty($diff)) {
            return true;
        }
        return false;
    }

    /**
     * 根据用户id获取用户组,返回值为数组
     * @param int $uid     用户id
     * @return array       用户所属的用户组 array(
     *     array('uid'=>'用户id','group_id'=>'用户组id','title'=>'用户组名称','rules'=>'用户组拥有的规则id,多个,号隔开'),
     *     ...)   
     */
    public function getGroups($uid) {
        static $groups = array();
        if (isset($groups[$uid]))
            return $groups[$uid];
        $user_groups = M()
                        ->table($this->_config['AUTH_GROUP_ACCESS'] . ' a')
                        ->where("a.uid='$uid' and g.status='1'")
                        ->join($this->_config['AUTH_GROUP'] . " g on a.group_id=g.id")
                        ->field('uid,group_id,title,rules')->select();
        $groups[$uid] = $user_groups? : array();
        return $groups[$uid];
    }

    /**
     * 获得权限列表
     * @param integer $uid  用户id
     * @param integer $type 
     */
    protected function getAuthList($uid, $type) {
        static $_authList = array(); //保存用户验证通过的权限列表
        $t = implode(',', (array) $type);
        if (isset($_authList[$uid . $t])) {
            return $_authList[$uid . $t];
        }
        if ($this->_config['AUTH_TYPE'] == 2 && isset($_SESSION['_AUTH_LIST_' . $uid . $t])) {
            return $_SESSION['_AUTH_LIST_' . $uid . $t];
        }

        //读取用户所属用户组
        $groups = $this->getGroups($uid);
        $ids = array(); //保存用户所属用户组设置的所有权限规则id
        foreach ($groups as $g) {
            $ids = array_merge($ids, explode(',', trim($g['rules'], ',')));
        }
        $ids = array_unique($ids);
        if (empty($ids)) {
            $_authList[$uid . $t] = array();
            return array();
        }

        $map = array(
            'id' => array('in', $ids),
            //'type'=>$type, // 2015-9-23 SoChishun 注释
            'status' => 1,
        );
        //读取用户组所有权限规则
        $rules = M()->table($this->_config['AUTH_RULE'])->where($map)->field('condition,name')->select();

        //循环规则，判断结果。
        $authList = array();   //
        foreach ($rules as $rule) {
            if (!empty($rule['condition'])) { //根据condition进行验证
                $user = $this->getUserInfo($uid); //获取用户信息,一维数组

                $command = preg_replace('/\{(\w*?)\}/', '$user[\'\\1\']', $rule['condition']);
                //dump($command);//debug
                @(eval('$condition=(' . $command . ');'));
                if ($condition) {
                    $authList[] = strtolower($rule['name']);
                }
            } else {
                //只要存在就记录
                $authList[] = strtolower($rule['name']);
            }
        }
        $_authList[$uid . $t] = $authList;
        if ($this->_config['AUTH_TYPE'] == 2) {
            //规则列表结果保存到session
            $_SESSION['_AUTH_LIST_' . $uid . $t] = $authList;
        }
        return array_unique($authList);
    }

    /**
     * 获得用户资料,根据自己的情况读取数据库
     */
    protected function getUserInfo($uid) {
        static $userinfo = array();
        if (!isset($userinfo[$uid])) {
            $userinfo[$uid] = M()->where(array('uid' => $uid))->table($this->_config['AUTH_USER'])->find();
        }
        return $userinfo[$uid];
    }

    /**
     * 获得菜单列表
     * @param int $uid
     * @param int|array $cache 缓存选项
     * @return array
     * @since 1.0 <2015-9-23> SoChishun <14507247@qq.com> Added.
     * @example $menus = $auth->getMenus($login_data['id']);
     */
    public function getMenus($uid,$cache=20) {
        //缓存处理
        $key='auth_menu_'.$uid;
        if(S($key)){
            return S($key);
        }
        //读取用户所属用户组
        $groups = $this->getGroups($uid);
        $ids = array(); //保存用户所属用户组设置的所有权限规则id
        foreach ($groups as $g) {
            $ids = array_merge($ids, explode(',', trim($g['rules'], ',')));
        }
        $ids = array_unique($ids);
        if (empty($ids)) {
            return array('status' => false, 'info' => '用户未归组或用户组权限未配置');
        }
        $map = array(
            'id' => array('in', $ids),
            'type' => 'M',
            'status' => 1,
        );
        //读取用户组所有权限规则
        $rules = M()->table($this->_config['AUTH_RULE'])->where($map)->field('id,name,title,pid,code,url,addon')->select();
        if ($rules) {
            foreach ($rules as &$row) {
                $row['url'] = $row['addon'] ? U($row['url'], 'addon=' . $row['addon']) : U($row['url']);
            }
            $out = array('status' => true, 'info' => list_to_tree($rules, 'id', 'pid', 'children'));
        } else {
            $out = array('status' => false, 'info' => '用户组没有任何权限');
        }
        S($key,$out,$cache);
        return $out;
    }

}
