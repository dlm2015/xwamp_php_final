<?php

namespace Org\SueCrypt;

/**
 * SuEncrypt 实用加密类
 * 用途：数组转序列号,字符串加密,支持中文
 * 吐槽：PHP加密功能的完成总算解决了自己的一个心病。
 *  以前somda挺强大的，但是逻辑想法太复杂，原来的草稿又遗失了，现在想看代码回忆流程有点头疼。
 *  这个加密版本比较简洁，性能也还不错，速度方面理论上应该比somda要高，虽然强度没有somda复杂，
 *  但是满足生产环境的需求应该是绰绰有余了，关键是要把本文件加密，不然被下载了就容易破解了。
 * @since 1.0 <2015-7-9> SoChishun Added.
 */
class SueStrmix {

    // ==字符串加密==
    /**
     * 混淆字典
     * @var array
     * @since 1.0 <2015-7-9> SoChishun Added.
     */
    private static $dict = array(
        'QFLUWrsvzf#T7xjum6ly2.bkAtSBGqgEDn5oICd*N4aHiROMwYV8J%eX3KPh9cZ10p',
        '%rXuJ*e#i9Mp2tZs4Nw8fy0hdEWlaFjQTgA.vkUVxK6mqBDI1HG7O5LoSbnY3CRczP',
        'LWsk2yhdt*Cxv5H3ZuRS#lK6zOgUGcMPqYNI4jQimfaDE.0nFBTX7eoVpr1Abw9%8J',
        'MonD#NBSmXid*12Qvc9CAOjb5qTyJHf4IL7xFVsPKGU0ktR%glze6YW8hZ.pw3ruaE',
        '9SgQzxHVy56Jboj4GiEF.UD%*rfuqpP0OR1hsBtZXdkcYIn2eT3AL#lC7MwamNK8Wv',
        '%5cwubKe0t2WXsA*ENB9JVOY4Gq67jLQfalrCn3pHI#dogvyDRPm1M8SUihFZzxTk.',
        'agjQ0Gzq7*x9oYp6BS5VWRsmDdJZUXF.Ovc#8I%f2rLyikTe1w4CtP3EHhuMNKblAn',
        'AgqY95yjbv2IMfn*swdhxoUic0kOJpF7S%3z6XHLGBVEZaWtT8KPrCR4le#N1D.Qmu',
        'GhsXRTk6nJ9c3QPf*pK1#tMyE4zgwoW5I2.B%AbqmCDrUZYl0HuOi7SxedjVL8vNFa',
        '9bBdNLqRm#YAoUIh1OZnspczEX7WQwe82y3g*.G4H%xTjM5PlCD0Ffv6SkVrutKaJi',
        'TmxEoMCfe8y4dAVP1ROlh#0c.jw%i7uXQInUq9ZFbJg6Kra*kGsvY5BHND3Lztp2WS',
        'xopdIXB98kTAn.j5Nfb%YR4tgai2ch*PuU0KHyrq#CVlezZWF61QvJEMO3Ssm7LGDw',
        'nryjbPJqDCtZQAsFwWNG7Ucev16.K8%hf5oX2xgVma0kuY4l9M*#LpBdEzRiTS3IOH',
        'h9PlIS7Vexu3UJfgNaFdXkzLHTEqpbCGvB8wnAZRci0KD#.jr5t4YosO2y6*Wm1M%Q',
        '4svEum9WXIq%ZpHt2KQS06gbR#AD81OF5BVkfahUj3zdnGPwJMCNyc.o7Ll*TxiYer',
        'iSzogqXRycxd7K0%ME3Y1NBIl9kTW*UDrQZG5a86Abhujw2LtpvOJCFes.fm4P#HnV',
        'JyMAxqsNIm2YTe#a3Xjg1KVbwGlSHPiF46pU0R*8LhCcnEf5dWBQruzk%97OvoD.Zt',
        'mxIKSiDHyzERYVleu*AGPjhC4fU.2JFkt%pocON#Z0qaTdM9Qrbw1W3Lgv85s6nXB7',
        'xZODUrNJ*0S6qQz4Yl8gsi%ChPKtEBb9FwnLjXW1pmadu27y5fHTGkIVec#ovA3MR.',
        'FlIbiRJn*LgYxuEszvk5P3ZSH#QTwy10Nat.WA7hD2VBCOfUcqp%oe4mKd98rj6GMX',
        'fI*em41Yc58W23O7sZ.vPKti#rBjwNabMqDhXzRxGd0LCSEJAoUk%FyQVuH9nlgpT6',
        'UoFS0hVO32Diuyxml8awLdHbqATz#rPfNvZ7JpI4Yg.MK6*WEX%cenGQBjR5t9kCs1',
        'ZicP5xpvmO67jCDVbqXtT*E9.zB0JgY%WIN3yAheUnd2QLSMkrR1#KG4Hsuflowa8F',
        'rQbmVTZestvclIgYW4SjKhODPapRGx*.yE79uHFN6JAL3C#zB2U0dqoXw5nfk%iM81',
        'gbInoyD7kpZz6cQB.jNsSWqXK9xEG2CweYLOa3iR%Mf0#u5Jd14VhAv8lt*PHrmUFT',
        'ygEVcZvmoWbRIOK%5tA4MBGa36sHCL*hX9lP7S2wj8JDeFU0NxfT.dpY1i#qQzkunr',
        'q5o4euyjflNTQDix7d#R0atsBYPZAGLVE*nzJvm8XHKwC9M2bkg%IO3.FcUpWS1h6r',
    );

    /**
     * 字符串混淆
     * 只支持英文和数字,不支持中文
     * @param string $str 原文
     * @return string 混淆后的字符串, 首字符是大小写字母
     * @since 1.0 <2015-7-9> SoChishun Added.
     * @since 1.1 2016-4-26 SoChishun 修正参数为数值类型的时候加密错误的问题
     */
    public static function encrypt($str) {
        if (!$str) {
            return $str;
        }
        $str = strval($str);
        $dict_idx = 'hUBQDoaSmipMGgtfqvFZsCJreVKxLPAjTbkOWcnwYdzXyRlNIuEH';
        $dict_org = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=/-';
        $id = rand(0, 26);
        $dictmix = self::$dict[$id]; // 随机抽取一组混淆码
        $idx = $dict_idx[$id];
        $n = strlen($str) - 1;
        $mix = '';
        while ($n >= 0) {
            $i = strpos($dict_org, $str[$n]);
            $mix.=$dictmix[$i];
            $n--;
        }
        return $idx . $mix;
    }

    /**
     * 反字符串混淆
     * @param string $str 混淆后的字符串
     * @return string 未混淆的字符串原文
     * @since 1.0 <2015-7-9> SoChishun Added.
     */
    public static function decrypt($str) {
        if (!$str) {
            return $str;
        }
        $dict_idx = 'hUBQDoaSmipMGgtfqvFZsCJreVKxLPAjTbkOWcnwYdzXyRlNIuEH';
        $dict_org = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+=/-';
        $idx = $str[0];
        $id = strpos($dict_idx, $idx);
        $str = substr($str, 1);
        $dictmix = self::$dict[$id];
        $n = strlen($str) - 1;
        $org = '';
        while ($n >= 0) {
            $i = strpos($dictmix, $str[$n]);
            $org.=$dict_org[$i];
            $n--;
        }
        return $org;
    }

    // ==/字符串加密==
}
