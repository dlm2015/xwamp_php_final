#xwamp_php_final

**前言：**<br />
十来年的互联网开发经验，感觉自己依然是一只技术上的菜鸟，至今没有什么得意作品，以前ASP时代的作品"ISU"已经丢失，ASP.NET时代并没有具体深耕，.Net客户端的UAS也是简陋甚至未完成的作品，后来接触了Linux服务器，就选定了PHP。PHP不是完美的语言，但是，至少在跨平台、开发效率方面，它是屈指可数的选择。<br />
xwamp已经历经数个发行版，对UI框架的探索也渐渐成熟，从第一次系统地、规范化地基于Jquery UI的原创的SoUI，到高性能的第三方框架Jquery EeasyUI，后来因为Jquery EasyUI版权保护而在SoUI基础上重构的SuUIX1, 再到借鉴Bootstrap、AmazeUI等框架而再次重构的SuUIX2,再到现在集以往所有经验而再次重构的SuUIX3，辛苦一番后，最终找到了最适合自己需求的UI框架。
从final版本开始，成熟可靠、灵活高效将成为xwamp的基因。对框架和架构的探索决定停止，接下来时间，将把精力用在final版本的开发上，最终成为一个完整的正式版，而不是带有浮躁基因的应付性试验品。
final版本开发完成后，理论上不会再进行大的架构调整，除非ThinkPHP版本升级。SuUIX3也将不再对框架进行大的调整，但将继续改进和优化用户体验。其余精力，都将用在业务模块的开发和优化改进方面，让xwamp_final成为自己的PHP语言方面的得意作品。
<br />
**简介：**<br />
XWAMP_PHP_Final是基于ThinkPHP框架开发的"可扩展网络应用程序模块平台"，旨在通过灵活的扩展性和模块化的开发思维，让xwamp成为囊括crm、cms、eshop等行业需求的通用型网站产品。<br />
系统环境需求：PHP 5.3以上版本，MySQL 5.5以上版本。
<br />
**特性：**<br />
1、基于ThinkPHP，广大TPer上手无难度，不断挖掘TP的潜力，把TP效能最大化。<br />
2、基于SuUIX3界面框架，代码易懂，界面架构简单，排版很容易。<br />
3、拥抱开源，版权协议与ThinkPHP一致，向ThinkPHP作者致敬！<br />
<br />
<br />
项目主页：http://git.oschina.net/sutroon/xwamp_php_final<br />
