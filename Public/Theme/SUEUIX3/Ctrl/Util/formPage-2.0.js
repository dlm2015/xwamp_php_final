/**
 * 表单页面类
 * @type Object
 * @since 1.0 <2015-10-17> SoChishun Added.
 * @since 2.0 <2016-5-9> SoChishun 重构, FormPage 重命名为 formPage.
 */
var formPage = {
    /* 
     * 表单页面视图初始化入口
     * @since 1.0 <2015-10-17> SoChishun Added.
     */
    ui_init: function () {
        // 调试模式显示提交按钮 2015-10-17 SoChishun Added.
        if (window.opener) {
            //$(':submit').show();
        }
    },
    /**
     * 添加对话框按钮
     * @param {type} ifrm
     * @param {type} btn
     * @param {type} pos
     * @returns {undefined}
     * @since 1.0 <2015-11-3> SoChishun Added.
     * @example add_dialog_button(window, { text: "返回", icons: {primary: "fa fa-reply"}, click: function () { $('#lnk-backward').trigger('click'); }, });
     */
    add_dialog_button: function (ifrm, btn, pos) {
        window.parent.ifrmDialog.add_button(ifrm, btn, pos);
    }
}