/**
 * common-dialog.js
 * 
 * @since 1.0 <2016-1-27> SoChishun <14507247@qq.com> Added.
 */

// 免费字体资源:http://www.fontsquirrel.com 2015-12-4 SoChishun Added.
$(document).ready(function () {
    // 表单fieldset的展开和收起 2016-2-2 SoChishun Added.    
    $('legend .xsui-switch').click(function () {
        $(this).text('[-]' == $(this).text() ? '[+]' : '[-]').parent().next('.xsui-switch-content').toggle();
        return false;
    })
    // 网格辅助,显示行号和复选框 2015-10-17 SoChishun Added.
    $('.xsui-table').table();
    // 日历功能 2015-12-4 SoChishun Added.
    $('#calendar').datepicker({showWeek: true, autoSize: true});
    // 设置日期选择器
    $(".datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd',
        showWeek: true
    });
    // 设置时间日期选择器 see:http://trentrichardson.com/examples/timepicker/
    if ('undefined' !== typeof ($.fn.datetimepicker)) {
        $('.datetimepicker').datetimepicker({
            currentText: '现在',
            closeText: '确定',
            timeText: '时间',
            hourText: '小时',
            minuteText: '分钟',
            secondText: '秒钟',
            showSecond: true,
            changeMonth: true,
            changeYear: true,
            timeFormat: "HH:mm:ss",
            altFormat: "yy-mm-dd",
            altTimeFormat: "HH:mm:ss",
            showWeek: true
        });
        $('.datetimepicker-hm').datetimepicker({
            currentText: '现在',
            closeText: '确定',
            timeText: '时间',
            hourText: '小时',
            minuteText: '分钟',
            showSecond: false,
            changeMonth: true,
            changeYear: true,
            altFormat: "yy-mm-dd",
            altTimeFormat: "h:m t",
            showWeek: true
        });
        $('.timepicker').timepicker({
            timeOnlyTitle: '选择时间',
            currentText: '现在',
            closeText: '确定',
            timeText: '时间',
            hourText: '小时',
            minuteText: '分钟',
            secondText: '秒钟',
            timeFormat: "HH:mm:ss",
        });
        $('.timepicker-hm').timepicker({
            timeOnlyTitle: '选择时间',
            currentText: '现在',
            closeText: '确定',
            timeText: '时间',
            hourText: '小时',
            minuteText: '分钟',
            timeFormat: "HH:mm",
        });
    }

    // 设置可视化编辑器,控件不能加属性 require
    if ("undefined" !== typeof KindEditor) {
        if (undefined == window.config || undefined == window.config.KINDEDITOR_UPLOADER) {
            alert('未设置上传处理页面!');
        } else {
            // 按钮属性查看 http://kindeditor.net/docs/option.html
            KindEditor.create('.editor', {
                uploadJson: window.config.KINDEDITOR_UPLOADER,
                allowFileManager: false,
                autoHeightMode: true,
                items: [
                    'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
                    'removeformat', '|', 'wordpaste', 'plainpaste', '|', 'justifyleft', 'justifycenter', 'justifyright', '|', 'table', 'insertorderedlist',
                    'insertunorderedlist', '|', 'emoticons', 'baidumap', '|', 'image', 'multiimage', 'insertfile', 'link', 'unlink', '|', 'print', 'source', 'fullscreen']
            });
            KindEditor.create('.pm-editor', {
                uploadJson: window.config.KINDEDITOR_UPLOADER,
                allowFileManager: false,
                autoHeightMode: true,
                items: [
                    'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
                    'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', '|', 'table', 'insertorderedlist',
                    'insertunorderedlist', '|', 'emoticons', 'baidumap', '|', 'image', 'insertfile', 'link', 'unlink']
            });
        }
    }
});

// 动态更改排序 2016-1-18
// 如：<td class="sort"><input type="text" value="$vo.sort" size="3" data-value="$vo.sort" data-url="AddonU('change_sort','id='.$vo['id'].'&sort=varsort')" title="更改后,鼠标点击页面任意位置即可自动生效" /></td>
function change_sort($input) {
    var old_v = $input.data('value');
    if ($input.val() == old_v) {
        return;
    }
    $.get($input.data('url').replace('varsort', $input.val()), function (data) {
        if ('string' == typeof (data)) {
            data = JSON.parse(data);
        }
        if (data.status) {
            location.reload();
        } else {
            $('<div>' + data.info + '</div>').dialog();
        }
    })

}