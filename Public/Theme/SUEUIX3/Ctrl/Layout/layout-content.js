/**
 * common.js
 * 
 * @since 1.0 <2015-12-5> SoChishun <14507247@qq.com> Added.
 */
// 窗体尺寸调整的时候自动设置主内容容器高度 2016-3-10
$(window).resize(function () {
    auto_container_height();
});
// 免费字体资源:http://www.fontsquirrel.com 2015-12-4 SoChishun Added.
$(document).ready(function () {
    auto_container_height();
    // 表单fieldset的展开和收起 2016-2-2 SoChishun Added.    
    $('legend .xsui-switch').click(function () {
        $(this).text('[-]' == $(this).text() ? '[+]' : '[-]').parent().next('.xsui-switch-content').toggle();
        return false;
    })
    // 设置日期选择器
    $(".datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd',
        showWeek: true
    });
    // 设置时间日期选择器 see:http://trentrichardson.com/examples/timepicker/
    if ('undefined' !== typeof ($.fn.datetimepicker)) {
        $('.datetimepicker').datetimepicker({
            currentText: '现在',
            closeText: '确定',
            timeText: '时间',
            hourText: '小时',
            minuteText: '分钟',
            secondText: '秒钟',
            showSecond: true,
            changeMonth: true,
            changeYear: true,
            timeFormat: "HH:mm:ss",
            altFormat: "yy-mm-dd",
            altTimeFormat: "HH:mm:ss",
            showWeek: true
        });
        $('.datetimepicker-hm').datetimepicker({
            currentText: '现在',
            closeText: '确定',
            timeText: '时间',
            hourText: '小时',
            minuteText: '分钟',
            showSecond: false,
            changeMonth: true,
            changeYear: true,
            altFormat: "yy-mm-dd",
            altTimeFormat: "h:m t",
            showWeek: true
        });
        $('.timepicker').timepicker({
            timeOnlyTitle: '选择时间',
            currentText: '现在',
            closeText: '确定',
            timeText: '时间',
            hourText: '小时',
            minuteText: '分钟',
            secondText: '秒钟',
            timeFormat: "HH:mm:ss",
        });
        $('.timepicker-hm').timepicker({
            timeOnlyTitle: '选择时间',
            currentText: '现在',
            closeText: '确定',
            timeText: '时间',
            hourText: '小时',
            minuteText: '分钟',
            timeFormat: "HH:mm",
        });
    }
    // 设置可视化编辑器,控件不能加属性 require
    if ("undefined" !== typeof KindEditor) {
        if (undefined == window.config || undefined == window.config.KINDEDITOR_UPLOADER) {
            alert('未设置上传处理页面!');
        } else {
            // 按钮属性查看 http://kindeditor.net/docs/option.html
            KindEditor.create('.editor', {
                uploadJson: window.config.KINDEDITOR_UPLOADER,
                allowFileManager: false,
                autoHeightMode: true,
                items: [
                    'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
                    'removeformat', '|', 'wordpaste', 'plainpaste', '|', 'justifyleft', 'justifycenter', 'justifyright', '|', 'table', 'insertorderedlist',
                    'insertunorderedlist', '|', 'emoticons', 'baidumap', '|', 'image', 'multiimage', 'insertfile', 'link', 'unlink', '|', 'print', 'source', 'fullscreen']
            });
            KindEditor.create('.pm-editor', {
                uploadJson: window.config.KINDEDITOR_UPLOADER,
                allowFileManager: false,
                autoHeightMode: true,
                items: [
                    'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
                    'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', '|', 'table', 'insertorderedlist',
                    'insertunorderedlist', '|', 'emoticons', 'baidumap', '|', 'image', 'insertfile', 'link', 'unlink']
            });
        }
    }
});
/**
 * 计算当前日期在本年度的周数
 * @param {Number} weekStart 每周开始于周几：周日：0，周一：1，周二：2 ...，默认为周日
 * @returns {Number}
 * @since 1.0 <2015-12-4> by http://blog.csdn.net/orain/article/details/7063273
 * @example
 (new Date(2015, 11, 4)).getWeekOfYear(); // 当前日期是本年度第几周
 (new Date(2015, 11, 31)).getWeekOfYear(); // 2015 年度有几周(注意：JS 中月的取值范围为 0~11)
 */
Date.prototype.getWeekOfYear = function (weekStart) {
    weekStart = (weekStart || 0) - 0;
    if (isNaN(weekStart) || weekStart > 6)
        weekStart = 0;

    var year = this.getFullYear();
    var firstDay = new Date(year, 0, 1);
    var firstWeekDays = 7 - firstDay.getDay() + weekStart;
    var dayOfYear = (((new Date(year, this.getMonth(), this.getDate())) - firstDay) / (24 * 3600 * 1000)) + 1;
    return Math.ceil((dayOfYear - firstWeekDays) / 7) + 1;
}

/**
 * 计算当前日期在本月份的周数
 * @param {Number} weekStart 每周开始于周几：周日：0，周一：1，周二：2 ...，默认为周日
 * @returns {Number}
 * @since 1.0 <2015-12-4> by http://blog.csdn.net/orain/article/details/7063273
 * @example
 (new Date(2015, 11, 31)).getWeekOfMonth(); // 当前日期是本月第几周(注意：JS 中月的取值范围为 0~11)
 (new Date(2015, 0, 31)).getWeekOfMonth(); // 2015年度1月有几周
 */
Date.prototype.getWeekOfMonth = function (weekStart) {
    weekStart = (weekStart || 0) - 0;
    if (isNaN(weekStart) || weekStart > 6)
        weekStart = 0;

    var dayOfWeek = this.getDay();
    var day = this.getDate();
    return Math.ceil((day - dayOfWeek - 1) / 7) + ((dayOfWeek >= weekStart) ? 1 : 0);
}

// 动态更改排序 2016-1-18
// 如：<td class="sort"><input type="text" value="$vo.sort" size="3" data-value="$vo.sort" data-url="AddonU('change_sort','id='.$vo['id'].'&sort=varsort')" title="更改后,鼠标点击页面任意位置即可自动生效" /></td>
function change_sort($input) {
    var old_v = $input.data('value');
    if ($input.val() == old_v) {
        return;
    }
    $.get($input.data('url').replace('varsort', $input.val()), function (data) {
        if ('string' == typeof (data)) {
            data = JSON.parse(data);
        }
        if (data.status) {
            location.reload();
        } else {
            $('<div>' + data.info + '</div>').dialog();
        }
    })

}

/**
 * 自动设置主内容容器的高度
 * @returns {undefined}
 * @since 1.0 <2016-3-10> SoChishun Added.
 */
function auto_container_height() {
    var winHeight = 0;
    if (window.innerHeight) {
        winHeight = window.innerHeight;
    } else if ((document.body) && (document.body.clientHeight)) {
        winHeight = document.body.clientHeight;
    }
    var h = winHeight - $('.xsui-page-header').height() - 10;
    $('#grid-container').height(h);
}

// 关闭对话框窗口
function close_dialog(name) {
    $('iframe[name="' + name + '"]').attr('src', "about:blank").parent().dialog('close');
}