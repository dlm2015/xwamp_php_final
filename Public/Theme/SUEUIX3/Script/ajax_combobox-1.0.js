/**
 * 动态加载select控件的options
 * 支持的格式(子集名称:children)：
 * 格式1：[{"id":"1","text":"name1"}]
 * 格式2：[{"item":"1"},{"item":"2"}]
 * 格式3：["item1","item2"]
 * 格式4：{"a":"a1","b":"b2"}
 * @param {string} selector 选择器
 * @param {string} uri ajax数据源地址
 * @param {string} def_option 默认第一行显示的文本,如:请选择...
 * @param {string} value 默认值
 * @param {string} perpends 添加的额外options,如果加前缀+则追加到后面,默认追加到前面
 * @param {function} func ajax加载完成后执行的方法
 * @returns {void}
 * @since 1.0 2014-6-17 by sutroon
 *      1.1 2014-7-16 by sutroon 新增如果控件不存在则跳出方法的逻辑
 *      1.2 2014-8-22 by sutroon 新增对树形数据的支持
 *      1.3 2015-6-29 SoChishun 重构参数(selector, uri, def_option, value, prepends, func)合并为options
 *      1.4 2015-10-16 SoChishun 控件名称 ajax_load_options 重命名为 ajax_combobox
 * @example 
 *      <select name="pid" data-url="{:AddonU('get_channel_tree_json')}" data-value="{$data.pid}"></select>
 *      ajax_combobox({selector: 'select[name="pid"]', def_option: '请选择位置'});
 *      var strQueueOpts = ajax_combobox({ url:$('#queues').data('url'), def_option: '请选择位置', value:$('#queues').data('value'), prepends:'<option value="">新增项</option>',func:after_queue});
 */
function ajax_combobox(options) {
    var config = {
        selector: '', // string:选择器
        url: null, // string:ajax数据源地址,为空则会读取data-url属性
        queryParams: {}, // json:查询参数
        def_option: '', // string:默认第一行显示的文本,如:请选择...
        value: null, // string:默认值,null则会读取data-value属性
        prepends: '', // string:添加的额外options,如果加前缀+则追加到后面,默认追加到前面
        func: null, // function(str):ajax加载完成后执行的方法
    };
    $.extend(config, options);
    if (!config.func && !$(config.selector)[0]) {
        alert('[ajax_load_options]配置无效');
        return;
    }
    if (config.selector) {
        if (null===config.url) {
            config.url = $(config.selector).data('url');
        }
        if (null===config.value) {
            config.value = $(config.selector).data('value');
        }
    }
    $.getJSON(config.url, config.queryParams, function (data) {
        if (config.selector) {
            $(config.selector).empty();
        }
        if (!data) {
            return;
        }
        var $data = (typeof (data) == "object") ? data : $.parseJSON(data);
        var str = '';
        str = generate_options($data);
        if (config.prepends) {
            if (config.prepends[0] == '+') {
                str += config.prepends;
            } else {
                str = config.prepends + str;
            }
        }
        if (config.def_option) { // 添加默认选项到第一行
            str = '<option value="">' + config.def_option + '</option>' + str;
        }
        if (config.selector) {
            $(config.selector).html(str);
        }
        if (config.value && config.selector) {
            if (config.value.length > 4 && config.value.substring(0, 4) == 'TXT:') {
                $(config.selector).find('option:contains("' + config.value.substring(4) + '")').prop('selected', true);
            } else {
                $(config.selector).find('option[value="' + config.value + '"]').prop('selected', true);
            }
            str = $(config.selector).html();
        }
        // 如果有定义函数则执行
        if (config.func) {
            config.func(str);
        }
    });
}

/**
 * 根据json数据生成option树形控件
 * 如果有children节点则自动生成树形数据
 * @param {JSON} data
 * @param {int} n 节点深度
 * @param {string} char 节点名称前缀
 * @returns {string}
 * @since 1.0 2014-8-22 by sutroon
 * @since 1.1 <2015-10-22> SoChishun 修正前置符号错位问题
 * @example
 *      var data = (typeof (data) == "object") ? data : $.parseJSON(data);
 *      var str = '';
 *      str=generate_options(data);
 */
function generate_options(data, n, char) {
    if (undefined == n) {
        n = 0;
    }
    if (undefined == char) {
        char = '....';
    }
    var pad = '';
    var x=n+1;
    while (n > 0) {
        pad += char;
        n--;
    }
    var str = '';
    if (data.length) {
        // 多行格式
        for (var i = 0; i < data.length; i++) {
            if (data[i].id) {
                // 用于[{"id":"1","text":"name1"}]格式                
                str += '<option value="' + data[i].id + '">' + pad + (data[i].text ? data[i].text : data[i].text) + '</option>';
            } else if (data[i].item) {
                // 用于[{"item":"1"},{"item":"2"}]
                str += '<option value="' + data[i].item + '">' + pad + data[i].item + '</option>';
            } else {
                // 用于["item1","item2"]格式
                str += '<option value="' + data[i] + '">' + pad + data[i] + '</option>';
            }
            if (data[i].children) {
                str += generate_options(data[i].children, x, char);
            }
            n=0;
        }
    }
    else {
        // 用于{"a":"a1","b":"b2"}单行格式
        for (var attr in data) {
            str += '<option value="' + attr + '">' + data[attr] + '</option>';
        }
    }
    return str;
}