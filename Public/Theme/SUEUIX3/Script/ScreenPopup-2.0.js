/**
 * ScreenPopup-1.0.js
 * 来电弹屏插件
 * 
 * @since 1.0 <2016-2-1> SoChishun <14507247@qq.com> Added.
 */
var ScreenPopup = {
    // 默认配置
    config: {
        jsonp_handler: 'ScreenPopup.extension_screen_popup_jsonp', // jsonp方法
        data_url: '', // 来电弹屏数据来源地址
        interval: 5000, // 定时器
        dialog_limit: 10, // 最大弹窗数量(防止弹窗太多导致浏览器卡死)
        call_reminder_selector: '#call-reminder',
    },
    /**
     * 初始化
     * @param {mode} options
     * @returns {undefined}
     * @since 1.0 2016-2-1 SoChishun Added.
     */
    init: function (options) {
        $.extend(this.config, options);
        if (!this.config.data_url) {
            this.config.data_url = $(this.config.call_reminder_selector).data('screenpopupUrl');
        }
        this.extension_screen_popup();
        setInterval(this.extension_screen_popup, this.config.interval);
        this.exten_workbench_init();
    },
    /**
     * 获取分机状态
     * @param {type} url
     * @returns {undefined}
     * @since 1.0 2016-4-18 SoChishun Added.
     */
    exten_get_status: function (url) {
        $.get(url, function (data) {
            console.log(data);
            if (data.status) {
                $('.exten-workbench-status-content').load($('.exten-workbench-status').attr('href').replace('varrnd', Math.random()));
            } else {
                alert(data.info);
            }
        })
    },
    /**
     * 分机工作台初始化
     * @returns {undefined}
     * @since 1.0 2016-3-11 SoChishun Added.
     */
    exten_workbench_init: function () {
        // 坐席控制台按钮控制 2016-3-12
        // 2016-4-18 SoChishun Added.
        $('.exten-workbench-sign-switch').click(function () {
            var $i = $(this).find('i');
            var val = '';
            if ($i.is('.fa-sign-out')) {
                $i.removeClass('fa-sign-out').addClass('fa-sign-in');
                $i.next().text('签入队列');
                val = 'LOGOUT';
            } else {
                $i.removeClass('fa-sign-in').addClass('fa-sign-out');
                $i.next().text('签出队列');
                val = 'LOGIN';
            }
            ScreenPopup.exten_get_status($(this).attr('href').replace('varsign', val));
            return false;
        })
        // 2016-4-18 SoChishun Added.
        $('.exten-workbench-idle-switch').click(function () {
            var $i = $(this).find('i');
            var val = '';
            if ($i.is('.fa-hourglass-1')) {
                $i.removeClass('fa-hourglass-1').addClass('fa-hourglass-o');
                $i.next().text('分机示闲');
                val = 'BUSY';
            } else {
                $i.removeClass('fa-hourglass-o').addClass('fa-hourglass-1');
                $i.next().text('分机示忙');
                val = 'FREE';
            }
            ScreenPopup.exten_get_status($(this).attr('href').replace('varidle', val));
            return false;
        })
        // 2016-4-18 SoChishun Added.
        $('.exten-workbench-dialout').click(function () {
            if ($('.exten-workbench-status-content').text().indexOf('签入') < 0) {
                alert('分机未签入!');
                return false;
            }
            var tel = $($(this).data('for')).val();
            if (!tel) {
                alert('电话号码无效!');
                return false;
            }
            ScreenPopup.exten_get_status($(this).attr('href').replace('varphone', tel));
            return false;
        })
        // 2016-4-18 SoChishun Added.
        $('.exten-workbench-handup').click(function () {
            ScreenPopup.exten_get_status($(this).attr('href'));
            return false;
        })
        // 2016-4-18 SoChishun Added.
        $('.exten-workbench-status').click(function () {
            $('.exten-workbench-status-content').load($('.exten-workbench-status').attr('href').replace('varrnd', Math.random()));
            return false;
        })
        if ($('.exten-workbench-status')[0]) {
            $('.exten-workbench-status-content').load($('.exten-workbench-status').attr('href').replace('varrnd', Math.random()), function (data) {
                if ('签出' == data) {
                    $('.exten-workbench-sign-switch').find('i').removeClass('fa-sign-out').addClass('fa-sign-in').next().text('签入队列');
                } else {
                    $('.exten-workbench-sign-switch').find('i').removeClass('fa-sign-in').addClass('fa-sign-out').next().text('签出队列');
                    if (data.indexOf('示闲') > 0) {
                        $('.exten-workbench-idle-switch').find('i').removeClass('fa-hourglass-o').addClass('fa-hourglass-l').next().text('分机示忙');
                    } else {
                        $('.exten-workbench-idle-switch').find('i').removeClass('fa-hourglass-1').addClass('fa-hourglass-o').next().text('分机示闲');
                    }
                }
            });
        }
    },
    /**
     * 座席来电弹屏
     * @returns {undefined}
     * @since 2014-12-16 by sutroon;
     * @example <input type="hidden" id="extension-screen-popup-url" value="{:U('XCall/ApiCallRecord/seat_screen_popup','exten=300&uid=5')}"/>
     */
    extension_screen_popup: function () {
        var url = ScreenPopup.config.data_url;
        if (!url) {
            alert('弹屏数据源地址无效!')
            return;
        }
        var jsonp_handler = ScreenPopup.config.jsonp_handler;
        $.ajax({
            url: url.replace('varjsonp', jsonp_handler).replace('varrnd', Math.random()),
            dataType: "jsonp",
            jsonp: jsonp_handler,
            beforeSend: function () {
                //ajax_show_loading(false);
            }
        });
    },
    /**
     * 来电弹屏处理方法
     * @param json data
     * @returns {undefined}
     * @since 1.0 2014-12-16 by sutroon
     * @example <a id="call-reminder" title="来电提醒" href="#" onclick="$(this).hide();" data-url="{:U('Customer/customer_edit','addon=POrgCustomer&tel=vartel')}" data-screenpopup-url="{:U('ApiCallRecord/screen_popup','jsonp=varjsonp&addon=POrgCustomer&exten='.$login_data['user_name'])}" data-screenpopup-mode="{$SCREENPOPUP_MODE}" style="position: absolute; left:40%; top:0px; background-color: #F60; padding: 5px 80px 8px 80px; color:#FFF; display: none; z-index:999">您有新来电</a>
     */
    extension_screen_popup_jsonp: function (data) {
        if (!data) {
            return;
        }
        if (!data.line) {
            return; // 如果没有电话号码则返回,用于修正手拨电话联系弹出两次窗口的问题 2015-1-2 by sutroon
        }
        var $a = $(this.config.call_reminder_selector);
        var url = $a.data('url');
        var mode = $a.data('screenpopupMode'); // J=直接跳转客户;D=直接弹窗;M:消息提醒;F=关闭来电弹屏
        if (!url) {
            alert('链接无效');
            return;
        }
        url = url.replace('vartel', data.line);
        $a.attr('href', url);
        $a.attr('title', '来电弹屏 ' + data.line);
        $a.html('您有新的来电<br />' + data.line);
        switch (mode) {
            case 'M': // 消息提醒
                $a.show();
                return;
            case 'J': // 直接跳转
                window.open(url, "_blank");
                break;
            case 'D': // 直接弹窗
                ifrmDialog.open_edit_dialog(url, {title: '来电弹屏 ' + data.line}, 'screen_popup{n}', 'screen_popup', this.config.dialog_limit);
                break;
        }
        //$a.show().trigger('click');
        return false;
        /*
         if (undefined == show || 'dialog' == show) {
         show = 'dialog';
         } else {
         show = 'page';
         }
         if ('page' == show) {
         $('#main').attr('src', "about:blank").attr('src', url).load(function () {
         setIframeHeight();
         });
         } else {
         open_dialog(url, {}, undefined, 'customer-popup', 5);
         }*/
    },
    /**
     * jsonp事件
     * @param string url url
     * @param string handler 处理方法,如 extension_screen_popup_jsonp
     * @returns {undefined}
     * @since 1.0 2014-12-16 by sutroon
     */
    extension_jsonp_event: function (url, handler) {
        if (!url) {
            return;
        }
        $.ajax({url: url.replace('.html', '/jsonp/' + handler + '.html'), dataType: "jsonp", jsonp: handler, beforeSend: function () {
                ajax_show_loading(false);
            }});
    }
}