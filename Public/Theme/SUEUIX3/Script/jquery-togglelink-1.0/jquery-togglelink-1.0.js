/**
 * jquery-togglelink-1.0.js
 * 
 * @since 1.0 <2016-1-18> SoChishun <14507247@qq.com> Added.
 */
// example:
// <td class="status"><a href="{:AddonU('change_status','id='.$vo['id'].'&status=varstatus')}" title="点击隐藏" data-value="{$vo.status}" class="xsui-success"><i class="fa fa-check fa-fw"></i></a></td>
// $('td.status a').togglelink();
(function ($) {
    $.fn.togglelink = function (options) {
        // 插件初始化
        var opts = $.extend({}, $.fn.togglelink.defaults, options);
        this.click(function () {
            var $a = $(this), status = (opts.onValue == $a.data('value'));
            $.get($a.attr('href').replace(opts.varName, status ? opts.offValue : opts.onValue), function (data) {
                if ('string' == typeof (data)) {
                    data = JSON.parse(data);
                }
                if (data.status) {
                    if (status) {
                        $a.attr({title: opts.onTip}).data('value', opts.offValue).removeClass('xsui-success').addClass('xsui-danger').find('i').removeClass('fa-check').addClass('fa-times');
                    } else {
                        $a.attr({title: opts.offTip}).data('value', opts.onValue).removeClass('xsui-danger').addClass('xsui-success').find('i').removeClass('fa-times').addClass('fa-check');
                    }
                } else {
                    $('<div>' + data.info + '</div>').dialog();
                }
            });
            return false;
        })
        return this.each(function () {
            var $a = $(this), status = (opts.onValue == $a.data('value'));
            if (status) {
                $a.attr({title: opts.offTip}).removeClass('xsui-danger').addClass('xsui-success').find('i').removeClass('fa-times').addClass('fa-check');
            } else {
                $a.attr({title: opts.onTip}).removeClass('xsui-success').addClass('xsui-danger').find('i').removeClass('fa-check').addClass('fa-times');
            }
        });
    };
    /**
     * 默认配置
     * @since 1.0 <2016-1-18> SoChishun Added.
     */
    $.fn.togglelink.defaults = {
        onValue: 1,
        offValue: 0,
        onTip: '点击显示',
        offTip: '点击隐藏',
        onClass: 'xsui-success',
        offClass: 'xsui-danger',
        onIcon: 'fa-check',
        offIcon: 'fa-times',
        varName: 'varstatus',
    };
})(jQuery);