/**
 * IFrame对话框类
 * 
 * @type Object
 * @since 1.0 <2015-10-16> SoChishun <14507247@qq.com> Added.
 * @since 1.1 <2015-12-2> SoChishun 新增open_view_dialog()方法
 * @since VER:1.2; DATE:2016-1-19; AUTHOR:SoChishun; DESC:优化对话框自动宽度功能
 * @since 2.0 <2016-5-9> SoChishun 重构,取消实例对象, IFrameDialog 重命名为 ifrmDialog
 */
var ifrmDialog = {
    /**
     * 打开查询对话框
     * @param {type} dialog_selector
     * @param {type} options
     * @returns {undefined}
     */
    open_search_dialog: function (dialog_selector, options) {
        var config = {
            title: '高级搜索',
            buttons: [{
                    text: "搜索",
                    icons: {primary: "fa fa-search"},
                    click: function () {
                        $(this).find(':submit').trigger('click');
                    },
                }, {
                    text: "取消",
                    icons: {primary: "fa fa-close"},
                    click: function () {
                        $(this).dialog("close");
                    },
                }]
        };
        config.width = $(dialog_selector).width();
        $.extend(config, options);
        $(dialog_selector).dialog(config);
        // 修正按钮图标 2015-10-16 SoChishun Added.
        $('.ui-dialog-buttonset .fa').removeClass('ui-button-icon-primary ui-icon');
    },
    /**
     * 打开新增对话框
     * @param {type} src
     * @param {type} options
     * @param {type} ifrmName
     * @param {type} groupName
     * @param {type} limit
     * @returns {undefined}
     */
    open_add_dialog: function (src, options, ifrmName, groupName, limit) {
        var config = {
            title: '新增',
            buttons: [{
                    text: "保存",
                    icons: {primary: "fa fa-save"},
                    click: function () {
                        var ifrm = $(this).find('iframe')[0];
                        if ('function' == typeof (ifrm.contentWindow.submit_form)) {
                            ifrm.contentWindow.submit_form();
                        } else {
                            $(ifrm).contents().find(':submit').trigger('click');
                        }
                    },
                }, {
                    text: "取消",
                    icons: {primary: "fa fa-close"},
                    click: function () {
                        $(this).dialog("close");
                    },
                }]
        };
        $.extend(config, options);
        this.open(src, config, ifrmName, groupName, limit);
    },
    /**
     * 打开编辑对话框
     * @param {type} src
     * @param {type} options
     * @param {type} ifrmName
     * @param {type} groupName
     * @param {type} limit
     * @returns {undefined}
     */
    open_edit_dialog: function (src, options, ifrmName, groupName, limit) {
        var config = {
            title: '编辑',
            buttons: [{
                    text: "保存",
                    icons: {primary: "fa fa-save"},
                    click: function () {
                        var ifrm = $(this).find('iframe')[0];
                        if ('function' == typeof (ifrm.contentWindow.submit_form)) {
                            ifrm.contentWindow.submit_form();
                        } else {
                            $(ifrm).contents().find(':submit').trigger('click');
                        }
                    },
                }, {
                    text: "取消",
                    icons: {primary: "fa fa-close"},
                    click: function () {
                        $(this).dialog("close");
                    },
                }]
        };
        $.extend(config, options);
        this.open(src, config, ifrmName, groupName, limit);
    },
    /**
     * 打开查看对话框
     * @param {type} src
     * @param {type} options
     * @param {type} ifrmName
     * @param {type} groupName
     * @param {type} limit
     * @returns {undefined}
     */
    open_view_dialog: function (src, options, ifrmName, groupName, limit) {
        var config = {
            title: '查看',
        };
        $.extend(config, options);
        this.open(src, config, ifrmName, groupName, limit);
    },
    /**
     * 打开iframe对话框
     * 用于iframe子窗体调用
     * @since 1.0 2014-10-6 by sutroon created; 
     *      2.0 2014-10-21 by sutroon 新增多弹窗支持; 
     *      2.1 2014-10-29 by sutroon 新增是否显示关闭按钮的功能{closeable:false}; 
     *      2.2 2014-11-5 by sutroon 新增如果指定ifrmName则独占窗口的功能
     *      2.3 2015-1-7 by sutroon 新增组和窗口个数限制功能
     *      2.4 2015-10-16 SoChishun 新增参数验证功能
     *      2.5 2015-10-24 SoChishun 新增对话框尺寸调整,内容页尺寸也跟着调整的功能
     *      2.6 2015-11-10 SoChishun 新增ifrmName中的变量替换占位符{n}
     * @example window.parent.open_dialog(src,settings);
     *      open_dialog($(this).attr('href'),{},undefined,'customer',2);
     */
    open: function (src, settings, ifrmName, groupName, limit) {
        var err = '';
        if (!src) {
            err += '<br />IFRAME地址无效!';
        }
        if (!ifrmName) {
            err += '<br />IFRAME名称无效!';
        }
        if (err) {
            $('<div title="弹窗错误!">' + err + '</div>').dialog({buttons: {'确定': function () {
                        $(this).dialog('close');
                    }}});
            return false;
        }
        if (ifrmName.indexOf('{n}') > -1) {
            var date = new Date();
            ifrmName = ifrmName.replace('{n}', (date.getDate() + date.getHours() + date.getMinutes() + date.getSeconds()));
        }
        var $ifrm = $('iframe[name="' + ifrmName + '"]');
        if ($ifrm[0]) {
            if ($ifrm.parent().parent().css('display') == 'none') {
                $ifrm.attr('src', "about:blank").attr('src', src).parent().dialog();
            }
            return false;
        }

        var opts = $.extend({width: 800}, settings);
        if (false == opts.closeable) {
            opts.closeOnEscape = false;
        }
        opts.close = function () {
            $(this).remove(); // 2014-11-12 by sutroon 清除缓存
        };
        // 2015-10-24 SoChishun 新增内容随对话框伸缩而改变尺寸功能
        opts.resize = function (event, ui) {
            var $ifrm = $(this).find('iframe');
            $ifrm.css({'width': '100%', 'height': $ifrm.parent().height()});
        }
        $('<div><iframe name="' + ifrmName + '" style="width:100%; overflow:hidden;" frameborder="0" scrolling="no" src="' + src + '"' + (groupName ? ' data-group="' + groupName + '"' : '') + '></iframe></div>').dialog(opts).find('iframe').load(function () {
            ifrmDialog.resize_dialog(ifrmName);
        });
        // 修正按钮图标 2015-10-16 SoChishun Added.
        $('.ui-dialog-buttonset .fa').removeClass('ui-button-icon-primary ui-icon');
        if (false == opts.closeable) {
            $('iframe[name="' + ifrmName + '"]').parent().parent().find('.ui-dialog-titlebar button').hide();
        }
        // 如果有限制数量则自动关闭第一个
        if (groupName && limit) {
            var count = $('iframe[data-group="' + groupName + '"]').length;
            if (count > limit) {
                $('iframe[data-group="' + groupName + '"]').eq(0).attr('src', "about:blank").parent().dialog('close');
            }
        }
        return true;
    },
    /**
     * 添加对话框按钮
     * @param object ifrm
     * @param object btn
     * @param number pos
     * @returns {undefined}
     * @since 1.0 <2015-11-03> SoChishun Added.
     * @example window.parent.gridPage.ifrmDialog.add_button(window, { text: "返回", icons: {primary: "fa fa-reply"}, click: function () { $('#lnk-backward').trigger('click'); }, });
     */
    add_button: function (ifrm, btn, pos) {
        var a = $('iframe[name="' + ifrm.name + '"]').dialog("option", "buttons");
        if (undefined == pos) {
            a.unshift(btn);
        } else if (0 == pos) {
            a.push(btn);
        } else {
            a.splice(pos, 0, btn);
        }
        $('iframe[name="' + ifrm.name + '"]').dialog({buttons: a});
        $('.ui-dialog-buttonset .fa').removeClass('ui-button-icon-primary ui-icon');

    },
    /**
     * 重置对话框尺寸
     * 用于本页面或iframe子页面刷新对话框尺寸
     * @since 1.0 2014-9-24 by sutroon Added
     * @since 1.1 2014-12-2 by sutroon 新增width参数
     * @since 1.2 <2015-11-12> SoChishun 新增自适应内容宽度功能
     * @since 1.3 <2015-11-13> SoChishun 修正自适应位置在iframe右键重新加载后会重复偏移的问题
     * @example
     *  if(window.parent && window.parent.resize_dialog){ window.parent.resize_dialog();}
     *  resize_dialog(window.name,700);
     */
    resize_dialog: function ($ifrm, width, height, reposition) {
        var name = '';
        if ('string' == typeof ($ifrm)) {
            name = $ifrm;
            $ifrm = $('iframe[name="' + $ifrm + '"]');
        } else {
            name = $ifrm.attr('name');
        }
        if (!height) {
            height = $ifrm.contents().height();
        }
        if (!width) {
            width = $ifrm.contents().width() + 20;
        }
        $('iframe[name="' + name + '"]').height(height).parent().height(height + 20).parent().width(width);
        if (!$ifrm.data('reposition') && width > 640) {
            $ifrm.data('reposition', true); // 取消这句则每次iframe右键重新加载框架,对话框会重复向左移动!
            var $dialog = $('iframe[name="' + name + '"]').parent().parent();
            var left = parseInt($dialog.css('left'));
            var leftpos = left ? (left - (width - 640) / 2) : 0;
            $dialog.css('left', leftpos < 0 ? 50 : leftpos);
            var top = parseInt($dialog.css('top'));
            var toppos = top ? (top - height / 2) : 0;
            $dialog.css('top', toppos < 0 ? 50 : toppos);
        }
    },
    /**
     * 关闭对话框
     * 用于iframe子窗体调用
     * @since 1.0 2014-9-28 by sutroon created; 1.1 2014-10-10 by sutroon 改进缓存清理; 2.0 2014-10-21 by sutroon 新增多弹窗支持
     * @example window.parent.close_dialog(window.name);
     */
    close_dialog: function (ifrmName) {
        $('iframe[name="' + ifrmName + '"]').attr('src', "about:blank").parent().dialog('close');
    },
};