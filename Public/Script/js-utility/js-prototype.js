/* 
 * javascript 原生对象扩展
 * @since 1.0 2014-6-18 by sutroon
 */

String.prototype.padLeft = function (padChar, width) {
    var ret = this;
    while (ret.length < width) {
        if (ret.length + padChar.length < width) {
            ret = padChar + ret;
        }
        else {
            ret = padChar.substring(0, width - ret.length) + ret;
        }
    }
    return ret;
};
String.prototype.padRight = function (padChar, width) {
    var ret = this;
    while (ret.length < width) {
        if (ret.length + padChar.length < width) {
            ret += padChar;
        }
        else {
            ret += padChar.substring(0, width - ret.length);
        }
    }
    return ret;
};
String.prototype.repeat = function (count) {
    var ret = this;
    while (count > 0) {
        ret += ret;
        count--;
    }
    return ret;
}
String.prototype.reverse = function () {
    var ret = '';
    for (var i = this.length - 1; i >= 0; i--) {
        ret += this.charAt(i);
    }
    return ret;
};
String.prototype.startWith = function (compareValue, ignoreCase) {
    if (ignoreCase) {
        return this.toLowerCase().indexOf(compareValue.toLowerCase()) == 0;
    }
    return this.indexOf(compareValue) == 0
};
String.prototype.endWith = function (compareValue, ignoreCase) {
    if (ignoreCase) {
        return this.toLowerCase().lastIndexOf(compareValue.toLowerCase()) == this.length - compareValue.length;
    }
    return this.lastIndexOf(compareValue) == this.length - compareValue.length;
};

/**
 * 格式化时间
 * @param {String} format yyyy-MM-dd hh:mm:ss W
 * @returns {string}
 * @example var d =new Date().format('yyyy-MM-dd HH:mm:ss W');
 * @since 1.1 2014-9-26 by sutroon 新增W支持
 */
Date.prototype.format = function (format) {
    if (!format) {
        format = 'yyyy-MM-dd hh:mm:ss';
    }
    var week=['日','一','二','三','四','五','六'];
    var o = {
        'W+':week[this.getDay()], // week
        "M+": this.getMonth() + 1, //month
        "d+": this.getDate(), //day
        "h+": this.getHours(), //hour
        "H+": this.getHours(), //hour
        "m+": this.getMinutes(), //minute
        "i+": this.getMinutes(), //minute
        "s+": this.getSeconds(), //second
        "q+": Math.floor((this.getMonth() + 3) / 3), //quarter
        "S": this.getMilliseconds() //millisecond
    }
    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o)
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
        }
    return format;
}
