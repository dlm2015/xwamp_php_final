<?php

/*
 * 列出目录下所有子目录名称
 * @version: 1.0 2016-5-9 SoChishun Added.
 */
$dir = opendir('./');
while (false !== ($file = readdir($dir))) {
    if ('.' == $file || '..' == $file) {
        continue;
    }
    echo $file . ';';
}
closedir($dir);
