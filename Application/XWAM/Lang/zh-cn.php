<?php

/*
 * zh-cn语言文件
 * @version 1.0 2016-05-09 15:12:47 SoChishun Added (by TPLangHelper 1.0).
 */
return array(
    'add' => '新增',
    'edit' => '编辑',
    'delete' => '删除',
    'refresh' => '刷新',
    'site_id' => '租户ID',
    'create_time' => '创建时间',
    'operate' => '操作',
    'title_suffix' => '后台管理系统',
    'setting' => '设置',
    'logout' => '注销',
    'logout_tip' => '您确定要注销登录吗',
    'edit_profile_tip' => '编辑个人资料',
    'profile' => '个人资料',
    'department' => '部门',
    'last_login' => '上次登录',
    'login_at' => '登陆于',
    'login_count' => '登录次数',
    'pm' => '短消息',
    'history' => '历史记录',
    'website' => '网站',
    'help' => '帮助',
    'desktop' => '桌面',
);
