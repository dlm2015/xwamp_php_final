<?php
/*
 * en-us语言文件
 * @version 1.0 2016-05-09 15:12:47 SoChishun Added (by TPLangHelper 1.0).
*/
return array(
'add'=>'Add',
'edit'=>'Edit',
'delete'=>'Delete',
'refresh'=>'Refresh',
'site_id'=>'SiteID',
'create_time'=>'Create Time',
'operate'=>'Operate',
'title_suffix'=>'Management System',
'setting'=>'Setting',
'logout'=>'Logout',
'logout_tip'=>'Are you want to logout',
'edit_profile_tip'=>'Edit my profile',
'profile'=>'Profile',
'department'=>'Department',
'last_login'=>'Last Login',
'login_at'=>'Login At',
'login_count'=>'Login count',
'pm'=>'Message',
'history'=>'History',
'website'=>'Website',
'help'=>'Help',
'desktop'=>'Desktop',
);