<?php
/**
 * Tp语言文件帮助类
 * @version 1.0 2016-5-9 SoChishun Added.
 */
$ver = '1.0';
$lang = array('zh-cn', 'en-us');
$lang_info = array(
    /* 通过 2014-9-25 */
    'add' => array('zh-cn' => '新增', 'en-us' => 'Add'),
    'edit' => array('zh-cn' => '编辑', 'en-us' => 'Edit'),
    'delete' => array('zh-cn' => '删除', 'en-us' => 'Delete'),
    'refresh' => array('zh-cn' => '刷新', 'en-us' => 'Refresh'),
    'site_id' => array('zh-cn' => '租户ID', 'en-us' => 'SiteID'),
    'create_time' => array('zh-cn' => '创建时间', 'en-us' => 'Create Time'),
    'operate' => array('zh-cn' => '操作', 'en-us' => 'Operate'),
    /* 首页 2016-5-9 */
    'title_suffix' => array('zh-cn' => '后台管理系统', 'en-us' => 'Management System'),
    'setting' => array('zh-cn' => '设置', 'en-us' => 'Setting'),
    'logout' => array('zh-cn' => '注销', 'en-us' => 'Logout'),
    'logout_tip' => array('zh-cn' => '您确定要注销登录吗', 'en-us' => 'Are you want to logout'),
    'edit_profile_tip' => array('zh-cn' => '编辑个人资料', 'en-us' => 'Edit my profile'),
    'profile' => array('zh-cn' => '个人资料', 'en-us' => 'Profile'),
    'department' => array('zh-cn' => '部门', 'en-us' => 'Department'),
    'last_login' => array('zh-cn' => '上次登录', 'en-us' => 'Last Login'),
    'login_at' => array('zh-cn' => '登陆于', 'en-us' => 'Login At'),
    'login_count' => array('zh-cn' => '登录次数', 'en-us' => 'Login count'),
    'pm'=>array('zh-cn'=>'短消息','en-us'=>'Message'),
    'history'=>array('zh-cn'=>'历史记录','en-us'=>'History'),
    'website'=>array('zh-cn'=>'网站','en-us'=>'Website'),
    'help'=>array('zh-cn'=>'帮助','en-us'=>'Help'),
    'desktop'=>array('zh-cn'=>'桌面','en-us'=>'Desktop'),
);
// 生成文件 2016-5-9
if ('make' == $_GET['action']) {
    foreach ($lang as $lng) {
        $file = fopen('./' . $lng . '.php', 'w');
        fwrite($file, '<?php' . PHP_EOL);
        fwrite($file, '/*' . PHP_EOL);
        fwrite($file, ' * ' . $lng . '语言文件' . PHP_EOL);
        fwrite($file, ' * @version 1.0 ' . date('Y-m-d H:i:s') . ' SoChishun Added (by TPLangHelper ' . $ver . ').' . PHP_EOL);
        fwrite($file, '*/' . PHP_EOL);
        fwrite($file, 'return array(' . PHP_EOL);
        foreach ($lang_info as $key => $avals) {
            fwrite($file, "'$key'=>'" . $avals[$lng] . "'," . PHP_EOL);
        }
        fwrite($file, ');');
        fclose($file);
    }
    header('location:?success');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>TP lang helper 1.0</title>
        <style type="text/css">
            body, td, th{ font-size:12px; color:#333;}
            table { border:solid 1px #CCC; border-left:none; border-top:none;}
            td, th { border:solid 1px #CCC; border-right: none; border-bottom: none; padding:3px 5px;}
            a{ text-decoration: none;}
        </style>
    </head>
    <body>
        <a href="?action=make" onclick="return confirm('您确定要生成语言文件吗?')">[生成语言文件]</a>
        <div style="max-height:600px; max-width:560px; overflow: auto">
            <table cellspacing="0">
                <tr><th>变量</th><th>文本</th></tr>
                <?php
                $rowspan = count($lang);
                foreach ($lang_info as $key => $avals) {
                    echo '<tr><td rowspan="', $rowspan, '" style="color:#090">', $key, '</td>';
                    $i = 0;
                    foreach ($avals as $val) {
                        if ($i < 1) {
                            echo '<td>', $val, '</td></tr>';
                        } else {
                            echo '<tr><td>', $val, '</td></tr>';
                        }
                        $i++;
                    }
                }
                ?>
            </table>
        </div>
    </body>
</html>
