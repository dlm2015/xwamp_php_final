<?php

namespace XWAM\Model;

/**
 * DocumentContentModel类
 *
 * @since 1.0 <2015-10-26> SoChishun <14507247@qq.com> Added.
 */
class DocumentContentModel extends AppbaseModel {

    protected $tableName = 't_porg_document_content';

    function get_content($id){
        return $this->where(array('document_id'=>$id))->getField('content');
    }

}
