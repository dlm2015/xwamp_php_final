<?php

namespace XWAM\Model;

/**
 * LogAnalysisModel 类
 *
 * @since 1.0 <2015-12-2> SoChishun <14507247@qq.com> Added.
 */
class LogAnalysisModel {

    /**
     * 日志级别 从上到下，由低到高
     * @return type
     * @since 1.0 <2015-12-2> SoChishun Added.
     */
    function get_typeset() {
        return array('ALL' => '所有', 'EMERG' => '严重错误: 导致系统崩溃无法使用', 'ALERT' => '警戒性错误: 必须被立即修改的错误', 'CRIT' => '临界值错误: 超过临界值的错误，例如一天24小时，而输入的是25小时这样', 'ERR' => '一般错误: 一般性错误', 'WARN' => '警告性错误: 需要发出警告的错误', 'NOTIC' => '通知: 程序可以运行但是还不够完美的错误', 'INFO' => '信息: 程序输出信息', 'DEBUG' => '调试: 调试信息', 'SQL' => 'SQL：SQL语句 注意只在调试模式开启时有效', 'OTHER' => '其他：用户自定义级别，例如LOGIN,LOGOUT,OPERATE等');
    }

    function get_dirs() {
        $logroot = RUNTIME_PATH . 'Logs/';
        return sofn_read_dir($logroot, array('size' => true, 'real_path' => false, 'path' => false, 'is_dir' => false));
    }

    function get_files($dirname) {
        if (!$dirname) {
            return false;
        }
        $logroot = RUNTIME_PATH . 'Logs/';
        return sofn_read_dir($logroot . $dirname, array('size' => true, 'real_path' => false, 'path' => false, 'is_dir' => false));
    }

}
