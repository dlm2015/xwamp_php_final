<?php

namespace XWAM\Model;

/**
 * Description of UserAttrModel
 *
 * @since 1.0 <2015-11-3> SoChishun <14507247@qq.com> Added.
 */
class UserAttrModel extends AppbaseModel {

    protected $tableName = 't_porg_user_attr';

    /**
     * 保存属性
     * @return type
     * @since 1.0 <2015-11-3> SoChishun Added.
     */
    function save_attr() {
        $rule = array(
            array('title', 'require', '用户名无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('title', 'require', '用户名已被注册!', self::MUST_VALIDATE, 'unique', self::MODEL_INSERT),
            array('status', 'number', '状态有误!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rule)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        if (empty($this->id)) {
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    function change_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('status' => $status));
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

    function delete_attr($id) {
        if ($this->table('t_porg_user')->where(array('type_id' => $id))->count() > 0) {
            return $this->returnMsg(false, '该类型下面有用户,请先转移或删除用户!');
        }
        $result = $this->delete($id);
        return $this->returnMsg($result);
    }

}
