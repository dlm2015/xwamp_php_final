<?php

namespace XWAM\Model;

/**
 * 部门数据库模型
 *
 * @since 1.0.0 <2016-1-14> SoChishun <14507247@qq.com> Added.
 */
class DepartmentModel extends AppbaseModel {

    /**
     * 数据表名称
     * @var string
     */
    protected $tableName = 'think_auth_department';

    /**
     * 是否批量验证
     * @var boolean
     */
    protected $patchValidate = true;

    public function save_rule($department_id, $rules) {
        $result = $this->where(array('id' => $department_id))->setField('rules', implode(',', $rules));
        return $this->returnMsg($result);
    }

    public function delete_department($id = '') {
        if (!$id) {
            return $this->returnMsg(false, '参数无效');
        }
        /* 判断是否有子项目
          $list = M()->query('select a.title from think_auth_group a inner join think_auth_group b on b.pid=a.id where b.pid in (' . $id . ')');
          $names = false;
          if ($list) {
          foreach ($list as $row) {
          $names.=',' . $row['name'];
          }
          }
          if ($names) {
          return $this->returnMsg(false, '以下项目有子项目,请先转移子项目:' . "\n" . ltrim($names, ','));
          }
         */
        // 判断是否有引用
        $user_department_ids = M('think_auth_group_access')->where(array('group_id' => array('in', $id)))->getField('group_id', true);
        if ($user_department_ids) {
            $names = $this->where(array('id' => array('in', $user_department_ids)))->getField('title', true);
            return $this->returnMsg(false, '以下项目正在被用户使用,无法删除:' . "\n" . implode(',', $names));
        }
        $result = $this->delete($id);
        return $this->returnMsg($result);
    }

    function select_tree($select_options = array(), $data_options = array()) {
        $select_options = array_merge(array('field' => 'id, title, pid, code, rules, sort, status, create_time', 'order' => 'sort, id'), $select_options);
        return $this->base_get_tree_list($select_options, $data_options);
    }

    function select_json_tree($select_options = array(), $data_options = array()) {
        $select_options = array_merge(array('field' => 'id, title as text', 'order' => 'sort, id'), $select_options);
        return $this->base_get_tree_list($select_options, $data_options);
    }

    function save_department() {
        $rules = array(
            array('title', 'require', '名称无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->error);
        }
        if (empty($this->id)) {
            unset($this->data['id']);
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    /**
     * 复制部门
     * @return type
     * @since 1.0 <2016-1-14> SoChishun Added.
     */
    function copy_department($id) {
        $table = $this->tableName;
        M()->execute("insert into $table (title,pid,`code`,rules,status,site_id) select concat(title,'_new'),pid,`code`,rules,status,site_id from $table where id=$id");
        return $this->returnMsg(true);
    }

    function change_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('status' => $status));
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

    function delete_channel($id) {
        if ($this->table('t_porg_document')->where(array('department_id' => $id))->count() > 0) {
            return $this->returnMsg(false, '该栏目下有文章,请先转移或删除文章!');
        }
        if ($this->table('t_porg_file')->where(array('department_id' => $id))->count() > 0) {
            return $this->returnMsg(false, '该栏目下有下载资源,请先转移或删除下载资源!');
        }
        $result = $this->delete($id);
        if (false !== $result) {
            $this->table('t_porg_document_department_content')->delete($id);
            $this->table('t_porg_document_department_link')->delete();
        }
        return $this->returnMsg($result);
    }

}
