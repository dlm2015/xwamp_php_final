<?php

namespace XWAM\Model;

/**
 * Description of UserModel
 *
 * @author Su 2015-3-5
 */
class UserCustomerModel extends AppbaseModel {

    protected $tableName = 't_porg_user';
    protected $patchValidate = true;

    /**
     * 保存用户
     * @param int $admin_id 用户编号
     * @return array
     * @since 1.0 2015-3-6 by sutroon
     */
    public function save_user($admin_id = 0) {
        $validator = array(
            array('name', 'require', '姓名无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('id_no', 'require', '证件号无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('id_no', 'require', '证件号已被注册!', self::MUST_VALIDATE, 'unique', self::MODEL_INSERT),
            array('mobile', 'require', '手机号码无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
        );
        if (!$this->validate($validator)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        $this->user_name = 'U' . date('Ymdhis');
        if ($this->id_type2) {
            $this->id_type = $this->id_type2;
            unset($this->data['id_type2']);
        }
        if (empty($this->id)) {
            $this->type_name = 'CUSTOMER';
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    /**
     * 删除用户
     * @param string $id 主键编号,多个之间以逗号隔开
     * @param int $admin_id 操作员编号
     * @return array
     * @since 1.0 <2015-6-10> SoChishun Added.
     */
    public function delete_user($id, $admin_id) {
        if (!$admin_id) {
            return $this->returnMsg(false, '用户无效');
        }
        if (!$id) {
            return $this->returnMsg(false, '编号无效');
        }
        $result = $this->delete($id);
        D('Log')->log_user_operate($status, "删除用户账户($id){%} $msg,操作员:$admin_id", $admin_id, LogModel::ACTION_DELETE);
        return $this->returnMsg($result);
    }

    /**
     * 更改在线状态
     * @param type $id
     * @param type $status
     * @return type
     * @since 1.0 <2015-10-28> SoChishun Added.
     */
    function change_online_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('online_status' => $status));
        return $this->returnMsg($result);
    }

}
