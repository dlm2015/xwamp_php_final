<?php

namespace XWAM\Model;

/**
 * CustomerAddressModel 类
 *
 * @since VER:1.0; DATE:2016-1-27; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class CustomerAddressModel extends AppbaseModel {

    protected $tableName = 't_porg_customer_addr';

    function save_address() {
        $rules = array(
            array('name', 'require', '姓名无效!', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
            array('telphone', 'require', '电话号码无效!', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
            array('province', 'require', '省份未选择!', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
            array('city', 'require', '城市未选择!', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
            array('area', 'require', '地区未选择!', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
            array('street', 'require', '街道未填写!', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
            array('customer_id', 'require', '客户无效!', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->error);
        }
        if (empty($this->id)) {
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    /**
     * 保存客户的时候自动添加客户地址
     * @param array $data array('name','telphone','address')
     * @param type $customer_id
     * @since VER:1.0; DATE:2016-1-28; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
     */
    function add_customer_address($data, $customer_id) {
        // 福建省 厦门市 思明区 莲岳路189号武夷工贸6#附7楼
        if (!isset($data['address']) || !isset($data['telphone'])) {
            return;
        }
        $set = array('name' => $data['name'], 'telphone' => $data['telphone'], 'zip' => $data['zip'], 'customer_id' => $customer_id);
        $aaddr = explode(' ', $data['address']);
        if (count($aaddr) == 1) {
            $set['street'] = $data['address'];
        } else {
            $set['province'] = isset($aaddr[0]) ? $aaddr[0] : '';
            $set['city'] = isset($aaddr[1]) ? $aaddr[1] : '';
            $set['area'] = isset($aaddr[2]) ? $aaddr[2] : '';
            $set['street'] = isset($aaddr[3]) ? $aaddr[3] : '';
        }
        $data = $this->field('id, name, zip')->where(array('telphone' => $set['telphone'], 'street' => $set['street']))->find();
        if ($data) {
            if ($data['zip'] != $set['zip'] || $data['name'] != $set['name']) {
                $data['zip'] = $set['zip'];
                $data['name'] = $set['name'];
                $this->save($data);
            }
        } else {
            $this->add($set);
        }
    }

    function change_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('status' => $status));
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

    function delete_address($id) {
        $result = $this->delete($id);
        return $this->returnMsg($result);
    }

}
