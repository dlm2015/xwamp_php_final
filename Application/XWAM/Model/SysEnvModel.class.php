<?php

namespace XWAM\Model;

/**
 * 主平台环境检测模型类
 * <br />检测PHP版本、MySQL版本，自动安装平台依赖数据表
 *
 * @since 1.0 <2015-9-28> SoChishun <14507247@qq.com> Added.
 */
class SysEnvModel extends \Think\Model {

    protected $autoCheckFields = false;

    /**
     * 检测系统
     * @since 1.0 <2015-9-28> SoChishun Added.
     */
    public function check() {
        $last_check_date = F('SysEnv');
        // 1天检测一次
        if (!$last_check_date || (strtotime('now') < strtotime(date('Y-m-d', strtotime('1 day', strtotime($last_check_date)))))) {
            $this->check_env();
            $this->check_db();
            F('SysEnv', date('Y-m-d H:i:s'));
        }
    }

    public function check_env() {
        version_compare(PHP_VERSION, '5.3.0', '<') and exit('系统需要PHP版本5.3.0以上环境(当前版本:' . PHP_VERSION . ').');
        function_exists('mysql_close') or exit('系统需要MySQL环境支持.');
        $ver = '';
        if (function_exists("mysql_get_server_info")) {
            $ver = mysql_get_server_info();
        }
        if (!$ver) {
            $result = M()->query('select VERSION()');
            $ver = current($result[0]);
        }
        version_compare($ver, '5.1.38', '<') and exit('系统需要MySQL版本5.1.0以上环境(当前版本:' . $ver . ').');
    }

    public function check_db() {
        return; // 2016-4-6 SoChishun 作废
        array(
            /* Auth设置 */
            'AUTH_CONFIG' => array(
                'AUTH_ON' => true, // 认证开关
                'AUTH_TYPE' => 1, // 认证方式，1为实时认证；2为登录认证。
                'AUTH_GROUP' => 'think_auth_group', // 用户组(/角色)数据表名
                'AUTH_GROUP_ACCESS' => 'think_auth_group_access', // 用户-用户组(/角色)关系表
                'AUTH_RULE' => 'think_auth_rule', // 权限规则表
                'AUTH_USER' => 't_porg_user', // 用户信息表
            ),
        );
        $tables = C('AUTH_CONFIG');
        $t_group = $tables['AUTH_GROUP'];
        $t_group_access = $tables['AUTH_GROUP_ACCESS'];
        $t_rule = $tables['AUTH_RULE'];
        $result = M()->query("show tables like '$t_rule'");
        if ($result) {
            return true;
        }
        $sqls = array(
            // 权限规则表
            "DROP TABLE IF EXISTS `$t_rule`;",
            "CREATE TABLE IF NOT EXISTS $t_rule (
                `id` int auto_increment primary key comment '主键编号',
                `name` varchar(32) not null default '' comment '权限名称(模块、控制器或操作的名称)',
                `title` varchar(50) not null comment '权限标题',
                `code` varchar(32) not null default '' comment '菜单代码(必须加厂商代码前缀,如：P10000_M1_YH,P10000_YH_M2_YHGL,P10000_YH_M3_GLYGL,P10000_YH_GLYGL_XZGLY)',
                `pid` smallint(6) unsigned not null comment '父级编号(模块pid默认是0)',
                `addon` varchar(32) not null default '' comment '插件名称(应用模块名称,用于卸载应用模块时索引)',
                `level` tinyint(1) unsigned not null comment '节点等级(1:模块,2:控制器,3:操作)',
                `type` varchar(16) not null default '' comment '类型(M(Menu)=菜单,O(Operate)=操作,F(File)=文件,E(Element)=页面元素)',
                `url` varchar(64) not null default '' comment '链接URL',
                `condition` varchar(128) not null default '' comment '规则附件条件,满足附加条件的规则,才认为是有效的规则',
                `remark` varchar(32) not null default '' comment '备注',
                `sort` smallint not null default 0 comment '排列次序',
                `status` smallint not null default '0' comment '状态(0=禁用,1=可用)',
                `create_time` timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
                INDEX ix_code (code),
                INDEX ix_pid (pid),
                INDEX ix_addon (addon)
                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='权限规则(节点)表\r\n
                @since 1.0 <2014-6-28> SoChishun <14507247@qq.com> Added.\r\n
                @since 1.1 <2015-8-28> SoChishun 修改:parent_id 改为 parent_code.\r\n
                @since 1.2 <2015-8-29> SoChishun 新增:vendor_code,appmodule_name.\r\n
                @since 2.0 <2015-9-19> SoChishun 重构以适合RBAC的自动拦截';",
            // 用户组(角色)表
            "DROP TABLE IF EXISTS `$t_group`;",
            "CREATE TABLE `$t_group` (
                `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
                `title` varchar(30) NOT NULL DEFAULT '',
                `display_title` varchar(30) NOT NULL DEFAULT '',
                `status` tinyint(1) NOT NULL DEFAULT '1',
                `rules` varchar(800) NOT NULL DEFAULT '',
                `code` varchar(32) not null default '' comment '角色代码',
                `remark` varchar(32) not null default '' comment '备注',
                `sort` smallint not null default 0 comment '排列次序',
                `create_time` timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
                PRIMARY KEY (`id`)
                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户组(角色)表';",
            "DROP TABLE IF EXISTS `$t_group_access`;",
            // 用户和用户组(角色)的对应表
            "CREATE TABLE `$t_group_access` (
                `uid` mediumint(8) unsigned NOT NULL,
                `group_id` mediumint(8) unsigned NOT NULL,
                UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
                KEY `uid` (`uid`),
                KEY `group_id` (`group_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户和用户组(角色)的对应表';"
        );
        foreach ($sqls as $sql) {
            M()->execute($sql);
        }
        return true;
    }

}
