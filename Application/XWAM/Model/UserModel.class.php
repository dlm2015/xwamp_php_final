<?php

namespace XWAM\Model;

/**
 * Description of UserModel
 *
 * @author Su 2015-3-5
 */
class UserModel extends AppbaseModel {

    protected $tableName = 't_porg_user';
    protected $patchValidate = true;

    /**
     * 修改密码
     * @param string|array $where 查询条件
     * @param string $pwd 新密码
     * @return false|int 成功则返回受影响数量,失败返回false
     * @since 1.0 2015-3-6 by sutroon
     */
    public function update_password($where, $pwd) {
        $result = $M->where($where)->setField('password', $pwd);
        $condition = is_array($where) ? sofunc_array_implode(',', $where) : $where;
        $m_log = new LogModel();
        $m_log->log_user_operate(false !== $result, "[修改密码%] $condition, password=$pwd", 0, LogModel::ACTION_EDIT);
        return $result;
    }

    /**
     * 设置用户列表的角色和部门名称
     * 角色名称：role_names, 部门名称：department_names
     * @param array $list
     * @since VER:1.0; DATE:2016-1-18; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
     */
    function set_role2department_names(&$list) {
        $m_role = new RoleModel();
        $roles = $m_role->getField('id, title, display_title');
        $m_department = new DepartmentModel();
        $departments = $m_department->getField('id,title');
        foreach ($list as &$row) {
            $row['role_names'] = '';
            $row['department_names'] = '';
            if ($row['role_ids']) {
                $role_ids = explode(',', $row['role_ids']);
                foreach ($role_ids as $role_id) {
                    if (array_key_exists($role_id, $roles)) {
                        if ($row['role_names']) {
                            $row['role_names'].=', ';
                        }
                        $row['role_names'].=empty($roles[$role_id]['display_title']) ? $roles[$role_id]['title'] : $roles[$role_id]['display_title'];
                    }
                }
            }
            if ($row['department_ids']) {
                $department_ids = explode(',', $row['department_ids']);
                foreach ($department_ids as $department_id) {
                    if (array_key_exists($department_id, $departments)) {
                        if ($row['department_names']) {
                            $row['department_names'].=', ';
                        }
                        $row['department_names'].=$departments[$department_id];
                    }
                }
            }
        }
    }

    /**
     * 更改在线状态
     * @param type $uid
     * @param type $status
     * @return type
     * @since 1.0 <2015-10-28> SoChishun Added.
     */
    function change_online_status($uid, $status) {
        $result = $this->where(array('id' => $uid))->setField(array('online_status' => $status));
        return $this->returnMsg($result);
    }

    function save_role($uid, $role_ids) {
        $result = $this->where(array('id' => $uid))->setField(array('role_ids' => $role_ids));
        return $this->returnMsg($result);
    }

    function save_department($uids, $department_ids) {
        $result = $this->where(array('id' => array('in', $uids)))->setField(array('department_ids' => $department_ids));
        return $this->returnMsg($result);
    }

}
