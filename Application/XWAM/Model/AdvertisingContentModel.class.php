<?php

namespace XWAM\Model;

/**
 * AdvertisingContentModel类
 *
 * @since 1.0 <2015-11-7> SoChishun <14507247@qq.com> Added.
 */
class AdvertisingContentModel extends AppbaseModel {

    protected $tableName = 't_porg_advertising_content';

    function save_content() {
        $rules = array(
            array('title', 'require', '标题无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('link_url', 'require', '链接地址无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('begin_time', 'require', '生效时间无效!', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
            array('end_time', 'require', '失效时间无效!', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
            array('status', 'number', '类型无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->error);
        }
        $msg = \Common\Controller\UploadHandlerController::upload(array('savePath' => 'ImageAdv/', 'skipEmpty' => true));
        if (empty($this->id) && !is_array($msg)) {
            return $this->returnMsg(false, $msg);
        }
        if (is_array($msg) && isset($msg['file_path'])) {
            $fileinfo = $msg['file_path'];
            $image = new \Think\Image();
            $image->open(sofn_path_rtoa($fileinfo['filepath']));
            $this->data['image_width'] = $image->width();
            $this->data['image_height'] = $image->height();
            unset($image);
            $this->data['file_name'] = $fileinfo['savename'];
            $this->data['file_size'] = format_bytes($fileinfo['size']);
            $this->data['file_type'] = $fileinfo['type'];
            $this->data['file_ext'] = $fileinfo['ext'];
            $this->data['file_path'] = $fileinfo['filepath'];
        }
        if (empty($this->id)) {
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    function delete_content($id = '') {
        $path = $this->where(array('id' => $id))->getField('file_path');
        if ($path && is_file($path)) {
            @unlink($path);
        }
        $result = $this->delete($id);
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

}
