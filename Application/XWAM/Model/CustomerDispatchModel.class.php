<?php

namespace XWAM\Model;

/**
 * CustomerDispatchModel 类
 *
 * @since VER:1.0; DATE:2016-2-2; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class CustomerDispatchModel extends AppbaseModel {

    protected $tableName = 't_porg_customer_dispatch';

    /**
     * 分页查询
     * @param string $pager
     * @param array $afields
     * @param integer $site_id
     * @param array $asearch
     * @return array
     * @since 1.0 2016-5-11 SoChishun Added.
     */
    function paging_select(&$pager, &$afields, $site_id, $asearch) {
        $m_attr = new CustomerAttrModel();
        $afields = $m_attr->get_customer_list_fields($site_id, array('id' => '', 'serial_no' => '客户ID', 'name' => '姓名', 'telphone' => '电话', 'sex' => '性别', 'address' => '地址', 'user_name' => '工号', 'old_user_name' => '原工号'));
        if ($afields['custom']) {
            $table = 't_porg_customer c left join t_porg_customer_s' . $site_id . ' s on c.id=s.customer_id';
            $field = implode(',c.', array_keys($afields['system'])) . implode(',s.', array_keys($afields['custom']));
        } else {
            $table = 't_porg_customer';
            $field = array_keys($afields['system']);
        }
        return $this->get_paging_list($pager, array('table' => $table, 'field' => $field, 'where' => $asearch['where'], 'order' => 'id desc'), array('page_params' => $asearch['search']));
    }

    /**
     * 批量调度客户
     * @param type $admin_name
     * @return type
     * @since VER:1.0; DATE:2016-2-2; AUTHOR:SoChishun; DESC:Added.
     */
    function save_dispatch($admin_name) {
        $aids = I('id');
        $new_users = I('new_users');
        $per_num = I('per_num');
        $remark = I('remark');
        $err = array();
        if (!$aids) {
            $err[] = '客户无效!';
        }
        if (!$new_users) {
            $err[] = '分配工号无效!';
        }
        if ($err) {
            return $this->returnMsg(false, $err);
        }
        $ausers = explode(',', $new_users);
        if (!$per_num || !is_numeric($per_num) || $per_num < 1) {
            $per_num = ceil(count($aids) / count($ausers));
        }
        $old_users = $this->table('t_porg_customer')->where(array('id' => array('in', $aids)))->getField('id, user_name');
        $spilt_old_users = array_chunk($old_users, $per_num);
        $users = array();
        $i = 0;
        foreach ($split_old_users as $per_old_users) {
            $users[$aids[$i]] = $per_old_users;
            $i++;
        }
        $title = sprintf('KHDD-%s-%d', date('Ymd-his'), count($aids));
        $sqls = array();
        // 写入调度表
        $sql = 'insert into ' . $this->tableName . ' (title,customer_id,old_user_name,new_user_name,admin_name,dispatch_remark) values';
        $i = 0;
        foreach ($users as $user_name => $per_old_users) {
            foreach ($per_old_users as $customer_id => $old_user_name) {
                $sqls[] = "('$title', $customer_id, '$old_user_name', '$user_name', $admin_name, '$remark')";
            }
            if ($i > 0 && ($i % 300 == 0)) {
                $sqltemp = $sql . implode(',', $sqls);
                // echo '<br />' . $sqltemp;
                $result = $this->execute($sqltemp);
                if (false === $result) {
                    return $this->returnMsg(false, $this->getDbError());
                }
                empty($sqls);
            }
            $i++;
        }
        if ($sqls) {
            $sqltemp = $sql . implode(',', $sqls);
            // echo '<br />' . $sqltemp;
            $result = $this->execute($sqltemp);
            if (false === $result) {
                return $this->returnMsg(false, $this->getDbError());
            }
            empty($sqls);
        }
        // 更新到客户表
        $this->execute("update t_porg_customer c inner join t_porg_customer_dispatch d set c.user_name=d.new_user_name, c.old_user_name=d.old_user_name where c.id=d.customer_id and d.title='$title'");
        return $this->returnMsg(true);
    }

    /**
     * 坐席领取客户
     * @param type $customer_ids
     * @param type $user_name
     * @param type $admin_name
     * @return type
     * @since VER:1.0; DATE:2016-5-23; AUTHOR:SoChishun; DESC:Added.
     */
    function save_simple($customer_ids, $user_name, $admin_name) {
        $err = array();
        if (!$customer_ids) {
            $err[] = '客户无效!';
        }
        if (!$user_name) {
            $err[] = '分配工号无效!';
        }
        if ($err) {
            return $this->returnMsg(false, $err);
        }
        $n = count(explode(',', $customer_ids));
        $ndb = $this->where("create_time > '$date 0:0:0' and create_time < '$date 23:59:59'")->count();
        $n = $ndb + $n;
        if ($n >= 20) {
            return $this->returnMsg(false, "您今天已领取 $n 个客户, 已达到今天最大限额 20 个客户!");
        }
        $title = sprintf('KHDD-%s-%d', date('Ymd-his'), $user_name);
        // 写入调度表
        $table = $this->tableName;
        $sql = "insert into $table (title,customer_id,old_user_name,new_user_name,admin_name,dispatch_remark) values select '$title', id, user_name, '$user_name', '$admin_name', '' from t_porg_user where id in ($customer_ids)";
        // 更新到客户表
        $this->execute("update t_porg_customer set old_user_name=user_name, user_name='$user_name' where id in ($customer_ids)");
        // 统计
        $date = date('Y-m-d');
        return $this->returnMsg(true);
    }

}
