<?php

namespace XWAM\Model;

/**
 * NoticesModel类
 *
 * @since 1.0 <2016-2-3> SoChishun <14507247@qq.com> Added.
 */
class NoticesModel extends AppbaseModel {

    protected $tableName = 't_porg_notices';

    function save_document($user_name) {
        $rules = array(
            array('title', 'require', '标题无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('user_name', 'require', '用户无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->error);
        }
        //图片上传
        $msg = \Common\Controller\UploadHandlerController::upload(array('savePath' => 'Notices/', 'skipEmpty' => true));
        if (is_array($msg) && isset($msg['picture_url'])) {
            $this->data['picture_url'] = $msg['picture_url']['filepath'];
        }
        if (isset($this->data['create_time']) && empty($this->data['create_time'])) {
            unset($this->data['create_time']);
        }
        if (empty($this->id)) {
            unset($this->data['id']);
            $this->user_name = $user_name;
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    function change_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('status' => $status));
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

    function delete_document($id) {
        $result = $this->delete($id);
        return $this->returnMsg($result);
    }

}
