<?php

namespace XWAM\Model;

/**
 * Description of UserModel
 *
 * @author Su 2015-3-5
 */
class UserAdminModel extends AppbaseModel {

    protected $tableName = 't_porg_user';
    protected $patchValidate = true;

    /**
     * 修改密码
     * @param string|array $where 查询条件
     * @param string $pwd 新密码
     * @return false|int 成功则返回受影响数量,失败返回false
     * @since 1.0 2015-3-6 by sutroon
     */
    public function update_password($where, $pwd) {
        $result = $M->where($where)->setField('password', $pwd);
        $condition = is_array($where) ? sofunc_array_implode(',', $where) : $where;
        $log = new LogModel();
        $log->log_user_operate(false !== $result, "[修改密码%] $condition, password=$pwd", 0, LogModel::ACTION_EDIT);
        return $result;
    }

    /**
     * 保存用户
     * @param int $admin_id 用户编号
     * @return array
     * @since 1.0 2015-3-6 by sutroon
     */
    public function save_user($admin_id) {
        $validator = array(
            array('user_name', 'require', '用户名无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('user_name', 'check_user_name', '用户名已被注册!', self::MUST_VALIDATE, 'callback', self::MODEL_INSERT),
            array('password', 'require', '密码无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('status', 'number', '状态有误!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        $is_edit = empty($_POST['id']) ? false : true;
        $log = new LogModel();
        if (!$this->validate($validator)->create()) {
            $log->log_user_operate(false, sprintf('%s用户账户失败 user=%s, siteID=%d %s', ($is_edit ? '编辑' : '新增'), $this->data['user_name'], $this->data['siteID'], implode('; ', $this->getError())), $admin_id, ($is_edit ? LogModel::ACTION_EDIT : LogModel::ACTION_ADD));
            return $this->returnMsg(false, $this->getError());
        }
        //图片上传
        $msg = \Common\Controller\UploadHandlerController::upload(array('savePath' => 'Face/', 'skipEmpty' => true));
        if (is_array($msg)) {
            $this->data['face_url'] = $msg['face_url']['filepath'];
        }
        $data = $this->data;
        if (empty($this->id)) {
            unset($this->data['id']);
            $this->type_name = 'ADMIN';
            $result = $this->add();
        } else {
            unset($this->data['user_name']);
            $result = $this->save();
        }
        $log->log_user_operate($result, sprintf('%s用户账户%s user=%s', ($is_edit ? '编辑' : '新增'), '{%}', $data['user_name']), $admin_id, ($is_edit ? LogModel::ACTION_EDIT : LogModel::ACTION_ADD));
        return $this->returnMsg($result);
    }

    // 检测用户名是否重复 2016-4-8 SoChishun Added.
    function check_user_name($val) {
        $n = $this->where(array('user_name' => trim($val)))->count('id');
        return $n < 1;
    }

    /**
     * 删除用户
     * @param string $id 主键编号,多个之间以逗号隔开
     * @param int $admin_id 操作员编号
     * @return array
     * @since 1.0 <2015-6-10> SoChishun Added.
     */
    public function delete_user($id, $admin_id) {
        if (!$admin_id) {
            return $this->returnMsg(false, '用户无效');
        }
        if (!$id) {
            return $this->returnMsg(false, '编号无效');
        }
        $result = $this->delete($id);
        $log = new LogModel();
        $log->log_user_operate($status, "删除用户账户($id){%} $msg,操作员:$admin_id", $admin_id, LogModel::ACTION_DELETE);
        return $this->returnMsg($result);
    }

    /**
     * 更改在线状态
     * @param type $id
     * @param type $status
     * @return type
     * @since 1.0 <2015-10-28> SoChishun Added.
     */
    function change_online_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('online_status' => $status));
        return $this->returnMsg($result);
    }

}
