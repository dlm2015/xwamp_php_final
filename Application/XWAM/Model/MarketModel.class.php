<?php

namespace XWAM\Model;

/**
 * Market模型类
 * <br />可用：从ModulesDist目录读取所有封装好的插件包
 * <br />已下载：从Usr/local/Package/读取所有zip插件包
 * <br />已安装：从Usr/etc/
 * <br />更新：
 * <br />设置：
 * @since 1.0 <2015-9-25> SoChishun <14507247@qq.com> Added.
 */
class MarketModel {

    /**
     * 获取已安装的插件列表
     * <br />字段：名称|标题|版本号|描述|许可证|厂商
     */
    public function get_installed_list() {
        $source = MODULE_PATH . basename(ADDON_PATH);
        $cnf_file = 'Install/module.config.php';
        $out=array();
        if ($handle = opendir($source)) {
            while (false !== ( $f = readdir($handle) )) {
                if ('.' == $f || '..' == $f) {
                    continue;
                }
                $cnf_path=$source . '/' . $f . '/' . $cnf_file;
                if (is_file($cnf_path)) {
                    $config=  require $cnf_path;
                    if(isset($config['MODULE_INFO'])){
                        $out[]=$config['MODULE_INFO'];
                    }
                }
            }
            closedir($handle);
        }
        return $out;
    }
    
    public function install(){
        
    }
    
    public function uninstall($name){
        
    }

}
