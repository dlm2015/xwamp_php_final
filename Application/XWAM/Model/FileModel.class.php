<?php

namespace XWAM\Model;

/**
 * FileModel类
 *
 * @since 1.0 <2015-10-24> SoChishun <14507247@qq.com> Added.
 */
class FileModel extends AppbaseModel {

    protected $tableName = 't_porg_file';

    function save_file($user_name) {
        set_time_limit(0);
        $rules = array(
            array('title', 'require', '标题无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('category_id', 'number', '类别无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('user_name', 'require', '用户无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->error);
        }
        $upload_conf = array('savePath' => 'Downloads/', 'skipEmpty' => true, 'maxSize' => 0);
        // 2016-1-13 SoChishun 修正Win服务器下rename无效的问题
        if (empty($this->auto_name) && IS_WIN) {
            $upload_conf['saveName'] = array();
        }
        $msg = \Common\Controller\UploadHandlerController::upload($upload_conf);
        if (empty($this->id) && !is_array($msg)) {
            return $this->returnMsg(false, $msg);
        }
        if (is_array($msg) && isset($msg['file_path'])) {
            $fileinfo = $msg['file_path'];
            $name = $fileinfo['savename'];
            $path = $fileinfo['filepath'];
            // 2015-11-17 SoChishun 修正保持原文件名,文件名是中文的时候保存出错的问题(用rename)
            if (empty($this->auto_name) && !IS_WIN) {
                $name = $fileinfo['name'];
                $path = substr($path, 0, strrpos($path, '/') + 1) . $name;
                if (!rename($fileinfo['filepath'], $path)) {
                    unlink($fileinfo['filepath']);
                    exit('重命名失败!');
                }
            }
            $this->data['file_name'] = $name;
            $this->data['file_size'] = format_bytes($fileinfo['size']);
            $this->data['file_type'] = $fileinfo['type'];
            $this->data['file_ext'] = $fileinfo['ext'];
            $this->data['file_path'] = $path;
        }
        if (is_array($msg) && isset($msg['picture_url'])) {
            $path = $msg['picture_url']['filepath'];
            $result = \Common\Controller\UploadHandlerController::save_thumb($path, $msg);
            if (!$result) {
                return $this->returnMsg(false, $msg);
            }
            $this->data['picture_url'] = $path;
        }
        if (isset($this->data['auto_name'])) {
            unset($this->data['auto_name']);
        }
        if (empty($this->id)) {
            unset($this->data['id']);
            $this->user_name = $user_name;
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

    function delete_file($id) {
        $paths = $this->where(array('id' => array('in', $id)))->field('picture_url,file_path')->select();
        $result = $this->delete($id);
        if ($result) {
            foreach ($paths as $row) {
                foreach ($row as $path) {
                    if ($path && file_exists($path)) {
                        @unlink($path);
                    }
                }
            }
        }
        return $this->returnMsg($result);
    }

}
