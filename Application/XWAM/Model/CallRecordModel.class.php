<?php

namespace XWAM\Model;

/**
 * CallRecordModel 类
 *
 * @since VER:1.0; DATE:2016-1-23; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class CallRecordModel extends AppbaseModel {

    protected $tableName = 't_porg_callrecord';

    /**
     * 获取分机最近通话10条记录
     * @param type $exten
     * @param type $field
     * @param type $limit
     * @return type
     * @since VER:1.0; DATE:2016-2-1; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
     */
    function get_exten_call_records($exten, $field = 'line,create_time', $limit = 10) {
        return $this->field($field)->where(array('exten' => $exten, 'direction' => 1))->order('id desc')->limit($limit)->select();
    }

    // 2016-4-21
    function change_status($id, $field, $status) {
        $result = $this->where(array('id' => $id))->setField(array($field => $status));
        return $this->returnMsg($result);
    }

    // 同步cdr表字段 2016-4-8
    function synchronous_data() {
        $this->execute('update t_porg_callrecord r inner join db_xcall_cdr.cdr c set r.caller_no=c.src, r.called_no=c.dst, r.start_time=c.start, r.end_time=c.xcallAnswer, r.duration=c.duration, r.billsec=c.billsec, r.direction=c.direction, r.recording_file=c.recordingfile where r.line=c.dst and r.exten=c.extension and r.start_time=0');
        $this->execute("update t_porg_callrecord r INNER JOIN t_porg_customer c set r.customer_name=c.name, r.customer_no=c.serial_no where r.line=c.telphone and r.customer_no = ''");
    }

}
