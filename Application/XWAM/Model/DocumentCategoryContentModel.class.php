<?php

namespace XWAM\Model;

/**
 * DocumentCategoryModel类
 *
 * @since 1.0 <2015-10-24> SoChishun <14507247@qq.com> Added.
 */
class DocumentCategoryContentModel extends AppbaseModel {

    protected $tableName = 't_porg_document_category_content';
    protected $pk = 'category_id';
    protected $patchValidate = true;

    function save_content() {
        $rules = array(
            array('content', 'require', '内容无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->error);
        }
        $action = $this->data['action'];
        unset($this->data['action']);
        if ('add' == $action) {
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

}
