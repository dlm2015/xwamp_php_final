<?php

namespace XWAM\Model;

/**
 * 模型基础类
 *
 * @since 1.0 <2015-3-18> sutroon <14507247@qq.com> Added.
 */
abstract class AppbaseModel extends \Common\Model\CommonModel {
}
