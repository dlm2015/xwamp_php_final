<?php

namespace XWAM\Model;

/**
 * CustomerOverdueModel 类
 *
 * @since VER:1.0; DATE:2016-5-12; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class CustomerOverdueModel extends AppbaseModel {

    protected $tableName = 't_porg_customer';

    /**
     * 分页查询
     * @param string $pager
     * @param array $afields
     * @param integer $site_id
     * @param array $asearch
     * @return array
     * @since 1.0 2016-5-11 SoChishun Added.
     */
    function paging_select(&$pager, &$afields, $site_id, $asearch) {
        $m_attr = new CustomerAttrModel();
        $afields = $m_attr->get_customer_list_fields($site_id, array('id' => '', 'serial_no' => '客户ID', 'name' => '姓名', 'telphone' => '电话', 'sex' => '性别', 'address' => '地址', 'user_name' => '工号', 'review_time' => '最后联系时间'));
        if ($afields['custom']) {
            $table = $this->tableName . ' c left join ' . $this->tableName . '_s' . $site_id . ' s on c.id=s.customer_id';
            $field = implode(',c.', array_keys($afields['system'])) . implode(',s.', array_keys($afields['custom']));
        } else {
            $table = $this->tableName;
            $field = array_keys($afields['system']);
        }
        return $this->get_paging_list($pager, array('table' => $table, 'field' => $field, 'where' => $asearch['where'], 'order' => 'id desc'), array('page_params' => $asearch['search']));
    }

}
