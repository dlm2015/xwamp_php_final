<?php

namespace XWAM\Model;

/**
 * Description of ConfigModel
 *
 * @since 1.0 <2015-10-20> SoChishun <14507247@qq.com> Added.
 */
class ConfigModel extends AppbaseModel {

    protected $tableName = 't_porg_config';

    /**
     * 获取配置项的值
     * @param type $name
     * @param type $defv
     * @return type
     * @since 1.0 <2015-10-20> SoChishun Added.
     */
    function c($name = null, $defv = '') {
        $cache_key = 'config_c' . ($name ? $name : '');
        $cache_data = S($cache_key);
        if ($cache_data) {
            return $cache_data;
        }
        $config = $this->getField('name, value', true);
        $akey = array('HOME_URL', 'LOGIN_URL', 'LOGOUT_URL');
        foreach ($akey as $skey) {
            if (!empty($config[$skey])) {
                $aurl = explode(':', $config[$skey]);
                $config[$skey] = U($aurl[1], 'addon=' . $aurl[0]);
            }
        }
        if (empty($config['WEB_SITE_TITLE'])) {
            $config['WEB_SITE_TITLE'] = C('WEB_SITE_TITLE');
        }
        if (!isset($config['WEB_SITE_STATUS'])) {
            $config['WEB_SITE_STATUS'] = C('WEB_SITE_STATUS');
        }
        if (is_null($name)) {
            S($cache_key, $config, 15);
            return $config;
        }
        $val = $config && isset($config[$name]) ? $config[$name] : $defv;
        S($cache_key, $val, 15);
        return $val;
    }

    /**
     * 获取配置列表
     * @return type
     * @since 1.0 <2015-10-20> SoChishun Added.
     */
    function get_list() {
        $cache_key = 'config_get_list';
        $cache_data = S($cache_key);
        if ($cache_data) {
            return $cache_data;
        }
        $list = $this->where('status=1')->order('sort, id')->select();
        S($cache_key, $list, 15);
        return $list;
    }

    /**
     * 保存配置
     * @return boolean
     * @since 1.0 <2015-10-20> SoChishun Added.
     */
    function save_configs() {
        $data = $_POST;
        $sqls = array();
        foreach ($data as $key => $value) {
            $sqls[] = "update t_porg_config set `value`='$value' where name='$key'";
        }
        $m = new \Think\Model\AdvModel();
        $m->patchQuery($sqls);
        // 清除缓存,使修改立即生效
        $cache_key = 'config_c';
        S($cache_key, null);
        return true;
    }

    /**
     * 保存单个配置
     * @return type
     */
    function save_var() {
        $validator = array(
            array('name', 'require', '名称无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('name', 'require', '名称已被使用!', self::MUST_VALIDATE, 'unique', self::MODEL_INSERT),
            array('title', 'require', '标题无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('type', 'require', '类型无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('group', 'require', '分组无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('user_name', 'require', '用户无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('status', 'number', '状态有误!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($validator)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        if (empty($this->id)) {
            unset($this->data['id']);
            $this->prop = 'C';
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    function change_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('status' => $status));
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

    function remove_var($id) {
        $result = $this->where(array('id' => array('in', $id), 'prop' => 'C'))->delete();
        return $this->returnMsg($result);
    }

}
