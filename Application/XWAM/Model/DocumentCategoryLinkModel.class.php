<?php

namespace XWAM\Model;

/**
 * DocumentCategoryLinkModel类
 *
 * @since 1.0 <2015-10-26> SoChishun <14507247@qq.com> Added.
 */
class DocumentCategoryLinkModel extends AppbaseModel {

    protected $tableName = 't_porg_document_category_link';
    protected $pk = 'category_id';
    protected $patchValidate = true;

    function save_link() {
        $rules = array(
            array('url', 'require', '链接无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->error);
        }
        $action = $this->data['action'];
        unset($this->data['action']);
        if ('add' == $action) {
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

}
