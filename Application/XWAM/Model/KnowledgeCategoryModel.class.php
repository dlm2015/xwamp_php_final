<?php

namespace XWAM\Model;

/**
 * KnowledgeCategoryModel 类
 *
 * @since 1.0 <2016-2-3> SoChishun <14507247@qq.com> Added.
 */
class KnowledgeCategoryModel extends AppbaseModel {

    protected $tableName = 't_porg_knowledge_category';
    protected $patchValidate = true;

    function select_tree($select_options = array(), $data_options = array()) {
        $select_options = array_merge(array('field' => 'id, title, pid, sort, status, create_time', 'order' => 'sort, id'), $select_options);
        return $this->base_get_tree_list($select_options, $data_options);
    }

    function select_json_tree($select_options = array(), $data_options = array()) {
        $select_options = array_merge(array('field' => 'id, title as text', 'order' => 'sort, id'), $select_options);
        return $this->base_get_tree_list($select_options, $data_options);
    }

    function save_category($user_name) {
        $rules = array(
            array('title', 'require', '名称无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('user_name', 'require', '用户无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            return $this->returnMsg(false, $this->error);
        }
        if (empty($this->id)) {
            unset($this->data['id']);
            $this->user_name = $user_name;
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    /**
     * 复制栏目
     * @return type
     * @since 1.0 <2016-1-14> SoChishun Added.
     */
    function copy_category($id) {
        $table = $this->tableName;
        M()->execute("insert into $table (title, pid) select concat(title,'_new'), pid from $table where id=$id");
        return $this->returnMsg(true);
    }

    function change_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('status' => $status));
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

    function delete_channel($id) {
        if ($this->table('t_porg_knowledge')->where(array('category_id' => $id))->count() > 0) {
            return $this->returnMsg(false, '该栏目下有文章项目,请先转移或删除文章!');
        }
        $result = $this->delete($id);
        return $this->returnMsg($result);
    }

}
