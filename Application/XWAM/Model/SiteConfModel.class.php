<?php

namespace XWAM\Model;

/**
 * SiteConfModel 类
 *
 * @since VER:1.0; DATE:2016-4-21; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class SiteConfModel extends AppbaseModel {

    protected $tableName = 't_porg_site_conf';

    // 2016-4-21
    public function find_site($site_id, $user_name, $create = true) {
        $data = $this->find($site_id);
        if (!$data && $create) {
            $site_code = sofn_generate_serial('S' . $site_id);
            $data = array(
                'id' => $site_id,
                'site_title' => 'XCRM客户资料管理系统',
                'site_code' => $site_code,
                'site_app_name' => 'XWAMP',
                'site_app_version' => '1.0.0.0',
                'site_sub_path' => '/xcrm',
                'session_prefix' => 'xwamp' . $site_code,
                'content_list_rows' => 30,
                'user_name' => $user_name,
                'site_closed_announcement' => '抱歉,网站正在维护,请您稍后访问~',
                'ui_aside_status' => 'Y',
            );
            $id = $this->add($data);
            $data = $this->find($id);
        }
        return $data;
    }

    public function save_site() {
        $validator = array(
            array('site_title', 'require', '网站标题无效!', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT),
            array('site_code', '', '网站代号已被使用!', self::MUST_VALIDATE, 'unique', self::MODEL_INSERT),
        );
        if (!$this->validate($validator)->create()) {
            return $this->returnMsg(false, $this->getError());
        }
        //图片上传
        //$msg = \Common\Controller\UploadHandlerController::upload(array('savePath' => 'Logo/', 'skipEmpty' => true));
        //if (is_array($msg)) {
        //    $this->data['site_logo_url'] = $msg['site_logo_url']['filepath'];
        //}
        $this->session_prefix = $this->site_code;
        $result = $this->save();
        return $this->returnMsg($result);
    }

}
