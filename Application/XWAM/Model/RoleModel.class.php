<?php

namespace XWAM\Model;

/**
 * 角色数据库模型
 *
 * @since 1.0.0 <2015-3-25> SoChishun <14507247@qq.com> Added.
 */
class RoleModel extends AppbaseModel {

    /**
     * 数据表名称
     * @var string
     */
    protected $tableName = 'think_auth_group';

    /**
     * 是否批量验证
     * @var boolean
     */
    protected $patchValidate = true;

    /**
     * 保存角色
     * @return true|string|array
     * @since 1.0 2015-3-6 by sutroon
     */
    public function save_role() {
        $rules = array(
            array('title', 'require', '角色标题必须填写!'),
            array('status', 'number', '状态有误!'),
            array('site_id', 'number', 'SiteID无效!', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        );
        if (!$this->validate($rules)->create()) {
            $this->returnMsg(false, $this->getError());
        }
        // 显示名称不填则默认与角色名称一致
        if (empty($this->display_title)) {
            $this->display_title = $this->title;
        }
        if (empty($this->id)) {
            unset($this->data['id']);
            $result = $this->add();
        } else {
            $result = $this->save();
        }
        return $this->returnMsg($result);
    }

    /**
     * 复制角色
     * @return type
     * @since 1.0 <2015-10-28> SoChishun Added.
     */
    function copy_role($id) {
        $table = $this->tableName;
        M()->execute("insert into $table (title,`code`,rules,site_id) select concat(title,'_new'),`code`,rules,site_id from $table where id=$id");
        return $this->returnMsg(true);
    }

    public function save_rule($role_id, $rules) {
        $result = $this->where(array('id' => $role_id))->setField('rules', implode(',', $rules));
        return $this->returnMsg($result);
    }

    public function delete_role($id = '') {
        if (!$id) {
            return $this->returnMsg(false, '参数无效');
        }
        /* 判断是否有子项目
          $list = M()->query('select a.title from think_auth_group a inner join think_auth_group b on b.pid=a.id where b.pid in (' . $id . ')');
          $names = false;
          if ($list) {
          foreach ($list as $row) {
          $names.=',' . $row['name'];
          }
          }
          if ($names) {
          return $this->returnMsg(false, '以下项目有子项目,请先转移子项目:' . "\n" . ltrim($names, ','));
          }
         */
        // 判断是否有引用
        $user_role_ids = M('think_auth_group_access')->where(array('group_id' => array('in', $id)))->getField('group_id', true);
        if ($user_role_ids) {
            $names = $this->where(array('id' => array('in', $user_role_ids)))->getField('title', true);
            return $this->returnMsg(false, '以下项目正在被用户使用,无法删除:' . "\n" . implode(',', $names));
        }
        $result = $this->delete($id);
        return $this->returnMsg($result);
    }

    function change_status($id, $status) {
        $result = $this->where(array('id' => $id))->setField(array('status' => $status));
        return $this->returnMsg($result);
    }

    function change_sort($id, $sort) {
        $result = $this->where(array('id' => $id))->setField(array('sort' => $sort));
        return $this->returnMsg($result);
    }

}
