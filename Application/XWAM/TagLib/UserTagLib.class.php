<?php

namespace XWAM\TagLib;

/**
 * UserTagLib类
 *
 * @since 1.0 <2015-10-29> SoChishun <14507247@qq.com> Added.
 */
class UserTagLib extends \Think\Template\TagLib{

    protected $tags = array(
        'list' => array('attr' => 'attrs', 'level' => 1),
    );

    /**
     * list标签解析 循环输出数据集
     * @access public
     * @param array $tag 标签属性
     * @param string $content  标签内容
     * @return string|void
     * @example path description
     * @since 1.0 <2015-10-27> SoChishun Added.
     */
    public function _list($tag, $content) {
        $sqloptions = array('where', 'field', 'limit', 'order', 'table');
        $options = array();
        foreach ($sqloptions as $key) {
            if (!empty($tag[$key])) {
                $options[$key] = $tag[$key];
            }
        }
        // 标签不支持<>等html标签,只能用neq,gt,lt代替
        if (!empty($options['where'])) {
            $alias = array('neq' => '<>', 'gt' => '>', 'lt' => '<', 'egt' => '>=', 'elt' => '<=');
            foreach ($alias as $key => $value) {
                $options['where'] = str_replace(" $key ", " $value ", $options['where']);
            }
        }
        $name = '$' . (empty($tag['name']) ? 'ds_document' : $tag['name']);
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $key = !empty($tag['key']) ? $tag['key'] : 'key';
        $parseStr = '<?php ' . $name . '=D(\'t_porg_document\')->select(' . var_export($options, true) . '); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';

        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }
    
}
