<?php

namespace XWAM\TagLib;

/**
 * DodumentCategoryContent标签类
 *
 * @since 1.0 <2015-11-12> SoChishun <14507247@qq.com> Added.
 */
class DocumentCategoryContent extends \Think\Template\TagLib {

    protected $tags = array(
        'item' => array('attr' => 'attrs', 'level' => 1),
    );

    /**
     * 获取字段值
     * @param type $tag
     * @param type $content
     * @return string
     * @since 1.0 <2015-11-12> SoChishun Added.
     * @example <DocumentCategoryContent:item pk="$Think.Get.id??3" field="title">{$vo.title}</documentcategory:item>
     */
    function _item($tag, $content) {
        $pk = $tag['pk'];
        if (strpos($pk, '??')) {
            $apk = explode('??', $pk);
            $pk = $apk[0];
            if (false !== strpos($pk, '$Think')) {
                $pk = $this->parseThinkVar($pk);
            }
            $pk = '(isset(' . $pk . ') ? ' . $pk . ' : ' . $apk[1] . ')';
        } else {
            if (false !== strpos($pk, '$Think')) {
                $pk = $this->parseThinkVar($pk);
            }
        }
        $options = array('limit' => 1);
        if (!empty($tag['field'])) {
            $options['field'] = $tag['field'];
        }
        $options['cache'] = empty($tag['cache']) ? 5 : $tag['cache'];
        if ($pk) {
            $options['where'] = array('category_id' => '{#id}');
        }
        $soptions = '';
        if ($options) {
            $soptions = var_export($options, true);
            if ($pk) {
                $soptions = str_replace('\'{#id}\'', $pk, $soptions);
            }
        }
        $name = '$' . (empty($tag['name']) ? 'ds_item' : $tag['name']);
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $key = !empty($tag['key']) ? $tag['key'] : 'key';
        $extract = !empty($tag['extract']);
        $parseStr = '<?php ' . $name . ' = M(\'t_porg_document_category_content\')->select(' . $soptions . '); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';
        if ($extract) {
            $parseStr .= '<?php if(is_array(' . $name . ')): extract(' . $name . '[0]); endif; ?>';
        }

        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }

}
