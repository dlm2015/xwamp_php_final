<?php

namespace XWAM\TagLib;

/**
 * Doduments标签类
 *
 * @since 1.0 <2015-10-27> SoChishun <14507247@qq.com> Added.
 */
class Document extends \Think\Template\TagLib {

    protected $tags = array(
        'list' => array('attr' => 'attrs', 'level' => 1),
        'paginglist' => array('attr' => 'attrs', 'level' => 1),
        'item' => array('attr' => 'attrs', 'level' => 1),
    );

    /**
     * list标签解析 循环输出数据集
     * @access public
     * @param array $tag 标签属性
     * @param string $content  标签内容
     * @return string|void
     * @example path description
     * @since 1.0 <2015-10-27> SoChishun Added.
     */
    public function _list($tag, $content) {
        $sqloptions = array('where', 'field', 'limit', 'order', 'table');
        $options = array();
        foreach ($sqloptions as $key) {
            if (!empty($tag[$key])) {
                $options[$key] = $tag[$key];
            }
        }
        // 标签不支持<>等html标签,只能用neq,gt,lt代替
        if (!empty($options['where'])) {
            $alias = array('neq' => '<>', 'gt' => '>', 'lt' => '<', 'egt' => '>=', 'elt' => '<=');
            foreach ($alias as $key => $value) {
                $options['where'] = str_replace(" $key ", " $value ", $options['where']);
            }
        }
        $name = '$' . (empty($tag['name']) ? 'ds_document' : $tag['name']);
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $key = !empty($tag['key']) ? $tag['key'] : 'key';
        $parseStr = '<?php ' . $name . '=D(\'t_porg_document\')->select(' . var_export($options, true) . '); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';

        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }

    /**
     * list标签解析 循环输出数据集
     * @access public
     * @param array $tag 标签属性
     * @param string $content  标签内容
     * @return string|void
     * 
     */
    public function _paginglist($tag, $content) {
        $sqloptions = array('where', 'field', 'limit', 'order', 'table');
        $options = array('TABLE'=>'t_porg_document');
        foreach ($sqloptions as $key) {
            if (!empty($tag[$key])) {
                $options[$key] = $tag[$key];
            }
        }
        // 标签不支持<>等html标签,只能用neq,gt,lt代替
        if (!empty($options['where'])) {
            $alias = array('neq' => '<>', 'gt' => '>', 'lt' => '<', 'egt' => '>=', 'elt' => '<=');
            foreach ($alias as $key => $value) {
                $options['where'] = str_replace(" $key ", " $value ", $options['where']);
            }
        }
        $pagination_name = empty($tag['pagination_name']) ? '' : $tag['pagination'];
        $name = '$' . (empty($tag['name']) ? 'ds_document' : $tag['name']);
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $key = !empty($tag['key']) ? $tag['key'] : 'key';
        $parseStr = '<?php ' . $name . '=M(\'Common\')->get_paging_list(' . var_export($options, true) . '); ?>';
        $parseStr .= '<?php ' . $pagination_name . '="123"; ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';

        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }

    /**
     * 获取字段值
     * @param type $tag
     * @param type $content
     * @return string
     * @since 1.0 <2015-11-13> SoChishun Added.
     * @example <DocumentContent:item pk="$Think.Get.id??3" field="title">{$vo.title}</DocumentContent:item>
     */
    function _item($tag, $content) {
        $pk = $tag['pk'];
        if (strpos($pk, '??')) {
            $apk = explode('??', $pk);
            $pk = $apk[0];
            if (false !== strpos($pk, '$Think')) {
                $pk = $this->parseThinkVar($pk);
            }
            $pk = '(isset(' . $pk . ') ? ' . $pk . ' : ' . $apk[1] . ')';
        } else {
            if (false !== strpos($pk, '$Think')) {
                $pk = $this->parseThinkVar($pk);
            }
        }
        $options = array('limit' => 1);
        if (!empty($tag['field'])) {
            $options['field'] = $tag['field'];
        }
        $options['cache'] = empty($tag['cache']) ? 5 : $tag['cache'];
        if ($pk) {
            $options['where'] = array('id' => '{#id}');
        }
        $soptions = '';
        if ($options) {
            $soptions = var_export($options, true);
            if ($pk) {
                $soptions = str_replace('\'{#id}\'', $pk, $soptions);
            }
        }
        $name = '$' . (empty($tag['name']) ? 'ds_item' : $tag['name']);
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $key = !empty($tag['key']) ? $tag['key'] : 'key';
        $extract = !empty($tag['extract']);
        $parseStr = '<?php ' . $name . ' = M(\'t_porg_document\')->select(' . $soptions . '); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';
        if ($extract) {
            $parseStr .= '<?php if(is_array(' . $name . ')): extract(' . $name . '[0]); endif; ?>';
        }

        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }

}
