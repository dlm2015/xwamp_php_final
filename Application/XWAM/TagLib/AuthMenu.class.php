<?php

namespace XWAM\TagLib;

/**
 * DodumentCategory标签类
 *
 * @since 1.0 <2015-10-27> SoChishun <14507247@qq.com> Added.
 */
class AuthMenu extends \Think\Template\TagLib {

    protected $tags = array(
        'main_menu' => array('attr' => 'attrs', 'level' => 3),
        'aside_menu' => array('attr' => 'attrs', 'level' => 3),
        'history_menu' => array('attr' => 'attrs', 'level' => 3),
    );

    /**
     * foreach标签解析 循环输出数据集
     * @access public
     * @param array $tag 标签属性
     * @param string $content  标签内容
     * @return string|void
     * @example <AuthMenu:aside_menu item="vo"></AuthMenu:aside>
     * @since 1.0 <2015-10-27> SoChishun Added.
     */
    public function _main_menu($tag, $content) {
        $name = '$' . (empty($tag['name']) ? 'ds_main_menu' : $tag['name']);
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $key = !empty($tag['key']) ? $tag['key'] : 'key';
        $parseStr = '<?php ' . $name . '=D(\'Rule\')->get_menus(); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';
        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }
    public function _aside_menu($tag, $content) {
        $name = '$' . (empty($tag['name']) ? 'ds_main_menu' : $tag['name']);
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $key = !empty($tag['key']) ? $tag['key'] : 'key';        
        $parseStr = '<?php ' . $name . '=D(\'Rule\')->get_aside_menus_by_addon(); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';
        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }
    public function _history_menu($tag, $content) {
        $name = '$' . (empty($tag['name']) ? 'ds_main_menu' : $tag['name']);
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $key = !empty($tag['key']) ? $tag['key'] : 'key';        
        $parseStr = '<?php ' . $name . '=D(\'Rule\')->get_history_menus(); ?>';
        $parseStr .= '<?php if(is_array(' . $name . ')): foreach(' . $name . ' as $' . $key . '=>$' . $item . '): ?>';
        $parseStr .= $this->tpl->parse($content);
        $parseStr .= '<?php endforeach; endif; ?>';
        if (!empty($parseStr)) {
            return $parseStr;
        }
        return;
    }

}
