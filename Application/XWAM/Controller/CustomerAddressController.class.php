<?php

namespace XWAM\Controller;

/**
 * 地址控制器
 *
 * @since 1.0 <2014-12-25> SoChishun <14507247@qq.com> Added.
 */
class CustomerAddressController extends AppbaseController {

    /**
     * 地址选择对话框
     * @since VER:1.0; DATE:2016-1-27; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
     */
    public function address_picker($customer_id = 0) {
        $asearch_info = array('search' => '', 'where' => array('customer_id' => $customer_id));
        $m_common = new \Common\Model\CommonModel();
        $list = $m_common->get_paging_list($show, array('table' => 't_porg_customer_addr', 'where' => $asearch_info['where'], 'order' => 'id desc'), array('page_params' => $asearch_info['search']));
        $this->assign('page', $show);
        $this->assign('list', $list);
        $this->assign('search', $asearch_info['search']);
        $this->display_cpp();
    }

    // 编辑地址 2014-12-12 by sutroon
    public function address_edit($id = 0, $customer_id = 0) {
        if (!$id && !$customer_id) {
            $this->dialogClose(false, array('error' => '参数有误!'));
        }
        $data = array();
        if ($id) {
            $m_address = new \XWAM\Model\CustomerAddressModel();
            $data = $m_address->find($id);
        }
        if (!$data) {
            $data['customer_id'] = $customer_id;
        }
        $this->assign('data', $data);
        $this->display();
    }

    // 保存地址 2014-12-12 by sutroon
    public function address_edit_save() {
        $m_address = new \XWAM\Model\CustomerAddressModel();
        $result = $m_address->save_address();
        $this->dialogJump($result['status'], $result['info']);
    }

    function change_status($id, $status) {
        $m_address = new \XWAM\Model\CustomerAddressModel();
        $this->ajaxReturn($m_address->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_address = new \XWAM\Model\CustomerAddressModel();
        $this->ajaxReturn($m_address->change_sort($id, $sort));
    }

    // 删除地址 2014-12-12 by sutroon
    function address_delete($id = '') {
        $m_address = new \XWAM\Model\CustomerAddressModel();
        $result = $m_address->delete_address($id);
        $this->dialogJump($result['status'], $result['info']);
    }

    /**
     * 拆分地区数据到数组
     * @param string $address 地区,如 福建省 厦门市 思明区 ?湖滨南路
     * @param string $splitor 如空格" "
     * @return array 返回数组array(province,city,area,street)
     * @since 1.0 2014-12-12 by sutroon
     */
    public static function split_cnregion($address, $splitor = ' ') {
        $arr = array('', '', '', '');
        if (!$address) {
            return $arr;
        }
        $address = str_replace('?', '', $address);
        $arrAddress = explode($splitor, $address);
        $arrAddress = array_filter($arrAddress);
        sort($arrAddress);
        if (count($arrAddress) > 2) {
            $arr[0] = $arrAddress[0];
            $arr[1] = $arrAddress[1];
            $arr[2] = $arrAddress[2];
            $arr[3] = isset($arrAddress[3]) ? $arrAddress[3] : '';
        } else {
            $arr[3] = $address;
        }
        return $arr;
    }

}
