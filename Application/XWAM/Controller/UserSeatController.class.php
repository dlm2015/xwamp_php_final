<?php

namespace XWAM\Controller;

/**
 * 坐席控制器
 *
 * @since 1.0 <2016-2-2> sutroon Added.
 */
class UserSeatController extends AppbaseController {

    function user_list_search_data() {
        $search = $_GET;
        if (!empty($search['search_key'])) {
            $searchkey = $search['search_key'];
            $where['_string'] = "user_name like '%$searchkey%' or name like '%$searchkey%' or mobile like '%$searchkey%'";
        }
        if (!empty($search['user_name'])) {
            $where['user_name'] = array('like', '%' . $search['user_name'] . '%');
        }
        $search['type_name'] = 'SEAT';
        $where['type_name'] = $search['type_name'];
        $where['site_id'] = $this->site_id;
        return array('search' => $search, 'where' => $where);
    }

    public function user_list() {
        $asearch = $this->user_list_search_data();
        $m_seat = new \XWAM\Model\UserSeatModel();
        $list = $m_seat->get_paging_list($show, array('where' => $asearch['where'], 'order' => 'user_name'), array('page_params' => $asearch['search']));
        $m_user = new \XWAM\Model\UserModel();
        $m_user->set_role2department_names($list);
        $this->assign('search', $asearch['search']);
        $this->assign('list', $list);
        $this->assign('page', $show);
        $this->display_cpp();
    }

    public function user_export() {
        $m_user = new \XWAM\Model\UserModel();
        $list = $m_user->select();
        if (!$list) {
            $this->error('找不到符合条件的记录!');
        }
        sofn_excel_export($list);
    }

    /**
     * 编辑个人资料
     * @since 1.0 <2015-11-11> SoChishun Added.
     */
    function profile_edit() {
        $m_seat = new \XWAM\Model\UserSeatModel();
        $data = $m_seat->find_seat($this->user_login_data['id']);
        $this->assign('data', $data);
        $this->display();
    }

    public function profile_edit_save() {
        $m_seat = new \XWAM\Model\UserSeatModel();
        $result = $m_seat->save_user($this->user_login_data['id']);
        $this->dialogClose($result['status'], array('success'=>'保存成功!<br /><span style="font-size:12px">如有变更[来电弹屏模式]内容，需刷新浏览器才会生效!</span>','error' => $result['info'],'success_action'=>'!window.parent.location.reload();','ajax'=>5));
    }

    public function user_edit($id = 0) {
        $m_seat = new \XWAM\Model\UserSeatModel();
        $data = $id ? $m_seat->find_seat($id) : array();
        $this->assign('data', $data);
        $this->display();
    }

    public function user_password_reset($id) {
        if (!$id) {
            $this->result(false, '参数有误!');
        }
        $m_seat = new \XWAM\Model\UserSeatModel();
        $result = $m_seat->where(array('id' => $id))->setField('password', '123456');
        $this->ajaxMsg($result, '密码重置{%}!');
    }

    public function user_edit_save() {
        $m_seat = new \XWAM\Model\UserSeatModel();
        $result = $m_seat->save_user($this->user_login_data['id']);
        $this->dialogJump($result['status'], $result['info']);
    }

    public function user_delete($id = '') {
        if (!$id) {
            $this->ajaxMsg(false, '编号丢失!');
        }
        $m_seat = new \XWAM\Model\UserSeatModel();
        $result = $m_seat->delete_user($id, $this->user_login_data['id']);
        $this->ajaxMsg($result, '删除{%}!');
    }

    function change_online_status($id, $status) {
        $m_user = new \XWAM\Model\UserModel();
        $this->ajaxReturn($m_user->change_online_status($id, $status));
    }

    // 2016-1-20
    function user_role_one($id) {
        $m_user = new \XWAM\Model\UserModel();
        $role_ids = $m_user->where(array('id' => $id))->getField('role_ids');
        $this->assign('role_ids', $role_ids);
        $this->assign('uid', $id);
        $this->display();
    }

    function user_role($id) {
        $m_user = new \XWAM\Model\UserModel();
        $role_ids = $m_user->where(array('id' => $id))->getField('role_ids');
        $m_role = new \XWAM\Model\RoleModel();
        $list = $m_role->select();
        $this->assign('list', $list);
        $this->assign('role_ids', $role_ids);
        $this->assign('uid', $id);
        $this->display();
    }

    /**
     * 保存管理员角色
     * @since 1.0 <2015-8-4> SoChishun Added.
     */
    public function user_role_save() {
        $ids = I('id');
        $uid = I('uid');
        if (!$ids || !$uid) {
            $this->dialogClose(false, array('error' => '参数有误!'));
        }
        $m_user = new \XWAM\Model\UserModel();
        $result = $m_user->save_role($uid, implode(',', $ids));
        $this->dialogJump($result['status'], $result['info']);
    }

    // 2016-1-20
    function user_department_one($id = '') {
        if (!$id) {
            $this->dialogClose(false, array('error' => '参数有误!'));
        }
        $aids = explode(',', $id);
        $msg = count($aids) > 1 ? '共' . count($aids) . '个坐席.' : '';
        $m_user = new \XWAM\Model\UserModel();
        $role_ids = $m_user->where(array('id' => $id))->getField('department_ids');
        $this->assign('department_ids', $role_ids);
        $this->assign('uid', $id);
        $this->assign('msg', $msg);
        $this->display();
    }

    function user_department($id) {
        $m_user = new \XWAM\Model\UserModel();
        $department_ids = $m_user->where(array('id' => $id))->getField('department_ids');
        $m_department = new \XWAM\Model\DepartmentModel();
        $this->assign('tree', $m_department->select_tree());
        $this->assign('department_ids', $department_ids);
        $this->assign('uid', $id);
        $this->display();
    }

    /**
     * 保存管理员角色
     * @since 1.0 <2015-8-4> SoChishun Added.
     */
    public function user_department_save() {
        $ids = I('id');
        $uid = I('uid');
        if (!$ids || !$uid) {
            $this->dialogClose(false, array('error' => '参数有误!'));
        }
        $m_user = new \XWAM\Model\UserModel();
        $result = $m_user->save_department($uid, is_array($ids) ? implode(',', $ids) : $ids);
        $this->dialogJump($result['status'], $result['info']);
    }

    /**
     * 从XCall同步分机到坐席
     * @since 1.0 <2016-2-15> SoChishun Added.
     */
    public function user_from_xcall() {
        $site_conf = session('site_conf');
        if (!$site_conf) {
            exit('登录超时');
        }
        $site_id = $site_conf['id'];
        $m_seat = new \XWAM\Model\UserSeatModel();
        $count = $m_seat->user_from_xcall($site_id);
        $this->success('同步成功,共同步 ' . $count . ' 个坐席!');
    }

}
