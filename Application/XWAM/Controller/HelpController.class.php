<?php

namespace XWAM\Controller;

/**
 * HelpController 类
 *
 * @since VER:1.0; DATE:2016-3-12; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class HelpController extends \Think\Controller {

    public function index() {
        $this->display();
    }

}
