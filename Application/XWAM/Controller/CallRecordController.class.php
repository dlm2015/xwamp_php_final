<?php

namespace XWAM\Controller;

/**
 * CallRecordController 类
 *
 * @since VER:1.0; DATE:2016-1-23; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class CallRecordController extends AppbaseController {

    // 2016-2-1 by sutroon
    function record_list() {
        $m_callrecord = new \XWAM\Model\CallRecordModel();
        $m_callrecord->synchronous_data(); // 同步数据
        //D('CallRecord')->add(array('exten' => 'admin', 'line' => '13950000003'));
        if ('SEAT' == $this->user_login_data['type_name']) {
            $where['exten'] = $this->user_login_data['user_name'];
        } else {
            $where['site_id'] = $this->site_id;
        }
        $asearch_info = array('search' => $_GET);
        $m_common = new \Common\Model\CommonModel();
        $list = $m_common->get_paging_list($show, array('table' => 't_porg_callrecord', 'where' => $where, 'order' => 'id desc'), array('page_params' => $asearch_info['search']));
        $this->assign('page', $show);
        $this->assign('list', $list);
        $this->assign('search', $asearch_info['search']);
        $this->display_cpp();
    }

    function change_status($id, $status, $type = '') {
        if ('status' != $type) {
            $type = 'is_' . $type;
        }
        $m_callrecord = new \XWAM\Model\CallRecordModel();
        $this->ajaxReturn($m_callrecord->change_status($id, $type, $status));
    }

}
