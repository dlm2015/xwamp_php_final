<?php

namespace XWAM\Controller;

/**
 * CustomerController 类
 *
 * @since 1.0 <2015-12-5> SoChishun <14507247@qq.com> Added.
 */
class CustomerOverdueController extends AppbaseController {

    /**
     * 获取客户列表搜索数据
     * @return array
     * @since 1.0 2014-12-30 by sutroon
     */
    function customer_list_get_search() {
        $search = $_GET;
        $is_seat = ('SEAT' == $this->user_login_data['type_name']);
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $has_extable = $m_attr->has_extable($this->site_id); // 是否有扩展表
        $base_prefix = $has_extable ? 'c.' : ''; // 基础表前缀
        // 固定条件        
        $where[$base_prefix . 'site_id'] = $this->site_id;
        // 过滤回收站记录
        $where[$base_prefix . 'status'] = 1;
        $where[$base_prefix . 'is_shared'] = 'N';
        $where['review_time'] = array('lt', date('Y-m-d', strtotime('-7 day')));
        // 坐席只能看到自己的
        if ($is_seat) {
            $where[$base_prefix . 'user_name'] = $this->user_login_data['user_name'];
        }
        // 简单查询
        if (!empty($search['search_key'])) {
            $searchkey = str_replace("'", '', $search['search_key']); // 工号/编号/姓名/手机号
            if (!$is_seat) {
                $where_or[$base_prefix . 'user_name'] = $searchkey;
            }
            $where_or[$base_prefix . 'serial_no'] = $searchkey;
            $where_or[$base_prefix . 'name'] = array('like', "%$searchkey%");
            $where_or[$base_prefix . 'telphone'] = array('like', "%$searchkey%");
            $where_or['_logic'] = 'or';
            $where['_complex'] = $where_or;
            return array('search' => $search, 'where' => $where);
        }
        return array('search' => $search, 'where' => $where);
    }

    // 客户列表
    public function customer_list() {
        $asearch = $this->customer_list_get_search();
        $m_overdue = new \XWAM\Model\CustomerOverdueModel();
        $list = $m_overdue->paging_select($show, $afields, $this->site_id, $asearch);
        $this->assign('page', $show);
        $this->assign('list', $list);
        $this->assign('afields', $afields['custom'] ? array_merge($afields['system'], $afields['custom']) : $afields['system']);
        $this->assign('search', $asearch['search']);
        $this->display_cpp();
    }

}
