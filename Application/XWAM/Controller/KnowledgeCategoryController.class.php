<?php

namespace XWAM\Controller;

/**
 * KnowledgeCategoryController 类
 *
 * @since 1.0 <2016-2-3> SoChishun <14507247@qq.com> Added.
 */
class KnowledgeCategoryController extends AppbaseController {

    function category_list_search() {
        $where['site_id'] = $this->site_id;
        $where['user_name'] = $this->user_login_data['user_name'];
        return array('search' => array(), 'where' => $where);
    }

    public function category_list() {
        $search_info = $this->category_list_search();
        $m_category = new \XWAM\Model\KnowledgeCategoryModel();
        $this->assign('tree', $m_category->select_tree(array('where' => $search_info['where'])));
        $this->display_cpp();
    }

    public function category_edit($id = 0, $pid = 0) {
        $data = array();
        if ($id) {
            $m_category = new \XWAM\Model\KnowledgeCategoryModel();
            $data = $m_category->find($id);
        }
        if (!$data) {
            $data = array('user_name' => $this->user_login_data['user_name'], 'site_id' => $this->user_login_data['site_id']);
        }
        if ($pid) {
            $data['pid'] = $pid;
        }
        $this->assign('data', $data);
        $this->display();
    }

    public function category_edit_save() {
        $m_category = new \XWAM\Model\KnowledgeCategoryModel();
        $result = $m_category->save_category($this->user_login_data['user_name']);
        $this->dialogJump($result['status'], $result['info']);
    }

    public function get_category_tree_json($pid = 0) {
        $options = array();
        if ($pid) {
            $options['where']['pid'] = $pid;
        }
        $m_category = new \XWAM\Model\KnowledgeCategoryModel();
        $data = $m_category->select_json_tree($options);
        $this->ajaxReturn($data);
    }

    function category_copy($id = 0) {
        if (!$id) {
            $this->ajaxMsg(false, '参数无效');
        }
        $m_category = new \XWAM\Model\KnowledgeCategoryModel();
        $m_category->copy_category($id);
        $this->ajaxMsg(true);
    }

    function change_status($id, $status) {
        $m_category = new \XWAM\Model\KnowledgeCategoryModel();
        $this->ajaxReturn($m_category->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_category = new \XWAM\Model\KnowledgeCategoryModel();
        $this->ajaxReturn($m_category->change_sort($id, $sort));
    }

    function category_delete($id = '') {
        $m_category = new \XWAM\Model\KnowledgeCategoryModel();
        $this->ajaxReturn($m_category->delete_channel($id));
    }

}
