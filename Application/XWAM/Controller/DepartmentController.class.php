<?php

namespace XWAM\Controller;

/**
 * DepartmentController 控制器
 * @since 1.0 <2016-1-14> SoChishun <14507247@qq.com> Added.
 */
class DepartmentController extends AppbaseController {

    /**
     * 导出数据
     * @since 1.0 <2014-6-13> SoChishun Added.
     */
    public function department_export() {
        $search_data = $this->get_department_search_data();
        $m_department = new \XWAM\Model\DepartmentModel();
        $list = $m_department->scope('export')->where($search_data['where'])->select();
        if (!$list) {
            $this->ajaxMsg(false, '找不到符合条件的记录!');
        }
        sofn_excel_export($list);
    }

    public function department_copy($id = 0) {
        if (!$id) {
            $this->ajaxMsg(false, '参数无效');
        }
        $m_department = new \XWAM\Model\DepartmentModel();
        $m_department->copy_department($id);
        $this->ajaxMsg(true);
    }

    public function department_list() {
        $m_department = new \XWAM\Model\DepartmentModel();
        $this->assign('tree', $m_department->select_tree());
        $this->display_cpp();
    }

    public function department_edit($id = 0, $pid = 0) {
        $data = array();
        if ($id) {
            $m_department = new \XWAM\Model\DepartmentModel();
            $data = $m_department->find($id);
        }
        if (!$data) {
            $data = array('pid' => $pid, 'site_id' => $this->site_id);
        }
        $this->assign('data', $data);
        $this->display();
    }

    public function department_edit_save() {
        $m_department = new \XWAM\Model\DepartmentModel();
        $result = $m_department->save_department();
        $this->dialogJump($result['status'], $result['info']);
    }

    public function get_department_tree_json($pid = 0) {
        $options = array();
        if ($pid) {
            $options['where']['pid'] = $pid;
        }
        $m_department = new \XWAM\Model\DepartmentModel();
        $data = $m_department->select_json_tree($options);
        $this->ajaxReturn($data);
    }

    function change_status($id, $status) {
        $m_department = new \XWAM\Model\DepartmentModel();
        $this->ajaxReturn($m_department->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_department = new \XWAM\Model\DepartmentModel();
        $this->ajaxReturn($m_department->change_sort($id, $sort));
    }

    /**
     * department_delete操作
     * @param string $id 主键编号
     * @since 1.0<2015-7-31> SoChishun Added.
     */
    public function department_delete($id = '') {
        $m_department = new \XWAM\Model\DepartmentModel();
        $this->ajaxReturn($m_department->delete_department($id));
    }

    public function department_permission_edit($id = 0) {
        $m_department = new \XWAM\Model\DepartmentModel();
        $rules = $m_department->where(array('id' => $id))->getField('rules');
        $m_rule = new \XWAM\Model\RuleModel();
        $tree = $m_rule->select_tree();
        $this->assign('tree', $tree);
        $this->assign('department_id', $id);
        $this->assign('ids', $rules);
        $this->display();
    }

    public function department_permission_edit_save() {
        $ids = I('id');
        $department_id = I('department_id');
        if (!$department_id || !$ids) {
            $this->dialogClose(false, array('error' => '参数有误!'));
        }
        $m_department = new \XWAM\Model\DepartmentModel();
        $result = $m_department->save_rule($department_id, $ids);
        $this->dialogJump($result['status'], $result['info']);
    }

}
