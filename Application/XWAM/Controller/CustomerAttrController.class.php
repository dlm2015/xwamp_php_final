<?php

namespace XWAM\Controller;

/**
 * 系统扩展字段控制器
 *
 * @author Alen
 * @since 1.0 <2014-11-11> by sutroon, 
 * @since 2.0 <2014-12-12> by sutroon
 */
class CustomerAttrController extends AppbaseController {

    public function attr_list() {
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $m_attr->generate_default_fields($this->site_id); // 自动初始化
        $list = $m_attr->get_paging_list($show, array('order' => 'sort, id', 'where' => array('site_id' => $this->site_id)));
        $this->assign('page', $show);
        $this->assign('list', $list);
        $this->display_cpp();
    }

    // 新增 2014-12-12 by sutroon
    public function attr_add() {
        $this->display();
    }

    // 编辑 2014-11-11 by sutroon
    public function attr_edit($id = 0) {
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $data = $m_attr->find($id);
        $this->assign('data', $data);
        $this->display();
    }

    // 保存新增的数据 2014-12-12 by sutroon
    public function attr_add_save() {
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $result = $m_attr->add_attr();
        if ($result['status']) {
            $this->success('保存成功', U('attr_list'));
        } else {
            $this->error($result['info']);
        }
    }

    // 保存编辑的数据 2014-11-11 by sutroon
    public function attr_edit_save() {
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $result = $m_attr->save_attr();
        $this->dialogJump($result['status'], $result['info']);
    }

    function attr_delete($id = '') {
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $this->ajaxReturn($m_attr->delete_attr($id));
    }

    // 检测字段是否存在 2014-11-11 by sutroon
    public function check_attr_name($name) {
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $result = $m_attr->check_attr_name($name, $this->site_id);
        $this->ajaxReturn($result);
    }

    // 检测字段是否存在 2014-11-11 by sutroon
    public function check_attr_title($title) {
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $result = $m_attr->check_attr_title($title, $this->site_id);
        $this->ajaxReturn($result);
    }

    function change_status($id, $status, $type = '') {
        if ('status' != $type) {
            $type = 'is_' . $type;
        }
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $this->ajaxReturn($m_attr->change_status($id, $type, $status, $this->site_id));
    }

    function change_sort($id, $sort) {
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $this->ajaxReturn($m_attr->change_sort($id, $sort, $this->site_id));
    }

}
