<?php

namespace XWAM\Controller;

/**
 * RuleController 控制器
 * @since 1.0 <2016-3-9> SoChishun <14507247@qq.com> Added.
 */
class RuleController extends AppbaseController {

    /**
     * 导出数据
     * @since 1.0 <2014-6-13> SoChishun Added.
     */
    public function rule_export() {
        $search_data = $this->get_rule_search_data();
        $m_rule = new \XWAM\Model\RuleModel();
        $list = $m_rule->scope('export')->where($search_data['where'])->select();
        if (!$list) {
            $this->ajaxMsg(false, '找不到符合条件的记录!');
        }
        sofn_excel_export($list);
    }

    public function rule_copy($id = 0) {
        if (!$id) {
            $this->ajaxMsg(false, '参数无效');
        }
        $m_rule = new \XWAM\Model\RuleModel();
        $m_rule->copy_rule($id);
        $this->ajaxMsg(true);
    }

    public function rule_list() {
        $m_rule = new \XWAM\Model\RuleModel();
        $this->assign('tree', $m_rule->select_tree());
        $this->display_cpp();
    }

    public function rule_edit($id = 0, $pid = 0) {
        $data = array();
        if ($id) {
            $m_rule = new \XWAM\Model\RuleModel();
            $data = $m_rule->find($id);
        }
        if ($pid) {
            $data['pid'] = $pid;
        }
        $this->assign('data', $data);
        $this->display();
    }

    public function rule_edit_save() {
        $m_rule = new \XWAM\Model\RuleModel();
        $result = $m_rule->save_rule();
        $this->dialogJump($result['status'], $result['info']);
    }

    public function get_rule_tree_json($pid = 0) {
        $options = array();
        if ($pid) {
            $options['where']['pid'] = $pid;
        }
        $m_rule = new \XWAM\Model\RuleModel();
        $data = $m_rule->select_json_tree($options);
        $this->ajaxReturn($data);
    }

    function change_status($id, $status) {
        $m_rule = new \XWAM\Model\RuleModel();
        $this->ajaxReturn($m_rule->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_rule = new \XWAM\Model\RuleModel();
        $this->ajaxReturn($m_rule->change_sort($id, $sort));
    }

    /**
     * rule_delete操作
     * @param string $id 主键编号
     * @since 1.0<2015-7-31> SoChishun Added.
     */
    public function rule_delete($id = '') {
        $m_rule = new \XWAM\Model\RuleModel();
        $this->ajaxReturn($m_rule->delete_rule($id));
    }

}
