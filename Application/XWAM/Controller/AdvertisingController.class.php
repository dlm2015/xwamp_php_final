<?php

namespace XWAM\Controller;

/**
 * AdvertisingController类
 *
 * @since 1.0 <2015-10-23> SoChishun <14507247@qq.com> Added.
 */
class AdvertisingController extends AppbaseController {

    function advert_list_search() {
        $where['site_id'] = $this->site_id;
        return array('where' => $where);
    }

    public function advert_list() {
        $search_info = $this->advert_list_search();
        $m_advertising = new \XWAM\Model\AdvertisingModel();
        $list = $m_advertising->where($where)->select();
        $this->assign('list', $list);
        $this->display_cpp();
    }

    function advert_copy($id = 0) {
        if (!$id) {
            $this->ajaxMsg(false, '参数无效');
        }
        $m_advertising = new \XWAM\Model\AdvertisingModel();
        $m_advertising->copy_advert($id);
        $this->ajaxMsg(true);
    }

    function advert_edit($id = 0) {
        $data = array();
        if ($id) {
            $m_advertising = new \XWAM\Model\AdvertisingModel();
            $data = $m_advertising->find($id);
        }
        if (!$data) {
            $data = array('user_name' => $this->user_login_data['user_name'], 'site_id' => $this->user_login_data['site_id']);
        }
        $this->assign('data', $data);
        $this->display();
    }

    function advert_edit_save() {
        $m_advertising = new \XWAM\Model\AdvertisingModel();
        $result = $m_advertising->save_advert($this->user_login_data['user_name']);
        $this->dialogJump($result['status'], $result['info']);
    }

    function advert_delete($id = '') {
        $m_advertising = new \XWAM\Model\AdvertisingModel();
        $result = $m_advertising->delete_advert($id);
        $this->ajaxReturn($result);
    }

}
