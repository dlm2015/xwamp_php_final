<?php

namespace XWAM\Controller;

/**
 * 用户控制器
 * <br />中转站
 *
 * @since 1.0 <2014-6-13> sutroon Added.
 */
class UserController extends AppbaseController {

    public function user_list($type_name = 'ADMIN') {
        $urls = array(
            'ADMIN' => U('UserAdmin/user_list'),
            'SERVICES' => U('UserServices/user_list'),
            'CUSTOMER' => U('UserCustomer/user_list'),
        );
        if (array_key_exists($type_name, $urls)) {
            redirect($urls[$type_name]);
        } else {
            exit('不支持的用户类型[' . $type_name . ']!');
        }
    }

    function profile_edit() {
        $type_name = $this->user_login_data['type_name'];
        $urls = array(
            'SYSTEMADMIN' => U('UserAdmin/profile_edit'),
            'ADMIN' => U('UserAdmin/profile_edit'),
            'SERVICES' => U('UserServices/profile_edit'),
            'CUSTOMER' => U('UserCustomer/profile_edit'),
            'SEAT' => U('UserSeat/profile_edit'),
        );
        if (array_key_exists($type_name, $urls)) {
            redirect($urls[$type_name]);
        } else {
            exit('不支持的用户类型[' . $type_name . ']!');
        }
    }

    function setting() {
        $id = $this->user_login_data['id'];
        if (!$id) {
            exit('未登录!');
        }
        $m_user = new \XWAM\Model\UserModel();
        $data = $m_user->field('id, page_size, lang, theme')->find($id);
        $this->assign('data', $data);
        $this->display();
    }

    public function setting_save() {
        $data = I('post.');
        $data['id'] = $this->user_login_data['id'];
        $m_user = new \XWAM\Model\UserModel();
        $result = $m_user->save($data);
        $this->dialogClose($result, array('error' => $m_user->getError()));
    }

    function change_online_status($id, $status) {
        $m_user = new \XWAM\Model\UserModel();
        $this->ajaxReturn($m_user->change_online_status($id, $status));
    }

    public function user_role($id) {
        $m_user = new \XWAM\Model\UserModel();
        $this->assign('data', $m_user->find($id));
        $this->display();
    }

}
