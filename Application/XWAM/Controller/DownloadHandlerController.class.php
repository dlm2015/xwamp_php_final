<?php

namespace XWAM\Controller;

/**
 * 文件下载控制器
 *
 * @since 1.0 2014-12-31 by sutroon
 */
class DownloadHandlerController extends \Think\Controller {

    /**
     * 下载文件
     * @since 1.0 2014-12-31 by sutroon
     * @example U('DownloadHandler/download','file=customer_import_file')
     */
    public function download($file = '') {
        if (!$file) {
            die('文件名有误!');
        }
        $path = '';
        switch ($file) {
            case 'customer_import_file':
                $filename = $file . '.xlsx';
                $path = './Assets/ImportTemplate/' . $filename;
                break;
        }
        if (!$path) {
            die('名称无效!');
        }
        $content = file_get_contents($path);
        sofn_save_file_dialog($filename, $content);
    }

}
