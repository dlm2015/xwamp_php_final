<?php

namespace XWAM\Controller;

/**
 * Description of IndexController
 *
 * @since 1.0 <2015-10-21> SoChishun <14507247@qq.com> Added.
 */
class HomeController extends AppbaseController {

    public function index() {
        $this->display();
    }

}
