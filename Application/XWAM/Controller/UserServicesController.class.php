<?php

namespace XWAM\Controller;

/**
 * 用户控制器（包含管理员和座席）
 *
 * @since 1.0 <2014-6-13> sutroon Added.
 */
class UserServicesController extends AppbaseController {

    function user_list_search_data() {
        $search = $_GET;
        if (!empty($search['search_key'])) {
            $searchkey = $search['search_key'];
            $where['_string'] = "user_name like '%$searchkey%' or name like '%$searchkey%' or mobile like '%$searchkey%'";
        }
        if (!empty($search['user_name'])) {
            $where['user_name'] = array('like', '%' . $search['user_name'] . '%');
        }
        $search['type_name'] = 'SERVICES';
        $where['type_name'] = $search['type_name'];
        $where['site_id']=$this->site_id;
        return array('search' => $search, 'where' => $where);
    }

    public function user_list() {
        $search_data = $this->user_list_search_data();
        if ('SYSTEMADMIN' == $this->user_login_data['type_name']) {
            $usertype_list = array('SYSTEMADMIN' => '系统管理员', 'SITEADMIN' => '网站创始人', 'ADMIN' => '管理员');
        } else {
            $usertype_list = array('ADMIN' => '管理员', 'SERVICES' => '客服人员', 'CUSTOMER' => 'VIP客户');
        }
        $this->assign('usertype_list', $usertype_list);
        $list = D('UserServices')->get_paging_list($show, array('where' => $search_data['where'], 'order' => 'sort, id desc'), array('page_params' => $search_data['search']));
        $this->assign('search', $search_data['search']);
        $this->assign('list', $list);
        $this->assign('page', $show);
        $this->display_cpp();
    }

    public function user_export() {
        $list = D('UserServices')->select();
        if (!$list) {
            $this->error('找不到符合条件的记录!');
        }
        sofn_excel_export($list);
    }

    /**
     * 编辑个人资料
     * @since 1.0 <2015-11-11> SoChishun Added.
     */
    function profile_edit() {
        $this->user_edit($this->user_login_data['id']);
    }

    public function user_edit($id = 0) {
        $data = $id ? D('UserServices')->find($id) : array();
        $this->assign('data', $data);
        $this->display('user_edit');
    }

    public function user_password_reset($id) {
        if (!$id) {
            $this->result(false, '参数有误!');
        }
        $result = D('UserServices')->where(array('id' => $id))->setField('password', '123456');
        $this->ajaxMsg($result, '密码重置{%}!');
    }

    public function user_save() {
        $result = D('UserServices')->save_user($this->user_login_data['id']);
        $this->dialogJump($result['status'], $result['info']);
    }

    public function user_delete($id = '') {
        if (!$id) {
            $this->ajaxMsg(false, '编号丢失!');
        }
        $result = D('UserServices')->delete_user($id, $this->user_login_data['id']);
        $this->ajaxMsg($result, '删除{%}!');
    }

    function change_online_status($id, $status) {
        $this->ajaxReturn(D('User')->change_online_status($id, $status));
    }

}
