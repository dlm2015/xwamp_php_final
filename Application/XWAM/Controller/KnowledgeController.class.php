<?php

namespace XWAM\Controller;

/**
 * KnowledgeController类
 *
 * @since 1.0 <2016-2-3> SoChishun <14507247@qq.com> Added.
 */
class KnowledgeController extends AppbaseController {

    function document_list_search_data($is_shared = false) {
        $search = $_GET;
        $where = array();
        $alias = 'k.';
        if (!empty($search['search_key'])) {
            $where[$alias . 'title|' . $alias . 'content_tags'] = array('like', '%' . $search['search_key'] . '%');
        }
        if (!empty($search['category_id'])) {
            $where[$alias . 'category_id'] = $search['category_id'];
        }
        if (!empty($search['title'])) {
            $where[$alias . 'title'] = array('like', '%' . $search['title'] . '%');
        }
        if ($is_shared) {
            $where[$alias . 'is_shared'] = 1;
        } else {
            $where[$alias . 'user_name'] = $this->user_login_data['user_name'];
        }
        $where[$alias . 'site_id'] = $this->site_id;
        return array('search' => $search, 'where' => $where);
    }

    function document_shared_list() {
        $search_data = $this->document_list_search_data(true);
        $m_common = new \Common\Model\CommonModel();
        $list = $m_common->get_paging_list($show, array('table' => 't_porg_knowledge k left join t_porg_knowledge_category c on k.category_id=c.id', 'field' => 'k.*, c.title as category_title', 'where' => $search_data['where'], 'order' => 'sort, id desc'), array('page_params' => $search_data['search']));
        $this->assign('list', $list);
        $this->assign('page', $show);
        $this->assign('search', $search_data['search']);
        $this->display_cpp();
    }

    function document_list() {
        $search_data = $this->document_list_search_data();
        $m_common = new \Common\Model\CommonModel();
        $list = $m_common->get_paging_list($show, array('table' => 't_porg_knowledge k left join t_porg_knowledge_category c on k.category_id=c.id', 'field' => 'k.*, c.title as category_title', 'where' => $search_data['where'], 'order' => 'sort, id desc'), array('page_params' => $search_data['search']));
        $this->assign('list', $list);
        $this->assign('page', $show);
        $this->assign('search', $search_data['search']);
        $this->display_cpp();
    }

    function get_notices_list_json($limit = 10) {
        $m_knowledeg = new \XWAM\Model\KnowledgeModel();
        $list = $m_knowledeg->where(array('status' => 1))->limit($limit)->field('id, title')->order('sort, id desc')->select();
        $this->ajaxReturn($list);
    }

    function document_edit($id = 0, $category_id = 0) {
        $data = array();
        if ($id) {
            $m_knowledeg = new \XWAM\Model\KnowledgeModel();
            $data = $m_knowledeg->find($id);
        }
        if (!$data) {
            $data = array('user_name' => $this->user_login_data['user_name'], 'site_id' => $this->user_login_data['site_id']);
        }
        if ($category_id) {
            $data['category_id'] = $category_id;
        }
        $this->assign('meta_title', $id ? '编辑 ' . $data['title'] : '新增知识库');
        $this->assign('data', $data);
        $this->display();
    }

    function document_edit_save() {
        $m_knowledeg = new \XWAM\Model\KnowledgeModel();
        $result = $m_knowledeg->save_document($this->user_login_data['user_name']);
        if ($result['status']) {
            $this->success('保存成功', U('document_list'));
        } else {
            $this->error($result['info']);
        }
    }

    function document_view($id = 0) {
        $data = array();
        if ($id) {
            $m_knowledeg = new \XWAM\Model\KnowledgeModel();
            $data = $m_knowledeg->find($id);
        }
        $this->assign('meta_title', $data['title']);
        $this->assign('data', $data);
        $this->display();
    }

    function change_is_shared($id, $status) {
        $m_knowledeg = new \XWAM\Model\KnowledgeModel();
        $this->ajaxReturn($m_knowledeg->change_is_shared($id, $status));
    }

    function change_sort($id, $sort) {
        $m_knowledeg = new \XWAM\Model\KnowledgeModel();
        $this->ajaxReturn($m_knowledeg->change_sort($id, $sort));
    }

    function document_delete($id = '') {
        $m_knowledeg = new \XWAM\Model\KnowledgeModel();
        $result = $m_knowledeg->delete_document($id);
        $this->dialogJump($result['status'], $result['info']);
    }

}
