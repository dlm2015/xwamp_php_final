<?php

namespace XWAM\Controller;

/**
 * DocumentCategoryController类
 *
 * @since 1.0 <2015-10-22> SoChishun <14507247@qq.com> Added.
 */
class ChannelsController extends AppbaseController {

    public function channel_list() {
        $where['site_id'] = $this->site_id;
        $this->assign('type_list', array('DOCUMENT' => array('title' => '文章', 'url' => U('Documents/document_list')), 'FILE' => array('title' => '文件', 'url' => U('File/file_list'))));
        $m_category = new \XWAM\Model\DocumentCategoryModel();
        $this->assign('tree', $m_category->select_tree(array('where' => $where)));
        $this->display_cpp();
    }

    public function channel_edit($id = 0, $pid = 0) {
        $data = array();
        if ($id) {
            $m_category = new \XWAM\Model\DocumentCategoryModel();
            $data = $m_category->find($id);
        }
        if (!$data) {
            $data = array('user_name' => $this->user_login_data['user_name'], $this->user_login_data['site_id']);
        }
        if ($pid) {
            $data['pid'] = $pid;
        }
        $this->assign('data', $data);
        $this->display();
    }

    public function channel_edit_save() {
        $m_category = new \XWAM\Model\DocumentCategoryModel();
        $result = $m_category->save_category($this->user_login_data['user_name']);
        $this->dialogJump($result['status'], $result['info']);
    }

    public function get_channel_tree_json($type_id = 0, $pid = 0) {
        $options['where']['site_id'] = $this->user_login_data['site_id'];
        if ($type_id) {
            $options['where']['type_id'] = $type_id;
        }
        if ($pid) {
            $options['where']['pid'] = $pid;
        }
        $m_category = new \XWAM\Model\DocumentCategoryModel();
        $data = $m_category->select_json_tree($options);
        $this->ajaxReturn($data);
    }

    function channel_copy($id = 0) {
        if (!$id) {
            $this->ajaxMsg(false, '参数无效');
        }
        $m_category = new \XWAM\Model\DocumentCategoryModel();
        $m_category->copy_category($id);
        $this->ajaxMsg(true);
    }

    function change_status($id, $status) {
        $m_category = new \XWAM\Model\DocumentCategoryModel();
        $this->ajaxReturn($m_category->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_category = new \XWAM\Model\DocumentCategoryModel();
        $this->ajaxReturn($m_category->change_sort($id, $sort));
    }

    function channel_delete($id = '') {
        $m_category = new \XWAM\Model\DocumentCategoryModel();
        $this->ajaxReturn($m_category->delete_channel($id));
    }

}
