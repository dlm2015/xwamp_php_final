<?php

namespace XWAM\Controller;

/**
 * Description of LoginController
 *
 * @since 1.0 <2015-10-20> SoChishun <14507247@qq.com> Added.
 */
class LoginController extends \Think\Controller {

    public function index() {
        $this->display();
    }

    public function login($login_name = '', $password = '', $captcha = '') {
        if (!isset($_POST['captcha'])) {
            $captcha = null;
        }
        if (!$login_name || !$password || (!is_null($captcha) && !$captcha)) {
            $this->error('表单填写不完整!');
        }
        $m_login = new \XWAM\Model\LoginModel();
        $result = $m_login->login($login_name, $password, $captcha, $msg);
        if ($result) {
            session('login', $msg);
            // read site_info
            $data = D('SiteConf')->find_site($msg['site_id'], $msg['user_name'], false);
            if (!$data) {
                exit('未开通CRM功能!');
            }
            session('site_conf', $data);
            // redirect
            redirect(U('Index/index'));
        } else {
            $this->error($msg);
        }
    }

    public function logout() {
        $m_login = new \XWAM\Model\LoginModel();
        $m_login->logout();
        header('location:' . U('Login/index'));
    }

}
