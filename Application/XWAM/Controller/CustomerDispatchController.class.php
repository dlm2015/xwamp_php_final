<?php

namespace XWAM\Controller;

/**
 * CustomerController 类
 *
 * @since 1.0 <2015-12-5> SoChishun <14507247@qq.com> Added.
 */
class CustomerDispatchController extends AppbaseController {

    /**
     * 获取客户列表搜索数据
     * @return array
     * @since 1.0 2014-12-30 by sutroon
     */
    function customer_list_get_search() {
        $search = $_GET;
        $is_seat = ('SEAT' == $this->user_login_data['type_name']);
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $has_extable = $m_attr->has_extable($this->site_id); // 是否有扩展表
        $base_prefix = $has_extable ? 'c.' : ''; // 基础表前缀
        // 固定条件        
        $where[$base_prefix . 'site_id'] = $this->site_id;
        $where[$base_prefix . 'old_user_name'] = array('neq', '');
        // 过滤回收站记录
        $where[$base_prefix . 'status'] = 1;
        $where[$base_prefix . 'is_shared'] = 'N';
        // 坐席只能看到自己的
        if ($is_seat) {
            $where[$base_prefix . 'user_name'] = $this->user_login_data['user_name'];
        }
        // 简单查询
        if (!empty($search['search_key'])) {
            $searchkey = str_replace("'", '', $search['search_key']); // 工号/编号/姓名/手机号
            if (!$is_seat) {
                $where_or[$base_prefix . 'user_name'] = $searchkey;
            }
            $where_or[$base_prefix . 'serial_no'] = $searchkey;
            $where_or[$base_prefix . 'name'] = array('like', "%$searchkey%");
            $where_or[$base_prefix . 'telphone'] = array('like', "%$searchkey%");
            $where_or['_logic'] = 'or';
            $where['_complex'] = $where_or;
            return array('search' => $search, 'where' => $where);
        }
        return array('search' => $search, 'where' => $where);
    }

    // 客户列表
    public function customer_list() {
        $asearch = $this->customer_list_get_search();
        $m_dispatch = new \XWAM\Model\CustomerDispatchModel();
        $list = $m_dispatch->paging_select($show, $afields, $this->site_id, $asearch);
        $this->assign('page', $show);
        $this->assign('list', $list);
        $this->assign('afields', $afields['custom'] ? array_merge($afields['system'], $afields['custom']) : $afields['system']);
        $this->assign('search', $asearch['search']);
        $this->display_cpp();
    }

    // 客户调度记录 2014-12-25 by sutroon
    public function dispatch_log_list($customer_id) {
        if (!$customer_id) {
            $this->dialogClose(false, array('error' => '参数有误!'));
        }
        $m_view = new \Think\Model('v_porg_customer_appointment');
        $list = $m_view->where("customer_id=$customer_id")->select();
        $this->assign('list', $list);
        $this->display_cpp();
    }

    // 获取客户调度查询表单数据 2014-12-25 by sutroon
    function dispatch_edit_get_search() {
        $search = I('post.');
        if ($search['lastBuyTimeExpBegin'] && $search['lastBuyTimeBegin']) {
            $where['lastBuyTime'] = array($search['lastBuyTimeExpBegin'], $search['lastBuyTimeBegin']);
        }
        if ($search['lastBuyTimeExpEnd'] && $search['lastBuyTimeEnd']) {
            if (isset($where['lastBuyTime'])) {
                $where['lastBuyTime'] = array($where['lastBuyTime'], array($search['lastBuyTimeExpEnd'], $search['lastBuyTimeEnd']));
            } else {
                $where['lastBuyTime'] = array($search['lastBuyTimeExpEnd'], $search['lastBuyTimeEnd']);
            }
        }

        if ($search['lastVisitTimeExpBegin'] && $search['lastVisitTimeBegin']) {
            $where['lastVisitTime'] = array($search['lastVisitTimeExpBegin'], $search['lastVisitTimeBegin']);
        }
        if ($search['lastVisitTimeExpEnd'] && $search['lastVisitTimeEnd']) {
            if (isset($where['lastVisitTime'])) {
                $where['lastVisitTime'] = array($where['lastVisitTime'], array($search['lastVisitTimeExpEnd'], $search['lastVisitTimeEnd']));
            } else {
                $where['lastVisitTime'] = array($search['lastVisitTimeExpEnd'], $search['lastVisitTimeEnd']);
            }
        }
        if ($search['buyCount'] && $search['buyCountExp']) {
            $where['buyCount'] = array($search['buyCountExp'], $search['buyCount']);
        }
        if (strlen($search['buyCountState']) > 0) {
            switch ($search['buyCountState']) {
                case '1':
                    $where['buyCount'] = array('GT', 0);
                    break;
                case '0':
                    $where['buyCount'] = array('EQ', 0);
                    break;
                case '2':
                    if ($where['buyCount']) {
                        unset($where['buyCount']);
                    }
                    break;
            }
        }
        if ($search['cumulativeAmount'] && $search['cumulativeAmountExp']) {
            $where['cumulativeAmount'] = array($search['cumulativeAmountExp'], $search['cumulativeAmount']);
        }
        if ($search['oldUsers']) {
            $search['oldUsers'] = str_replace("'", '', $search['oldUsers']);
            $search['oldUsers'] = str_replace(' ', '', $search['oldUsers']);
            $search['oldUsers'] = str_replace(",", "','", $search['oldUsers']);
            $uids = M('tuser')->where("userType='SEAT' and userName in ('" . $search['oldUsers'] . "')")->getField('id', true);
            if ($uids) {
                $where['userID'] = array('IN', $uids);
            }
        }
        return array('search' => $search, 'where' => $where);
    }

    // 客户调度 2014-12-25 by sutroon
    public function dispatch_full_edit($customer_id = 0) {
        if ($customer_id) {
            $search = false;
            $where['id'] = $customer_id;
        } else {
            $arr = $this->dispatch_edit_get_search();
            $search = $arr['search'];
            $where = $arr['where'];
        }
        $count = 0;
        $hasOrderCount = 0;
        if ($where) {
            $m_customer = new \XWAM\Model\CustomerModel();
            $list = $m_customer->where($where)->field('id, name, buy_count, user_name')->select();
            if ($list) {
                $count = count($list);
                foreach ($list as $row) {
                    if ($row['buyCount'] > 0) {
                        $hasOrderCount++;
                    }
                }
            }
        }
        $this->assign('count', $count);
        $this->assign('hasOrderCount', $hasOrderCount);
        $this->assign('list', $list);
        $this->assign('search', $search);
        $this->display();
    }

    // 客户调度 2016-2-2 by sutroon
    public function dispatch_edit($customer_id = '') {
        if (!$customer_id) {
            $this->dialogClose(false, array('error' => '参数无效!'));
        }
        $count = 0;
        $has_order_count = 0;
        $where = array('id' => array('in', $customer_id));
        $m_customer = new \XWAM\Model\CustomerModel();
        $list = $m_customer->where($where)->field('id, serial_no, name, buy_count, user_name')->select();
        if ($list) {
            $count = count($list);
            foreach ($list as $row) {
                if ($row['buy_count'] > 0) {
                    $has_order_count++;
                }
            }
        }
        $this->assign('sum', array('count' => $count, 'has_order_count' => $has_order_count));
        $this->assign('list', $list);
        $this->display();
    }

    // 客户调度数据保存 2014-12-25 by sutroon
    public function dispatch_edit_save() {
        $m_dispatch = new \XWAM\Model\CustomerDispatchModel();
        $result = $m_dispatch->save_dispatch();
        $this->dialogJump($result['status'], $result['info']);
    }
    
    public function dispatch_simple($customer_id=''){
        if(!$customer_id){
            $this->ajaxMsg(false, '客户无效!');
        }
        $m_dispatch = new \XWAM\Model\CustomerDispatchModel();
        $result = $m_dispatch->save_simple($customer_id,$this->user_login_data['user_name'],$this->user_login_data['user_name']);
        $this->ajaxMsg($result['status']);
    }

}
