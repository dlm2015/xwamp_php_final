<?php

namespace XWAM\Controller;

/**
 * FileController类
 *
 * @since 1.0 <2015-10-24> SoChishun <14507247@qq.com> Added.
 */
class FileController extends AppbaseController {

    function file_list_search_data() {
        $search = $_GET;
        $where = array();
        if (!empty($search['search_key'])) {
            $where['title'] = array('like', '%' . $search['search_key'] . '%');
        }
        if (!empty($search['category_id'])) {
            $where['category_id'] = $search['category_id'];
        }
        if (!empty($search['title'])) {
            $where['title'] = array('like', '%' . $search['title'] . '%');
        }
        $where['site_id']=$this->site_id;
        return array('search' => $search, 'where' => $where);
    }

    function file_list() {
        $search_data = $this->file_list_search_data();
        $m_file = new \XWAM\Model\FileModel();
        $list = $m_file->get_paging_list($show, array('where' => $search_data['where'], 'order' => 'sort, id desc'), array('page_params' => $search_data['search']));
        $list2 = false;
        $m_catetory = new \XWAM\Model\DocumentCategoryModel();
        if ($list) {
            $channels = $m_catetory->where(array('type_id' => 5))->getField('id, title');
            foreach ($list as $row) {
                $row['category_name'] = isset($channels[$row['category_id']]) ? $channels[$row['category_id']] : '';
                $list2[] = $row;
            }
        }
        $category_id = I('category_id');
        $this->assign('meta_title', $category_id ? $m_catetory->where(array('id' => $category_id))->getField('title') : '文件管理');
        $this->assign('list', $list2);
        $this->assign('page', $show);
        $this->assign('search', $search_data['search']);
        $this->display_cpp();
    }

    function file_edit($id = 0, $category_id = 0) {
        $data = array();
        if ($id) {
            $m_file = new \XWAM\Model\FileModel();
            $data = $m_file->find($id);
        }
        if(!$data){
            $data=array('user_name'=>$this->user_login_data['user_name'],'site_id'=>$this->user_login_data['site_id']);
        }
        if ($category_id) {
            $data['category_id'] = $category_id;
        }
        $this->assign('data', $data);
        $this->display();
    }

    function file_edit_save() {
        $m_file = new \XWAM\Model\FileModel();
        $result = $m_file->save_file($this->user_login_data['user_name']);
        $this->dialogJump($result['status'], $result['info']);
    }

    function change_sort($id, $sort) {
        $m_file = new \XWAM\Model\FileModel();
        $this->ajaxReturn($m_file->change_sort($id, $sort));
    }

    function file_delete($id = '') {
        $m_file = new \XWAM\Model\FileModel();
        $this->ajaxReturn($m_file->delete_file($id));
    }

}
