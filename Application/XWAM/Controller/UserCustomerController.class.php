<?php

namespace XWAM\Controller;

/**
 * 用户控制器（包含管理员和座席）
 *
 * @since 1.0 <2014-6-13> sutroon Added.
 */
class UserCustomerController extends AppbaseController {

    function user_list_search_data() {
        $search = $_GET;
        if (!empty($search['search_key'])) {
            $searchkey = $search['search_key'];
            $where['_string'] = "user_name like '%$searchkey%' or name like '%$searchkey%' or mobile like '%$searchkey%'";
        }
        if (!empty($search['user_name'])) {
            $where['user_name'] = array('like', '%' . $search['user_name'] . '%');
        }
        $search['type_name'] = 'CUSTOMER';
        $where['type_name'] = $search['type_name'];
        $where['site_id']=$this->site_id;
        return array('search' => $search, 'where' => $where);
    }

    public function user_list() {
        $search_data = $this->user_list_search_data();
        //$this->assign('usertype_list', D('UserType')->order('sort, id desc')->select());
        $this->assign('usertype_list', array('ADMIN' => '管理员', 'SERVICES' => '客服人员', 'CUSTOMER' => 'VIP客户'));
        $list = D('UserCustomer')->get_paging_list($show, array('where' => $search_data['where'], 'order' => 'id'), array('page_params' => $search_data['search']));
        $this->assign('search', $search_data['search']);
        $this->assign('list', $list);
        $this->assign('page', $show);
        $this->display_cpp();
    }

    public function user_export() {
        $list = D('UserCustomer')->select();
        if (!$list) {
            $this->error('找不到符合条件的记录!');
        }
        sofn_excel_export($list);
    }

    /**
     * 编辑个人资料
     * @since 1.0 <2015-11-11> SoChishun Added.
     */
    function profile_edit() {
        $this->user_edit($this->user_login_data['id']);
    }

    public function user_edit($id = 0) {
        $data = $id ? D('UserCustomer')->find($id) : array();
        $this->assign('data', $data);
        $this->display('user_edit');
    }

    public function user_password_reset($id) {
        if (!$id) {
            $this->result(false, '参数有误!');
        }
        $result = D('UserCustomer')->where(array('id' => $id))->setField('password', '123456');
        $this->ajaxMsg($result, '密码重置{%}!');
    }

    public function user_save() {
        $result = D('UserCustomer')->save_user($this->user_login_data['id']);
        $this->dialogJump($result['status'], $result['info']);
    }

    public function user_delete($id = '') {
        if (!$id) {
            $this->ajaxMsg(false, '编号丢失!');
        }
        $result = D('UserCustomer')->delete_user($id, $this->user_login_data['id']);
        $this->ajaxMsg($result, '删除{%}!');
    }

}
