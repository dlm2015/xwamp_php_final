<?php

namespace XWAM\Controller;

/**
 * AdvertisingContentController类
 *
 * @since 1.0 <2015-11-7> SoChishun <14507247@qq.com> Added.
 */
class AdvertisingContentController extends AppbaseController {

    public function content_list($advert_id = 0) {
        $m_content = new \XWAM\Model\AdvertisingContentModel();
        $list = $m_content->where(array('advertising_id' => $advert_id))->select();
        $this->assign('list', $list);
        $this->assign('advert_id', $advert_id);
        $m_advertising = new \XWAM\Model\AdvertisingModel();
        $this->assign('meta_title', $m_advertising->where(array('id' => $advert_id))->getField('title'));
        $this->display_cpp();
    }

    function content_edit($id = 0, $advert_id = 0) {
        $data = array();
        if ($id) {
            $m_content = new \XWAM\Model\AdvertisingContentModel();
            $data = $m_content->find($id);
        } else {
            $data['advertising_id'] = $advert_id;
        }
        $this->assign('data', $data);
        $this->display();
    }

    function content_edit_save() {
        $m_content = new \XWAM\Model\AdvertisingContentModel();
        $result = $m_content->save_content();
        $this->dialogJump($result['status'], $result['info']);
    }

    function content_delete($id = '') {
        $m_content = new \XWAM\Model\AdvertisingContentModel();
        $result = $m_content->delete_content($id);
        $this->ajaxReturn($result);
    }

    function change_sort($id, $sort) {
        $m_content = new \XWAM\Model\AdvertisingContentModel();
        $this->ajaxReturn($m_content->change_sort($id, $sort));
    }

}
