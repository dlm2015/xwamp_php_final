<?php

namespace XWAM\Controller;

/**
 * 号码归属地控制器
 *
 * @since 1.0 2015-1-5 by sutroon
 */
class NumberRegionalController extends AppbaseController {

    // 获取归属地
    public function get_regional($tel = '') {
        $m_region = new \XWAM\Service\NumberRegionalModel();
        echo $m_region->get_regional($tel);
        exit;
    }

    /**
     * 获取开始号码 如：areaname=厦门市
     * @param string $areaname (地区名称)
     * @param string $phonegear 号码类型(手机、固话)
     * @param string $isp 运营商名称
     * @return string
     * @example ?areaname=上海市&isp=联通
     */
    public function get_segment_numbers_json($areaname, $phonegear, $isp) {
        $m_region = new \XWAM\Service\NumberRegionalModel();
        $this->ajaxReturn($m_region->get_segment_numbers($areaname, $phonegear, $isp));
    }

}
