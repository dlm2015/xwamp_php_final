<?php

namespace XWAM\Controller;

/**
 * CustomerController 类
 *
 * @since 1.0 <2015-12-5> SoChishun <14507247@qq.com> Added.
 */
class CustomerAppointmentController extends AppbaseController {

    /**
     * 获取客户列表搜索数据
     * @return array
     * @since 1.0 2014-12-30 by sutroon
     */
    function customer_list_get_search() {
        $search = $_GET;
        $is_seat = ('SEAT' == $this->user_login_data['type_name']);
        $base_prefix =  'c.'; // 基础表前缀
        // 固定条件        
        $where[$base_prefix . 'site_id'] = $this->site_id;
        // 过滤回收站记录
        $where[$base_prefix . 'status'] = 1;
        $where[$base_prefix . 'is_shared'] = 'N';
        // 坐席只能看到自己的
        if ($is_seat) {
            $where[$base_prefix . 'user_name'] = $this->user_login_data['user_name'];
        }
        // 简单查询
        if (!empty($search['search_key'])) {
            $searchkey = str_replace("'", '', $search['search_key']); // 工号/编号/姓名/手机号
            if (!$is_seat) {
                $where_or[$base_prefix . 'user_name'] = $searchkey;
            }
            $where_or[$base_prefix . 'serial_no'] = $searchkey;
            $where_or[$base_prefix . 'name'] = array('like', "%$searchkey%");
            $where_or[$base_prefix . 'telphone'] = array('like', "%$searchkey%");
            $where_or['_logic'] = 'or';
            $where['_complex'] = $where_or;
            return array('search' => $search, 'where' => $where);
        }
        return array('search' => $search, 'where' => $where);
    }

    // 客户列表
    public function customer_list() {
        $asearch = $this->customer_list_get_search();
        $m_appo = new \XWAM\Model\CustomerAppointmentModel();
        $list = $m_appo->paging_select($show, $afields, $this->site_id, $asearch);
        $this->assign('page', $show);
        $this->assign('list', $list);
        $this->assign('afields', $afields['custom'] ? array_merge($afields['system'], $afields['appo'], $afields['custom']) : array_merge($afields['system'], $afields['appo']));
        $this->assign('search', $asearch['search']);
        $this->display_cpp();
    }

    // 预约回访 2014-12-24 by sutroon
    public function appointment_edit($id = 0, $customer_id = 0) {
        if (!$customer_id) {
            $this->dialogClose(false, array('error' => '参数有误!'));
        }
        $m_customer = new \XWAM\Model\CustomerModel();
        $data = $m_customer->field('id as customer_id, `name`, serial_no')->find($customer_id);
        if (!$data) {
            $this->dialogClose(false, array('error' => '客户不存在!'));
        }
        $m_appointment = new \XWAM\Model\CustomerAppointmentModel();
        if ($id) {
            $data2 = $m_appointment->find($id);
            $data = array_merge($data, $data2);
        }
        $list = $m_appointment->where(array('customer_id' => $customer_id))->select();
        $this->assign('data', $data);
        $this->assign('list', $list);
        $this->display();
    }

    // 预约回访数据保存 2014-12-24 by sutroon
    public function appointment_edit_save($id = 0, $customer_id = 0) {
        $m_appointment = new \XWAM\Model\CustomerAppointmentModel();
        $result = $m_appointment->save_appointment();
        $this->dialogJump($result['status'], $result['info']);
    }    

    // 客户物理删除 2014-12-25 by sutroon
    public function appointment_delete($id = '') {
        $m_customer = new \XWAM\Model\CustomerAppointmentModel();
        $result = $m_customer->delete_customer($id);
        $this->dialogJump($result['status'], $result['info']);
    }

}
