<?php

namespace XWAM\Controller;

/**
 * CallRecordShareController 类
 *
 * @since VER:1.0; DATE:2016-1-23; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class CallRecordShareController extends AppbaseController {

    // 2016-2-1 by sutroon
    function record_list() {
        if ($this->user_login_data['type_name'] == 'SEAT') {
            $where['exten'] = $this->user_login_data['user_name'];
        } else {
            $where['site_id'] = $this->site_id;
        }
        $where['is_shared'] = 'Y';
        $asearch_info = array('search' => $_GET);
        $m_common=new \Common\Model\CommonModel();
        $list = $m_common->get_paging_list($show, array('table' => 't_porg_callrecord', 'where' => $where, 'order' => 'id desc'), array('page_params' => $asearch_info['search']));
        $this->assign('page', $show);
        $this->assign('list', $list);
        $this->assign('search', $asearch_info['search']);
        $this->display_cpp();
    }

}
