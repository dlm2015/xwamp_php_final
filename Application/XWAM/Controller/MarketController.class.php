<?php

namespace XWAM\Controller;

/**
 * 模块市场控制器
 *
 * @since 1.0 <2015-9-17> SoChishun <14507247@qq.com> Added.
 */
class MarketController extends \Think\Controller {

    /**
     * 首页
     * @since 1.0 <2015-9-25> SoChishun Added.
     */
    public function index() {

        // 可用：从ModulesDist目录读取所有封装好的插件包
        // 已下载：从Usr/local/Package/读取所有zip插件包
        // 已安装：从Usr/etc/
        // 更新：
        // 设置：
        $list = D('Market')->get_installed_list();
        $this->assign('list', $list);
        $content = $this->fetch('index_part_installed');
        $this->assign('content', $content);
        $this->display();
    }

    public function uninstall() {
        
    }

}
