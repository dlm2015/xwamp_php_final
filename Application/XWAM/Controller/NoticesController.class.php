<?php

namespace XWAM\Controller;

/**
 * NoticesController 类
 *
 * @since 1.0 <2016-2-3> SoChishun <14507247@qq.com> Added.
 */
class NoticesController extends AppbaseController {

    function document_list_search_data() {
        $search = $_GET;
        $where = array();
        if (!empty($search['search_key'])) {
            $where['title'] = array('like', '%' . $search['search_key'] . '%');
        }
        if (!empty($search['category_id'])) {
            $where['category_id'] = $search['category_id'];
        }
        if (!empty($search['title'])) {
            $where['title'] = array('like', '%' . $search['title'] . '%');
        }
        if ('SEAT' == $this->user_login_data['type_name']) {
            $where['user_name'] = $this->user_login_data['user_name'];
        } else {
            $where['site_id'] = $this->site_id;
        }
        return array('search' => $search, 'where' => $where);
    }

    function document_list() {
        $search_data = $this->document_list_search_data();
        $m_notices = new \XWAM\Model\NoticesModel();
        $list = $m_notices->get_paging_list($show, array('where' => $search_data['where'], 'order' => 'sort, id desc'), array('page_params' => $search_data['search']));
        $this->assign('list', $list);
        $this->assign('page', $show);
        $this->assign('search', $search_data['search']);
        $this->display_cpp();
    }

    /**
     * 获取公告列表
     * @param type $limit
     * @param type $sign
     * @since 2.0 2016-3-12 SoChishun 重构:使用Su-Sign内容签名比对技术,优化界面刷新视觉效果，减少流量消耗.
     */
    function get_notices_list_json($limit = 10, $sign = '') {
        $m_notices = new \XWAM\Model\NoticesModel();
        $list = $m_notices->where(array('status' => 1))->limit($limit)->field('id, title')->order('sort, id desc')->select();
        $md5_sign = $list ? md5(serialize($list)) : 'none';
        $this->ajaxReturn(array('value' => ($sign && $sign == $md5_sign ? '' : $list), 'sign' => $md5_sign));
    }

    function document_view($id = 0) {
        $data = array();
        if ($id) {
            $m_notices = new \XWAM\Model\NoticesModel();
            $data = $m_notices->find($id);
        }
        $this->assign('meta_title', $data['title']);
        $this->assign('data', $data);
        $this->display();
    }

    function document_edit($id = 0, $category_id = 0) {
        $data = array();
        if ($id) {
            $m_notices = new \XWAM\Model\NoticesModel();
            $data = $m_notices->find($id);
        }
        if (!$data) {
            $data = array('user_name' => $this->user_login_data['user_name'], 'site_id' => $this->user_login_data['site_id']);
        }
        if ($category_id) {
            $data['category_id'] = $category_id;
        }
        $this->assign('meta_title', $id ? '编辑 ' . $data['title'] : '新增公告');
        $this->assign('data', $data);
        $this->display();
    }

    function document_edit_save() {
        $m_notices = new \XWAM\Model\NoticesModel();
        $result = $m_notices->save_document($this->user_login_data['user_name']);
        if ($result['status']) {
            $this->success('保存成功', U('document_list'));
        } else {
            $this->error($result['info']);
        }
    }

    function change_status($id, $status) {
        $m_notices = new \XWAM\Model\NoticesModel();
        $this->ajaxReturn($m_notices->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_notices = new \XWAM\Model\NoticesModel();
        $this->ajaxReturn($m_notices->change_sort($id, $sort));
    }

    function document_delete($id = '') {
        $m_notices = new \XWAM\Model\NoticesModel();
        $result = $m_notices->delete_document($id);
        $this->dialogJump($result['status'], $result['info']);
    }

}
