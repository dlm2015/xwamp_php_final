<?php

namespace XWAM\Controller;

/**
 * 通话记录控制器
 *
 * @since 1.0 2015-1-1 by sutroon
 */
class ApiCallRecordController extends \Think\Controller {

    /**
     * 座席来电弹屏
     * @param type $exten
     * @param  string $options 如：{"debug":false,"exten":202,"line":"13950000001","exten_field":"exten","line_field":"line","jsonp":"extension_screen_popup_jsonp"}
     * @since 2014-12-16 by sutroon
     * @since VER:2.0; DATE:2016-1-29; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:重构.
     * @example data-screenpopup-url="{:U('ApiCallRecord/screen_popup','options={"jsonp":"varjsonp"}&addon=POrgCustomer&exten='.$login_data['user_name'])}"
     */
    public function screen_popup($exten, $jsonp = 'extension_screenpopup_jsonp', $debug_status = 'N', $debug_exten = '301', $debug_line = '13950000001', $exten_field = 'exten', $line_field = 'line') {
        if ('Y' == $debug_status) {
            $interval = S('screen_popup_debug_line');
            if ($interval) {
                $debug_line = '';
            } else {
                S('screen_popup_debug_line', '1', 2); // 不管客户端几秒刷新,后台数据10秒重复一次
            }
            $data = array($exten_field => $debug_exten, $line_field => $debug_line);
            exit($jsonp . '(' . json_encode($data) . ')');
        }
        if (!$exten) {
            die('');
        }
        $line = '';
        if (false) {
            $memcache = new \Memcache;
            $memcache->connect('127.0.0.1', 11211);
            $line = $memcache->get($exten); // 电话号码
            $memcache->delete($exten); // 获取后就删除，避免重复弹屏
            $memcache->close();
        }
        if (true) {
            $redis = new \Redis();
            if (is_null($redis)) {
                exit('Redis Create Failure!');
            }
            try {
                //$redis->pconnect('127.0.0.1', 6379);
                $redis->connect('127.0.0.1', 6379);
            } catch (Exception $ex) {
                exit('Redis Connect Failure:' . $ex->getMessage());
            }
            $line = $redis->get('LDTP' . $exten);
            $redis->delete('LDTP' . $exten);
            unset($redis);
        }
        $data = array($exten_field => $exten, $line_field => $line);
        $save_db = true;
        if ($save_db && $line) {
            $m_callrecord = new \XWAM\Model\CallRecordModel();
            $m_callrecord->add(array('exten' => $exten, 'line' => $line));
        }
        echo $jsonp . '(' . json_encode($data) . ')';
    }

    /**
     * 座席获取最近10条通话记录
     * <br />JSONP方式获取
     * @since 2014-12-16 by sutroon
     * @example 
     */
    //<select id="exten-callrecord-topn" data-screenpop-url="{:U('customer_edit','tel=vartel')}" data-source-url="{:U('ApiCallRecord/seat_get_callrecord_list','limit=10&exten='.$login_data['user_name'])}"></select>
    //exten_get_callrecord_list();
    ///**
    // * 获取分机最近10条通话记录
    // * @returns {undefined}
    // * @since 1.0 2016-12-16 by sutroon
    // */
    //function exten_get_callrecord_list() {
    //    var url = $('#exten-callrecord-topn').data('sourceUrl');
    //    $.ajax({
    //        url: url,
    //        dataType: "jsonp",
    //        jsonp: "exten_get_callrecord_list_jsonp"
    //    })
    //}
    //// 获取历史通话记录 2014-12-16 by sutroon
    //function exten_get_callrecord_list_jsonp(data) {
    //    var $topn=$('#exten-callrecord-topn');
    //    var url=$topn.data('screenpopUrl');
    //    var str = '<option>==最近通话记录==</option>';
    //    for (var i in data) {
    //        str += '<option value="'+data[i].line+'">'+data[i].line+' 于'+data[i].create_time.substring(11)+'</option>';
    //    }
    //    $topn.html(str);
    //}
    public function exten_get_callrecord_list($exten = '', $limint = 10, $jsonp = 'exten_get_callrecord_list_jsonp') {
        if (!$exten) {
            die('');
        }
        $list = false;
        $m_callrecord = new \XWAM\Model\CallRecordModel();
        $list = $m_callrecord->get_exten_call_records($exten, 'line,create_time', $limit);
        echo $jsonp . '(' . json_encode($list) . ')';
    }

    public function get_customer($tel = '', $exten = '') {
        $users = M('db_xcall.tuser_member')->where(array('userType' => 'SEAT'))->getField('userName, siteID');
        $site_id = array_key_exists($exten, $users) ? $users[$exten] : false;
        if (!$site_id) {
            return '';
        }
        $data = '';
        $tsids = M('db_xcall.tcti_task_strategy')->where("siteID=$site_id")->getField('id', true);
        foreach ($tsids as $tsid) {
            $data = M('db_xcall.tcti_call_number' . $tsid)->field('customerName as personalName, phoneNumber as telphone, detailMsg')->order('id desc')->where("phoneNumber like '%$tel' and source='import'")->find();
            if (false === $data) {
                die($M->getDbError());
            }
            if ($data) {
                break;
            }
        }
        echo $data['telphone'], ',', $data['detailMsg'];
    }

}
