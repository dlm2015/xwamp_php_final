<?php

namespace XWAM\Controller;

/**
 * 用户控制器（包含管理员和座席）
 *
 * @since 1.0 <2014-6-13> sutroon Added.
 */
class UserAdminController extends AppbaseController {

    function user_list_search_data() {
        $search = $_GET;
        if (!empty($search['search_key'])) {
            $searchkey = $search['search_key'];
            $where['_string'] = "user_name like '%$searchkey%' or name like '%$searchkey%' or mobile like '%$searchkey%'";
        }
        if (!empty($search['user_name'])) {
            $where['user_name'] = array('like', '%' . $search['user_name'] . '%');
        }
        $search['type_name'] = 'ADMIN';
        $where['type_name'] = $search['type_name'];
        $where['site_id'] = $this->site_id;
        return array('search' => $search, 'where' => $where);
    }

    public function user_list() {
        $search_data = $this->user_list_search_data();
        //$this->assign('usertype_list', D('UserType')->order('sort, id desc')->select());
        $this->assign('usertype_list', array('ADMIN' => '管理员', 'SERVICES' => '客服人员', 'CUSTOMER' => 'VIP客户'));
        $m_admin = new \XWAM\Model\UserAdminModel();
        $list = $m_admin->get_paging_list($show, array('where' => $search_data['where'], 'order' => 'id'), array('page_params' => $search_data['search']));
        $m_user = new \XWAM\Model\UserModel();
        $m_user->set_role2department_names($list);
        $this->assign('search', $search_data['search']);
        $this->assign('list', $list);
        $this->assign('page', $show);
        $this->display_cpp();
    }

    public function user_export() {
        $list = $m_user->select();
        if (!$list) {
            $this->error('找不到符合条件的记录!');
        }
        sofn_excel_export($list);
    }

    /**
     * 编辑个人资料
     * @since 1.0 <2015-11-11> SoChishun Added.
     */
    function profile_edit() {
        $this->user_edit($this->user_login_data['id']);
    }

    /**
     * 保存个人资料
     * @since 1.0 <2016-3-11> SoChishun Added.
     */
    function profile_edit_save() {
        $m_admin = new \XWAM\Model\UserAdminModel();
        $result = $m_admin->save_user($this->user_login_data['id']);
        $this->dialogClose($result['status'], $result['info']);
    }

    public function user_edit($id = 0) {
        $m_admin = new \XWAM\Model\UserAdminModel();
        $data = $id ? $m_admin->find($id) : array();
        $this->assign('data', $data);
        $this->assign('meta_title', $id ? '编辑 ' . $data['user_name'] : '新增管理员');
        $this->display();
    }

    public function user_password_reset($id) {
        if (!$id) {
            $this->result(false, '参数有误!');
        }
        $m_admin = new \XWAM\Model\UserAdminModel();
        $result = $m_admin->where(array('id' => $id))->setField('password', '123456');
        $this->ajaxMsg($result, '密码重置{%}!');
    }

    public function user_edit_save() {
        $m_admin = new \XWAM\Model\UserAdminModel();
        $result = $m_admin->save_user($this->user_login_data['id']);
        $this->dialogJump($result['status'], $result['info']);
    }

    public function user_delete($id = '') {
        if (!$id) {
            $this->ajaxMsg(false, '编号丢失!');
        }
        if ($id == $this->user_login_data['id']) {
            $this->ajaxMsg(false, '不能删除自己!');
        }
        $m_admin = new \XWAM\Model\UserAdminModel();
        $result = $m_admin->delete_user($id, $this->user_login_data['id']);
        $this->ajaxMsg($result, '删除{%}!');
    }

    function change_online_status($id, $status) {
        $m_user = new \XWAM\Model\UserModel();
        $this->ajaxReturn($m_user->change_online_status($id, $status));
    }

    // 2016-1-20
    function user_role_one($id) {
        $m_user = new \XWAM\Model\UserModel();
        $role_ids = $m_user->where(array('id' => $id))->getField('role_ids');
        $this->assign('role_ids', $role_ids);
        $this->assign('uid', $id);
        $this->display();
    }

    function user_role($id) {
        $m_user = new \XWAM\Model\UserModel();
        $role_ids = $m_user->where(array('id' => $id))->getField('role_ids');
        $m_rule = new \XWAM\Model\RoleModel();
        $list = $m_rule->select();
        $this->assign('list', $list);
        $this->assign('role_ids', $role_ids);
        $this->assign('uid', $id);
        $this->display();
    }

    /**
     * 保存管理员角色
     * @since 1.0 <2015-8-4> SoChishun Added.
     */
    public function user_role_save() {
        $ids = I('id');
        $uid = I('uid');
        if (!$ids || !$uid) {
            $this->dialogClose(false, array('error' => '参数有误!'));
        }
        $m_user = new \XWAM\Model\UserModel();
        $result = $m_user->save_role($uid, implode(',', $ids));
        $this->dialogJump($result['status'], $result['info']);
    }

    // 2016-1-20
    function user_department_one($id) {
        $m_user = new \XWAM\Model\UserModel();
        $role_ids = $m_user->where(array('id' => $id))->getField('department_ids');
        $this->assign('department_ids', $role_ids);
        $this->assign('uid', $id);
        $this->display();
    }

    function user_department($id) {
        $m_user = new \XWAM\Model\UserModel();
        $department_ids = $m_user->where(array('id' => $id))->getField('department_ids');
        $m_department = new \XWAM\Model\DepartmentModel();
        $this->assign('tree', $m_department->select_tree());
        $this->assign('department_ids', $department_ids);
        $this->assign('uid', $id);
        $this->display();
    }

    /**
     * 保存管理员角色
     * @since 1.0 <2015-8-4> SoChishun Added.
     */
    public function user_department_save() {
        $ids = I('id');
        $uid = I('uid');
        if (!$ids || !$uid) {
            $this->dialogClose(false, array('error' => '参数有误!'));
        }
        $m_user = new \XWAM\Model\UserModel();
        $result = $m_user->save_department($uid, implode(',', $ids));
        $this->dialogJump($result['status'], $result['info']);
    }

}
