<?php

namespace XWAM\Controller;

/**
 * DocumentCategoryController类
 *
 * @since 1.0 <2015-10-24> SoChishun <14507247@qq.com> Added.
 */
class ChannelsContentController extends AppbaseController {

    public function content_edit($id = 0) {
        if (!$id) {
            $this->error('参数有误!');
        }
        $m_category_content = new \XWAM\Model\DocumentCategoryContentModel();
        $data = $m_category_content->find($id);
        if (!$data) {
            $data['category_id'] = $id;
            $data['action'] = 'add';
        }
        $this->assign('data', $data);
        $this->display();
    }

    public function content_edit_save() {
        $m_category_content = new \XWAM\Model\DocumentCategoryContentModel();
        $result = $m_category_content->save_content();
        if ($result['status']) {
            $this->success('保存成功');
        } else {
            $this->error(implode('<br />', $result['info']));
        }
    }

}
