<?php

namespace XWAM\Controller;

/**
 * 客户控制器
 *
 * @since 1.0 <2014-10-20> SoChishun <14507247@qq.com> Added.
 */
class CustomerController extends AppbaseController {

    /**
     * 获取客户列表搜索数据
     * @return array
     * @since 1.0 2014-12-30 by sutroon
     */
    function customer_list_get_search() {
        $search = $_GET;
        $is_seat = ('SEAT' == $this->user_login_data['type_name']);
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $has_extable = $m_attr->has_extable($this->site_id); // 是否有扩展表
        $base_prefix = $has_extable ? 'c.' : ''; // 基础表前缀
        $ex_prefix = $has_extable ? 's.' : ''; // 扩展表前缀
        // 固定条件        
        $where[$base_prefix . 'site_id'] = $this->site_id;
        // 过滤回收站记录
        $where[$base_prefix . 'status'] = 1;
        $where[$base_prefix . 'is_shared'] = 'N';
        // 坐席只能看到自己的
        if ($is_seat) {
            $where[$base_prefix . 'user_name'] = $this->user_login_data['user_name'];
        }
        // 简单查询
        if (!empty($search['search_key'])) {
            $searchkey = str_replace("'", '', $search['search_key']); // 工号/编号/姓名/手机号
            if (!$is_seat) {
                $where_or[$base_prefix . 'user_name'] = $searchkey;
            }
            $where_or[$base_prefix . 'serial_no'] = $searchkey;
            $where_or[$base_prefix . 'name'] = array('like', "%$searchkey%");
            $where_or[$base_prefix . 'telphone'] = array('like', "%$searchkey%");
            $where_or['_logic'] = 'or';
            $where['_complex'] = $where_or;
            return array('search' => $search, 'where' => $where);
        }
        // 高级查询
        $afields = $m_attr->get_customer_search_fields($this->site_id);
        foreach ($afields as $row) {
            $field = $row['field_name'];
            if (empty($search[$field])) {
                continue;
            }
            $operate = I('operate_' . $field, 'EQ'); // EQ:等于;LIKE:模糊匹配;GT:大于;LT:小于;NEQ:不等于
            $val = $search[$field];
            if ('LIKE' == $operate) {
                $val = "%$val%";
            }
            $field = ('Y' == $row['is_system'] ? $base_prefix : $ex_prefix) . $field;
            $where[$field] = array($operate, $val);
            //$where[$base_prefix . $key] = array('like', '%' . $search[$key] . '%');
        }
        return array('search' => $search, 'where' => $where);
    }

    // 客户列表
    public function customer_list($action = '') {
        $asearch = $this->customer_list_get_search();
        $m_customer = new \XWAM\Model\CustomerModel();
        $list = $m_customer->paging_select($show, $afields, $this->site_id, $asearch);
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $this->assign('searchlist', $m_attr->get_customer_search_fields($this->site_id));
        $this->assign('page', $show);
        $this->assign('list', $list);
        $this->assign('afields', $afields['custom'] ? array_merge($afields['system'], $afields['custom']) : $afields['system']);
        $this->assign('search', $asearch['search']);
        $this->display_cpp();
    }

    /**
     * 导出客户
     * @since 1.0 2016-5-12 SoChishun Added.
     */
    function customer_export() {
        $asearch = $this->customer_list_get_search();
        $m_customer = new \XWAM\Model\CustomerModel();
        $list = $m_customer->export_select($this->site_id, $asearch);
        if (!$list) {
            $this->error('找不到符合条件的记录!');
        }
        sofn_excel_export($list, array('FileName' => 'KH' . date('Ymd-His')));
    }

    // 检测电话号码是否重复 2015-1-2 by sutroon
    public function check_telphone($tel = '') {
        $count = 0;
        if (C('APPCONF.CUSTOMER_EDIT_TELPHONE_CHECK') && $tel) {
            $tel = ltrim($tel, '0');
            $m_customer = new \XWAM\Model\CustomerModel();
            $count = $m_customer->where(array('telphone' => array('like', '%' . $tel)))->count();
        }
        $this->ajaxReturn(array('success' => $count < 1));
    }

    // 2016-1-21 SoChishun Added.
    public function customer_edit($id = 0, $tel = '') {
        // 自定义字段
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $afields = $m_attr->get_customer_form_fields($this->site_id);
        $this->assign('fields', $afields);

        $where = array();
        if ($id) {
            $where['id'] = $id;
        }
        if ($tel) {
            $where['telphone'] = array('like', '%' . $tel); // 从后面匹配
        }
        $m_customer = new \XWAM\Model\CustomerModel();
        $data = $m_customer->find_customer($where);
        $user_name = $this->user_login_data['user_name'];
        if (!$data) {
            $uid = is_numeric($user_name) ? 'N' . $user_name : $this->user_login_data['id'];
            $serial_no = sofn_generate_serial('K' . $uid);
            $data = array('serial_no' => $serial_no, 'site_id' => $this->site_id);
        }
        if (!isset($data['telphone'])) {
            $data['telphone'] = $tel;
        }
        if (!isset($data['user_name'])) {
            $data['user_name'] = $user_name;
        }
        // 标题
        $meta_title = '新增客户';
        if ($tel) {
            $meta_title = '来电：' . $tel;
        } else if ($id) {
            $meta_title = '编辑 ' . $data['name'];
        }
        $this->assign('meta_title', $meta_title);
        $this->assign('data', $data);
        $this->display();
    }

    function customer_edit_save() {
        $m_customer = new \XWAM\Model\CustomerModel();
        $result = $m_customer->save_customer();
        if ($result['status']) {
            $this->success('保存成功!', U('customer_list'), 1);
        } else {
            $this->error($result['info'], '', 2);
        }
    }

    // 客户弹屏 2016-4-18
    public function customer_popup($tel = '') {
        if (!$tel) {
            exit('参数无效!');
        }
        // 自定义字段
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $afields = $m_attr->get_customer_form_fields($this->site_id);
        $this->assign('fields', $afields);

        // 查询条件
        $where['telphone'] = array('like', '%' . $tel); // 从后面匹配
        $where['site_id'] = $this->user_login_data['site_id'];
        $m_customer = new \XWAM\Model\CustomerModel();
        $data = $m_customer->find_customer($where);

        $exten = $this->user_login_data['user_name'];
        if (!$data) {
            $serial_no = sofn_generate_serial('K' . $exten);
            $data = array('serial_no' => $serial_no, 'user_name' => $this->user_login_data['user_name'], 'site_id' => $this->user_login_data['site_id']);
            // 从xcall数据库的号码表读取 2016-4-18
            $tmpdata = $m_customer->find_customer_from_xcall($exten, $tel);
            if ($tmpdata) {
                $data = array_merge($data, $tmpdata);
            }
        }
        if (!isset($data['telphone'])) {
            $data['telphone'] = $tel;
        }
        if (!isset($data['user_name'])) {
            $data['user_name'] = $exten;
        }
        if (!isset($data['name'])) {
            $data['name'] = '客户' . $data['serial_no'];
        }
        // 标题        
        $this->assign('meta_title', '来电：' . $tel);
        $this->assign('data', $data);
        $this->display();
    }

    function customer_popup_save() {
        $m_customer = new \XWAM\Model\CustomerModel();
        $result = $m_customer->save_customer();
        if ($result['status']) {
            $this->dialogClose(true);
        } else {
            $this->error($result['info'], '', 2);
        }
    }

    // 客户物理删除 2014-12-25 by sutroon
    public function customer_delete($id = '') {
        $m_customer = new \XWAM\Model\CustomerModel();
        $result = $m_customer->delete_customer($id);
        $this->dialogJump($result['status'], $result['info']);
    }

    // 客户加入回收站 2014-12-25 by sutroon
    public function customer_recycle($id = '') {
        $m_customer = new \XWAM\Model\CustomerModel();
        $result = $m_customer->recycle_customer($id);
        $this->dialogJump($result['status'], $result['info']);
    }

    // 从回收站还原客户 2014-12-25 by sutroon
    public function restore_customer($id = '') {
        $m_customer = new \XWAM\Model\CustomerModel();
        $result = $m_customer->restore_customer($id);
        $this->ajaxMsg($result, '还原%!');
    }

    // 加入公共池 2016-4-8
    function customer_share($id) {
        $m_customer = new \XWAM\Model\CustomerModel();
        $m_customer->share_customer($id);
        $this->ajaxMsg($result, '操作成功!');
    }

}
