<?php

namespace XWAM\Controller;

/**
 * PMController 类
 *
 * @since 1.0 <2015-12-2> SoChishun <14507247@qq.com> Added.
 */
class PMController extends AppbaseController {

    function get_pm_list_search_data() {
        $search = $_GET;
        if (empty($search['type'])) {
            $search['type'] = 'INBOX';
        }
        return array('search' => $search);
    }

    function pm_list() {
        $search_data = $this->get_pm_list_search_data();
        $where = $search_data['where'];
        $where['to_user'] = $this->user_login_data['user_name'];
        $this->assign('typeset', array('INBOX' => '收件箱', 'OUTBOX' => '发件箱', 'DRAFTS' => '草稿箱', 'BINBOX' => '垃圾箱'));
        $m_pm = new \XWAM\Model\PMModel();
        $list = $m_pm->get_paging_list($pager, array('where' => $search_data['where']));
        $this->assign('list', $list);
        $this->assign('page', $pager);
        $this->assign('search', $search_data['search']);
        $this->display_cpp();
    }

    function my_inbox() {
        $m_pm = new \XWAM\Model\PMModel();
        $list = $m_pm->get_list($this->user_login_data['user_name'], 'INBOX');
        $this->assign('list', $list);
        $this->display_cpp();
    }

    function pm_view($id = 0) {
        if (!$id) {
            exit('短消息不存在!');
        }
        $m_pm = new \XWAM\Model\PMModel();
        $data = $m_pm->find($id);
        // 标记已读
        if (!$data['read_status']) {
            $m_pm->where(array('id' => $id))->setField(array('read_status' => 1, 'read_time' => date('Y-m-d h:i:s')));
        }
        $this->assign('data', $data);
        $this->assign('my', I('my'));
        $this->display();
    }

    function pm_edit($id = 0) {
        $data = array();
        if ($id) {
            $m_pm = new \XWAM\Model\PMModel();
            $data = $m_pm->find($id);
        }
        if (!$data) {
            $data = array('site_id' => $this->user_login_data['site_id']);
        }
        $this->assign('data', $data);
        $this->display();
    }

    function pm_edit_save() {
        $m_pm = new \XWAM\Model\PMModel();
        $result = $m_pm->save_pm($this->user_login_data['user_name']);
        $this->dialogJump($result['status'], $result['info']);
    }

    function pm_delete($id = '') {
        $m_pm = new \XWAM\Model\PMModel();
        $result = $m_pm->delete_pm($id);
        $this->ajaxReturn($result);
    }

    function change_read_status($id, $status) {
        $m_pm = new \XWAM\Model\PMModel();
        $result = $m_pm->change_read_status($id, $status);
        $this->ajaxReturn($result);
    }

    /**
     * 获取站内信数量
     * @since 2.0 2016-3-12 SoChishun 重构;
     */
    function read_count() {
        $m_pm = new \XWAM\Model\PMModel();
        $count = $m_pm->read_count($this->user_login_data['user_name']);
        $aout = array('value' => $count, 'sign' => md5($count));
        $this->ajaxReturn($aout);
    }

}
