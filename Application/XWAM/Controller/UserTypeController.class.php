<?php

namespace XWAM\Controller;

/**
 * Description of UserTypeController
 *
 * @since 1.0 <2015-11-3> SoChishun <14507247@qq.com> Added.
 */
class UserTypeController extends AppbaseController {

    public function type_list() {
        $list = D('UserType')->get_paging_list($show, array('order' => 'sort, id desc'));
        $this->assign('list', $list);
        $this->assign('page', $show);
        $this->display_cpp();
    }

    public function type_edit($id = 0) {
        if ($id) {
            $this->assign('data', D('UserType')->find($id));
        }
        $this->display();
    }

    function type_edit_save() {
        $result = D('UserType')->save_type();
        $this->dialogJump($result['status'], $result['info']);
    }

    function change_status($id, $status) {
        $this->ajaxReturn(D('UserType')->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $this->ajaxReturn(D('UserType')->change_sort($id, $sort));
    }

    function type_delete($id = '') {
        $this->ajaxReturn(D('UserType')->delete_type($id));
    }

}
