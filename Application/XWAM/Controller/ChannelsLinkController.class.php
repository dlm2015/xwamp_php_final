<?php

namespace XWAM\Controller;

/**
 * ChannelsLinkController类
 *
 * @since 1.0 <2015-10-26> SoChishun <14507247@qq.com> Added.
 */
class ChannelsLinkController extends AppbaseController {

    public function link_edit($id = 0) {
        if (!$id) {
            $this->error('参数有误!');
        }
        $m_category_link=new \XWAM\Model\DocumentCategoryLinkModel();
        $data = $m_category_link->find($id);
        if (!$data) {
            $data['category_id'] = $id;
            $data['action'] = 'add';
        }
        $this->assign('data', $data);
        $this->display();
    }

    public function link_edit_save() {
        $m_category_link=new \XWAM\Model\DocumentCategoryLinkModel();
        $result = $m_category_link->save_link();
        if ($result['status']) {
            $this->success('保存成功');
        } else {
            $this->error(implode('<br />', $result['info']));
        }
    }

}
