<?php

namespace XWAM\Controller;

/**
 * DocumentController类
 *
 * @since 1.0 <2015-10-22> SoChishun <14507247@qq.com> Added.
 */
class DocumentsController extends AppbaseController {

    function document_list_search_data() {
        $search = $_GET;
        $where = array();
        if (!empty($search['search_key'])) {
            $where['title'] = array('like', '%' . $search['search_key'] . '%');
        }
        if (!empty($search['category_id'])) {
            $where['category_id'] = $search['category_id'];
        }
        if (!empty($search['title'])) {
            $where['title'] = array('like', '%' . $search['title'] . '%');
        }
        $where['site_id'] = $this->site_id;
        return array('search' => $search, 'where' => $where);
    }

    function document_list() {
        $search_data = $this->document_list_search_data();
        $m_document = new \XWAM\Model\DocumentModel();
        $list = $m_document->get_paging_list($show, array('where' => $search_data['where'], 'order' => 'sort, id desc'), array('page_params' => $search_data['search']));
        $list2 = false;
        if ($list) {
            $m_category = new \XWAM\Model\DocumentCategoryModel();
            $channels = $m_category->where(array('type_id' => 2))->getField('id, title');
            foreach ($list as $row) {
                $row['category_name'] = isset($channels[$row['category_id']]) ? $channels[$row['category_id']] : '';
                $list2[] = $row;
            }
        }
        $category_id = I('category_id');
        $this->assign('meta_title', $category_id ? D('DocumentCategory')->where(array('id' => $category_id))->getField('title') : '文章管理');
        $this->assign('list', $list2);
        $this->assign('page', $show);
        $this->assign('search', $search_data['search']);
        $this->display_cpp();
    }

    function document_edit($id = 0, $category_id = 0) {
        $data = array();
        if ($id) {
            $m_document = new \XWAM\Model\DocumentModel();
            $data = $m_document->find($id);
            $data['content'] = D('DocumentContent')->get_content($id);
        }
        if (!$data) {
            $data = array('user_name' => $this->user_login_data['user_name'], 'site_id' => $this->user_login_data['site_id']);
        }
        if ($category_id) {
            $data['category_id'] = $category_id;
        }
        $this->assign('meta_title', $id ? '编辑' . $data['title'] : '新增文章');
        $this->assign('data', $data);
        $this->display();
    }

    function document_edit_save() {
        $m_document = new \XWAM\Model\DocumentModel();
        $result = $m_document->save_document($this->user_login_data['user_name']);
        $this->dialogJump($result['status'], $result['info']);
    }

    function change_status($id, $status) {
        $m_document = new \XWAM\Model\DocumentModel();
        $this->ajaxReturn($m_document->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_document = new \XWAM\Model\DocumentModel();
        $this->ajaxReturn($m_document->change_sort($id, $sort));
    }

    function document_delete($id = '') {
        $m_document = new \XWAM\Model\DocumentModel();
        $result = $m_document->delete_document($id);
        $this->dialogJump($result['status'], $result['info']);
    }

}
