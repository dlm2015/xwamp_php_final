<?php

namespace XWAM\Controller;

/**
 * Description of UserAttrController
 *
 * @since 1.0 <2015-11-3> SoChishun <14507247@qq.com> Added.
 */
class UserAttrController extends AppbaseController {

    function attr_list($type_id = 0) {
        $list = D('UserAttr')->where(array('type_id' => $type_id))->select();
        $this->assign('list', $list);
        $this->assign('type_id', $type_id);
        $this->display_cpp();
    }

    function attr_edit($id = 0, $type_id = 0) {
        if ($id) {
            $data = D('UserAttr')->find($id);
        } else {
            $data['type_id'] = $type_id;
        }
        $this->assign('data', $data);
        $this->display();
    }

    function attr_edit_save() {
        $result = D('UserAttr')->save_type();
        $this->dialogJump($result['status'], $result['info']);
    }

    function change_status($id, $status) {
        $this->ajaxReturn(D('UserAttr')->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $this->ajaxReturn(D('UserAttr')->change_sort($id, $sort));
    }

    function attr_delete($id = '') {
        $this->ajaxReturn(D('UserAttr')->delete_attr($id));
    }

}
