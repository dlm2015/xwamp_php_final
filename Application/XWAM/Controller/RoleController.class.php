<?php

namespace XWAM\Controller;

/**
 * RoleController.class 控制器
 * @since 1.0 <2015-5-23> SoChishun <14507247@qq.com> Added.
 */
class RoleController extends AppbaseController {

    /**
     * role_list操作
     * @since 1.0<2015-5-23> SoChishun Added.
     */
    public function role_list() {
        $where['site_id'] = $this->user_login_data['site_id'];
        $m_role = new \XWAM\Model\RoleModel();
        $list = $m_role->get_paging_list($show, array('where' => $where, 'order' => 'sort,id'));
        $this->assign('list', $list);
        $this->assign('page', $show);
        $this->display_cpp();
    }

    public function get_role_tree_json() {
        $where['site_id'] = $this->user_login_data['site_id'];
        $m_role = new \XWAM\Model\RoleModel();
        $list = $m_role->where($where)->field('id, title as text')->select();
        $this->ajaxReturn($list);
    }

    /**
     * 导出数据
     * @since 1.0 <2014-6-13> SoChishun Added.
     */
    public function role_export() {
        if (!$this->isSuperAdmin) {
            die('没有权限!');
        }
        $where['site_id'] = $this->user_login_data['site_id'];
        $m_role = new \XWAM\Model\RoleModel();
        $list = $m_role->where($where)->select();
        if (!$list) {
            $this->ajaxMsg(false, '找不到符合条件的记录!');
        }
        sofn_excel_export($list);
    }

    public function role_copy($id = 0) {
        if (!$id) {
            $this->ajaxMsg(false, '参数无效');
        }
        $m_role = new \XWAM\Model\RoleModel();
        $m_role->copy_role($id);
        $this->ajaxMsg(true);
    }

    /**
     * role_edit操作
     * @param int $id 主键编号
     * @since 1.0<2015-5-23> SoChishun Added.
     */
    public function role_edit($id = 0) {
        $meta_title = '新增角色';
        $data = array();
        if ($id) {
            $m_role = new \XWAM\Model\RoleModel();
            $data = $m_role->find($id);
            $meta_title = '编辑' . $data['title'];
        }        
        if (!$data) {
            $data = array('site_id' => $this->site_id);
        }
        $this->assign('data', $data);
        $this->assign('meta_title', $meta_title);
        $this->display();
    }

    /**
     * role_edit_save操作
     * @since 1.0<2015-5-23> SoChishun Added.
     */
    public function role_edit_save() {
        $m_role = new \XWAM\Model\RoleModel();
        $result = $m_role->save_role();
        $this->dialogJump($result['status'], $result['info']);
    }

    /**
     * role_delete操作
     * @param string $id 主键编号
     * @since 1.0<2015-7-31> SoChishun Added.
     */
    public function role_delete($id = '') {
        $m_role = new \XWAM\Model\RoleModel();
        $this->ajaxReturn($m_role->delete_role($id));
    }

    public function role_permission_edit($id = 0) {
        $m_role = new \XWAM\Model\RoleModel();
        $rules = $m_role->where(array('id' => $id))->getField('rules');
        $m_rule = new \XWAM\Model\RuleModel();
        $tree = $m_rule->select_tree();
        $this->assign('tree', $tree);
        $this->assign('role_id', $id);
        $this->assign('ids', $rules);
        $this->display();
    }

    public function role_permission_edit_save() {
        $ids = I('id');
        $role_id = I('role_id');
        if (!$role_id || !$ids) {
            $this->dialogClose(false, array('error' => '参数有误!'));
        }
        $m_role = new \XWAM\Model\RoleModel();
        $result = $m_role->save_rule($role_id, $ids);
        $this->dialogJump($result['status'], $result['info']);
    }

    function change_status($id, $status) {
        $m_role = new \XWAM\Model\RoleModel();
        $this->ajaxReturn($m_role->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_role = new \XWAM\Model\RoleModel();
        $this->ajaxReturn($m_role->change_sort($id, $sort));
    }

}
