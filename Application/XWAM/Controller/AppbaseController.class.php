<?php

namespace XWAM\Controller;

/**
 * 控制器基础类
 * 
 * @since 1.0 <2015-3-18> SoChishun <14507247@qq.com> Added.
 */
abstract class AppbaseController extends \Common\Controller\CommonController {

    /**
     * 站点编号
     * @var integer
     * @since 1.0 2016-5-11 SoChishun Added.
     */
    protected $site_id = 0;

    /**
     * 空操作，用于输出404页面
     * @since 1.0 <2015-3-23> SoChishun Added.
     */
    public function _empty() {
        $this->display('XPage/404');
    }

    public function _initialize() {
        $site_conf = session('site_conf'); // 读取动态网站配置 2016-4-21
        $login_url = U('Login/index');
        if (!$site_conf) {
            redirect($login_url);
        }

        // 站点是否关闭检测
        'Y' == $site_conf['site_status'] || exit($site_conf['site_closed_announcement']);

        // 用户登录检测
        if (!user_check_login($this->user_login_data)) {
            redirect($login_url);
        }

        if (!empty($this->user_login_data['lang'])) { // 手动加载语言包 2016-5-9 SoChishun Added.            
            $file = MODULE_PATH . 'Lang/' . $this->user_login_data['lang'] . '.php';
            if (is_file($file)) {
                L(include $file); // 读取模块语言包
            }
        }
        C('LIST_ROWS', empty($this->user_login_data['page_size']) ? $site_conf['content_list_rows'] : $this->user_login_data['page_size']); // 设置全局分页尺寸变量 2016-5-6
        // 更新用户在线统计(影响性能,暂时关闭)
        //D('Login')->active_online_state($this->user_login_data['id']);
        // 用户资料补充
        if ('SEAT' == $this->user_login_data['type_name']) {
            $m_seat = new \XWAM\Model\UserSeatModel();
            $this->assign('SCREENPOPUP_MODE', $m_seat->get_screenpopup_mode($this->user_login_data['id'], $site_conf));
        }
        $this->site_id = $this->user_login_data['site_id']; // site_id赋值
        $this->assign('meta_title', $site_conf['site_title']); // 站点标题
        $this->assign('site_conf', $site_conf); // 站点配置
        $this->assign('login_data', $this->user_login_data); // 用户登录资料
        $this->assign('theme', empty($this->user_login_data['theme']) ? 'start' : $this->user_login_data['theme']); // 用户主题样式 2016-5-9
    }

    /**
     * 通用框架结果(对话框刷新父页面)
     * @param boolean $status 结果状态
     * @param string|array $error 错误消息
     * @param string $success 成功消息
     * @since 1.0 <2015-10-27> SoChishun Added.
     */
    function dialogJump($status, $error = '保存失败!', $success = '保存成功!') {
        if (false === $status) {
            $this->error(is_array($error) ? implode('<br />', $error) : $error);
        } else {
            $this->success($success, '!window.parent.location.reload();', 1);
        }
    }

    /**
     * 通用框架结果(对话框关闭,不刷新父页面)
     * @param boolean $status 结果状态
     * @param array $info array('success'=>'','error'=>'','ajax'=>'');
     * @since 2.0 2016-3-25 SoChishun 重构
     */
    function dialogClose($status, $info = array()) {
        $ainfo = array(
            'success' => '保存成功!',
            'error' => '保存失败!',
            'ajax' => '',
            'success_action'=>'!window.parent.close_dialog(window.name);',
        );
        if ($info && is_array($info)) {
            $ainfo = array_merge($ainfo, $info);
        }
        if (false === $status) {
            if (is_array($ainfo['error'])) {
                $ainfo['error'] = implode('<br />', $ainfo['error']);
            }
            $this->error($ainfo['error']);
        } else {
            $this->success($ainfo['success'], $ainfo['success_action'], $ainfo['ajax']);
            // $this->success($success, '!$(\'iframe[name="\' + window.name + \'"]\').attr(\'src\', "about:blank").parent().dialog(\'close\');');
        }
    }

}
