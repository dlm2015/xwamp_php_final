<?php

namespace XWAM\Controller;

/**
 * CustomerController 类
 *
 * @since 1.0 <2015-12-5> SoChishun <14507247@qq.com> Added.
 */
class CustomerPublicController extends AppbaseController {

    /**
     * 获取客户列表搜索数据
     * @return array
     * @since 1.0 2014-12-30 by sutroon
     */
    function customer_list_get_search() {
        $search = $_GET;
        $is_seat = ('SEAT' == $this->user_login_data['type_name']);
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $has_extable = $m_attr->has_extable($this->site_id); // 是否有扩展表
        $base_prefix = $has_extable ? 'c.' : ''; // 基础表前缀
        // 固定条件        
        $where[$base_prefix . 'site_id'] = $this->site_id;
        // 过滤回收站记录
        $where[$base_prefix . 'status'] = 1;
        $where[$base_prefix . 'is_shared'] = 'Y';
        // 坐席只能看到自己的
        if ($is_seat) {
            $where[$base_prefix . 'user_name'] = $this->user_login_data['user_name'];
        }
        // 简单查询
        if (!empty($search['search_key'])) {
            $searchkey = str_replace("'", '', $search['search_key']); // 工号/编号/姓名/手机号
            if (!$is_seat) {
                $where_or[$base_prefix . 'user_name'] = $searchkey;
            }
            $where_or[$base_prefix . 'serial_no'] = $searchkey;
            $where_or[$base_prefix . 'name'] = array('like', "%$searchkey%");
            $where_or[$base_prefix . 'telphone'] = array('like', "%$searchkey%");
            $where_or['_logic'] = 'or';
            $where['_complex'] = $where_or;
            return array('search' => $search, 'where' => $where);
        }
        return array('search' => $search, 'where' => $where);
    }

    // 客户列表
    public function customer_list() {
        $asearch = $this->customer_list_get_search();
        $m_customer = new \XWAM\Model\CustomerModel();
        $list = $m_customer->paging_select($show, $afields, $this->site_id, $asearch);
        $this->assign('page', $show);
        $this->assign('list', $list);
        $this->assign('afields', $afields['custom'] ? array_merge($afields['system'], $afields['custom']) : $afields['system']);
        $this->assign('search', $asearch['search']);
        $this->display_cpp();
    }

    // 获取模板 2016-4-19 SoChishun Added.
    public function get_template_file() {
        $m_attr = new \XWAM\Model\CustomerAttrModel();
        $afields = $m_attr->get_customer_import_fields($this->site_id, array('name' => '姓名', 'telphone' => '电话', 'sex' => '性别', 'address' => '地址'));
        $afields = $afields['customer'] ? array_merge($afields['system'], $afields['custom']) : $afields['system'];
        $list = array();
        foreach ($afields as $name => $title) {
            $list[$title] = '';
        }
        sofn_excel_export(array($list), array('FileName' => 'KHMB-' . date('mdhi')));
    }

    function customer_import() {
        $cache_name = 'customer_import_' . $this->user_login_data['id'];
        if (F($cache_name)) {
            exit('<div style="line-height:21px; font-size:13px; color:#F00;">您有一个"客户资料导入"的操作正在处理中,请等待...<div style="color:#999;font-size:12px;">如果您确定是系统异常导致出现此提示，<a href="' . U('customer_import_clear') . '" onclick="return confirm(\'您确定要执行此操作吗?\n如果您确实在导入资料，则导入操作会终止!\');">[点击这里]</a>清空此提示!</div></div>');
        }
        $this->display();
    }

    /**
     * 清除异常提示
     * @since 1.0 2016-3-25 SoChishun Added.
     */
    function customer_import_clear() {
        $cache_name = 'customer_import_' . $this->user_login_data['id'];
        F($cache_name, null);
        redirect(U('customer_import'));
    }

    /**
     * 上传步骤：
     * 1. 保存文件到服务器(upload_file)
     * 2. 分批读取服务器文件(read_file)
     * @return boolean
     */
    function customer_import_save() {
        $m_import = new \XWAM\Model\CustomerImportModel();
        $info = $m_import->upload_excel($error);
        if (!$info) {
            die($error);
        }
        $uid = $this->user_login_data['id'];
        $cache_name = 'customer_import_' . $uid;
        F($cache_name, $info);
        // 创建内存临时表
        $m_import->create_temp_table($uid, $this->site_id);
        redirect(U('customer_import_read'));
    }

    function customer_import_read() {
        // array ( 'name' => 'import-call-number (1).xls', 'type' => 'application/vnd.ms-excel', 'size' => 18944, 'key' => 'xlsFile', 'ext' => 'xls', 'savename' => '56f4f02c3232a.xls', 'savepath' => 'CustomerImport/2016-03-25/', 'filepath' => '/Uploads/CustomerImport/2016-03-25/56f4f02c3232a.xls', )
        $uid = $this->user_login_data['id'];
        $cache_name = 'customer_import_' . $uid;
        $fileinfo = F($cache_name);
        if (!$fileinfo) {
            die('文件路径无效,操作终止!');
        }
        //$path = sofn_path_rtoa('/xcrm' . $fileinfo['filepath']); //  $_SERVER['DOCUMENT_ROOT'] . __ROOT__ . '/Upload/'
        $m_import = new \XWAM\Model\CustomerImportModel();
        $m_import->read_excel('./' . $fileinfo['filepath'], $uid, $this->site_id);
        $info = $m_import->insert_db($uid, $this->site_id, $this->user_login_data['user_name']);
        $this->customer_import_finish($info);
    }

    function customer_import_finish($info) {
        $uid = $this->user_login_data['id'];
        $cache_name = 'customer_import_' . $uid;
        F($cache_name, null);
        // 丢弃内存临时表        
        $m_import = new \XWAM\Model\CustomerImportModel();
        $m_import->drop_temp_table($uid);
        $msg = sprintf('<span style="font-size:13px;">Excel文件包含 <strong style="font-weight:bold; color:#F00;">%d</strong> 条记录,过滤 <strong style="font-weight:bold; color:#F00;">%d</strong> 条电话号码重复记录,实际导入 <strong style="font-weight:bold; color:#F00;">%d</strong> 条记录.</span>', $info['count'], $info['delete'], $info['count'] - $info['delete']);
        $this->dialogClose(true, array('success' => $msg, 'ajax' => 6));
    }

}
