<?php

namespace XWAM\Controller;

/**
 * Description of SiteConfigController
 *
 * @since 1.0 <2015-10-20> SoChishun <14507247@qq.com> Added.
 */
class SiteConfigController extends AppbaseController {

    // 2016-4-21
    public function site_edit($site_id = 0, $user_name = '') {
        if (!$site_id) {
            $site_id = $this->site_id;
            $user_name = $this->user_login_data['user_name'];
        }
        $data = D('SiteConf')->find_site($site_id, $user_name);
        $this->assign('data', $data);
        $this->display();
    }

    public function site_edit_save() {
        $result = D('SiteConf')->save_site();
        $this->success('保存成功');
    }

    public function site_config() {
        $m_config = new \XWAM\Model\ConfigModel();
        $configs = $m_config->get_list();
        $this->assign('configs', $configs);
        $this->display();
    }

    public function site_config_save() {
        $m_config = new \XWAM\Model\ConfigModel();
        $result = $m_config->save_configs();
        $this->success('保存成功');
    }

}
