<?php

namespace XWAM\Controller;

/**
 * LinksController 类
 *
 * @since 1.0 <2015-11-10> SoChishun <14507247@qq.com> Added.
 */
class LinksController extends AppbaseController {

    function links_list_search() {
        $where['site_id'] = $this->site_id;
        return array('search' => array(), 'where' => $where);
    }

    function links_list() {
        $search_info = $this->links_list_search();
        $m_links = new \XWAM\Model\LinksModel();
        $list = $m_links->get_paging_list($show, array('where' => $search_info['where']));
        $this->assign('list', $list);
        $this->assign('page', $show);
        $this->display_cpp();
    }

    function change_sort($id, $sort) {
        $m_links = new \XWAM\Model\LinksModel();
        $this->ajaxReturn($m_links->change_sort($id, $sort));
    }

    function link_edit($id = 0) {
        $data = array();
        if ($id) {
            $m_links = new \XWAM\Model\LinksModel();
            $data = $m_links->find($id);
        }
        if (!$data) {
            $data = array('user_name' => $this->user_login_data['user_name'], 'site_id' => $this->user_login_data['site_id']);
        }
        $this->assign('data', $data);
        $this->display();
    }

    function link_edit_save() {
        $m_links = new \XWAM\Model\LinksModel();
        $result = $m_links->save_link($user_name);
        $this->dialogJump($result['status'], $result['info']);
    }

    function link_delete($id = '') {
        $m_links = new \XWAM\Model\LinksModel();
        $result = $m_links->delete_link($id);
        $this->ajaxReturn($result);
    }

}
