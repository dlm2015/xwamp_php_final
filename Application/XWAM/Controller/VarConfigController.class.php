<?php

namespace XWAM\Controller;

/**
 * Description of VarConfigController
 *
 * @since 1.0 <2015-10-20> SoChishun <14507247@qq.com> Added.
 */
class VarConfigController extends AppbaseController {

    function var_config_search() {
        $search = $_GET;
        if ($search['search_key']) {
            $where['_string'] = sprintf("name like '%s' or title like '%s'", $search['search_key'], $search['search_key']);
        }
        $where['site_id'] = $this->user_login_data['site_id'];
        return array('where' => $where, 'search' => $search);
    }

    function var_config() {
        $asearch = $this->var_config_search();
        $m_conf = new \XWAM\Model\ConfigModel();
        $list = $m_conf->get_paging_list($show, array('where' => $asearch['where'], 'order' => 'sort, id desc'), array('page_params' => $asearch['search']));
        $this->assign('list', $list);
        $this->assign('page', $show);
        $this->assign('search', $asearch['search']);
        $this->display_cpp();
    }

    function var_edit($id = '') {
        $data = array();
        $m_conf = new \XWAM\Model\ConfigModel();
        if ($id) {
            $data = $m_conf->find($id);
        }
        if (!$data) {
            $data = array('user_name' => $this->user_login_data['user_name'], 'site_id' => $this->user_login_data['site_id']);
        }
        $this->assign('data', $data);
        $this->display();
    }

    function var_edit_save() {
        $m_conf = new \XWAM\Model\ConfigModel();
        $result = $m_conf->save_var();
        $this->dialogJump($result['status'], $result['info']);
    }

    function var_delete($id = '') {
        $m_conf = new \XWAM\Model\ConfigModel();
        $result = $m_conf->remove_var($id);
        $this->ajaxReturn($result);
    }

    function change_status($id, $status) {
        $m_conf = new \XWAM\Model\ConfigModel();
        $this->ajaxReturn($m_conf->change_status($id, $status));
    }

    function change_sort($id, $sort) {
        $m_conf = new \XWAM\Model\ConfigModel();
        $this->ajaxReturn($m_conf->change_sort($id, $sort));
    }

}
