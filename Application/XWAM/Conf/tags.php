<?php

/**
 * 行为配置文件
 * @since 1.0 <2016-5-9> SoChishun <14507247@qq.com> Added.
 */
return array(
    // 配置开启多语言行为,启用多语言功能
    'app_begin' => array('Behavior\CheckLangBehavior'),
);
