<?php

/**
 * 主配置文件
 * @since 1.0 <2014-12-13> sutroon <14507247@qq.com> Added.
 * @since 2.0 2016-5-10 SoChishun 14507247@qq.com 针对TP3.2.3专项优化
 */
return array(
    /* 模板配置 */
    'TMPL_ACTION_ERROR' => 'XPage/dispatch_jump', // 默认错误跳转对应的模板文件
    'TMPL_ACTION_SUCCESS' => 'XPage/dispatch_jump', // 默认成功跳转对应的模板文件
    'TMPL_EXCEPTION_FILE' => 'XPage/exception.html', // 默认异常跳转对应的模板文件(MODULE_PATH . 'View/XPage/exception.html')

    /* 日志设置 */
    'LOG_RECORD' => TRUE, // 默认不记录日志(优化项*)
    'LOG_LEVEL' => 'INFO', // 允许记录的日志级别

    /* 调试设置 */
    'SHOW_PAGE_TRACE' => FALSE, // 开启页面调试
    'DB_DEBUG' => TRUE, // 数据库调试模式 开启后可以记录SQL日志(优化项*)

    /* 缓存设置 */
    'SESSION_PREFIX' => 'xcrm', // session 前缀
    'DATA_CACHE_PREFIX' => 'xcrm', // 缓存前缀,用以区分不同的应用，以免混淆。
    'DATA_CACHE_TIME' => 0, // 数据缓存有效期 0表示永久缓存
    'DB_CACHE_TIME' => 15, // 数据库查询缓存有效期(秒)

    /* 主数据库配置信息 */
    'DB_TYPE' => 'mysql', // 数据库类型
    'DB_HOST' => '127.0.0.1', // 服务器地址
    'DB_USER' => 'xcall', // 用户名
    'DB_PWD' => 'root!@#xcall098)(*Admin%^&', // 密码 *FD1FF7C8A00F770D0578167BC4BA51A7F236C1F6
    'DB_PORT' => 3306, // 端口
    'DB_NAME' => 'db_xcrm_v2', // 数据库名
    'DB_PREFIX' => '', // 数据库表前缀
    'DB_CHARSET' => 'utf8', // 字符集
    'DB_PARAMS' => array(\PDO::ATTR_CASE => \PDO::CASE_NATURAL), // 数据库连接参数(保持大小写)
    'DB_FIELDS_CACHE' => false, // 关闭字段缓存   

    /* 语言设置 */
    'LANG_SWITCH_ON' => true, // 启用语言包功能
    'LANG_AUTO_DETECT' => true, // 自动侦测语言 开启多语言功能后有效
    'LANG_LIST' => 'zh-cn,en-us', // 允许切换的语言列表 用逗号分隔
    'DEFAULT_LANG' => 'zh-cn', // 默认语言
    'VAR_LANGUAGE' => 'l', // 默认语言切换变量

    /* 模板内容替换设置 */
    'TMPL_PARSE_STRING' => array(
        '__THEME__' => __ROOT__ . '/Public/Theme/SUEUIX3/', // 主题路径
        '__SCRIPT__' => __ROOT__ . '/Public/Script/', // 增加新的JS类库路径替换规则
        '__CSS__' => __ROOT__ . '/Public/Css/', // CSS库路径替换规则
    ),
);
