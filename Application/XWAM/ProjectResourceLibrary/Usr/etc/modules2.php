<?php

/**
 * 模块总配置文件
 * @since 1.0 <2015-9-16> SoChishun Added.
 */
return array(
    /* 平台框架基本url设置 */
    'base_url' => array(
        'login_url' => array('Login/index', 'PXCallSystem'), // 登录地址
        'logout_url' => array('Login/logout', 'PXCallSystem'), // 注销地址
    ),
    /* 平台包含的所有模块信息 */
    'modules' => array(
        'PXCallAdmin' => array(
            'enable' => true, // 是否可用
            'install_time' => '', // 安装时间
            'upgrate_time' => '', // 最后更新时间
            'uninstall_time' => '', // 卸载时间
        // 其他版本号等更多信息直接读取目录中的安装配置文件
        ),
    ),
);
