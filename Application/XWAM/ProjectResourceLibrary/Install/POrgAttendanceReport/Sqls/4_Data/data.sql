-- 清理数据
truncate table t_porg_user;
truncate table t_porg_attendance_report;
truncate table t_porg_attendance_report_detail;
truncate table t_porg_attendance_card_records;
truncate table t_porg_attendance_leave_form_records;
-- 新增用户
insert into t_porg_user (user_name, name, password, type_name, role_ids, status) values ('admin','管理员','123456','SUPERADMIN','*', 1);
-- 新增部门
insert into think_auth_department (`id`, `title`, `pid`, `code`, `rules`, `sort`, `remark`, `status`, `create_time`, `site_id`) VALUES ('1', '综合部', '0', '', '', '0', '', '1', '2016-05-13 10:02:02', '0'),('2', '财务', '0', '', '', '0', '', '1', '2016-05-13 10:02:02', '0'),('3', '供电部', '0', '', '', '0', '', '1', '2016-05-13 10:02:02', '0'),('4', '港口变', '0', '', '', '0', '', '1', '2016-05-13 10:02:02', '0'),('5', '兴港变', '0', '', '', '0', '', '1', '2016-05-13 10:02:02', '0'),('6', '业务部', '0', '', '', '0', '', '1', '2016-05-13 10:02:02', '0'),('7', '安技部', '0', '', '', '0', '', '1', '2016-05-13 10:02:02', '0'),('8', '通信部', '0', '', '', '0', '', '1', '2016-05-13 10:02:02', '0');
-- 批量设置员工部门
update t_porg_user set department_ids='1' where department_ids='' and name in ('罗丽云','曹玲毅','吴青青','张宝莲','连柽鑫');
update t_porg_user set department_ids='2' where department_ids='' and name in ('王榕','陈惠贞','章祺弘','许文营','唐潇');
update t_porg_user set department_ids='3' where department_ids='' and name in ('陈汉忠','杨志民','叶玮姣','曾细玲','黄灿文','黄伟','白耿昌','陈毅恩');
update t_porg_user set department_ids='4' where department_ids='' and name in ('傅凡凡','李金治','纪华强','刘景乐','冯毅杰','刘敬周','陈武彬','冯银丽','康志伟','郑月芳','王志凤','傅雅华','黄芳','陈艳青','张雪娇','康红梅','潘雅燕','孙建兴');
update t_porg_user set department_ids='5' where department_ids='' and name in ('曾荣华','李达伟','刘惠萍','张秋薏','林盈','陈清珍','林俊霆','周丽虹','阮丽容','陈亚云','陈雅惠','黄媛媛','支军宏','洪玲玲','张澜','王丽蓉','林振坤','林文斌','吴铭山','李王争','黄灿文');
update t_porg_user set department_ids='6' where department_ids='' and name in ('苏娜贞','柳爱华','刘志勇','林玲','林雪琳','张石羡珠','张加荣','郭雁峰','刘怀茂');
update t_porg_user set department_ids='7' where department_ids='' and name in ('林水民','许晓晔','胡智峰','陈毅英');
update t_porg_user set department_ids='8' where department_ids='' and name in ('李东苏','潘巍','宋伟坚','周文福','周全宝','傅志婷','刘敏','袁军','陈勇锋','林颖','王涛');
-- 权限节点
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (1,'','设置','0','POrgAttendance','M1_SZ',1,'','M',1,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (2,'','系统设置','1','POrgAttendance','SZ_M2_XTSZ',2,'','M',2,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (3,'Conf-WorkTimeSetting','上班时间设置','2','POrgAttendance','SZ_M3_SBSJSZ',3,'Conf/WorkTimeSetting','M',3,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (4,'Conf-WorkDaySetting','工作日设置','2','POrgAttendance','SZ_M3_GZRSZ',4,'Conf/WorkDaySetting','M',3,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (5,'','用户','0','POrgAttendance','M1_YH',5,'','M',1,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (6,'','员工管理','5','POrgAttendance','YH_M2_YGGL',6,'','M',2,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (7,'Employee-EmployeeList','员工列表','6','POrgAttendance','YH_M3_YGLB',7,'Employee/EmployeeList','M',3,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (8,'Employee-EmployeeAdd','新增员工','7','POrgAttendance','YH_YHGL_XZYG',8,'','O',3,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (9,'Employee-EmployeeEdit','编辑员工','7','POrgAttendance','YH_YHGL_BJYG',9,'','O',3,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (10,'Employee-EmployeeDelete','删除员工','7','POrgAttendance','YH_YHGL_SCYG',10,'','O',3,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (11,'Department-department_list','部门管理','6','POrgAttendance','YH_M3_BMGL',11,'Department/DepartmentList','M',3,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (12,'','报表','0','POrgAttendance','M1_BB',12,'','M',1,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (13,'','考勤数据导入','12','POrgAttendance','BB_M2_KQSJDR',13,'','M',2,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (14,'Records-CardRecords','打卡记录','13','POrgAttendance','BB_M3_DRDKJL',14,'Records/CardRecords','M',3,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (15,'Records-LeaveRecords','请假记录','13','POrgAttendance','BB_M3_DRQJJL',15,'Records/LeaveRecords','M',3,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (16,'','考勤汇总统计','12','POrgAttendance','BB_M2_KQHZTJ',16,'','M',2,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (17,'Report-MenjinReport','门禁报表','16','POrgAttendance','BB_M3_MJBB',17,'Report/MenjinReport','M',3,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (18,'Report-ReportExport','报表导出','17','POrgAttendance','BB_M3_BBDC',18,'','O',3,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (19,'Report-ReportSearch','报表查询','17','POrgAttendance','BB_M3_BBCX',19,'','O',3,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (20,'Report-KaoqindengjiReport','考勤登记表','16','POrgAttendance','BB_M3_KQDJB',20,'Report/KaoqindengjiReport','M',3,1);
insert into think_auth_rule (`id`, `name`, `title`, `pid`, `addon`, `code`, `sort`, `url`, `type`, `level`,`status`) values (21,'Report-TongjiReport','考勤统计表','16','POrgAttendance','BB_M3_KQTJB',21,'Report/TongjiReport','M',3,1);
-- 通用配置
insert into t_porg_config (`id`, `name`, `title`, `type`, `group`, `prop`, `content`, `value`, `remark`, `status`, `sort`, `create_time`, `update_time`) VALUES ('1', 'WORKDAYS_2016', '', 'C', '', 'C', '', '[{\"year\":2016,\"month\":1,\"days\":31,\"workdays\":22,\"holiday\":\"\"},{\"year\":2016,\"month\":2,\"days\":29,\"workdays\":23,\"holiday\":\"\"},{\"year\":2016,\"month\":3,\"days\":31,\"workdays\":22,\"holiday\":\"\"},{\"year\":2016,\"month\":4,\"days\":30,\"workdays\":23,\"holiday\":\"\"},{\"year\":2016,\"month\":5,\"days\":31,\"workdays\":21,\"holiday\":\"\"},{\"year\":2016,\"month\":6,\"days\":30,\"workdays\":22,\"holiday\":\"\"},{\"year\":2016,\"month\":7,\"days\":31,\"workdays\":23,\"holiday\":\"\"},{\"year\":2016,\"month\":8,\"days\":31,\"workdays\":21,\"holiday\":\"\"},{\"year\":2016,\"month\":9,\"days\":30,\"workdays\":22,\"holiday\":\"\"},{\"year\":2016,\"month\":10,\"days\":31,\"workdays\":23,\"holiday\":\"\"},{\"year\":2016,\"month\":11,\"days\":30,\"workdays\":21,\"holiday\":\"\"},{\"year\":2016,\"month\":12,\"days\":31,\"workdays\":21,\"holiday\":\"\"}]', '', '0', '0', '2016-01-28 20:32:50', NULL), ('2', 'WORKTIME', '', 'C', '', 'C', '', '{\"am_s\":\"08:15:00\",\"am_s1\":\"06:00:00\",\"am_s2\":\"10:00:00\",\"am_e\":\"12:00:00\",\"am_e1\":\"10:00:01\",\"am_e2\":\"13:30:00\",\"pm_s\":\"15:15:00\",\"pm_s1\":\"14:00:00\",\"pm_s2\":\"16:00:00\",\"pm_e\":\"18:00:00\",\"pm_e1\":\"16:00:01\",\"pm_e2\":\"23:00:00\"}', '', '0', '0', '2016-01-28 20:36:30', NULL);
