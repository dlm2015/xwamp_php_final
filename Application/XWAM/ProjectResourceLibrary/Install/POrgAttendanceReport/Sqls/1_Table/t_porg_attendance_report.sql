drop table if exists t_porg_attendance_report;
create table if not exists t_porg_attendance_report(
    id int auto_increment primary key comment '主键编号',	
    time_ym char(7) not null default '' comment '年月',
    attendance_no varchar(16) not null comment '考勤卡号码',
    personal_name varchar(32) not null comment '员工姓名',
    month_workdays smallint not null default 0 comment '正常出勤天数',
    chuqin_days smallint not null default 0 comment '实际出勤天数',
    kuanggong_days smallint not null default 0 comment '旷工天数',
    chidao_count smallint not null default 0 comment '迟到次数',
    zaotui_count smallint not null default 0 comment '早退次数',
    qingjia_days smallint not null default 0 comment '请假天数',
    gongjia_days smallint not null default 0 comment '公假',
    xuexijia_days smallint not null default 0 comment '学习假',
    gongchu_days smallint not null default 0 comment '公出',
    nianjia_days smallint not null default 0 comment '年假',
    tiaoxiu_days smallint not null default 0 comment '调休',
    shijia_days smallint not null default 0 comment '事假',
    bingjia_days smallint not null default 0 comment '病假',
    hunjia_days smallint not null default 0 comment '婚假',
    chanjia_days smallint not null default 0 comment '产假',
    gongshang_days smallint not null default 0 comment '工伤',
    shangjia_days smallint not null default 0 comment '丧假',
    qita_days smallint not null default 0 comment '其他',
    ri_days smallint not null default 0 comment '日班',
    ye_days smallint not null default 0 comment '夜班',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    INDEX idx_attendance_no (attendance_no),
    INDEX idx_time_ym (time_ym),
    INDEX idx_personal_name(personal_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '考勤报告表(月份汇总表)\r\n@since 1.0 <2015-12-25> SoChishun <14507247@qq.com> Added.';

-- 从每日考勤表更新统计到统计表
/*
UPDATE t_porg_attendance_report r LEFT JOIN t_porg_attendance_report_detail d ON d.attendance_no=r.attendance_no
      SET r.chuqin_days=(SELECT COUNT(id) FROM t_porg_attendance_report_detail WHERE attendance_no=r.attendance_no and status_am='勤' and status_pm='勤' and time_ym='2015-11')
      WHERE r.attendance_no=d.attendance_no and r.time_ym=d.time_ym and r.time_ym='2015-11';
*/