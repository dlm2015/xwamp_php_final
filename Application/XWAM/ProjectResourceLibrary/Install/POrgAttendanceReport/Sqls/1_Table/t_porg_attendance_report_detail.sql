/*
每天固定4条记录
多次打卡只取符合条件的最优一条记录
*/
drop table if exists t_porg_attendance_report_detail;
create table if not exists t_porg_attendance_report_detail(
    id int auto_increment primary key comment '主键编号',
    personal_name varchar(32) not null comment '员工姓名',
    attendance_no varchar(16) not null comment '考勤卡号码',
    time_ym char(7) not null default '' comment '年月',
    time_ymd date comment '年月日',
    time_day smallint not null default 0 comment '日期(1-30号)',
    time_weekday smallint not null default 0 comment '星期几',
    am_s varchar(9) not null default '' comment '早上上班考勤值,这是报表直接输出的值(09:00:00,请假类别)',
    am_e varchar(9) not null default '' comment '早上下班考勤值,这是报表直接输出的值(09:00:00,请假类别)',
    pm_s varchar(9) not null default '' comment '下午上班考勤值,这是报表直接输出的值(09:00:00,请假类别)',
    pm_e varchar(9) not null default '' comment '下午下班考勤值,这是报表直接输出的值(09:00:00,请假类别)',
    time_am_s time comment '早上上班考勤时间',
    time_am_e time comment '早上下班考勤时间',
    time_pm_s time comment '下午上班考勤时间',
    time_pm_e time comment '下午下班考勤时间',
    status_am_s char(1) not null default '' comment '早上上班考勤状态(Y:异常,迟到或早退)',
    status_am_e char(1) not null default '' comment '早上下班考勤状态(Y:异常,迟到或早退)',
    status_pm_s char(1) not null default '' comment '下午上班考勤状态(Y:异常,迟到或早退)',
    status_pm_e char(1) not null default '' comment '下午下班考勤状态(Y:异常,迟到或早退)',
    status_am varchar(1) not null comment '早上考勤状态(勤;假)',
    status_pm varchar(1) not null comment '下午考勤状态(勤;假)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    INDEX idx_time_ymd (time_ymd),
    INDEX idx_time_ym (time_ym),
    INDEX idx_attendance_no(attendance_no),
    INDEX idx_personal_name(personal_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '考勤报告每日明细表(月份明细表-每天汇总表)\r\n@since 1.0 <2015-12-25> SoChishun <14507247@qq.com> Added.';

-- 从请假表更新请假类别到每日考勤表
-- update t_porg_attendance_report_detail d inner join t_porg_attendance_leave_form_records l set d.status_am=l.leave_am,d.status_pm=l.leave_pm where d.attendance_no=l.attendance_no and d.time_ymd=l.time_ymd and d.time_ym='2015-11';
-- 更新早上出勤状态
-- update t_porg_attendance_report_detail set status_am='勤' where time_ymd='2015-11' and time_am_s is not null and time_am_e is not null and status_am_s='' and status_am_e='';
-- 更新下午出去状态
-- update t_porg_attendance_report_detail set status_pm='勤' where time_ymd='2015-11' and time_pm_s is not null and time_pm_e is not null and status_pm_s='' and status_pm_e='';