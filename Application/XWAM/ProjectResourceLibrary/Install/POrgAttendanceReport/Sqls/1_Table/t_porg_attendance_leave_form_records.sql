drop table if exists t_porg_attendance_leave_form_records;
create table if not exists t_porg_attendance_leave_form_records (
    id int auto_increment primary key comment '主键编号',
    file_id int not null default 0 comment '文件表编号',
    attendance_no varchar(16) not null comment '考勤卡号码',
    personal_name varchar(32) not null default '' comment '员工姓名',
    time_ym char(7) not null default '' comment '年月',
    time_ymd date comment '年月日',
    time_day smallint not null default 0 comment '日期',
    leave_am varchar(4) not null default '' comment '早上请假类别',
    leave_pm varchar(4) not null default '' comment '下午请假类别',
    request_time datetime comment '申请时间',
    confirm_time datetime comment '审核时间',
    status smallint not null default 0 comment '状态(0=待批准,1=批准,4=退回,5=取消)',
    INDEX idx_attendance_no (attendance_no),
    INDEX idx_personal_name (personal_name),
    INDEX idx_time_ymd (time_ymd),
    INDEX idx_time_ym (time_ym)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '考勤请假手工填表记录\r\n@since 1.0 <2015-12-25> SoChishun <14507247@qq.com> Added.';
