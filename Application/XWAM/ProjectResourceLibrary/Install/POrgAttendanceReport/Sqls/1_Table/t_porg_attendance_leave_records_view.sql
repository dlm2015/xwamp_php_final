drop table if exists t_porg_attendance_leave_records_view;
create table if not exists t_porg_attendance_leave_records_view (
    id int auto_increment primary key comment '主键编号',
    file_id int not null default 0 comment '文件表编号',
    attendance_no varchar(16) not null comment '考勤卡号码',
    personal_name varchar(32) not null default '' comment '员工姓名',
    department_id int not null default 0 comment '部门编号(冗余)',
    department_name varchar(32) not null default '' comment '部门名称(冗余)',
    time_ym char(7) not null default '' comment '年月',
    month_days smallint not null default 0 comment '当月总天数(冗余)',
    meridiem char(2) not null default '' comment '上午或下午(AM/PM)',
    day1_leave varchar(4) not null default '' comment '1号M午请假类别',
    day2_leave varchar(4) not null default '' comment '2号M午请假类别',
    day3_leave varchar(4) not null default '' comment '3号M午请假类别',
    day4_leave varchar(4) not null default '' comment '4号M午请假类别',
    day5_leave varchar(4) not null default '' comment '5号M午请假类别',
    day6_leave varchar(4) not null default '' comment '6号M午请假类别',
    day7_leave varchar(4) not null default '' comment '7号M午请假类别',
    day8_leave varchar(4) not null default '' comment '8号M午请假类别',
    day9_leave varchar(4) not null default '' comment '9号M午请假类别',
    day10_leave varchar(4) not null default '' comment '10号M午请假类别',
    day11_leave varchar(4) not null default '' comment '11号M午请假类别',
    day12_leave varchar(4) not null default '' comment '12号M午请假类别',
    day13_leave varchar(4) not null default '' comment '13号M午请假类别',
    day14_leave varchar(4) not null default '' comment '14号M午请假类别',
    day15_leave varchar(4) not null default '' comment '15号M午请假类别',
    day16_leave varchar(4) not null default '' comment '16号M午请假类别',
    day17_leave varchar(4) not null default '' comment '17号M午请假类别',
    day18_leave varchar(4) not null default '' comment '18号M午请假类别',
    day19_leave varchar(4) not null default '' comment '19号M午请假类别',
    day20_leave varchar(4) not null default '' comment '20号M午请假类别',
    day21_leave varchar(4) not null default '' comment '21号M午请假类别',
    day22_leave varchar(4) not null default '' comment '22号M午请假类别',
    day23_leave varchar(4) not null default '' comment '23号M午请假类别',
    day24_leave varchar(4) not null default '' comment '24号M午请假类别',
    day25_leave varchar(4) not null default '' comment '25号M午请假类别',
    day26_leave varchar(4) not null default '' comment '26号M午请假类别',
    day27_leave varchar(4) not null default '' comment '27号M午请假类别',
    day28_leave varchar(4) not null default '' comment '28号M午请假类别',
    day29_leave varchar(4) not null default '' comment '29号M午请假类别',
    day30_leave varchar(4) not null default '' comment '30号M午请假类别',
    day31_leave varchar(4) not null default '' comment '31号M午请假类别',
    INDEX idx_attendance_no (attendance_no),
    INDEX idx_personal_name (personal_name),
    INDEX idx_time_ym (time_ym)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '考勤请假手工填表记录\r\n@since 1.0 <2015-12-25> SoChishun <14507247@qq.com> Added.';
-- 每个员工每天2条记录(AM/PM)