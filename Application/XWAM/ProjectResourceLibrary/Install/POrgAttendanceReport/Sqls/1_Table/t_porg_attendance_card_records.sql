drop table if exists t_porg_attendance_card_records;
create table if not exists t_porg_attendance_card_records (
    id int auto_increment primary key comment '主键编号',
    file_id int not null default 0 comment '文件表编号',
    attendance_no varchar(16) not null comment '考勤卡号码',
    personal_name varchar(32) not null default '' comment '员工姓名',
    address varchar(64) not null default '' comment '考勤机地点(如:前门_门1)',
    recording_time datetime not null comment '打卡时间',
    time_ymd date comment '日期(年月日)',
    time_hms time comment '时间(时分秒)',
    time_ym char(7) not null default '' comment '年月',
    event_name varchar(32) not null default '' comment '事件类型(如:刷卡开门;指纹/指静脉比对通过)',
    event_source varchar(32) not null default '' comment '事件源(如:前门;后门)',
    INDEX idx_attendance_no (attendance_no),
    INDEX idx_recording_time (recording_time),
    INDEX idx_time_ymd (time_ymd),
    INDEX idx_time_hms (time_hms),
    INDEX idx_time_ym (time_ym),
    INDEX idx_personal_name (personal_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '考勤打卡记录\r\n@since 1.0 <2015-12-25> SoChishun <14507247@qq.com> Added.';
