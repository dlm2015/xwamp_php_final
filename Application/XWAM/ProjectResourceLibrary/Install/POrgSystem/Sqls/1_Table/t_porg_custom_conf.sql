drop table if exists t_porg_custom_conf;
create table if not exists t_porg_custom_conf (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null default '' comment '配置标题',
    `group` varchar(32) not null default '' comment '配置分组',
    content varchar(128) not null default '' comment '配置内容',
    lcontent varchar(12800) not null default '' comment '配置值(存储大量数据)',
    remark varchar(32) not null default '' comment '配置说明',
    user_name varchar(32) not null default '' comment '创建人用户名',
    status smallint not null default 0 comment '状态',
    sort smallint not null default 0 comment '排序',
    create_time timestamp not null default current_timestamp comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    update_time datetime comment '更新时间',
    INDEX idx_title (`title`),
    INDEX `idx_group` (`group`),
    INDEX idx_user_name (user_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户自定义配置表\r\n@since 1.0 <2016-5-25> SoChishun <14507247@qq.com> Added.';

