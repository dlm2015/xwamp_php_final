drop table if exists t_porg_site_content;
create table if not exists t_porg_site_content (
    id int auto_increment primary key comment '主键编号',
    site_id int not null comment '站点编号',
    `name` varchar(32) not null comment '键名(ABOUT_US:关于我们;CONTACT_US:联系我们;KEYWORD:关键词;DESCRIPTION:描述;)',
    title varchar(64) not null default '' comment '标题',
    content varchar(20000) not null default '' comment '内容',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime not null default 0 comment '更新时间',
    INDEX idx_site_id (site_id),
    INDEX idx_name (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '站点内容表(包含关于我们、联系我们等单页资料)\r\n@since 1.0 <2015-5-19> SoChishun <14507247@qq.com> Added.';
