-- database:db_xcrm_common
drop table if exists t_porg_site_conf;
create table if not exists t_porg_site_conf (
    id int auto_increment primary key comment '主键编号',
    -- 站点配置
    site_title varchar(32) not null comment '站点名称(网站标题)',
    site_code varchar(16) not null comment '站点代号(如:XCALL;TTF)',
    site_app_name varchar(32) not null default 'XWAMP' comment '应用程序名称',
    site_app_version varchar(16) not null default '1.0.0.0' comment '应用程序版本',
    site_status char(1) not null default 'Y' comment '网站运行状态(用户控制)',
    site_closed_announcement varchar(255) not null default '' comment '停业通知',
    site_logo_url varchar(255) not null default '' comment '图标地址',
    site_icp_code varchar(32) not null default '' comment 'ICP备案号',
    site_summary varchar(128) not null default '' comment '站点简介',
    site_home_url varchar(128) not null default 'index' comment '主页地址',
    site_login_url varchar(128) not null default 'login' comment '后台登录路径',
    site_db_sharding char(1) not null default '' comment '数据库分片(分库分表,S=单机单库,M=单机多库,D=分布式数据库)',
    site_sub_path varchar(16) not null default '/xcrm' comment '应用程序子目录名称,注意结尾不能为斜杠"/",如:/xcrm',
    -- 业务配置
    site_category_id int not null default 0 comment '类别编号',
    site_company_name varchar(32) not null default '' comment '公司名称',
    -- 统计配置
    visit_count int not null default 0 comment '浏览次数',
    -- 通用配置
    session_prefix varchar(32) not null default '' comment '命名前缀(用于cookie或session等名称前缀)',
    encrypt_key varchar(16) not null default 'sue123!@#' comment '加密密匙',
    -- 界面配置
    ui_login_captcha_status char(1) not null default 'Y' comment '是否显示登录验证码',
    ui_aside_status char(1) not null default 'Y' comment '是否显示侧边栏',
    -- 内容配置
    content_draft_aotosave_interval smallint not null default 0 comment '自动保存草稿时间',
    content_draftbox_status char(1) not null default 'N' comment '是否开启草稿功能',
    content_list_rows smallint not null default 25 comment '主列表每页记录数',
    content_reply_list_rows smallint not null default 25 comment '回复列表每页条数',
    -- 访问配置
    visit_allow_register char(1) not null default 'Y' comment '是否允许用户注册',
    -- 数据配置
    data_backup_path varchar(255) not null default '' comment '数据库备份根路径',
    data_backup_part_size varchar(16) not null default '2M' comment '数据库备份卷大小(如:10M;200K)',
    -- 管理员配置
    admin_email varchar(32) not null default '' comment '管理员电子邮件',
    admin_name varchar(32) not null default '' comment '管理员姓名',
    admin_telphone varchar(32) not null default '' comment '管理员联系电话',
    admin_mobile varchar(32) not null default '' comment '管理员手机号码',
    admin_address varchar(255) not null default '' comment '管理员联系地址',
    -- CTI配置
    cti_screenpop_mode char(1) not null default 'M' comment '来电弹屏模式(D:直接弹屏;J:直接跳转客户;M:消息提醒;F:关闭来电弹屏)',
    cti_screenpop_duplicate_expire smallint not null default 180 comment '来电弹屏同号码弹屏间隔时间(秒)',
    -- 其他配置
    user_name varchar(32) not null default '' comment '创建人用户名(支持一人多站)',
    sort smallint not null default 0 comment '排序',
    ex_tags varchar(16) not null default '' comment '扩展标签(SHOP:网店,BLOG:博客)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间',
    status smallint not null default 0 comment '状态(0:待审核;1:运行中;4:关停)',
    INDEX idx_site_title (site_title),
    INDEX idx_site_category_id (site_category_id),
    INDEX idx_user_name (user_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '站点配置表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.\r\n@since 1.1<2015-3-26> SoChishun <14507247@qq.com> tuser_shop改为tsite_config.\r\n@since 1.2<2015-5-19> SoChishun <14507247@qq.com> 拆分部分大字段到新的tsite_infomation表.\r\n@since 2.0<2016-4-15> SoChishun <14507247@qq.com> 重构,把配置文件和conf垂直表合并到这个平面表';