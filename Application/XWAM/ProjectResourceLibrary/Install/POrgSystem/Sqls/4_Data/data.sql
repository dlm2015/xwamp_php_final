/*
默认数据
@since 1.0 <2015-10-19> SoChishun Added.
*/
-- 变量配置表
-- 基本配置
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('WEB_SITE_TITLE','网站标题','C','B','O','','','网站标题前台显示标题');
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('WEB_SITE_LOGO','网站LOGO','C','B','O','','','');
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('WEB_SITE_ICP','网站备案号','C','B','O','','','网站底部显示的备案号，如“ICP备xxxx号');
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('WEB_SITE_KEYWORD','网站关键字','C','B','O','','','网站搜索引擎关键字');
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('WEB_SITE_DESCRIPTION','网站描述','T','B','O','','','网站搜索引擎描述');
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('WEB_SITE_CLOSE','关闭站点','E','B','O','Y:关闭\nN:开放','N','站点关闭后其他用户不能访问，管理员可以正常访问');
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('CLOSED_ANNOUNCEMENT','站点关闭公告','C','B','O','','抱歉,网站正在维护,请您稍后访问~','站点关闭后，用户的提示消息');
-- 内容配置
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('DRAFT_AOTOSAVE_INTERVAL','自动保存草稿时间','N','C','O','','60','自动保存草稿的时间间隔，单位：秒');
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('OPEN_DRAFTBOX','是否开启草稿功能','E','C','O','Y:开启\nN:关闭','N','新增文章时的草稿功能配置');
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('LIST_ROWS','后台每页记录数','N','C','O','','30','');
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('REPLY_LIST_ROWS','回复列表每页条数','N','C','O','','10','');
-- 用户配置
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('USER_ALLOW_REGISTER','是否允许用户注册','E','U','O','Y:允许\nN:禁止','Y','是否开放用户注册');
-- 系统配置
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('HOME_URL','后台首页路径','C','S','O','','POrgSystem:Home/index','格式：[{插件名称}:]{控制器名称}/{动作名称}');
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('LOGIN_URL','后台登录路径','C','S','O','','POrgUser:Login/index','格式：[{插件名称}:]{控制器名称}/{动作名称}');
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('LOGOUT_URL','后台登录路径','C','S','O','','POrgUser:Login/logout','格式：[{插件名称}:]{控制器名称}/{动作名称}');
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('DATA_BACKUP_PATH','数据库备份根路径','C','S','O','','./Data/','');
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('DATA_BACKUP_PART_SIZE','数据库备份卷大小','N','S','O','','20971520','');
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('ADMIN_EMAIL','系统管理员邮箱帐号','C','S','O','','','用于通过电子邮箱接收系统信息');
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('ADMIN_MOBILE','系统管理员手机号码','C','S','O','','','用于通过短信接收系统信息');
-- CTI配置 2016-2-1
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('SCREENPOP_MODE','来电弹屏模式','A','S','O','J:直接跳转客户\nM:消息提醒\nF:关闭来电弹屏','M','来电弹屏模式');
insert into t_porg_config (`name`, title, `type`, `group`, prop, content, `value`, remark) values ('SCREENPOP_DUPLICATE_EXPIRE','来电弹屏同号码弹屏间隔时间','N','S','O','','120','来电弹屏同号码弹屏间隔时间');

update t_porg_config set status=1 where `name` <> 'WEB_SITE_LOGO';
