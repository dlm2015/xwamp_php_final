<?php

/*
 * 权限生成工具
 * <br />运行方式：http://localhost:8801/Tool/Incubator/SuPermissionSql_PHP_1_0/SoPermissionSql.php
 * 支持字段：[string]title:标题,[string]name:名称,[string]code:代码,[string]type:节点类型(M=菜单,O=操作,E=页面元素),[boolean]enable:是否可用,[string]url:链接地址,[array]children:子项目,[string]comment:注解
 * 说明：默认ismenu=true,如要特殊声明，需强制加上ismenu=false
 * @since 1.0 2014-7-11 by sutroon
 * @since 2.0 <2015-4-23> SoChishun 重新组织url
 * @since 3.0 <2015-7-30> SoChishun 新增code字段
 * @since 4.0 <2015-9-19> SoChishun 重构以适合RBAC自动拦截规则
 */
return array(
    /* 客户模块 */
    array(
        'title' => '客户',
        'code' => 'M1_KH',
        'children' => array(
            array(
                'title' => '客户管理',
                'code' => 'POrg_KH_M2_KHGL',
                'children' => array(
                    array('name' => 'Customer-customer_list', 'level' => '2', 'title' => '客户管理', 'code' => 'YH_M3_KHGL', 'url' => 'Customer/customer_list',
                        'O' => array(
                            array('name' => 'Customer-customer_edit', 'level' => '3', 'title' => '新增客户', 'code' => 'KH_KHGL_XZKH', 'enable' => false),
                            array('name' => 'Customer-customer_edit', 'level' => '3', 'title' => '编辑客户', 'code' => 'KH_KHGL_BJKH', 'enable' => false),
                            array('name' => 'Customer-customer_recycle', 'level' => '3', 'title' => '放入回收站', 'code' => 'KH_KHGL_FRHSZ', 'enable' => false),
                            array('name' => 'Customer-customer_delete', 'level' => '3', 'title' => '删除客户', 'code' => 'KH_KHGL_SCKH', 'enable' => false),
                        ),
                    ),
                    array('name' => 'CustomerMark-customer_list', 'level' => '2', 'title' => '标记客户', 'code' => 'KH_M3_BJKHGL', 'url' => 'CustomerMark/customer_list','enable'=>false),
                    array('name' => 'CustomerInterested-customer_list', 'level' => '2', 'title' => '意向客户', 'code' => 'KH_M3_BJKHGL', 'url' => 'CustomerInterested/customer_list'),
                    array('name' => 'CustomerAppointment-customer_list', 'level' => '2', 'title' => '预约客户', 'code' => 'KH_M3_YYKHGL', 'url' => 'CustomerAppointment/customer_list'),
                    array('name' => 'CustomerDispatch-customer_list', 'level' => '2', 'title' => '调度客户', 'code' => 'KH_M3_DDKHGL', 'url' => 'CustomerDispatch/customer_list'),
                    array('name' => 'CustomerOverdue-customer_list', 'level' => '2', 'title' => '超期预警', 'code' => 'KH_M3_CQKHGL', 'url' => 'CustomerOverdue/customer_list'),
                    array('name' => 'CustomerPublic-customer_list', 'level' => '2', 'title' => '公共池', 'code' => 'KH_M3_GGCGL', 'url' => 'CustomerPublic/customer_list'),
                    array('name' => 'CustomerRecycle-customer_list', 'level' => '2', 'title' => '回收站', 'code' => 'KH_M3_HSZGL', 'url' => 'CustomerRecycle/customer_list'),
                ),
            ),
            array(
                'title' => '客户配置',
                'code' => 'POrg_KH_M2_KHPZ',
                'children' => array(
                    array('name' => 'CustomerAttr-attr_list', 'level' => '2', 'title' => '字段管理', 'code' => 'KH_M3_ZDGL', 'url' => 'CustomerAttr/attr_list'),
                ),
            ),
            array(
                'title' => '录音管理',
                'code' => 'POrg_KH_M2_LYGL',
                'children' => array(
                    array('name' => 'CallRecord-record_list', 'level' => '2', 'title' => '通话记录', 'code' => 'KH_M3_THJLGL', 'url' => 'CallRecord/record_list'),
                    array('name' => 'CallRecord-share_list', 'level' => '2', 'title' => '录音分享', 'code' => 'KH_M3_LYFXGL', 'url' => 'CallRecord/share_list'),
                ),
            ),
        ),
    ),
    /* /客户模块 */
);