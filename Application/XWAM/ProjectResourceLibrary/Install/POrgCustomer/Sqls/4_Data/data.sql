/*
默认数据
@since 1.0 <2015-10-20> SoChishun Added.
*/
-- 用户表
truncate table t_porg_user;
truncate table think_auth_group;
truncate table think_auth_group_access;
insert into t_porg_user (user_name, password, type_name, regist_ip, status) values ('sa','123456', 'SYSTEMADMIN', '127.0.0.1', 1);
insert into t_porg_user (user_name, password, type_name, regist_ip, status) values ('admin','123456', 'SITEADMIN', '127.0.0.1', 1);
insert into think_auth_group (title, rules) values ('系统管理员','*');
insert into think_auth_group (title, rules) values ('站点创始人','*');
insert into think_auth_group_access (uid, group_id) values (1,1);
insert into think_auth_group_access (uid, group_id) values (2,2);