drop table if exists t_porg_customer_import_u0;
create table if not exists t_porg_customer_import_u0 (
    customer_id int primary key comment '主键编号',
    title varchar(16) not null comment '批次标题(dHis+uid)',
    `name` varchar(16) not null default '' comment '客户姓名',
    telphone varchar(32) not null default '' comment '电话号码',
    sex varchar(6) not null default '' comment '性别(女;男;未知)',
    address varchar(200) not null default '' comment '联系地址',
    INDEX idx_telphone (telphone),
    INDEX idx_title (title)
) ENGINE=MEMORY  default CHARSET=utf8 comment '客户导入临时表\r\n1.0 2016-2-25 SoChishun Added.\r\n2.0 2016-5-11 SoChishun 重构.';