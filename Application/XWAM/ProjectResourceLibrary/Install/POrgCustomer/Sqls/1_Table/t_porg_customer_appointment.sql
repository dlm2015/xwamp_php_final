drop table if exists t_porg_customer_appointment;
create table if not exists t_porg_customer_appointment(
    id int auto_increment primary key comment '主键编号',
    customer_id int not null comment '客户表主键编号',
    appointment_type varchar(8) not null default '' comment '预约类型(USER=人工预约,ORDER=订单回访)',
    appointment_time datetime comment '预约时间',
    appointment_remark varchar(160) not null default '' comment '预约备注',
    review_time datetime comment '回访时间',
    review_result varchar(180) not null default '' comment '回访结果(拒绝;成功;对方为老人或小孩;接通后即结束通话;接通后不说话)',
    review_remark varchar(160) not null default '' comment '回访备注',
    `status` smallint not null default 0 comment '回访状态(0=未回访,1=回访成功,2=回访失败,4=放弃回访)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX idxu_main (customer_id,appointment_type,appointment_time)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '客户预约表\r\n@since 1.0 <2014-12-24> sutroon <14507247@qq.com> Added.';
