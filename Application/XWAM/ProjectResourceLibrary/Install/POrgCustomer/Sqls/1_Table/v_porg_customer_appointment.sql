-- 视图字段构建语句 select concat(', c.',COLUMN_NAME) from information_schema.`COLUMNS` where TABLE_SCHEMA='db_xwam_v1' and TABLE_NAME='t_porg_customer';

-- 客户预约表视图 2016-1-27
create or replace view v_porg_customer_appointment as
select 
    c.serial_no
    , c.name
    , c.sex
    , c.telphone
    , c.address
    , c.buy_count
    , c.total_amount
    , c.buy_time
    , c.user_name
    , c.labels
    , c.create_time
    , c.site_id
    , a.id
    , a.customer_id
    , a.appointment_type
    , a.appointment_time
    , a.appointment_remark
    , a.review_time
    , a.review_result
    , a.review_remark
    , a.status
from t_porg_customer_appointment a left join t_porg_customer c on a.customer_id=c.id
order by id desc;
