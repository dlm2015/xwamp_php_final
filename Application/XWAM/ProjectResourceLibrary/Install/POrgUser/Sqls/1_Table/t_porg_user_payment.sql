drop table if exists t_porg_user_payment;
create table if not exists t_porg_user_payment (
    id int auto_increment primary key comment '主键编号',
    bank_name varchar(32) not null default '' comment '开户支行名称',
    bank_card_no varchar(32) not null default '' comment '银行卡卡号',
    bank_card_holder varchar(32) not null default '' comment '持卡人姓名',
    user_name varchar(32) not null default '' comment '创建人用户名',
    usage_count int not null default 0 comment '使用次数(自动排序)',
    sort smallint not null default 0 comment '排序',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX idx_user_name (user_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户支付方式表\r\n@since 1.0 <2014-6-28> sutroon <14507247@qq.com> Added.';
