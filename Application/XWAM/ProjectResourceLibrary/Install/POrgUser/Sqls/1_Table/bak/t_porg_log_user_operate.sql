create table if not exists t_porg_log_user_operate(
    id int auto_increment primary key comment '主键编号',
    user_name varchar(64) not null default '' comment '用户登录名',
    admin_name varchar(64) not null default '' comment '操作(管理)员名称',
    ip varchar(32) not null default '' comment 'IP',
    `action` varchar(16) not null default '' comment '事件动作(如:充值、查询、登录、新增、编辑、删除)',
    `object` varchar(32) not null default '' comment '事件对象(如:存储过程、表名、视图名、页面)',
    `source` varchar(255) not null default '' comment '事件来源(如:网址,PROCEDURE)',
    `level` varchar(12) not null default '' comment '事件级别(如:信息、错误、警告、攻击、调试)',
    `desc` varchar(20480) not null default '' comment '描述(如:充值100元)',
    `status` smallint not null default 0 comment '事件状态(1=成功,4=失败)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX ix_user_name (user_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_事件日志表\r\n@since 1.0 <2014-6-27> sutroon <14507247@qq.com> Added.\r\n@since 2.0 <2015-6-10> SoChishun tuser_event_log重命名为tlog_user_operate';
