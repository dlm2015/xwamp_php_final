create table t_porg_user_type (
    id int auto_increment primary key comment '主键编号',
    title varchar(20) not null default '' comment '类型名称',    
    remark  varchar(32) not null default '' comment '备注',
    sort smallint not null default 0 comment '排序',
    status smallint not null default 0 comment '状态',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户类型表\r\n@since 1.0 <2015-10-31> SoChishun <14507247@qq.com> Added.\r\n';


insert into t_porg_user_type (title) values ('管理员');
insert into t_porg_user_type (title) values ('客服人员');