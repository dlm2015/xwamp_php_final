drop table if exists t_porg_user_subscribe;
create table if not exists t_porg_user_subscribe (
    id int auto_increment primary key comment '主键编号',
    email varchar(32) not null default '' comment '电子邮件',
    user_name varchar(32) not null default '' comment '创建人用户名',
    subscribe varchar(800) not null default '' comment '订阅项目列表',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX idx_email (email),
    INDEX idx_user_name (user_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户订阅表\r\n@since 1.0 <2014-6-28> sutroon <14507247@qq.com> Added.';
