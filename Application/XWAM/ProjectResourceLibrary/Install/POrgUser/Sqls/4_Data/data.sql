/*
默认数据
@since 1.0 <2015-10-20> SoChishun Added.
*/
-- 用户表
truncate table t_porg_user;
truncate table think_auth_group;
insert into t_porg_user (user_name, password, type_name, regist_ip, role_ids, status) values ('sa','123456', 'SYSTEMADMIN', '127.0.0.1', 1, 1);
insert into t_porg_user (user_name, password, type_name, regist_ip, role_ids, status) values ('admin','123456', 'ADMIN', '127.0.0.1', 2, 1);
insert into think_auth_group (title, rules) values ('系统管理员','*');
insert into think_auth_group (title, rules) values ('管理员','*');
insert into think_auth_group (title, rules) values ('坐席','*');