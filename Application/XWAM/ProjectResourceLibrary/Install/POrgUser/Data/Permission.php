<?php

/*
 * 权限生成工具
 * <br />运行方式：http://localhost:8801/Tool/Incubator/SuPermissionSql_PHP_1_0/SoPermissionSql.php
 * 支持字段：[string]title:标题,[string]name:名称,[string]code:代码,[string]type:节点类型(M=菜单,O=操作,E=页面元素),[boolean]enable:是否可用,[string]url:链接地址,[array]children:子项目,[string]comment:注解
 * 说明：默认ismenu=true,如要特殊声明，需强制加上ismenu=false
 * @since 1.0 2014-7-11 by sutroon
 * @since 2.0 <2015-4-23> SoChishun 重新组织url
 * @since 3.0 <2015-7-30> SoChishun 新增code字段
 * @since 4.0 <2015-9-19> SoChishun 重构以适合RBAC自动拦截规则
 */
return array(
    /* 用户模块 */
    array(
        'title' => '用户',
        'code' => 'M1_YH', // 一级菜单不要加厂商代号 2015-10-19 SoChishun Added.
        'children' => array(
            array(
                'title' => '用户管理',
                'code' => 'POrg_YH_M2_YHGL',
                'children' => array(
                    array('name' => 'UserAdmin-user_list', 'level' => '2', 'title' => '管理员管理', 'code' => 'YH_M3_GLYGL', 'url' => 'UserAdmin/user_list',
                        'O' => array(
                            array('name' => 'UserAdmin-user_edit', 'level' => '3', 'title' => '新增管理员', 'code' => 'YH_YHGL_XZGLY', 'enable' => false),
                            array('name' => 'UserAdmin-user_edit', 'level' => '3', 'title' => '编辑管理员', 'code' => 'YH_YHGL_BJGLY', 'enable' => false),
                            array('name' => 'UserAdmin-user_delete', 'level' => '3', 'title' => '删除管理员', 'code' => 'YH_YHGL_SCGLY', 'enable' => false),
                        ),
                    ),
                    array('name' => 'UserSeat-user_list', 'level' => '2', 'title' => '坐席管理', 'code' => 'YH_M3_ZXGL', 'url' => 'UserSeat/user_list',
                        'O' => array(
                            array('name' => 'UserSeat-user_edit', 'level' => '3', 'title' => '新增坐席', 'code' => 'YH_YHGL_XZZX', 'enable' => false),
                            array('name' => 'UserSeat-user_edit', 'level' => '3', 'title' => '编辑坐席', 'code' => 'YH_YHGL_BJZX', 'enable' => false),
                            array('name' => 'UserSeat-user_delete', 'level' => '3', 'title' => '删除坐席', 'code' => 'YH_YHGL_SCZX', 'enable' => false),
                        ),
                    ),
                    array('name' => 'Department-department_list', 'level' => '2', 'title' => '部门管理', 'code' => 'YH_M3_BMGL', 'url' => 'Department/department_list'),
                    array('name' => 'Role-index', 'level' => '2', 'title' => '角色管理', 'code' => 'YH_M3_JSGL', 'url' => 'Role/role_list'),
                ),
            ),
        )
    ),
);
