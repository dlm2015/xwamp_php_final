drop table if exists t_porg_link;
create table if not exists t_porg_link (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null comment '链接标题',
    link_url varchar(255) not null comment '链接地址',
    `type` smallint not null default 1 comment '链接类型(1:友情链接)',
    image_url varchar(255) not null default '' comment '图片地址',
    file_size varchar(32) not null default '' comment '文件大小',
    remark varchar(128) not null default '' comment '备注',
    user_name varchar(32) not null comment '用户名',
    sort smallint not null default 0 comment '排序',
    `status` smallint not null default 0 comment '状态(0=未审核,1=显示,4=隐藏)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX idx_user_name (user_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '(友情)链接表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';
