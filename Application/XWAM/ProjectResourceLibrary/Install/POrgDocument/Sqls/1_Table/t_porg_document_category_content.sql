drop table if exists t_porg_document_category_content;
create table if not exists t_porg_document_category_content (
    category_id int primary key comment '栏目编号',
    summary varchar(128) not null default '' comment '摘要内容',
    content varchar(20480) not null default '' comment '文档内容'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '栏目内容表\r\n@since 1.0 <2015-10-23> SoChishun <14507427@qq.com> Added.\r\n';
