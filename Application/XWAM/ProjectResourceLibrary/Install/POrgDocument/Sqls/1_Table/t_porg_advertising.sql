drop table if exists t_porg_advertising;
create table if not exists t_porg_advertising (
    id int auto_increment primary key comment '主键编号',
    location varchar(32) not null comment '位置属性(如:全网首页,交易类频道首页,资讯类频道首页,功能交互类页面,列表页面,会员登录页面)',
    channel varchar(32) not null comment '频道(如:首页,会员登录页面,促销频道首页,服饰频道首页,社区频道首页)',
    `name` varchar(32) not null comment '名称(如:首页第2轮播焦点图,首页第3轮播焦点图,首页1栏通屏,首页2栏通屏,首页1屏右侧Banner,左侧Banner,会员登录页1屏Banner)',
    title varchar(64) not null comment '标题(格式:{频道}{名称},如:首页第2轮播焦点图,服饰频道首页1屏通栏)',
    code varchar(16) not null comment '英文名称(格式:{频道拼音缩写}_{名称拼音缩写}_{广告类型}:{n},如:SY_JDT_RM1:首页焦点图第1轮播,SY_FB1:首页底部1栏通屏,GYWMSY_B1:关于我们频道首页1栏通屏)',
    `type` char(3) not null default 'B' comment '广告类型(B:网幅广告,包含横幅、按钮、通栏、竖边、巨幅等;T:文本链接广告;EDM:电子邮件广告;S:赞助式广告;BC:内容式广告,包含关键字广告、比对内容广告等;C:分类式广告;I:插播式广告,一般指弹出式广告、浮动广告;RM:富媒体,使用浏览器插件或其他脚本语言,具有复杂视觉效果和交互功能的网络广告;V:视频广告',
    `count` smallint not null default 1 comment '数量',
    image_width smallint not null default 0 comment '宽度(像素)',
    image_height smallint not null default 0 comment '高度(像素)',
    file_type varchar(32) not null default 'jpg,gif,png' comment '文件格式(jpg,gif,png)',
    file_size smallint not null default 50 comment '文件大小规格(KB)',
    user_name varchar(32) not null default '' comment '创建人用户名',
    remark varchar(32) not null default '' comment '备注',
    `status` smallint not null default 1 comment '状态(0=隐藏,1=显示)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime not null default 0 comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    UNIQUE INDEX idxu_code (`code`),
    INDEX idx_title (title),
    INDEX idx_user_name (user_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '广告栏位表\r\n@since 1.0 <2015-10-23> SoChishun <14507427@qq.com> Added.\r\n';