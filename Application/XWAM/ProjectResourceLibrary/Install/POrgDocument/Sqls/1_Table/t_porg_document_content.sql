create table if not exists t_porg_document_content (
    document_id int primary key comment '文档编号',
    content varchar(20480) not null default '' comment '文档内容'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '文档内容表\r\n@since 1.0 <2015-10-20> SoChishun <14507427@qq.com> Added.\r\n';