<?php

/*
 * 权限生成工具
 * <br />运行方式：http://localhost:8801/Tool/Incubator/SuPermissionSql_PHP_1_0/SoPermissionSql.php
 * 支持字段：[string]title:标题,[string]name:名称,[string]code:代码,[string]type:节点类型(M=菜单,O=操作,E=页面元素),[boolean]enable:是否可用,[string]url:链接地址,[array]children:子项目,[string]comment:注解
 * 说明：默认ismenu=true,如要特殊声明，需强制加上ismenu=false
 * @since 1.0 2014-7-11 by sutroon
 * @since 2.0 <2015-4-23> SoChishun 重新组织url
 * @since 3.0 <2015-7-30> SoChishun 新增code字段
 * @since 4.0 <2015-9-19> SoChishun 重构以适合RBAC自动拦截规则
 */
return array(
    /* 系统模块 */
    array(
        'title' => '系统',
        'code' => 'M1_XT', // 一级菜单不要加厂商代号 2015-10-19 SoChishun Added.
        'children' => array(
            array(
                'title' => '系统设置',
                'code' => 'POrg_XT_M2_XTSZ',
                'children' => array(
                    array('name' => 'SiteConfig-index', 'level' => '2', 'title' => '网站设置', 'code' => 'POrg_XT_M3_WZSZ', 'url' => 'SiteConfig/site_config'),
                    array('name' => 'VarConfig-index', 'level' => '2', 'title' => '变量配置', 'code' => 'POrg_XT_M3_BLPZ', 'url' => 'VarConfig/var_config'),
                ),
            ),
            array(
                'title' => '站务管理',
                'code' => 'POrg_XT_M2_ZQGL',
                'children' => array(
                    array('name' => 'PM-pm_list', 'level' => '2', 'title' => '站内信管理', 'code' => 'POrg_XT_M3_ZNXGL', 'url' => 'PM/pm_list'),
                    array('name' => 'VarConfig-index', 'level' => '2', 'title' => '问题反馈', 'code' => 'POrg_XT_M3_WTFK', 'url' => 'VarConfig/var_config'),
                ),
            ),
            array(
                'title' => '网站安全',
                'code' => 'POrg_XT_M2_WZAQ',
                'children' => array(
                    array('name' => 'SiteBackup-index', 'level' => '2', 'title' => '数据备份', 'code' => 'POrg_XT_M3_SJBF', 'url' => 'SiteBackup/index'),
                    array('name' => 'SiteReduction-index', 'level' => '2', 'title' => '还原数据', 'code' => 'POrg_XT_M3_HYSJ', 'url' => 'SiteReduction/index'),
                    array('name' => 'LogAnalysis-index', 'level' => '2', 'title' => '日志分析', 'code' => 'POrg_XT_M3_RZFX', 'url' => 'LogAnalysis/index'),
                ),
            ),
        )
    ),
    /* 用户模块 */
    array(
        'title' => '用户',
        'code' => 'M1_YH', // 一级菜单不要加厂商代号 2015-10-19 SoChishun Added.
        'children' => array(
            array(
                'title' => '用户管理',
                'code' => 'POrg_YH_M2_YHGL',
                'children' => array(
                    array('name' => 'UserAdmin-user_list', 'level' => '2', 'title' => '管理员管理', 'code' => 'YH_M3_GLYGL', 'url' => 'UserAdmin/user_list',
                        'O' => array(
                            array('name' => 'UserAdmin-user_edit', 'level' => '3', 'title' => '新增管理员', 'code' => 'YH_YHGL_XZGLY', 'enable' => false),
                            array('name' => 'UserAdmin-user_edit', 'level' => '3', 'title' => '编辑管理员', 'code' => 'YH_YHGL_BJGLY', 'enable' => false),
                            array('name' => 'UserAdmin-user_delete', 'level' => '3', 'title' => '删除管理员', 'code' => 'YH_YHGL_SCGLY', 'enable' => false),
                        ),
                    ),
                    array('name' => 'UserSeat-user_list', 'level' => '2', 'title' => '坐席管理', 'code' => 'YH_M3_ZXGL', 'url' => 'UserSeat/user_list',
                        'O' => array(
                            array('name' => 'UserSeat-user_edit', 'level' => '3', 'title' => '新增坐席', 'code' => 'YH_YHGL_XZZX', 'enable' => false),
                            array('name' => 'UserSeat-user_edit', 'level' => '3', 'title' => '编辑坐席', 'code' => 'YH_YHGL_BJZX', 'enable' => false),
                            array('name' => 'UserSeat-user_delete', 'level' => '3', 'title' => '删除坐席', 'code' => 'YH_YHGL_SCZX', 'enable' => false),
                        ),
                    ),
                    array('name' => 'Department-department_list', 'level' => '2', 'title' => '部门管理', 'code' => 'YH_M3_BMGL', 'url' => 'Department/department_list'),
                    array('name' => 'Role-index', 'level' => '2', 'title' => '角色管理', 'code' => 'YH_M3_JSGL', 'url' => 'Role/role_list'),
                ),
            ),
        )
    ),
    /* 客户模块 */
    array(
        'title' => '客户',
        'code' => 'M1_KH',
        'children' => array(
            array(
                'title' => '客户管理',
                'code' => 'POrg_KH_M2_KHGL',
                'children' => array(
                    array('name' => 'Customer-customer_list', 'level' => '2', 'title' => '客户管理', 'code' => 'YH_M3_KHGL', 'url' => 'Customer/customer_list',
                        'O' => array(
                            array('name' => 'Customer-customer_edit', 'level' => '3', 'title' => '新增客户', 'code' => 'KH_KHGL_XZKH', 'enable' => true),
                            array('name' => 'Customer-customer_edit', 'level' => '3', 'title' => '编辑客户', 'code' => 'KH_KHGL_BJKH', 'enable' => true),
                            array('name' => 'Customer-customer_recycle', 'level' => '3', 'title' => '放入回收站', 'code' => 'KH_KHGL_FRHSZ', 'enable' => true),
                            array('name' => 'Customer-customer_delete', 'level' => '3', 'title' => '删除客户', 'code' => 'KH_KHGL_SCKH', 'enable' => true),
                            array('name' => 'Customer-customer_dispatch', 'level' => '3', 'title' => '调度客户', 'code' => 'KH_KHGL_DDKH', 'enable' => true),
                        ),
                    ),
                    array('name' => 'CustomerMark-customer_list', 'level' => '2', 'title' => '标记客户', 'code' => 'KH_M3_BJKHGL', 'url' => 'CustomerMark/customer_list','enable'=>false),
                    array('name' => 'CustomerInterested-customer_list', 'level' => '2', 'title' => '意向客户', 'code' => 'KH_M3_BJKHGL', 'url' => 'CustomerInterested/customer_list'),
                    array('name' => 'CustomerAppointment-customer_list', 'level' => '2', 'title' => '预约客户', 'code' => 'KH_M3_YYKHGL', 'url' => 'CustomerAppointment/customer_list'),
                    array('name' => 'CustomerDispatch-customer_list', 'level' => '2', 'title' => '调度客户', 'code' => 'KH_M3_DDKHGL', 'url' => 'CustomerDispatch/customer_list'),
                    array('name' => 'CustomerOverdue-customer_list', 'level' => '2', 'title' => '超期预警', 'code' => 'KH_M3_CQKHGL', 'url' => 'CustomerOverdue/customer_list'),
                    array('name' => 'CustomerPublic-customer_list', 'level' => '2', 'title' => '公共池', 'code' => 'KH_M3_GGCGL', 'url' => 'CustomerPublic/customer_list'),
                    array('name' => 'CustomerRecycle-customer_list', 'level' => '2', 'title' => '回收站', 'code' => 'KH_M3_HSZGL', 'url' => 'CustomerRecycle/customer_list'),
                ),
            ),
            array(
                'title' => '客户配置',
                'code' => 'POrg_KH_M2_KHPZ',
                'children' => array(
                    array('name' => 'CustomerAttr-attr_list', 'level' => '2', 'title' => '字段管理', 'code' => 'KH_M3_ZDGL', 'url' => 'CustomerAttr/attr_list'),
                ),
            ),
            array(
                'title' => '录音管理',
                'code' => 'POrg_KH_M2_LYGL',
                'children' => array(
                    array('name' => 'CallRecord-record_list', 'level' => '2', 'title' => '通话记录', 'code' => 'KH_M3_THJLGL', 'url' => 'CallRecord/record_list'),
                    array('name' => 'CallRecordShare-record_list', 'level' => '2', 'title' => '录音分享', 'code' => 'KH_M3_LYFXGL', 'url' => 'CallRecordShare/record_list'),
                ),
            ),
        ),
    ),
    /* 内容模块 */
    array(
        'title' => '内容',
        'code' => 'M1_NR', // 一级菜单不要加厂商代号 2015-10-19 SoChishun Added.
        'children' => array(
            array(
                'title' => '内容管理',
                'code' => 'POrg_NR_M2_NRGL',
                'children' => array(
                    array('name' => 'Notices-document_list', 'level' => '2', 'title' => '公告管理', 'code' => 'POrg_NR_M3_ZDLM', 'url' => 'Notices/document_list'),
                    array('name' => 'Knowledge-document_list', 'level' => '2', 'title' => '知识库', 'code' => 'POrg_NR_M3_ZDLM', 'url' => 'Knowledge/document_list'),
                ),
            ),
        )
    ),
);