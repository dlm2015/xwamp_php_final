<?php

// =============================标准模型====================================
class UserModel extends Model {

    protected $tableName = 'tuser_member';
    protected $_map = array();
    protected $_validate = array(
        array('verify', 'require', '验证码必须！'), //默认情况下用正则进行验证
        array('name', '', '帐号名称已经存在！', 0, 'unique', 1), // 在新增的时候验证name字段是否唯一
        array('value', array(1, 2, 3), '值的范围不正确！', 2, 'in'), // 当值不为空的时候判断是否在一个范围内
        array('repassword', 'password', '确认密码不正确', 0, 'confirm'), // 验证确认密码是否和密码一致
        array('password', 'checkPwd', '密码格式不正确', 0, 'function'), // 自定义函数验证密码格式
    );
    protected $_auto = array(
        array('status', '1'), // 新增的时候把status字段设置为1
        array('password', 'md5', 1, 'function'), // 对password字段在新增的时候使md5函数处理
        array('name', 'getName', 1, 'callback'), // 对name字段在新增的时候回调getName方法
        array('create_time', 'time', 2, 'function'), // 对create_time字段在更新的时候写入当前时间戳
    );
    // 视图模型
    public $viewFields = array(
        'Blog' => array('id', 'name', 'title', '_table' => "test_db.test_table", '_as' => 'myBlog', '_type' => 'LEFT'),
        'Category' => array('title' => 'category_name', '_on' => 'Blog.category_id=Category.id'),
        'User' => array('name' => 'username', '_on' => 'Blog.user_id=User.id'),
    );

    public function paging_list() {
        $list = $obj->paging_list($pager);
        $list = $obj->limit_list();

        $json = $obj->json_list();

        $data = $obj->single();

        $name = $obj->getName();

        $obj->save();
    }

}

// =============================范例模型====================================
class UserModelExample extends Model {

    protected $tablePrefix = 'other_';
    protected $tableName = 'tuser_member';
    protected $trueTableName = 'tuser_member';
    protected $dbName = 'db_xcall';
    protected $fields = array(
        'id', 'username', 'email', 'age', '_pk' => 'id', '_autoinc' => true
    );
    // 字段映射功能可以让你在表单中隐藏真正的数据表字段，而不用担心放弃自动创建表单对象的功能
    // 要把数据库中的数据显示在表单中，需要对查询的数据调用Model类的parseFieldsMap方法进行处理 $User = M('User'); $data = $User->find(3); $data = $User->parseFieldsMap($data);
    protected $_map = array(
        'name' => 'username', // 把表单中name映射到数据表的username字段
        'mail' => 'email', // 把表单中的mail映射到数据表的email字段
    );
    // 自动验证 array(验证字段,验证规则,错误提示,[验证条件,附加规则,验证时间])
    // 验证规则：require=字段必须、email=邮箱、url=URL地址、currency=货币、number=数字
    // 验证条件：0=存在字段就验证(默认)、1=必须验证、2=值不为空的时候验证
    // 附加规则：regex(默认),function(定义的验证规则是一个函数名),callback(定义的验证规则是当前模型类的一个方法),confirm,equal,in,length,between,expire,unique
    // 验证时：1=新增数据时候验证,2=编辑数据时候验证,3=全部情况下验证（默认）
    protected $_validate = array(
        array('verify', 'require', '验证码必须！'), //默认情况下用正则进行验证
        array('name', '', '帐号名称已经存在！', 0, 'unique', 1), // 在新增的时候验证name字段是否唯一
        array('value', array(1, 2, 3), '值的范围不正确！', 2, 'in'), // 当值不为空的时候判断是否在一个范围内
        array('repassword', 'password', '确认密码不正确', 0, 'confirm'), // 验证确认密码是否和密码一致
        array('password', 'checkPwd', '密码格式不正确', 0, 'function'), // 自定义函数验证密码格式
    );
    // 命名范围
    protected $_scope = array(
        // 默认的命名范围
        'default' => array(
            'where' => array('status' => 1),
            'limit' => 10,
        ),
        // 命名范围normal
        'normal' => array(
            'where' => array('status' => 1),
        ),
        // 命名范围latest
        'latest' => array(
            'order' => 'create_time DESC',
            'limit' => 10,
        ),
    );
    // 自动完成 array(填充字段,填充内容,[填充条件,附加规则])
    // 填充时间：1新增数据的时候处理(默认),2更新数据的时候处理,3所有情况都进行处理
    // 附加规则：function,callback,field(用其它字段填充),string字符串（默认方式）
    // $User-> setProperty("_auto",$auto); $User->create(); 或 $this->auto($auto)->create();
    protected $_auto = array(
        array('status', '1'), // 新增的时候把status字段设置为1
        array('password', 'md5', 1, 'function'), // 对password字段在新增的时候使md5函数处理
        array('name', 'getName', 1, 'callback'), // 对name字段在新增的时候回调getName方法
        array('create_time', 'time', 2, 'function'), // 对create_time字段在更新的时候写入当前时间戳
    );
    // 视图模型
    public $viewFields = array(
        'Blog' => array('id', 'name', 'title', '_table' => "test_db.test_table", '_as' => 'myBlog', '_type' => 'LEFT'),
        'Category' => array('title' => 'category_name', '_on' => 'Blog.category_id=Category.id'),
        'User' => array('name' => 'username', '_on' => 'Blog.user_id=User.id'),
    );
    protected $connection = 'mysql://root:1234@localhost:3306/thinkphp';
    protected $connection = 'DB_CONFIG1'; // 在config.php里面设置

}

// ====================================第一章：创建模型实例========================================
// 实例化基础模型（Model） 类
$User = new Model('User');
$User = M('User');
$User = M('user.User', 'other_'); // M方法可以支持跨库操作
$User = M('User', 'other_', 'mysql://root:1234@localhost/demo');
$User = M('User', Null, 'DB_CONFIG2');
// 实例化其他公共模型类
$User = new CommonModel('User');
$User = new CommonModel('User', 'think_', 'db_config');
$User = M('CommonModel:User', 'think_', 'db_config');
// 实例化用户自定义模型（×××Model）类
$User = new UserModel();
$User = D('User'); // D方法可以支持跨项目和分组调用
// 实例化Admin项目的User模型
D('Admin://User');
// 实例化Admin分组的User模型
D('Admin/User');
// ====================================第二章：创建数据对象========================================
// 实例化User模型
$User = M('User');
// 方式一：根据表单提交的POST数据创建数据对象
$User->create();
// 把创建的数据对象写入数据库(在没有调用add或者save方法之前，我们都可以改变create方法创建的数据对象)
$User->add();

// 方式二：从数组创建数据对象
$data['name'] = 'ThinkPHP';
$data['email'] = 'ThinkPHP@gmail.com';
$User->create($data);

// 方式三：从对象创建数据对象
// 从User数据对象创建新的Member数据对象
$User = M("User");
$User->find(1);
$Member = M("Member");
$Member->create($User);
/*
  create方法的工作流程
  1 获取数据源（默认是POST数组）
  2 验证数据源合法性（非数组或者对象会过滤） 失败则返回false
  3 检查字段映射
  4 判断提交状态（新增或者编辑  根据主键自动判断）
  5 数据自动验证 失败则返回false
  6 表单令牌验证 失败则返回false
  7 表单数据赋值（过滤非法字段和字符串处理）
  8 数据自动完成
  9 生成数据对象（保存在内存）
 */
// 如果只是想简单创建一个数据对象，并不需要完成一些额外的功能的话，可以使用data方法简单的创建数据对象
// 实例化User模型
$User = M('User');
// 创建数据后写入到数据库
$data['name'] = 'ThinkPHP';
$data['email'] = 'ThinkPHP@gmail.com';
$User->data($data)->add();
/*
说明：
1.Data方法也支持传入数组和对象，使用data方法创建的数据对象不会进行自动验证和过滤操作，请自行处理。但在进行add或者save操作的时候，数据表中不存在的字段以及非法的数据类型（例如对象、数组等非标量数据）是会自动过滤的，不用担心非数据表字段的写入导致SQL错误的问题。
2.create方法如果没有传值，默认取$_POST数据，如果用户提交的变量内容，含有可执行的html代码，请进行手工过滤。
$_POST['title'] = "<script>alert(1);</script>";
非法html代码可以使用htmlspecialchars进行编码，以防止用户提交的html代码在展示时被执行，以下是两种安全处理方法。
$_POST['title'] = htmlspecialchars($_POST['title']);
M('User')->create();
$data['title'] = $this->_post('title', 'htmlspecialchars');
M('User')->create($data);
 */

