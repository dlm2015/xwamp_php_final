/**
* 判断是否调试
* @since 1.0 2015-1-7 by sutroon
*/
DELIMITER ;;
drop function if exists func_isdebug;;
create function func_isdebug(vstr_procName varchar(32), vint_siteID int)
returns int
begin
    select dataValue, dataCode into @dataValue, @dataCode from tgeneral_data where exTags='SQL-DEBUG' and dataName=vstr_procName;
    if (@dataValue='1') or ((@dataValue='2') and (@dataCode=vint_siteID)) then
        return 1;
    else
        return 0;
    end if;
end;;
DELIMITER ;
