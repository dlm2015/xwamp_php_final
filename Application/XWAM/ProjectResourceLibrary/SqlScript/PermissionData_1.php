<?php

/*
 * 权限数据
 * <br />支持字段：[string]name:名称,[string]code:代码,[boolean]ismenu:是否显示为菜单,[boolean]enable:是否可用,[string]url:链接地址,[array]sub:子项目,[string]comment:注解
 * <br />说明：默认ismenu=true,如要特殊声明，需强制加上ismenu=false
 * @since 1.0 2014-7-11 by sutroon
 * @since 2.0 <2015-4-23> SoChishun 重新组织url
 * @since 3.0 <2015-7-30> SoChishun 新增code字段
 * @remark 获取原来管理员权限编号的sql语句：select concat(group_concat(id SEPARATOR ','),'') as ids from tuser_permission where name in ('系统管理', '系统管理');
 */
return array(
    array(
        'name' => '系统管理',
        'code' => 'M1_XT',
        'sub' => array(
            array('name' => '系统设置', 'code' => 'XT_M2_XTSZ', 'sub' => array(
                    array('name' => '网站信息', 'code' => 'XT_XTSZ_WZXX', 'url' => '/index.php/XWAM/Setting/setting_edit.html'),
                )
            ),
            array(
                'name' => '留言反馈',
                'code' => 'XT_M2_LYFK',
                'sub' => array(
                    array('name' => '留言反馈', 'code' => 'XT_M3_LYFK', 'url' => '/index.php/XWAM/Feedback/feedback_list.html', 'sub' => array(
                            array('name' => '审核留言', 'code' => 'XT_LYFK_SHLY', 'ismenu' => false),
                        )
                    ),
                )
            ),
            array(
                'name' => '站内信',
                'code' => 'XT_ZNXMK',
                'sub' => array(
                    array('name' => '站内信管理', 'code' => 'XT_ZNXGL', 'url' => '/index.php/XWAM/PM/pm_list.html', 'sub' => array(
                            array('name' => '发送站内信', 'code' => 'XT_FSZNX', 'ismenu' => false),
                            array('name' => '批量发送站内信', 'code' => 'XT_PLFSZNX', 'ismenu' => false),
                            array('name' => '删除站内信', 'code' => 'XT_SCZNX', 'ismenu' => false),
                            array('name' => '导出站内信', 'code' => 'XT_DCZNX', 'ismenu' => false),
                        )
                    ),
                )
            ),
            array(
                'name' => '手机短信',
                'code' => 'XT_SJDXMK',
                'sub' => array(
                    array('name' => '手机短信管理', 'code' => 'XT_SJDXGL', 'url' => '/index.php/XWAM/SMS/sms_list.html', 'sub' => array(
                            array('name' => '发送短信', 'code' => 'XT_FSSJDX', 'ismenu' => false),
                            array('name' => '批量发送短信', 'code' => 'XT_PLFSSJDX', 'ismenu' => false),
                            array('name' => '删除短信', 'code' => 'XT_SCSJDX', 'ismenu' => false),
                            array('name' => '导出短信', 'code' => 'XT_DCSJDX', 'ismenu' => false),
                        )
                    ),
                )
            ),
            array(
                'name' => '备份恢复',
                'code' => 'XT_BFHFMK',
                'sub' => array(
                    array('name' => '数据备份', 'code' => 'XT_SJBFGL', 'url' => '/index.php/XWAM/SysTool/data_backup.html', 'sub' => array(
                            array('name' => '备份数据', 'code' => 'XT_BFSJ', 'ismenu' => false),
                            array('name' => '下载备份', 'code' => 'XT_XZBF', 'ismenu' => false),
                        )),
                    array('name' => '数据恢复', 'code' => 'XT_SJHFGL', 'url' => '/index.php/XWAM/SysTool/data_backup.html', 'sub' => array(
                            array('name' => '恢复数据', 'code' => 'XT_HFSJ', 'ismenu' => false),
                        )),
                    array('name' => '数据清理', 'code' => 'XT_SJQLGL', 'url' => '/index.php/XWAM/SysTool/data_clear.html', 'sub' => array(
                            array('name' => '清理数据', 'code' => 'XT_QLSJ', 'ismenu' => false),
                        )),
                )
            )
        )
    ),
    // =================
    array(
        'name' => '用户管理',
        'code' => 'YH_MK',
        'sub' => array(
            array(
                'name' => '用户管理',
                'code' => 'YH_YHMK',
                'sub' => array(
                    array('name' => '管理员管理', 'code' => 'YH_GLYGL', 'url' => '/index.php/XWAM/Admin/admin_list.html'),
                    array('name' => '会员管理', 'code' => 'YH_HYGL', 'url' => '/index.php/XWAM/Member/member_list.html', 'sub' => array(
                            array('name' => '新增会员', 'code' => 'YH_XZHY', 'ismenu' => false),
                            array('name' => '编辑会员', 'code' => 'YH_BJHY', 'ismenu' => false),
                            array('name' => '删除会员', 'code' => 'YH_SCHY', 'ismenu' => false),
                            array('name' => '编辑会员权限', 'code' => 'YH_BJHYQX', 'ismenu' => false),
                            array('name' => '导出会员数据', 'code' => 'YH_DCHYSJ', 'ismenu' => false),
                        )
                    ),
                )
            ),
            array(
                'name' => '角色管理',
                'code' => 'YH_JSMK',
                'sub' => array(
                    array('name' => '角色管理', 'code' => 'YH_JSGL', 'url' => '/index.php/XWAM/Role/role_list.html', 'sub' => array(
                            array('name' => '新增角色', 'code' => 'YH_XZJS', 'ismenu' => false),
                            array('name' => '编辑角色', 'code' => 'YH_BJJS', 'ismenu' => false),
                            array('name' => '删除角色', 'code' => 'YH_SCJS', 'ismenu' => false),
                            array('name' => '编辑角色权限', 'code' => 'YH_BJJSQX', 'ismenu' => false),
                        )
                    ),
                )
            ),
            array('name' => '部门管理',
                'code' => 'YH_BMGLMK',
                'sub' => array(
                    array('name' => '部门管理', 'code' => 'YH_BMGL', 'url' => '/index.php/XWAM/Department/department_list.html', 'sub' => array(
                            array('name' => '新增部门', 'code' => 'YH_XZBM', 'ismenu' => false),
                            array('name' => '编辑部门', 'code' => 'YH_BJBM', 'ismenu' => false),
                            array('name' => '删除部门', 'code' => 'YH_SCBM', 'ismenu' => false),
                        )
                    ),
                )
            ),
        )
    ),
    // =================
    array(
        'name' => '文章管理',
        'code' => 'WZ_MK',
        'sub' => array(
            array(
                'name' => '话术管理',
                'code' => 'WZ_HSMK',
                'sub' => array(
                    array('name' => '话术管理', 'code' => 'WZ_HSGL', 'url' => '/index.php/XWAM/Lobbying/lobbying_list.html', 'sub' => array(
                            array('name' => '新增话术', 'code' => 'WZ_XZHS', 'ismenu' => false),
                            array('name' => '编辑话术', 'code' => 'WZ_BJHS', 'ismenu' => false),
                            array('name' => '删除话术', 'code' => 'WZ_SCHS', 'ismenu' => false),
                        )),
                )
            ),
            array(
                'name' => '公告管理',
                'code' => 'WZ_GGMK',
                'sub' => array(
                    array('name' => '公告管理', 'code' => 'WZ_GGGL', 'url' => '/index.php/XWAM/Notice/notice_list.html', 'sub' => array(
                            array('name' => '新增公告', 'code' => 'WZ_XZGG', 'ismenu' => false),
                            array('name' => '编辑公告', 'code' => 'WZ_BJGG', 'ismenu' => false),
                            array('name' => '删除公告', 'code' => 'WZ_SCGG', 'ismenu' => false),
                        )),
                )
            )
        )
    ),
    // =================
    array(
        'name' => '客户管理',
        'code' => 'KH_MK',
        'sub' => array(
            array(
                'name' => '客户管理',
                'code' => 'KH_KHMK',
                'sub' => array(
                    array('name' => '客户管理', 'code' => 'KH_KHGL', 'url' => '/index.php/XWAM/Customer/customer_list.html', 'sub' => array(
                            array('name' => '新增客户', 'code' => 'KH_XZKH', 'ismenu' => false),
                            array('name' => '编辑客户', 'code' => 'KH_BJKH', 'ismenu' => false),
                            array('name' => '删除客户', 'code' => 'KH_SCKH', 'ismenu' => false),
                            array('name' => '调度客户', 'code' => 'KH_DDKH', 'ismenu' => false),
                            array('name' => '预约客户', 'code' => 'KH_YYKH', 'ismenu' => false),
                            array('name' => '导出客户数据', 'code' => 'KH_DCKHSJ', 'ismenu' => false),
                        )),
                )
            ),
            array('name' => '字段管理', 'code' => 'KH_ZDMK', 'sub' => array(
                    array('name' => '字段管理', 'code' => 'KH_ZDGL', 'url' => '/index.php/XWAM/Exfield/index/target/CUSTOMER.html'),
                )),
        )
    ),
    // ======================
    array(
        'name' => '订单管理',
        'code' => 'DD_MK',
        'sub' => array(
            array(
                'name' => '订单模块',
                'code' => 'DD_DDMK',
                'sub' => array(
                    array('name' => '订单管理', 'code' => 'DD_DDGL', 'url' => '/index.php/XWAM/Order/order_list.html', 'sub' => array(
                            array('name' => '新增订单', 'code' => 'DD_XZDD', 'ismenu' => false),
                            array('name' => '编辑订单', 'code' => 'DD_BJDD', 'ismenu' => false),
                            array('name' => '删除订单', 'code' => 'DD_SCDD', 'ismenu' => false),
                            array('name' => '取消订单', 'code' => 'DD_QXDD', 'ismenu' => false),
                            array('name' => '审核订单', 'code' => 'DD_SHDD', 'ismenu' => false),
                            array('name' => '仓库发货', 'code' => 'DD_CKFH', 'ismenu' => false),
                            array('name' => '订单结算', 'code' => 'DD_DDJS', 'ismenu' => false),
                            array('name' => '导出订单数据', 'code' => 'DD_DCDDSJ', 'ismenu' => false),
                        )),
                )
            ),
        )
    ),
    // ===============================
    array(
        'name' => '商品管理',
        'code' => 'SP_MK',
        'sub' => array(
            array(
                'name' => '商品管理',
                'code' => 'SP_SPMK',
                'sub' => array(
                    array('name' => '商品管理', 'code' => 'SP_SPGL', 'url' => '/index.php/XWAM/Product/product_list.html', 'sub' => array(
                            array('name' => '新增商品', 'code' => 'SP_XZSP', 'ismenu' => false),
                            array('name' => '编辑商品', 'code' => 'SP_BJSP', 'ismenu' => false),
                            array('name' => '删除商品', 'code' => 'SP_SCSP', 'ismenu' => false),
                            array('name' => '盘点调仓', 'code' => 'SP_PDTC', 'ismenu' => false),
                            array('name' => '导出商品数据', 'code' => 'SP_DRSPSJ', 'ismenu' => false),
                            array('name' => '导入商品数据', 'code' => 'SP_DCSPSJ', 'ismenu' => false),
                        )),
                    array('name' => '商品类别管理', 'code' => 'SP_SPLBGL', 'url' => '/index.php/XWAM/ProductCategory/category_list.html', 'sub' => array(
                            array('name' => '新增商品类别', 'code' => 'SP_XZSPFL', 'ismenu' => false),
                            array('name' => '编辑商品类别', 'code' => 'SP_BJSPLB', 'ismenu' => false),
                            array('name' => '删除商品类别', 'code' => 'SP_SCSPLB', 'ismenu' => false),
                        )),
                )
            ),
            array(
                'name' => '进销存管理',
                'code' => 'SP_JXCMK',
                'sub' => array(
                    array('name' => '出库管理', 'code' => 'SP_CKGL', 'url' => '/index.php/XWAM/ProductStockout/stockout_list.html', 'sub' => array(
                            array('name' => '申请内部出库', 'code' => 'SP_SQNBCK', 'ismenu' => false),
                            array('name' => '审核内部出库', 'code' => 'SP_SHNBCK', 'ismenu' => false),
                            array('name' => '申请采购退货', 'code' => 'SP_SQCGTH', 'ismenu' => false),
                            array('name' => '审核采购退货', 'code' => 'SP_SHCGTH', 'ismenu' => false),
                            array('name' => '编辑出库资料', 'code' => 'SP_BJCKZL', 'ismenu' => false),
                            array('name' => '导出出库数据', 'code' => 'SP_DCCKSJ', 'ismenu' => false),
                        )),
                    array('name' => '入库管理', 'code' => 'SP_RKGL', 'url' => '/index.php/XWAM/ProductStockin/stockin_list.html', 'sub' => array(
                            array('name' => '退货入库', 'code' => 'SP_THRK', 'ismenu' => false),
                            array('name' => '申请采购入库', 'code' => 'SP_SQCGRK', 'ismenu' => false),
                            array('name' => '审核采购入库', 'code' => 'SP_SHCGRK', 'ismenu' => false),
                            array('name' => '编辑入库资料', 'code' => 'SP_BJRKZL', 'ismenu' => false),
                            array('name' => '导出入库数据', 'code' => 'SP_DCRKSJ', 'ismenu' => false),
                        )),
                    array('name' => '供应商管理', 'code' => 'SP_GYSGL', 'url' => '/index.php/XWAM/ProductSupplier/supplier_list.html', 'sub' => array(
                            array('name' => '新增供应商', 'code' => 'SP_XZGYS', 'ismenu' => false),
                            array('name' => '编辑供应商', 'code' => 'SP_BJGYS', 'ismenu' => false),
                            array('name' => '删除供应商', 'code' => 'SP_SCGYS', 'ismenu' => false),
                            array('name' => '导出供应商数据', 'code' => 'SP_DCGYSSJ', 'ismenu' => false),
                        )),
                )
            ),
        )
    ),
    // ======================
    array(
        'name' => '全局管理',
        'code' => 'M1_QJ',
        'ismenu' => false,
        'sub' => array(
            array(
                'name' => '通用设置',
                'code' => 'QJ_M2_TY',
                'ismenu' => false,
                'sub' => array(
                    array('name' => '访问范围', 'code' => 'QJ_M3_FWFW', 'ismenu' => false, 'sub' => array(
                            array('name' => '访问前台', 'code' => 'QJ_FWFW_FWQT', 'ismenu' => false),
                            array('name' => '访问后台', 'code' => 'QJ_FWFW_FWHT', 'ismenu' => false),
                        )),
                    array('name' => '呼叫管理', 'code' => 'QJ_M3_HJGL', 'ismenu' => false, 'sub' => array(
                            array('name' => '隐藏号码', 'code' => 'QJ_HJGL_YCHM', 'ismenu' => false),                        
                        )),
                )
            ),
        )
    ),
);
