-- 2015-1-6
-- Name: 调试计费

select eObject, eDesc from tuser_event_log order by id desc;

-- select * from tgeneral_data;
-- insert into tgeneral_data (dataName, dataValue, dataCode, exTags) values ('func_cdr_update_amount', '1', '0', 'SQL-DEBUG');
-- insert into tgeneral_data (dataName, dataValue, dataCode, exTags) values ('func_cdr_update_amount-2', '2', '[siteID]', 'SQL-DEBUG'); -- 调试指定租户存储过程内部流程标识2的消息
-- insert into tgeneral_data (dataName, dataValue, dataCode, exTags) values ('func_cdr_update_amount-2', '2', '[siteID]', 'SQL-DEBUG'); -- 调试指定租户存储过程内部流程标识2的消息
-- truncate table tgeneral_data;
-- truncate table tuser_event_log;

select id, siteID, billsec, direction from cdr where billsec > 0 order by id desc limit 100;
 -- func_cdr_update_amount(vint_cdrid int,vint_siteID int,vint_billsec int,vint_direction int)
call func_cdr_update_amount(92890, 1, 23, 2);
