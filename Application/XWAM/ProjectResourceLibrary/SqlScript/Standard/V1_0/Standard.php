<?php
/**
* SO-页面代码规范标准
* @since 1.0 2014-12-18 by sutroon
 */

/*
 * 一、ActionPage规范标准
 *  1. JSON方法命名：[业务名称]_[list|data]_json] 如：product_list_json, product_data_json, username_list_json
 *  2. Action辅助方法命名：[ActionName]_get_search，如 customer_list_get_search,dispatch_edit_get_search等
 *  3. 时效性较低的频繁读取的数据应做缓存处理，避免数据库负荷。
 */

/*
 * 二、HtmlPage规范标准
 *  1.模板变量应与javascript分离，值存储到页面隐藏字段中，每个值一个字段，防止值数据过大和特殊字符,隐藏字段ID命名规范 [页面ID简写]hidvar_[语义名称],如:idx-history-update-url表示index.html页面的history-update-url,或so-customer-edit-url表示框架全局的customer-edit-url, oe-product-intro-url表示order_edit.html的查看商品介绍url。隐藏字段中url有变量的，变量命名方法为：id=varid&name=varname
 *      框架隐藏变量有：so-region-api-url(省市区数据源url), so-upload-handler-url(文件上传处理url), so-history-update-url(更新导航标签url), so-history-remove-url(关闭导航标签url), so-history-load-url(加载导航标签url)
 *  2.javascript尽量剥离到独立的文件，利用浏览器自带缓存技术减少文件尺寸。
 *  
 */