/**
* SO-SQL规范标准
* @since 1.0 2014-10-10 by sutroon
 */

/**
* 一、存储过程和函数规范说明：===================================================
*   1. 存储过程命名规则：proc_[表名称]_[业务名称,全部小写,单词中间用下划线隔开], 如：proc_user_regist
    2. 参数变量命名规则：pstr_[名称],pint_[名称],pbln_[名称],oint_[传出参数名称], 如：pstr_userName, pint_userID, oint_result,其中前缀p=param,o=out
    3. 声明变量命名规则：str_[名称],int_[名称],bln_[名称], 如: str_userName, int_userID, int_result
    4.返回值规则：返回代码变量 out oint_result int
        1 执行成功
        4000 系统错误
        4100 参数错误
        4200 状态有误(重复操作等)
        4300 记录已存在
        4400 记录不存在
        4500 值不在范围内(小于下限或大于上限)
        .... 其他业务错误
    5.跳出函数或方法：
        5.1.存储过程跳出标签一律命名为proc,不要用如flag等容易混淆的名称, 如:
            create procedure proc_test (pint_a int, oint_result int)
            proc:begin
                if pint_a > 100 then
                  set oint_result=45;
                  leave proc; -- 需在这里跳出，否则oint_result永远等于1
                enf if;
                set oint_result=1;
            end$$
        5.2 函数跳出不需要标签，return后就不会再执行后面的内容了!
* @since 1.0 2014-7-1 by sutroon

* 二、存储过程和函数代码片段示例：=================================================
*/
-- # 创建函数
DELIMITER $$
drop function if exists func_make_serial_no;
create function func_make_serial_no(istr_prefix varchar(16), iint_userID int) returns varchar(128)
begin
    return concat(istr_prefix,'-',iint_userID,'-',date_format(now(),'%Y%m%d-%H%i%s'));
end$$
DELIMITER ;

-- # 调用函数
select func_make_serial_no('CZ',1);

-- # 创建存储过程
DELIMITER $$
drop procedure if exists proc_user_registe;
create procedure proc_user_registe(istr_userName varchar(32), istr_password varchar(32), istr_ip varchar(32), iint_referrerID int, out oint_userID int, out oint_result int)
flag:begin
    if (length(istr_userName) < 1) or (length(istr_password) < 1) then
        set oint_result=41;
        leave flag;
    end if;
    if exists(select id from tuser where userName=istr_userName) then
        set oint_result=43;
        leave flag;
    end if;

    if (iint_referrerID > 0) and not exists(select id from tuser where id=iint_referrerID) then
        set oint_result=44;
        leave flag;
    end if;

    insert into tuser (userName, password, mobile, referrerUserID, registIP, state) values (istr_userName, istr_password, istr_userName, iint_referrerID, istr_ip, 1);
    set oint_userID = LAST_INSERT_ID();
    call proc_finance_account_create(oint_userID, 1, '用户注册创建账户');
    insert into tuser_customer (userID,serialNo) values (oint_userID,func_make_serial_no('C',oint_userID));
    set oint_result=1;
end$$
DELIMITER ;
-- # 调用存储过程
call proc_user_registe('user8','123','',8,@uid,@cod);
select @uid,@cod;

-- # 在存储过程中使用动态语句和事务
create procedure proc_stmt_transaction(iint_a int, out oint_result int)
flag:begin
    declare i int default 0; -- 循环变量
    declare exit handler for sqlexception rollback; -- sqlexception 泛指所有的错误。只要出现任何错误，都执行后面的语句：rollback; 本语句必需在所有declare语句之后!
    start transaction; -- 开始事务

    if a < 0 then
        set d=41;
        rollback; -- 回滚事务
        leave flag;
    end if;

    -- 动态拼凑语句,只能用@变量,不能用declare 变量.
    set @str_sql=concat('select * from tuser limit ',iint_a);
    prepare stmt from @str_sql;
    execute stmt;
    deallocate prepare stmt;
    set oint_result=1;
    commit; -- 提交事务
end

-- # 在存储过程中使用事务
create procedure proc_recharge(pflt_amount decimal(12,4), pint_siteID int, pstr_origin varchar(32),out oint_result int)
begin
    declare vflt_blance decimal(12,4) default 0;
    declare vflt_allowOverdraftAmount decimal(12,4) default 0;
    declare vint_daybill_id int;
    declare vstr_userName varchar(32);
    declare vint_errno smallint default 0; -- 错误号码
    declare continue handler for sqlexception set vint_errno = 4000; -- 系统错误
    start transaction;
    
    if pflt_amount < 0 then
        set vint_errno=4100; -- 参数有误
    end if;

    select userName, blance, allowOverdraftAmount into vstr_userName, vflt_blance, vflt_allowOverdraftAmount from tuser_member where userType='ADMIN' and siteID=pint_siteID;
    if isnull(vstr_userName) then
        set vint_errno=4400; -- 记录不存在
    enf if;
    set vflt_blance=vflt_blance+pflt_amount;
    update tuser_member set blance=vflt_blance where userType='ADMIN' and siteID=pint_siteID;
    
    insert into tfinance_recharge (userName, amount, blance, origin, createdTime, siteID) values (vstr_userName, pflt_amount, vflt_blance, pstr_origin, now(), pint_siteID);
    if exists(select id from tcti_daybill where billdate=CURDATE() and siteID=pint_siteID) then
        update tcti_daybill set rechargeAmount=rechargeAmount+pflt_amount,blance=blance+pflt_amount where billdate=CURDATE() and siteID=pint_siteID;
    else
        insert into tcti_daybill (billdate,rechargeAmount,blance, allowOverdraftAmount,createdTime,siteID) values (CURDATE(),pflt_amount,vflt_blance, vflt_allowOverdraftAmount,now(),pint_siteID);
    end if;

    if vint_errno <> 0 then
        set oint_result=vint_errno;
        rollback;
    else        
        set oint_result=1; -- 操作成功
        commit;
    end if;
    -- select vint_errno; 如果在存储过程中使用select，则在php中执行存储过程后返回结果的结果可以用mysql_fetch_array($result)来获取结果集
end
-- # 在存储过程中使用游标 2014-9-5 by sutroon
create procedure func_cdr_update_amount_patch(pstr_accountcode varchar(255))
begin
    declare vint_siteID int;
    declare vint_direction int;
    declare vint_billsec int;
    declare vint_cdrid int;
    declare vint_i int default 0;
    declare done boolean default false; -- 遍历数据结束标志
    -- 计费游标
    declare cur1 cursor for select `id`, `siteID`, `billsec`, `direction` from `cdr` where (`accountcode`=pstr_accountcode) and (`amount`=0) and (`billsec`<100000) and (end is not null) and (`siteID` is not null) and (`direction` in (1,2,5));
    declare continue handler for not found set done = true; -- 将结束标志绑定到游标, 本语句必需在所有declare语句之后!

    open cur1; -- 打开游标
    loop_label: loop
    fetch cur1 into vint_cdrid, vint_siteID, vint_billsec, vint_direction;  -- 声明结束的时候
        -- if vint_i >= pint_limit then -- 可以设置跳出游标的条件
        --     set done=true;
        -- end if;
        if done then
            leave loop_label;
        end if;
        -- 这里做你想做的循环的事件
        call func_cdr_update_amount(vint_cdrid,vint_siteID,vint_billsec,vint_direction);
        set vint_i=vint_i+1;
    end loop;
    close cur1; -- 关闭游标
end;


/**
* 三、数据表创建规范说明：===================================================
* 1.一般事务表选择innodb，只读表选择myisam
* 2.尽量使用short、integer的主键
* 3.声明列为NOT NULL，可以提高效率，减少磁盘存储
* 4.仅当字符数量超过2万个时，才用text类型，并且必需和原表拆分，定长字段使用char，不定长字段使用varchar，且仅仅设定适当的最大长度，对状态字段，可以用char类型
* 5.被用来join的字段，应该是相同的类型，并确认两表中join字段是被建过索引的，这样MySQL内部会启动join优化机制。
* 6.分库分表分区
* 7.索引命名规则：单列索引用ix_前缀,复合索引用ixc_前缀,唯一索引用ixu_ (2015-7-14 SoChishun Modify.)

* 1.字段名称使用驼峰命名法,如userName, sex, createdTime等
* 2.枚举类型的值，值可以使用中文,不必担心在OO对象中无法转换枚举对象(现在编程语言都支持Unicode编码了),方便数据阅读,如`source` varchar(16) comment '客户来源(电话,QQ,微信,广告,网站,其他)'
* 3.创建数据库或表时需指定字符集，如CREATE DATABASE IF NOT EXISTS db_xcall DEFAULT CHARSET utf8 COLLATE utf8_general_ci; create table if not exists tuser () comment '用户表 1.0 2014-7-10 by sutroon' ENGINE=InnoDB default CHARSET=utf8;
* 4.添加或编辑列格式：alter table `mytable` add|modify column 列完整定义(`col1` varchar(32) not null default '' comment 'mycomment' after `othercol`);
* @since 1.0 2014-11-13 by sutroon
* 示例代码：
*/
-- drop database if exists db_xcall;
create database if not exists db_xcall default CHARSET utf8 COLLATE utf8_general_ci;

-- drop table if exists tuser;
create table if not exists tuser (
    id int auto_increment primary key comment '主键编号',
    userName varchar(32) not null comment '用户名称',
    password varchar(128) not null comment '登录密码(带有*的是加密过的密码,否则是明文密码)',
    sex smallint not null default 0 comment '性别(0=女,1=男,2=保密)',
    faceUrl varchar(255) comment '用户头像Url',
    birthday datetime comment '生日',
    idType varchar(32) comment '证件类型(身份证,学生证,工作证,士兵证,军官证,护照)',
    remark text comment '备注',
    exTags varchar(16) not null comment '可扩展标签(SEAT=座席,ADMIN=普通管理员,SUPERADMIN=超级管理员,EMPLOYEE=员工)',
    onlineState smallint not null default 0 comment '在线状态(0=离线,1=在线,2=隐身,3=离开)',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,3=冻结,4=禁用,7=注销)',
    createdTime timestamp not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP comment '创建时间',
    siteID int not null default 0 comment '公司编号,在此表功能相当于ParentID,超级管理员可以给每个公司分配一个顶级管理员(siteID=0),顶级管理员可以再给本公司分配多个普通管理员,siteID=该顶级管理员的uid'
) comment '用户表 1.0 2014-6-26 by sutroon' ENGINE=InnoDB  default CHARSET=utf8;

-- 添加列(列的完整定义要写全,包含after)
alter table `mytable`
add column `col1`  varchar(255) null after `create_time`,
add column `col2`  date null after `col1`;

-- 编辑列,把col1改为长度32, not null, 默认值 '', 注释 aaa; 把col2类型由date改为datetime（把列的定义重新写一次,和添加列差一样，只是把add column改为modify column）
alter table `mytable`
modify column `col1`  varchar(32) not null default '' comment 'aaa' after `create_time`,
modify column `col2`  datetime null default null after `col1`;

-- 删除列
alter table `mytable`
drop column `col2`;

-- 移动列,把col2上移到col1前（把要移动的col2列定义重新写一次,和添加列一样,但只修改after参数）
alter table `mytable`
modify column `col2`  datetime null default null after `create_time`;

/**
* 四、脚本操作规范
* 1.拆分大的 DELETE 或 INSERT 语句，这两个操作会锁表，应采用脚本形式批量删除部分记录，比如要删除10万条记录，最好在脚本批量循环删除，如limit 1000条删除一次。
* 2.用in() /union替换or，并注意in的个数小于300
* 3.避免使用%前缀模糊前缀查询
 */

/**
* 五、SQL代码头部注释规范
在SQL代码块（sql文件或存储过程）的头部进行注释，标注创建人(Author)、创始日期(Create date)、修改信息(Modify [n])
格式：
-- =============================================
-- Author:      [Author]
-- Create date: [Create Date]
-- Description: [Description]
-- Modify [n]:  [Modifier, Date, Description]
-- =============================================
示例：
-- ================================================
-- Author:      Zhanghaifeng
-- Create date: 2006-12-25
-- Description: H2000报关单回执处理
-- Modify [1]:  郑佐, 2006-12-31, 简化逻辑判断流程
-- Modify [2]:  郑佐, 2007-01-20, 更新条件判断
-- ================================================
注：日期格式使用 yyyy-MM-dd。Modify [n] n代表修改序号，从1开始，每次修改加1。
*/

/**
* 六、索引使用规范 [2015-7-17]
* 类型：
* 1. 普通索引：INDEX
* 2. 唯一索引：UNIQUE INDEX
* 3. 主索引：PRIMARY KEY
* 4. 外键索引：FOREIGN KEY
* 5. 复合索引：INDEX
* 缺点：
* 1. 索引会提高查询速度，但会降低修改效率，每修改一条记录，索引就必需刷新一次。(DELAY_KEY_WRITE)
* 2. 索引会增加硬盘空间
* 3. 对包含许多重复内容的列做索引没有太大的实际效果
* 4. MySQL同一个表最大索引数量为16个
* 5. where中有不等号、函数等，将无法使用索引
*       like搜索只有第一个字符不是通配符情况下才使用索引
*/

/*
* 七、数据库与文件系统选择 [2015-7-17]
* 数据库是用来保存持久的，可查询的数据，因此，以下三种情况不建议使用数据库系统：
* 1. 图片、文件、二进制数据推荐使用文件系统：
*       因为数据库的读写永远赶不上文件系统的处理速度，而且会使数据库变大而影响性能。对文件的访问需要经过应用层和数据库层，没有文件系统访问那么直接。
* 2. 短生命期数据，推荐使用缓存系统，如购物车、用户会话、临时统计数据等。因为这些频繁失效的资源，背离了数据库存储的本意，而且会抢占数据库资源开销。
* 3. 日志文件，推荐使用文本文件来存储，因为日志数据往往十分庞大，会消耗数据库的读写开销，而且会使数据库变大而影响性能。所以推荐直接存到文件系统，如果需要查询和分析，可以自己设计查询工具。
*/