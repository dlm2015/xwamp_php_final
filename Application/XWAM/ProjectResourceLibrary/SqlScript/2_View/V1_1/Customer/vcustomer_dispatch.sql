-- 客户-调度客户日志表视图 2014-12-26 by sutroon
create or replace view vcustomer_dispatch as
    select 
        d.id, d.name, d.customerID, d.oldUserID, d.userID, d.adminID, d.remark, d.state, d.createdTime,
        u.userName, u.personalName as agentPersonalName, uold.userName as oldUserName, uadm.userName as adminName
    from tcustomer_dispatch d left join tuser u on d.userID=u.id left join tuser uold on d.oldUserID=uold.id left join tuser uadm on d.adminID=uadm.id
    order by id desc;
