-- 客户-预约回访表视图 2014-12-25 by sutroon
create or replace view vcustomer_appointment as
    select 
        a.id, a.customerID, a.`time`, a.remark, a.`result`, a.`source`, a.`state`, a.updatedTime, a.createdTime, 
        c.siteID, c.serialNo, c.personalName, c.telphone, c.address, c.buyCount, c.cumulativeAmount, c.lastBuyTime, c.userID, c.oldUserID, 
        u.userName, u.personalName as agentPersonalName
    from tcustomer_appointment a left join tcustomer c on a.customerID=c.id left join tuser u on c.userID=u.id
    order by a.id desc;
