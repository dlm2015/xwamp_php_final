-- 客户表视图 2014-12-25 by sutroon
create or replace view vcustomer as
    select 
        c.id, c.serialNo, c.loginName, c.personalName, c.sex, c.faceUrl, c.maritalStatus, c.birthday, c.idType, c.idNo, c.telphone, c.address, c.interested, c.tags, c.level, c.buyCount, c.integral, c.cumulativeAmount, c.lastBuyTime, c.lastVisitTime, c.userID, c.oldUserID, c.remark, c.state, c.createdTime, c.siteID,
        u.userName, u.personalName as agentPersonalName
    from tcustomer c left join tuser u on c.userID=u.id
    order by c.id desc;
