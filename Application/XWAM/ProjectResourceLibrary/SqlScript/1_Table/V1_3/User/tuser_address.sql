create table if not exists tuser_address (
    id int auto_increment primary key comment '主键编号',
    user_id int not null comment '用户编号',
    consignee varchar(32) not null comment '收货人姓名',
    province varchar(32) not null default '' comment '省份名称',
    city varchar(32) not null default '' comment '城市名称',
    area varchar(32) not null default '' comment '地区名称',
    street varchar(255) not null default '' comment '街道',
    zip varchar(8) not null default '' comment '邮政编码',
    telphone varchar(32) not null default '' comment '联系电话',
    is_default char(1) not null default 0 comment '是否默认地址',
    ordinal smallint not null default 0 comment '排列次序',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX ix_user_id (user_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_收货地址表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';
