create table if not exists tuser_activation(
    id int auto_increment primary key comment '主键编号',
    user_id int not null default 0 comment '用户编号',
    `number` int not null default 0 comment '数量',
    price decimal(12,2) not null default 0.00 comment '价格',
    discount_amount decimal(12,2) not null default 0.00 comment '折扣金额',
    amount decimal(12,2) not null default 0.00 comment '金额',
    `count` int not null default 0 comment '激活次数',
    activate_type varchar(16) not null default '' comment '激活类型(register=注册,renewal=续费)',
    expire_time datetime comment '过期时间',
    ip varchar(32) not null default '' comment 'IP地址',
    `state` smallint not null default 0 comment '激活状态(0=待审核,2=受理中,1=审核成功,4=审核失败',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '申请时间',
    update_time datetime comment '受理时间'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户(代理)-激活表\r\n@since 1.0 <2014-6-27> sutroon <14507247@qq.com> Added.';
