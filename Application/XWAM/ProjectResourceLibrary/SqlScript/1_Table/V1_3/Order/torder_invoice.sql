create table if not exists torder_invoice(
    order_id int primary key comment '订单表主键编号',
    `type` varchar(16) not null default '' comment '发票类型(普通发票,增值税票)',
    title varchar(32) not null default '' comment '发票抬头',
    title_name varchar(32) not null default '' comment '发票抬头名称',
    title_content varchar(32) not null default '' comment '发票抬头内容名称',
    content_name varchar(32) not null default '' comment '发票内容',
    company_name varchar(32) not null default '' comment '增值税-公司全称',
    telphone varchar(32) not null default '' comment '增值税-联系电话',
    address varchar(255) not null default '' comment '增值税-公司地址',
    bank_name varchar(32) not null default '' comment '增值税-开户行',
    bank_no varchar(32) not null default '' comment '增值税-银行帐号',
    tax_code varchar(32) not null default '' comment '增值税-税号',
    tax  decimal(8,2) not null default 0.00 comment '开票税金',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 comment '订单_发票表\r\n@since 1.0 <2014-6-28> sutroon <14507247@qq.com> Added.';
