
create table if not exists tuser_coupon (
    id int auto_increment primary key comment '主键编号',
    user_id_to int not null comment '受赠人用户编号',
    user_id_from int not null comment '赠送人用户编号',
    ex_tags varchar(16) not null comment '扩展标签',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_优惠券表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';

create table if not exists tuser_credit (
    user_id int not null primary key comment '用户编号',
    credit  decimal(12,2) not null default 0.00 comment '主积分的值',
    ext_cerdit1  decimal(12,2) not null default 0.00 comment '扩展积分1',
    ext_cerdit2  decimal(12,2) not null default 0.00 comment '扩展积分2',
    ext_cerdit3  decimal(12,2) not null default 0.00 comment '扩展积分3',
    ext_cerdit4  decimal(12,2) not null default 0.00 comment '扩展积分4',
    ext_cerdit5  decimal(12,2) not null default 0.00 comment '扩展积分5',
    ext_cerdit6  decimal(12,2) not null default 0.00 comment '扩展积分6',
    ext_cerdit7  decimal(12,2) not null default 0.00 comment '扩展积分7',
    ext_cerdit8  decimal(12,2) not null default 0.00 comment '扩展积分8',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    modified_date datetime comment '记录修改时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_积分表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';

create table if not exists tuser_credit_log (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null default '' comment '标题',
    credit_name varchar(32) not null default '' comment '积分名称',
    credit_value  decimal(12,2) not null comment '交易金额',
    trade_type smallint not null default 0 comment '交易类型(对应tgeneral_data.ex_tags=CREDIT-TRADE-TYPE)',
    user_id_to int not null comment '接受交易用户编号',
    user_id_from int not null comment '发起交易用户编号',
    remark varchar(64) not null default '' comment '备注',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_积分日志表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';

create table if not exists tuser_address (
    id int auto_increment primary key comment '主键编号',
    user_id int not null comment '用户编号',
    consignee varchar(32) not null comment '收货人姓名',
    province varchar(32) not null default '' comment '省份名称',
    city varchar(32) not null default '' comment '城市名称',
    area varchar(32) not null default '' comment '地区名称',
    street varchar(255) not null default '' comment '街道',
    zip varchar(8) not null default '' comment '邮政编码',
    telphone varchar(32) not null default '' comment '联系电话',
    is_default char(1) not null default 0 comment '是否默认地址',
    ordinal smallint not null default 0 comment '排列次序',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    KEY user_id_ix (user_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_收货地址表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';

create table if not exists tuser_favorite (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null default '' comment '标题',
    url varchar(255) not null default '' comment 'URL地址',
    object_id int not null default 0 comment '对象主键编号',
    category_id int not null default 0 comment '类别编号',
    star smallint not null default 0 comment '星级',
    remark varchar(255) not null default '' comment '备注',    
    user_id int not null default 0 comment '用户编号',
    ex_tags varchar(16) not null comment '扩展标签',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_收藏表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';

create table if not exists tuser (
    id int auto_increment primary key comment '主键编号',
    login_name varchar(32) not null comment '用户名称(登录名)',
    password varchar(128) not null comment '登录密码(带有*的是加密过的密码,否则是明文密码)',
    user_type varchar(16) not null comment '内置用户类型(SEAT=座席,LEADER=营销组长,ADMIN=普通管理员,SUPERADMIN=超级管理员,AGENT=代理,EMPLOYEE=员工,CUSTOMER=客户)',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,3=冻结,4=禁用,7=注销,14=回收站)',
    UNIQUE KEY login_name_ix (login_name),
    KEY user_type_ix (user_type)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 comment '用户表\r\n@since 1.0 <2015-4-8> sutroon <14507247@qq.com> Added.';
-- --------------------------------------------------
-- Records of tuser
-- --------------------------------------------------
insert into tuser (login_name, `password`, user_type, `state`) values ('superadmin', 'superadmin!@#', 'SUPERADMIN', 1);

create table if not exists tuser_common (
    user_id int not null primary key comment '用户表主键编号',
    trade_password varchar(128) not null default '' comment '交易密码(带有*的是加密过的密码,否则是明文密码)',
    personal_name varchar(32) not null default '' comment '姓名',
    sex smallint not null default 0 comment '性别(0=女,1=男,2=保密)',
    face_url varchar(255) not null default '' comment '用户头像URL',
    marital_status smallint not null default 0 comment '婚姻状况(0=未婚,1=已婚)',
    birthday datetime comment '生日',
    id_type varchar(16) not null default '' comment '证件类型(身份证,学生证,工作证,士兵证,军官证,护照)',
    id_no varchar(32) not null default '' comment '证件号码',
    extension varchar(16) not null default '' comment '电话分机号',
    telphone varchar(18) not null default '' comment '电话号码',
    email varchar(64) not null default '' comment '电子邮件',
    contacts varchar(128) not null default '' comment '其他联系方式(格式:{QQ:value1,SKYPE:value2}',
    address varchar(255) not null default '' comment '联系地址',
    regist_ip varchar(16) not null comment '注册IP',
    login_ip varchar(16) not null comment '登录IP',
    login_count int not null default 0 comment '登录次数',
    update_time datetime comment '更新时间',
    role_id int not null default 0 comment '角色编号',
    personal_sign  varchar(128) not null default '' comment '个性签名',
    custom_status varchar(16) not null default '' comment '自定义头衔',
    remark  varchar(32) not null default '' comment '备注',
    online_state smallint not null default 0 comment '在线状态(0=离线,1=在线,2=隐身,3=离开)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号,在此表功能相当于parent_id,超级管理员可以给每个公司分配一个顶级管理员(site_id=0),顶级管理员可以再给本公司分配多个普通管理员,site_id=该顶级管理员的uid',
    KEY personal_name_ix (personal_name),
    KEY site_id_ix (site_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_通用属性表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.\r\n@since 2.0.0 <2015-4-8> SoChishun <14507247@qq.com> 拆分成tuser和tuser_common表';

create table if not exists tuser_cti (
    user_id int not null primary key comment '用户表主键编号',
    extension varchar(16) not null comment '电话分机号码',
    secret varchar(16) not null default '' comment '分机密码(注册在分机上面的密码,一般设置后不会再修改,默认123456)',
    queue varchar(16) not null default '' comment '队列号',
    trunk varchar(16) not null default '' comment '中继名称',
    caller_no varchar(16) not null default '' comment '主叫号码',
    reg_mode varchar(12) not null default '' comment '中继注册模式(register=注册模式,ip=IP模式)',
    model varchar(16) not null default '' comment '模式(Leader=班长模式,SEAT=普通员工)',
    forwarding_extension varchar(16) not null default '' comment '呼叫转移号码',
    forwarding_state smallint not null default 0 comment '呼叫转移状态(0=未设置,1=无条件转移,2=忙时转移,3=无应答或不可用时转移)',
    max_task int not null default 0 comment '任务最大数量',
    keep_call_history_days int not null default 0 comment '通话清单最大保留时间',
    keep_record_history_days int not null default 0 comment '录音最大保留时间',
    out_fee_rate int not null default 0 comment '呼出使用费率',
    in_fee_rate int not null default 0 comment '呼入使用费率',
    sms_fee_rate int not null default 0 comment '短信使用费率',
    overdraft_amount decimal(12,4) not null default 0.0000 comment '可透支金额',
    blance decimal(12,4) not null default 0.0000 comment '账户余额',
    pay_type smallint not null default 1 comment '支付类型(0=后付费,1=预付费)',
    rec_state smallint not null default 1 comment '录音标识(1=启用,0=禁用)',
    dtmfmode varchar(255) not null default '' comment 'DTMF模式(如:rfc2833)',
    canreinvite varchar(32) not null default '' comment '(如:no,update)',
    dialopts_cb smallint not null default 0 comment '是否覆盖拨号选项',
    dialopts varchar(32) not null default '' comment '拨号选项名称',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,3=冻结,4=禁用,7=注销)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    sys_code varchar(128) not null default '',
    KEY extension_ix (extension)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_呼叫中心属性表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';

create table if not exists tuser_employee(
    user_id int not null primary key comment '用户表主键编号',
    emp_no varchar(32) not null comment '员工编号',
    native_place varchar(64) not null default '' comment '籍贯',
    emergency_contact varchar(32) not null default '' comment '紧急联系人姓名',
    emergency_telphone varchar(32) not null default '' comment '紧急联系人电话',
    nation varchar(16) not null default '' comment '民族',
    education varchar(16) not null default '' comment '文化程度',
    political_appearance varchar(16) not null default '' comment '政治面貌',
    hobbies varchar(128) not null default '' comment '兴趣爱好',
    special_skill varchar(128) not null default '' comment '特殊技能',
    job varchar(16) not null default '' comment '职业',
    `position` varchar(16) not null default '' comment '职位', 
    commission_id smallint not null default 0 comment '提成编号',
    wages int not null default 0 comment '薪资',
    wages_grade_id int not null default 0 comment '薪资等级编号(tgeneral_data.ex_tags=WAGES-GRADE)',
    performance_bonus  decimal(12,2) not null default 0.00 comment '绩效奖金',
    kpi_amount  decimal(12,2) not null default 0.00 comment 'KPI金额(kpi_score,kpi_bonus)',
    employment_date datetime comment '入职日期',
    termination_date datetime comment '离职日期',
    termination_type smallint not null default 0 comment '离职类型(0=在职,1=自离,2=辞退)',
    position_type smallint not null default 0 comment '职位类型(0=实习生,1=合同工,2=兼职,3=临时工)',
    contract_state smallint not null default 0 comment '合同状态(0=未签,1=已签)',
    contract_begin datetime comment '合同生效时间',
    contract_end datetime comment '合同结束时间',
    social_security_state smallint not null default 0 comment '社保状态(0=未交,1=已交)',
    social_security_begin datetime comment '社保缴交时间',
    social_security_end datetime comment '社保结束时间',
    department_id int not null default 0 comment '部门编号'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_员工属性表\r\n@since 1.0 <2014-6-27> sutroon <14507247@qq.com> Added.';

create table if not exists tuser_agent(
    user_id int not null primary key comment '用户表主键编号',
    quota_number int not null default 0 comment '配额数量',
    expire_time datetime comment '过期时间',
    update_time datetime comment '上次续费时间',
    agent_state smallint not null default 0 comment '代理状态(0=未审核,1=正常,3=冻结,4=禁用,7=注销)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_代理商属性表\r\n@since 1.0 <2014-7-11> sutroon <14507247@qq.com> Added.';

create table if not exists tuser_preferences(
    user_id int not null primary key comment '用户表主键编号',
    page_size smallint not null default 25 comment '列表分页尺寸,默认25条',
    theme varchar(32) not null default '' comment '网站外观主题',
    lang varchar(32) not null default '' comment '语言'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_偏好设置(首选项)表\r\n@since 1.0 <2014-9-25> sutroon <14507247@qq.com> Added.';

create table if not exists tuser_customer(
    user_id int not null primary key comment '用户表主键编号',
    serial_no varchar(64) not null comment '客户编号(KH[site_id]0[user_id][Ymdhis])',
    interested smallint not null default 0 comment '购买意向(0=普通,1=意向客户,2=特别兴趣)',
    contact_results smallint not null default 0 comment '营销结果(联系结果,4=拒绝;1=成功;2=对方为老人或小孩;3=接通后即结束通话;5=接通后不说话)',
    tags varchar(64) not null default '' comment '自定义标签(多个值以英文逗号隔开)',
    `level` varchar(16) not null default '' comment '客户等级(金卡,银卡,铜卡)',
    buy_count smallint not null default 0 comment '消费次数',
    integral int not null default 0 comment '累计积分',
    cumulative_amount decimal(12,2) not null default 0.00 comment '累计消费金额',
    last_buy_time datetime comment '最后消费时间',
    last_visit_time datetime comment '最后沟通时间',
    referrer_user_id int not null default 0 comment '推荐人用户编号',
    agent_id int not null default 0 comment '营销员用户编号(user_id=0表示导入的客户)',
    old_agent_id int not null default 0 comment '被调度营销员用户编号(此值大于0说明是被调度的)',
    ex_tags varchar(32) not null default '' comment '系统标签(IMPORT=导入,ADD=手动添加,CTI=来电弹屏)',
    UNIQUE KEY serial_no_ix (serial_no)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_客户表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';

create table if not exists tuser_activation(
    id int auto_increment primary key comment '主键编号',
    user_id int not null default 0 comment '用户编号',
    `number` int not null default 0 comment '数量',
    price decimal(12,2) not null default 0.00 comment '价格',
    discount_amount decimal(12,2) not null default 0.00 comment '折扣金额',
    amount decimal(12,2) not null default 0.00 comment '金额',
    `count` int not null default 0 comment '激活次数',
    activate_type varchar(16) not null default '' comment '激活类型(register=注册,renewal=续费)',
    expire_time datetime comment '过期时间',
    ip varchar(32) not null default '' comment 'IP地址',
    `state` smallint not null default 0 comment '激活状态(0=待审核,2=受理中,1=审核成功,4=审核失败',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '申请时间',
    update_time datetime comment '受理时间'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户(代理)-激活表\r\n@since 1.0 <2014-6-27> sutroon <14507247@qq.com> Added.';

create table if not exists tuser_payment (
    id int auto_increment primary key comment '主键编号',
    bank_name varchar(32) not null default '' comment '开户支行名称',
    bankcard_no varchar(32) not null default '' comment '银行卡卡号',
    bankcard_holder varchar(32) not null default '' comment '持卡人姓名',
    user_id int not null default 0 comment '用户编号',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_支付方式表\r\n@since 1.0 <2014-6-28> sutroon <14507247@qq.com> Added.';

create table if not exists tuser_permission (
    id int auto_increment primary key comment '主键编号',
    `name` varchar(16) not null comment '权限名称',
    parent_id int not null default 0 comment '父级编号',
    code varchar(32) not null default '' comment '菜单编号(一级:XX_MK,二级:XX_X1MK,三级:XX_X11_GL,四级:XX_XZX111)',
    is_menu smallint not null default 0 comment '是否是菜单',
    link_url varchar(255) not null default '' comment '链接URL',
    ordinal smallint not null default 0 comment '排列次序',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '修改时间(根据修改时间决定menu_key_sha1是否变动)',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_权限表\r\n@since 1.0 <2014-6-28> sutroon <14507247@qq.com> Added.';

create table if not exists tuser_department(
    id int auto_increment primary key comment '主键编号',
    dpt_name varchar(32) not null comment '部门名称',
    parent_id int not null default 0 comment '父级编号',
    dpt_code varchar(8) not null default '' comment '部门代号(如营销部门=YXBM,营销小组=YXXZ)',
    permission_rule varchar(512) not null default '' comment '权限规则',
    ordinal smallint not null default 0 comment '排列次序',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_部门表\r\n@since 1.0 <2014-7-11> sutroon <14507247@qq.com> Added.';

create table if not exists tuser_role(
    id int auto_increment primary key comment '主键编号',
    role_name varchar(32) not null comment '角色名称',
    parent_id int not null default 0 comment '上级角色编号',
    role_code varchar(16) not null default '' comment '角色代码(SEAT=营销专员,LEADER=营销组长ADMIN=普通管理员,SUPERADMIN=超级管理员,AGENT=代理,EMPLOYEE=员工)',
    permission_rule varchar(512) not null default '' comment '权限规则',
    ordinal smallint not null default 0 comment '排列次序',
    remark varchar(32) not null default '' comment '备注',
    `state` smallint not null default 1 comment '角色状态(0=禁用,1=正常)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_角色表\r\n@since 1.0 <2014-7-11> sutroon <14507247@qq.com> Added.';
-- --------------------------------------------------
-- Records of tuser_role
-- --------------------------------------------------
insert into tuser_role (role_name, parent_id, role_code, permission_rule) values ('管理员', 0, 'ADMIN', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115');

create table if not exists tuser_subscribe (
    id int auto_increment primary key comment '主键编号',
    email varchar(32) not null default '' comment '电子邮件',
    user_id int not null default 0 comment '用户编号',
    ex_tags varchar(16) not null comment '扩展标签',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_订阅表\r\n@since 1.0 <2014-6-28> sutroon <14507247@qq.com> Added.';

create table if not exists tcustomer_appointment(
    id int auto_increment primary key comment '主键编号',
    customer_id int not null comment '客户表主键编号',
    `time` datetime comment '预约时间',
    remark varchar(32) not null default '' comment '预约备注',
    `result` varchar(128) not null default '' comment '回访结果',
    `source` varchar(16) not null default '' comment '预约来源(USER=人工预约,ORDER=订单回访)',
    `state` smallint not null default 0 comment '回访状态(0=未回访,1=回访成功,2=回访失败,4=放弃回访)',
    update_time datetime comment '回访时间',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '客户_预约表\r\n@since 1.0 <2014-12-24> sutroon <14507247@qq.com> Added.';

create table if not exists tcustomer_dispatch(
    id int auto_increment primary key comment '主键编号',
    `name` varchar(32) not null default '' comment '批次名称(默认时间戳)',
    customer_id int not null comment '客户表主键编号',
    old_user_id int not null comment '原营销员用户编号',
    user_id int not null comment '新营销员用户编号',
    admin_id int not null comment '管理员(操作者)用户编号',
    remark varchar(32) not null default '' comment '备注',
    `state` smallint not null default 0 comment '状态',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '客户_调度表\r\n@since 1.0 <2014-12-24> sutroon <14507247@qq.com> Added.';