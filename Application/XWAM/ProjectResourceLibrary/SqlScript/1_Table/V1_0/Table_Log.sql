
create table if not exists tlog_user_login (
    id int auto_increment primary key comment '主键编号',
    user_id int not null comment '用户表主键编号',
    user_name varchar(64) not null default '' comment '用户登录名',
    ip varchar(16) not null comment '登录IP',
    `status` bit not null default 0 comment '登录状态(1=成功,4=失败)',
    `type` bit not null default 1 comment '登录类型(1=登入,2=登出)',
    remark  varchar(128) not null default '' comment '备注(如登录信息：[登录错误] 密码：123)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '登录时间',
    KEY user_id_ix (user_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '日志_用户登录表\r\n@since 1.0 <2015-4-8> sutroon <14507247@qq.com> Added.\r\n@since 2.0 <2015-6-10> SoChishun tuser_login_log重命名为tlog_user_login';

create table if not exists tlog_user_operate(
    id int auto_increment primary key comment '主键编号',
    user_id int not null comment '用户编号',
    ip varchar(32) not null default '' comment 'IP',
    `action` varchar(16) not null default '' comment '事件动作(如:充值、查询、登录、新增、编辑、删除)',
    `object` varchar(32) not null default '' comment '事件对象(如:存储过程、表名、视图名、页面)',
    `source` varchar(255) not null default '' comment '事件来源(如:网址,PROCEDURE)',
    `level` varchar(12) not null default '' comment '事件级别(如:信息、错误、警告、攻击、调试)',
    `desc` varchar(20480) not null default '' comment '描述(如:充值100元)',
    `status` smallint not null default 0 comment '事件状态(1=成功,4=失败)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    KEY user_id_ix (user_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_事件日志表\r\n@since 1.0 <2014-6-27> sutroon <14507247@qq.com> Added.\r\n@since 2.0 <2015-6-10> SoChishun tuser_event_log重命名为tlog_user_operate';


create table if not exists tlog_product_operate(
    id int auto_increment primary key comment '主键编号',
    user_id int not null comment '用户编号',
    ip varchar(32) not null default '' comment 'IP',
    e_action varchar(16) not null default '' comment '事件动作(如:充值、查询、登录、新增、编辑、删除)',
    e_object varchar(32) not null default '' comment '事件对象(如:存储过程、表名、视图名、页面)',
    e_source varchar(255) not null default '' comment '事件来源(如:网址)',
    e_level varchar(12) not null default '' comment '事件级别(如:信息、错误、警告、攻击)',
    e_desc varchar(20480) not null default '' comment '描述(如:充值100元)',
    ex_tags varchar(16) not null comment '扩展标签',
    `state` smallint not null default 0 comment '事件结果(1=成功,4=失败)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '商品_操作日志表\r\n@since 1.0 <2014-6-27> sutroon <14507247@qq.com> Added.\r\n@since 2.0 <2015-6-10> SoChishun tproduct_event_log重命名为tlog_product_operate';
