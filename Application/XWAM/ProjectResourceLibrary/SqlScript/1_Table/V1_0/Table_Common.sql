/**
* 数据表
*
* ===================规范=================
1.拆分表(垂直分割)：如博客，你需要在许多不同地方显示标题（如最近的文章列表），只在某个特定页显示概要或者全文。水平垂直拆分是很有帮助的
CREATE TABLE posts (
    id int UNSIGNED NOT NULL AUTO_INCREMENT,
    author_id int UNSIGNED NOT NULL,
    title varchar(128),
    created timestamp NOT NULL,
    PRIMARY KEY(id)
);
 
CREATE TABLE posts_data (
    post_id int UNSIGNED NOT NULL,
    teaser text,
    body text,
    PRIMARY KEY(post_id)
);
2.不要过度使用artificial primary key，在没有必要使用自增长主键的地方就不要使用自增长主键
3.为搜索字段建立索引(like操作性能相差4倍左右）
4.尽可能使用not null,因为null需要额外的空间
5.拆分大的 DELETE 或 INSERT 语句
while (1) {
    //每次只做1000条
    mysql_query("DELETE FROM logs WHERE log_date <= '2009-11-01' LIMIT 1000");
    if (mysql_affected_rows() == 0) {
        // 没得可删了，退出！
        break;
    }
    // 每次都要休息一会儿
    usleep(50000);
}
* ===================/规范================
* @since\r\n@since 1.0 <2014-6-21 SoChishun <14507247@qq.com> Added.
* @since 1.1 2015-3-24 SoChishun 为适应_t_p323字段规范将大小写混合的字段名改为小写加下划线方式命名
* @since 2.0 2015-4-8 SoChishun 拆分Table.sql重名为Table_Common, Table_Finance, Table_Order, Table_Product, Table_Article, Table_User等文件, 新增表注释和默认记录,tgeneral_重名为tcommon_
* @remark sys_code为重要数据冗余加密字段,加密规则为:field1=value1&field2=value2&timestamp=now,使用somda加密
*/

create table if not exists tcommon_media (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null default '' comment '标题',
    parent_id int not null default 0 comment '父级编号',
    item_id int not null comment '项目编号',
    thumb_url varchar(255) not null default '' comment '缩略图路径',
    picture_url varchar(255) not null default '' comment '图片路径',
    link_url varchar(255) not null default '' comment '链接路径',
    content varchar(5120) not null default '' comment '内容',
    is_period_time smallint not null default 0 comment '是否周期时间',
    begin_time datetime comment '生效时间',
    end_time datetime comment '失效时间',
    tags_id varchar(16) not null default '' comment '标签编号',
    description varchar(10240) not null default '' comment '描述',
    region varchar(255) not null default '' comment '使用范围',
    ex_tags varchar(16) not null comment '扩展标签',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用广告媒体表\r\n@since 1.0 <2014-6-23> sutroon <14507247@qq.com> Added.';

create table if not exists tcommon_data (
    id int auto_increment primary key comment '主键编号',
    `name` varchar(32) not null default '' comment '名称',
    `value` varchar(20480) not null default '' comment '值',
    `code` varchar(32) not null default '' comment '代码',
    parent_id int not null default 0 comment '父级编号',
    group_tags varchar(32) not null default '' comment '群组标签',
    link_url varchar(255) not null default '' comment '链接路径',
    remark varchar(32) not null default '' comment '备注',
    user_id int not null default 0 comment '用户编号',
    ex_tags varchar(16) not null default '' comment '扩展标签(如:SQL-DEBUG)',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用字典表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';

create table if not exists tcommon_category (
    id int auto_increment primary key comment '主键编号',
    category_name varchar(32) not null comment '名称',
    parent_id int not null default 0 comment '父级编号',
    user_id int not null default 0 comment '用户编号',
    remark varchar(32) not null default '' comment '备注',
    ex_tags varchar(16) not null comment '扩展标签',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用-类别表\r\n@since 1.0 <2015-5-5> sutroon <14507247@qq.com> Added.';

create table if not exists tcommon_region (
    id int auto_increment primary key comment '主键编号',
    region_name varchar(32) not null comment '地区名称',
    parent_id int not null default 0 comment '上级地址编号',
    zip varchar(8) not null default '' comment '邮政编码'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '中国地区表\r\n@since 1.0 <2014-12-8> sutroon <14507247@qq.com> Added.';

create table if not exists tcommon_file (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null default '' comment '标题',
    file_name varchar(64) not null default '' comment '文件名称',
    file_path varchar(255) not null default '' comment '文件路径',
    file_size varchar(32) not null default '' comment '文件大小',
    file_type varchar(32) not null default '' comment '文件类型',
    file_ext varchar(8) not null default '' comment '文件扩展名',
    parent_id int not null default 0 comment '文件夹编号',
    last_dir varchar(32) not null default '' comment '所属文件夹名称',
    width int not null default 0 comment '图片宽度',
    height int not null default 0 comment '图片高度',
    category_id int not null default 0 comment '类别编号(相册编号)',
    item_id int not null default 0 comment '项目编号',
    item_target varchar(32) not null default '' comment '项目类型(如ARTICLE=文章)',
    description varchar(2048) not null default '' comment '描述',
    visit_count int not null default 0 comment '浏览次数',
    reply_count int not null default 0 comment '回复次数',
    download_count int not null default 0 comment '下载次数',
    access_permission varchar(1024) not null default '' comment '访问权限(如相册中FRIEND=朋友,ALL=公开,PRIVATE=私密,[uid1,uid2...]=指定用户)',
    user_id int not null default 0 comment '上传者用户编号',
    params varchar(255) not null default '' comment '参数(JSON字符串)',
    remark varchar(128) not null default '' comment '备注',
    ex_tags varchar(16) not null default '' comment '扩展标签',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=禁用,1=启用)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用文件表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';

create table if not exists tcommon_relationmap (
    id int auto_increment primary key comment '主键编号',
    master_item_id int not null comment '主项编号',
    slave_item_id int not null comment '附件编号',
    ordinal smallint not null default 0 comment '排列次序',
    ex_tags varchar(32) not null comment '扩展标签(如:PRODUCT-ACCESSORY=商品配件)',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用关联表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';

create table if not exists tcommon_server (
    id int auto_increment primary key comment '主键编号',
    provider varchar(32) not null default '' comment '服务器名称',
    alias_name varchar(32) not null default '' comment '别称',
    `host` varchar(255) not null default '' comment '服务器地址(URI)',
    port int not null default 0 comment '端口号',
    conn_type varchar(32) not null default '' comment '连接类型,URL或IP',
    login_id varchar(32) not null default '' comment '登录名',
    password varchar(128) not null default '' comment '密码',
    try_count int not null default 0 comment '登录失败重连次数',
    expire_time datetime comment '登录超时时间',
    remark varchar(32) not null default '' comment '备注',
    ex_tags varchar(16) not null default '' comment '扩展标签',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用服务器表(集群用户端表)\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';

create table if not exists tcommon_tag(
    id int auto_increment primary key comment '主键编号',
    `name` varchar(32) not null comment '标签名称',
    ordinal smallint not null default 0 comment '排列次序',
    ex_tags varchar(16) not null comment '扩展标签',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用标签表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';

create table if not exists tcommon_links (
    id int auto_increment primary key comment '主键编号',
    `name` varchar(32) not null comment '链接名称',
    link_url varchar(255) not null comment '链接URL',
    `type` varchar(16) not null default '' comment '链接类型(friend=友情链接)',
    image_url varchar(255) not null default '' comment '图片URL',
    remark varchar(128) not null default '' comment '备注',
    user_id int not null default 0 comment '用户名编号',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用友情链接表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';

create table if not exists tcommon_message (
    id int auto_increment primary key comment '主键编号',
    from_user varchar(64) not null comment '寄件人',
    to_user varchar(128) not null comment '收件人(多个之间以逗号隔开,如:user1,user2)',
    from_alias varchar(32) not null default '' comment '寄件人别名',
    subject varchar(32) not null default '' comment '消息标题',
    body varchar(2048) not null default '' comment '消息正文',
    from_folder varchar(16) not null comment 'pm或email发件箱名称',
    to_folder varchar(16) not null comment 'pm或email收件箱名称',
    limit_count smallint not null default 5 comment '重试发送次数',
    send_count smallint not null default 0 comment '已尝试发送次数',
    send_time datetime comment '发送开始时间',
    expire_time datetime comment '发送超时时间',
    read_time datetime comment '阅读时间',
    ex_tags varchar(16) not null comment '扩展标签,PM=站内信,SMS=手机短信,EMAIL=电子邮件',
    `state` smallint not null default 0 comment '状态(0=未发送,1=发送成功,4=发送失败,2=正在发送)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用消息表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';

create table if not exists tcommon_recruitment (
    id int auto_increment primary key comment '主键编号',
    serial_no varchar(64) not null comment '流水号(ZP[site_id]-[timestamp]-[user_id])',
    `name` varchar(32) not null comment '职位名称',
    category_id int not null default 0 comment '类别编号',
    department varchar(128) not null default '' comment '部门名称(如:市场部)',
    quantity varchar(16) not null default '' comment '招聘人数(如:若干)',
    office_region varchar(32) not null default '' comment '办公地点',
    expiry_date varchar(32) not null default '' comment '过期时间(如:不限)',
    education varchar(16) not null default '' comment '学历',
    foreign_language varchar(64) not null default '' comment '外语水平',
    work_experience varchar(1024) not null default '' comment '工作经历',
    responsibilities varchar(1024) not null default '' comment '岗位职责',
    job_requirements varchar(1024) not null default '' comment '岗位要求',
    wages varchar(64) not null default '' comment '薪资待遇(如:面谈)',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用招聘信息表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';

create table if not exists tcommon_import(
    id int auto_increment primary key comment '主键编号',
    `name` varchar(64) not null comment '名称,默认文件名称',
    file_path varchar(255) not null comment '文件保存路径',
    filter_file_duplicate smallint not null default 1 comment '是否比对文件本身并过滤重复号码',
    filter_db_duplicate smallint not null default 1 comment '是否比对数据库并过滤重复号码',
    rule char(1) not null default 'O' comment '生成规则(O(ORDER)=顺序保存,R(RANDOM)=乱序)',
    file_record_count int not null default 0 comment '源文件中记录数量',
    db_record_count int not null default 0 comment '保存到数据库中的记录数量',
    filter_record_count int not null default 0 comment '过滤的记录数量',
    params varchar(255) not null default '' comment '参数信息(JSON格式)',
    remark varchar(255) not null default '' comment '备注',
    ex_tags varchar(16) not null default '' comment '扩展标签(CUSTOMER=客户资料,CALL-NUMBER=外呼号码,PRODUCT=商品)',
    `state` smallint not null default 0 comment '状态',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    user_id int not null default 0 comment '用户编号',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用导入文件表\r\n@since 1.0 <2015-5-12 SoChishun Added';

create table if not exists tcommon_exfield(
    id int auto_increment primary key comment '主键编号',
    field varchar(32) not null comment '字段名称(英文)',
    `type` varchar(32) not null comment '数据类型(int,smallint,decimal,varchar,date,datetime,text)',
    `length` smallint not null default 0 comment '字段长度或小数点精度',
    `null` varchar(3) not null default 'YES' comment '是否允许null',
    `default` varchar(255) not null default '' comment '默认值',
    `comment` varchar(255) not null default '' comment '字段注释',
    `target` varchar(32) not null comment '模块名称',
    label varchar(32) not null default '' comment '标签名称(中文)',
    input_type varchar(32) not null default '' comment '组件类型(text,date,time,select,radio)',
    optional_value varchar(512) not null default '' comment '可选值,用于radio,select,checkbox等控件类型,值之间用英文逗号隔开',
    is_require smallint not null default 0 comment '是否必填',
    is_visiable smallint not null default 1 comment '是否显示',
    allow_search smallint not null default 0 comment '允许搜索',
    ordinal smallint not null default 0 comment '排序',
    `state` smallint not null default 1 comment '状态',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '数据库自定义扩展字段表\r\n@since 1.0 <2014-6-25> sutroon; 2.0 2014-11-11> sutroon <14507247@qq.com> Added.';

create table if not exists tcommon_statistics(
    id int auto_increment primary key comment '主键编号',
    stat_key varchar(32) not null default '' comment '统计键名',
    int_value int not null default 0 comment '整数值',
    flt_value decimal(12,2) not null default 0.00 comment '小数值',
    remark varchar(64) not null default '' comment '备注',
    `state` smallint not null default 0 comment '状态(0=无效,1=有效)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '系统统计表(可分表)\r\n@since 1.0 <2015-3-13> sutroon <14507247@qq.com> Added.';

create table if not exists tsite_config (
    id int auto_increment primary key comment '主键编号',
    user_id int not null default 0 comment '用户编号(0=系统默认)',
    site_name varchar(32) not null comment '站点名称',
    category_id int not null default 0 comment '类别编号',
    logo_url varchar(255) not null default '' comment '图标URL',
    home_page varchar(255) not null default '' comment '主页地址',
    icp_license_code varchar(32) not null default '' comment 'ICP备案号',
    contact_name varchar(32) not null default '' comment '联系人姓名',
    contact_telphone varchar(32) not null default '' comment '联系电话',
    contact_mobile varchar(32) not null default '' comment '手机号码',
    contact_address varchar(255) not null default '' comment '联系地址',
    company_name varchar(32) not null default '' comment '公司名称',
    closed_announcement varchar(255) not null default '' comment '停业通知',
    ordinal smallint not null default 0 comment '排列次序',
    ex_tags varchar(16) not null default '' comment '扩展标签(SHOP=网店,BLOG=博客)',
    `state` smallint not null default 0 comment '状态(0=待审核,1=运行中,4=关停)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    index idx_site_name (site_name),
    key idx_user_id (user_id, ex_tags)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '站点配置表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.\r\n@since 1.1<2015-3-26> SoChishun <14507247@qq.com> tuser_shop改为tsite_config.\r\n@since 1.2<2015-5-19> SoChishun <14507247@qq.com> 拆分部分大字段到新的tsite_infomation表';

create table if not exists tsite_infomation (
    id int auto_increment primary key comment '主键编号',    
    title varchar(32) not null comment '标题',
    content varchar(20000) not null default '' comment '内容',
    summary varchar(128) not null default '' comment '摘要',
    visit_count int not null default 0 comment '浏览次数',
    user_id int not null default 0 comment '用户编号',
    ex_tags varchar(16) not null default '' comment '扩展标签(ABOUT_US=关于我们,CONTACT_US=联系我们)',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime not null comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    key idx_extags (ex_tags, site_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '站点信息表(包含关于我们、联系我们等单页资料)\r\n@since 1.0 <2015-5-19> SoChishun <14507247@qq.com> Added.';

create table if not exists tcommon_special (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null comment '标题',
    category_id int not null default 0 comment '类别编号',
    content varchar(20480) not null default '' comment '内容',
    summary varchar(128) not null default '' comment '摘要',
    image_url varchar(255) not null default '' comment '图片路径',
    visit_count int not null default 0 comment '浏览次数',
    reply_count int not null default 0 comment '回复次数',
    user_id int not null default 0 comment '用户编号',
    begin_time datetime comment '开始时间',
    end_time datetime comment '结束时间',
    access_permission varchar(512) not null default '' comment '访问权限(如相册中FRIEND=朋友,ALL=公开,PRIVATE=私密)',
    ex_tags varchar(16) not null default '' comment '扩展标签',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime not null comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用专题表\r\n@since 1.0 <2015-5-19> SoChishun <14507247@qq.com> Added.';