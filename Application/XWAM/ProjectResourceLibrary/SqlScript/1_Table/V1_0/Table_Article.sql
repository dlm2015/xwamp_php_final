create table if not exists tarticle (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null comment '标题',
    category_id int not null default 0 comment '类别编号',
    content varchar(20480) not null default '' comment '内容',
    summary varchar(128) not null default '' comment '摘要',
    image_url varchar(255) not null default '' comment '图片路径',
    visit_count int not null default 0 comment '浏览次数',
    reply_count int not null default 0 comment '回复次数',
    item_id int not null default 0 comment '项目编号',
    item_table varchar(32) not null default '' comment '项目所属数据表',
    user_id int not null default 0 comment '用户编号',
    access_permission varchar(512) not null default '' comment '访问权限(如相册中FRIEND=朋友,ALL=公开,PRIVATE=私密)',
    ex_tags varchar(16) not null default '' comment '扩展标签',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime not null comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '文章表\r\n@since 1.0 <2014-6-22> sutroon<14507427@qq.com> Added.';

create table if not exists tarticle_timer(
    article_id int primary key comment '文章编号',
    start_time datetime comment '开始时间',
    end_time datetime comment '失效时间',
    `state` smallint not null default 0 comment '状态(0=待发布,1=已发布,4=已撤销)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '文章_定时表\r\n@since 1.0 <2015-5-19> sutroon <14507247@qq.com> Added.';

create table if not exists tarticle_category (
    id int auto_increment primary key comment '主键编号',
    category_name varchar(32) not null comment '名称',
    parent_id int not null default 0 comment '父级编号',
    type_id int not null default 0 comment '类型编号',
    item_count int not null default 0 comment '子项目数量(如相册显示相片数量等)',
    visit_count int not null default 0 comment '浏览次数',
    meta_keywords varchar(32) not null default '' comment 'meta关键词',
    meta_description varchar(128) not null default '' comment 'meta描述',
    image_url varchar(255) not null default '' comment '图片路径',
    link_url varchar(255) not null default '' comment '链接地址',
    user_id int not null default 0 comment '用户编号',
    access_permission varchar(512) not null default '' comment '访问权限(如相册中FRIEND=朋友,ALL=公开,PRIVATE=私密)',
    remark varchar(32) not null default '' comment '备注',
    ex_tags varchar(16) not null comment '扩展标签',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '文章-类别表\r\n@since 1.0 <2015-4-8> sutroon <14507247@qq.com> Added.';

create table if not exists tfeedback (
    id int auto_increment primary key comment '主键编号',
    serial_no varchar(64) not null default '' comment '流水号(FK[site_id]0[user_id][Ymdhis])',
    title varchar(32) not null default '' comment '标题',
    content varchar(1024) not null default '' comment '内容',
    email varchar(32) not null default '' comment '电子邮件',
    mobile varchar(32) not null default '' comment '手机号码',
    key_id int not null default 0 comment '主题留言编号(第一条留言)',
    parent_id int not null default 0 comment '父级编号(留言回复对应的主题编号)',
    reply_count int not null default 0 comment '回复数量',
    category_id int not null default 0 comment '类别编号',
    category_name varchar(32) not null default '' comment '类别编号',
    item_id int not null default 0 comment '项目编号(如商品编号)',
    item_table varchar(32) not null default '' comment '项目所属数据表(如:tproduct_goods)',
    user_id int not null default 0 comment '用户编号',
    is_visiable smallint not null default 0 comment '是否显示',
    remark varchar(64) not null default '' comment '备注',
    ex_tags varchar(16) not null default '' comment '扩展标签',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '留言反馈表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';
