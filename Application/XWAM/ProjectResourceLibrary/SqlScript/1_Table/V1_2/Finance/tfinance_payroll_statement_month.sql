create table if not exists tfinance_payroll_statement_month(
    id int auto_increment primary key comment '主键编号',
    bill_date char(7) not null comment '日期,格式：2015-03(年月)',
    user_name varchar(32) not null comment '营销员工号',
    personal_name varchar(32) not null default '' comment '营销员姓名',
    sale_amount decimal(12,2) not null default 0.00 comment '销售金额',
    commission_amount decimal(12,2) not null default 0.00 comment '提成收入',
    expenditure decimal(12,2) not null default 0.00 comment '支出金额',
    bonus decimal(12,2) not null default 0.00 comment '奖金',
    leave_deduction decimal(12,2) not null default 0.00 comment '请假扣款',
    base_wage decimal(12,2) not null default 0.00 comment '基本工资',
    kpi_coefficient decimal(12,2) not null default 0.00 comment 'KPI系数',
    kpi_value decimal(12,2) not null default 0.00 comment 'KPI值',
    accrued_wage decimal(12,2) not null default 0.00 comment '应付工资(参考工资)',
    admin_name varchar(32) not null default '' comment '管理员工号',
    remark varchar(64) not null default '' comment '备注',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '财务_员工工资报表(工资单)\r\n@since 1.0 <2015-3-11> sutroon <14507247@qq.com> Added.';
