create table if not exists tuser_credit_log (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null default '' comment '标题',
    credit_name varchar(32) not null default '' comment '积分名称',
    credit_value  decimal(12,2) not null comment '交易金额',
    trade_type smallint not null default 0 comment '交易类型(对应tgeneral_data.ex_tags=CREDIT-TRADE-TYPE)',
    user_id_to int not null comment '接受交易用户编号',
    user_id_from int not null comment '发起交易用户编号',
    remark varchar(64) not null default '' comment '备注',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_积分日志表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';
