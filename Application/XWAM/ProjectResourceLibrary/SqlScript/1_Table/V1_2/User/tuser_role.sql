create table if not exists tuser_role(
    id int auto_increment primary key comment '主键编号',
    role_name varchar(32) not null comment '角色名称',
    parent_id int not null default 0 comment '上级角色编号',
    role_code varchar(16) not null default '' comment '角色代码(SEAT=营销专员(座席),LEADER=营销组长,ADMIN=普通管理员,SYSTEMADMIN=系统管理员,AGENT=代理,EMPLOYEE=员工,CUSTOMER=客户)',
    permission_rule varchar(512) not null default '' comment '权限规则',
    ordinal smallint not null default 0 comment '排列次序',
    remark varchar(32) not null default '' comment '备注',
    `state` smallint not null default 1 comment '角色状态(0=禁用,1=正常)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_角色表\r\n@since 1.0 <2014-7-11> sutroon <14507247@qq.com> Added.';