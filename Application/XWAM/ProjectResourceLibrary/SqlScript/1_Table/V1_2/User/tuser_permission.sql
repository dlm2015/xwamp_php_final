create table if not exists tuser_permission (
    id int auto_increment primary key comment '主键编号',
    `name` varchar(16) not null comment '权限名称',
    parent_id int not null default 0 comment '父级编号',
    code varchar(32) not null default '' comment '菜单代码',
    is_menu smallint not null default 0 comment '是否是菜单',
    link_url varchar(255) not null default '' comment '链接URL',
    ordinal smallint not null default 0 comment '排列次序',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '修改时间(根据修改时间决定menu_key_sha1是否变动)',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_权限表\r\n@since 1.0 <2014-6-28> sutroon <14507247@qq.com> Added.';
