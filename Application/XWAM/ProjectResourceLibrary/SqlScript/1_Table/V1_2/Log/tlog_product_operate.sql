create table if not exists tlog_product_operate(
    id int auto_increment primary key comment '主键编号',
    user_id int not null comment '用户编号',
    ip varchar(32) not null default '' comment 'IP',
    e_action varchar(16) not null default '' comment '事件动作(如:充值、查询、登录、新增、编辑、删除)',
    e_object varchar(32) not null default '' comment '事件对象(如:存储过程、表名、视图名、页面)',
    e_source varchar(255) not null default '' comment '事件来源(如:网址)',
    e_level varchar(12) not null default '' comment '事件级别(如:信息、错误、警告、攻击)',
    e_desc varchar(20480) not null default '' comment '描述(如:充值100元)',
    ex_tags varchar(16) not null comment '扩展标签',
    `state` smallint not null default 0 comment '事件结果(1=成功,4=失败)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '商品_操作日志表\r\n@since 1.0 <2014-6-27> sutroon <14507247@qq.com> Added.\r\n@since 2.0 <2015-6-10> SoChishun tproduct_event_log重命名为tlog_product_operate';
