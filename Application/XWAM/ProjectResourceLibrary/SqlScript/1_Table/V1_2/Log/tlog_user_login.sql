create table if not exists tlog_user_login (
    id int auto_increment primary key comment '主键编号',
    user_name varchar(64) not null default '' comment '用户登录名',
    ip varchar(16) not null comment '登录IP',
    `status` smallint not null default 0 comment '登录状态(1=成功,4=失败)',
    is_login smallint not null default 1 comment '是否登入(0=登出,1=登入)',
    remark  varchar(128) not null default '' comment '备注(如登录信息：[登录错误] 密码：123)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '登录时间',
    INDEX ix_user_name (user_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '日志_用户登录表\r\n@since 1.0 <2015-4-8> sutroon <14507247@qq.com> Added.\r\n@since 2.0 <2015-6-10> SoChishun tuser_login_log重命名为tlog_user_login';
