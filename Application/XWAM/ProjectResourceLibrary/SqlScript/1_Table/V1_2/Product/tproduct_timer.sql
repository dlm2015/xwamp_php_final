create table if not exists tproduct_timer(
    product_id int primary key comment '商品编号',
    start_time datetime comment '开始时间(用于秒杀商品、团购商品等)',
    end_time datetime comment '失效时间',
    `state` smallint not null default 0 comment '状态(0=待上架,1=销售中,4=已下架,40=缺货中)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '商品_定时表\r\n@since 1.0 <2015-4-1> sutroon <14507247@qq.com> Added.';
