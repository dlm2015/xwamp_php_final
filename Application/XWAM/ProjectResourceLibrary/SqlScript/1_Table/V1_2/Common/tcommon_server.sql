create table if not exists tcommon_server (
    id int auto_increment primary key comment '主键编号',
    provider varchar(32) not null default '' comment '服务器名称',
    alias_name varchar(32) not null default '' comment '别称',
    `host` varchar(255) not null default '' comment '服务器地址(URI)',
    port int not null default 0 comment '端口号',
    conn_type varchar(32) not null default '' comment '连接类型,URL或IP',
    login_id varchar(32) not null default '' comment '登录名',
    password varchar(128) not null default '' comment '密码',
    try_count int not null default 0 comment '登录失败重连次数',
    expire_time datetime comment '登录超时时间',
    remark varchar(32) not null default '' comment '备注',
    ex_tags varchar(16) not null default '' comment '扩展标签',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用服务器表(集群用户端表)\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';
