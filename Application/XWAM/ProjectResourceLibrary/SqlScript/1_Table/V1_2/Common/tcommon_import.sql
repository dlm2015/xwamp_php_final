create table if not exists tcommon_import(
    id int auto_increment primary key comment '主键编号',
    `name` varchar(64) not null comment '名称,默认文件名称',
    file_path varchar(255) not null comment '文件保存路径',
    filter_file_duplicate smallint not null default 1 comment '是否比对文件本身并过滤重复号码',
    filter_db_duplicate smallint not null default 1 comment '是否比对数据库并过滤重复号码',
    rule char(1) not null default 'O' comment '生成规则(O(ORDER)=顺序保存,R(RANDOM)=乱序)',
    file_record_count int not null default 0 comment '源文件中记录数量',
    db_record_count int not null default 0 comment '保存到数据库中的记录数量',
    filter_record_count int not null default 0 comment '过滤的记录数量',
    params varchar(255) not null default '' comment '参数信息(JSON格式)',
    remark varchar(255) not null default '' comment '备注',
    ex_tags varchar(16) not null default '' comment '扩展标签(CUSTOMER=客户资料,CALL-NUMBER=外呼号码,PRODUCT=商品)',
    `state` smallint not null default 0 comment '状态',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    user_id int not null default 0 comment '用户编号',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用导入文件表\r\n@since 1.0 <2015-5-12 SoChishun Added';
