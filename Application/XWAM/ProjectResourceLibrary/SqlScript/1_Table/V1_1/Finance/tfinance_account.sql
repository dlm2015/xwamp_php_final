create table if not exists tfinance_account (
    id int auto_increment primary key comment '主键编号',
    user_id int not null comment '用户编号',
    total_amount  decimal(12,2) not null default 0.00 comment '账户金额',
    blance  decimal(12,2) not null default 0.00 comment '可用余额',
    frozen_blance  decimal(12,2) not null default 0.00 comment '不可用余额',
    admin_id int not null default 0 comment '管理员编号',
    remark varchar(64) not null default '' comment '备注',
    `state` smallint not null default 0 comment '账户状态(0=未审核,1=正常,4=冻结)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime comment '变动时间',
    sys_code varchar(128) not null default '',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    KEY user_id_ix (user_id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 comment '财务_资金账户表\r\n@since 1.0 <2014-6-21> sutroon <14507247@qq.com> Added.';
