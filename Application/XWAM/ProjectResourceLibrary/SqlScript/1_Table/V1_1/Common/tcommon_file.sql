create table if not exists tcommon_file (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null default '' comment '标题',
    file_name varchar(64) not null default '' comment '文件名称',
    file_path varchar(255) not null default '' comment '文件路径',
    file_size varchar(32) not null default '' comment '文件大小',
    file_type varchar(32) not null default '' comment '文件类型',
    file_ext varchar(8) not null default '' comment '文件扩展名',
    parent_id int not null default 0 comment '文件夹编号',
    last_dir varchar(32) not null default '' comment '所属文件夹名称',
    width int not null default 0 comment '图片宽度',
    height int not null default 0 comment '图片高度',
    category_id int not null default 0 comment '类别编号(相册编号)',
    item_id int not null default 0 comment '项目编号',
    item_target varchar(32) not null default '' comment '项目类型(如ARTICLE=文章)',
    description varchar(2048) not null default '' comment '描述',
    visit_count int not null default 0 comment '浏览次数',
    reply_count int not null default 0 comment '回复次数',
    download_count int not null default 0 comment '下载次数',
    access_permission varchar(1024) not null default '' comment '访问权限(如相册中FRIEND=朋友,ALL=公开,PRIVATE=私密,[uid1,uid2...]=指定用户)',
    user_id int not null default 0 comment '上传者用户编号',
    params varchar(255) not null default '' comment '参数(JSON字符串)',
    remark varchar(128) not null default '' comment '备注',
    ex_tags varchar(16) not null default '' comment '扩展标签',
    ordinal smallint not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '状态(0=禁用,1=启用)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用文件表\r\n@since 1.0 <2014-6-22> sutroon <14507247@qq.com> Added.';
