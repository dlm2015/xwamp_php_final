create table if not exists tcommon_message (
    id int auto_increment primary key comment '主键编号',
    from_user varchar(64) not null comment '寄件人',
    to_user varchar(128) not null comment '收件人(多个之间以逗号隔开,如:user1,user2)',
    from_alias varchar(32) not null default '' comment '寄件人别名',
    subject varchar(32) not null default '' comment '消息标题',
    body varchar(2048) not null default '' comment '消息正文',
    from_folder varchar(16) not null comment 'pm或email发件箱名称',
    to_folder varchar(16) not null comment 'pm或email收件箱名称',
    limit_count smallint not null default 5 comment '重试发送次数',
    send_count smallint not null default 0 comment '已尝试发送次数',
    send_time datetime comment '发送开始时间',
    expire_time datetime comment '发送超时时间',
    read_time datetime comment '阅读时间',
    ex_tags varchar(16) not null comment '扩展标签,PM=站内信,SMS=手机短信,EMAIL=电子邮件',
    `state` smallint not null default 0 comment '状态(0=未发送,1=发送成功,4=发送失败,2=正在发送)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '通用消息表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';
