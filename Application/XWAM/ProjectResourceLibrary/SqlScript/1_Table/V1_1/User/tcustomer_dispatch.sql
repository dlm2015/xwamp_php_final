create table if not exists tcustomer_dispatch(
    id int auto_increment primary key comment '主键编号',
    `name` varchar(32) not null default '' comment '批次名称(默认时间戳)',
    customer_id int not null comment '客户表主键编号',
    old_user_id int not null comment '原营销员用户编号',
    user_id int not null comment '新营销员用户编号',
    admin_id int not null comment '管理员(操作者)用户编号',
    remark varchar(32) not null default '' comment '备注',
    `state` smallint not null default 0 comment '状态',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '客户_调度表\r\n@since 1.0 <2014-12-24> sutroon <14507247@qq.com> Added.';
