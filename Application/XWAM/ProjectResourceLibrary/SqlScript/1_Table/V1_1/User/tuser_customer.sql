create table if not exists tuser_customer(
    user_id int not null primary key comment '用户表主键编号',
    serial_no varchar(64) not null comment '客户编号(KH[site_id]0[user_id][Ymdhis])',
    interested smallint not null default 0 comment '购买意向(0=普通,1=意向客户,2=特别兴趣)',
    contact_results smallint not null default 0 comment '营销结果(联系结果,4=拒绝;1=成功;2=对方为老人或小孩;3=接通后即结束通话;5=接通后不说话)',
    tags varchar(64) not null default '' comment '自定义标签(多个值以英文逗号隔开)',
    `level` varchar(16) not null default '' comment '客户等级(金卡,银卡,铜卡)',
    buy_count smallint not null default 0 comment '消费次数',
    integral int not null default 0 comment '累计积分',
    cumulative_amount decimal(12,2) not null default 0.00 comment '累计消费金额',
    last_buy_time datetime comment '最后消费时间',
    last_visit_time datetime comment '最后沟通时间',
    referrer_user_id int not null default 0 comment '推荐人用户编号',
    agent_id int not null default 0 comment '营销员用户编号(user_id=0表示导入的客户)',
    old_agent_id int not null default 0 comment '被调度营销员用户编号(此值大于0说明是被调度的)',
    ex_tags varchar(32) not null default '' comment '系统标签(IMPORT=导入,ADD=手动添加,CTI=来电弹屏)',
    UNIQUE KEY serial_no_ix (serial_no)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户_客户表\r\n@since 1.0 <2014-6-26> sutroon <14507247@qq.com> Added.';
