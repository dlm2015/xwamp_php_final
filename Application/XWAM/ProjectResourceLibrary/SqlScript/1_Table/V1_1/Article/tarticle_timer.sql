create table if not exists tarticle_timer(
    article_id int primary key comment '文章编号',
    start_time datetime comment '开始时间',
    end_time datetime comment '失效时间',
    `state` smallint not null default 0 comment '状态(0=待发布,1=已发布,4=已撤销)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '文章_定时表\r\n@since 1.0 <2015-5-19> sutroon <14507247@qq.com> Added.';
