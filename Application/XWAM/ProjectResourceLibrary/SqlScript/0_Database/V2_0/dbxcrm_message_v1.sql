-- 消息库
-- 数据量巨大,频繁读取(如短信、站内信、IM等，可能提供接口服务，那数据操作就更加频繁)
CREATE DATABASE IF NOT EXISTS dbxcrm_message_v1 DEFAULT CHARSET utf8 COLLATE utf8_general_ci;
