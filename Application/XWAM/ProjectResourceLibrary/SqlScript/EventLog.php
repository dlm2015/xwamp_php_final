<?php

/**
 * Created by PhpStorm.
 * User: Su
 * Date: 2014/12/3
 * Time: 16:23
 */

/**
 * 添加用户模块日志
 * @param $action 事件名称，如登录、充值
 * @param $msg 消息内容
 * @param bool $state 状态
 * @param int $userID 用户编号
 * @since 1.0 2014-9-26 by sutroon
 * @example add_user_event_log('登录',"[登录%] userName=$u, password=$pwd",false);
 */
function add_user_event_log($action, $msg, $state = true, $userID = 0) {
    if (false !== strpos($msg, '%')) {
        $msg = str_replace('%', false === $state ? '失败' : '成功', $msg);
    }
    $data['userID'] = $userID;
    $data['ip'] = get_client_ip();
    $data['eAction'] = $action;
    $data['eSource'] = $_SERVER['REQUEST_URI'];
    $data['eLevel'] = 'INFO';
    $data['eDesc'] = $msg;
    $data['state'] = $state ? 1 : 0;
    $M = M('tuser_event_log');
    $result = $M->add($data);
    if ($result === false) {
        die($M->getDbError());
    }
}

// 用户注册模块
add_user_event_log('注册', "[注册成功] userName=$userName", true);
// 创建账户
add_user_event_log('编辑用户', "[编辑%] id=$id, user=$userName, personalName=$personalName, telphone=$telphone siteID=$siteID", $result);
add_user_event_log('创建用户', "[新增%] id=$result, user=$userName, personalName=$personalName, telphone=$telphone, siteID=$siteID",$result);
add_user_event_log('创建销售员账户', "[新增成功] user=$u", true);
add_user_event_log('创建审单员账户', "[新增成功] user=$u", true);
add_user_event_log('创建仓管员账户', "[新增成功] user=$u", true);
add_user_event_log('创建采购员账户', "[新增成功] user=$u", true);
// 用户登录模块
add_user_event_log('登录', "[登录成功] user=$u, password=$pwd", true);
add_user_event_log('登录', "[登录失败] user=$u, password=$pwd", false);
add_user_event_log('登录', "[登出成功] user=$u", true);
// 用户充值模块
add_user_event_log('充值', "[充值成功] user=$u, amount=$amount, origin=$origin", true);
add_user_event_log('充值', "[充值失败] user=$u, amount=$amount, origin=$origin", false);
// 编辑密码操作
add_user_event_log('密码', "[修改成功] user=$u, password=$pwd", true);
add_user_event_log('密码', "[修改失败] user=$u, password=$pwd, oldpassword=$oldpwd", false);
// 个人资料操作
add_user_event_log('个人资料', "[编辑成功] user=$u, personalName=$personalName, sex=$sex, face=$face", true);
// 扩展字段操作
add_user_event_log('扩展字段', "[新增%] $user=$siteID, target=$target label=$label, field=$field, type=$type", $result);
add_user_event_log('扩展字段', "[编辑%] $user=$siteID, target=$target, $id=$id, field=$field, label=$label, inputType=$inputType", $result);
add_user_event_log('扩展字段', "[删除成功] $user=$u, target=$target, id=$id, field=$field, label=$label", true);

/**
 * 添加产品模块日志
 * @param string $action 事件动作(如:充值、查询、登录、新增、编辑、删除)
 * @param $msg 事件对象(如：存储过程、表名、视图名、页面)
 * @param bool $state
 * @param int $userID
 * @since 1.0 2014-9-26 by sutroon
 * @example add_product_event_log('登录',"用户登录失败,用户名:$u 密码：$pwd",false);
 */
function add_product_event_log($action, $msg, $state = true, $userID = 0) {
    if (false !== strpos($msg, '%')) {
        $msg = str_replace('%', false === $state ? '失败' : '成功', $msg);
    }
    $data['userID'] = $userID;
    $data['ip'] = get_client_ip();
    $data['eAction'] = $action;
    $data['eSource'] = $_SERVER['REQUEST_URI']; // 事件来源(如：网址)
    $data['eLevel'] = 'INFO'; // 事件级别(如：信息、错误、警告、攻击)
    $data['eDesc'] = $msg; // 描述(如：充值100元)
    $data['state'] = $state ? 1 : 0; // 事件结果(1=成功,4=失败)
    $M = M('tproduct_event_log');
    $result = $M->add($data);
    if ($result === false) {
        die($M->getDbError());
    }
}

// 产品增删改
add_product_event_log('新增商品', "[新增%] id=$result, productName=$productName, price=$price, userID=$userID", $result);
add_product_event_log('编辑商品', "[编辑%] id=$result, productName=$productName, price=$price, userID=$userID", $result);
add_product_event_log('删除商品', "[删除%] id=$id, userID=$userID", $result);
// 进销存
add_product_event_log('采购入库', "[入库成功] user=$u, productID=$id, productName=$productName, number=$number, price=$price, subtotal=$subtotal, stock=$stock", true);
add_product_event_log('内部出库', "[出库成功] user=$u, productID=$id, productName=$productName, number=$number, stock=$stock");
add_product_event_log('编辑供应商', "[编辑供应商成功] admin=$u, provider=$provider|old=$oldprovider", true);
add_product_event_log('删除进销存记录', "[删除成功] admin=$u, id=$id, productID=$id, productName=$productName, stock=$stock|old=$oldstock", true);
