
/**
* 调试存储过程
* 用于存储过程调试 call proc_debug('', , vint_siteID);
* @since 1.0 2015-1-6 by sutroon
* @remark 必需确认以下表的存在：tgeneral_data, tuser_event_log
*   说明：dataName=存储过程名称, dataValue=0|1(是否开启调试), dataCode=siteID
*   insert into tgeneral_data (dataName, dataValue, dataCode, exTags) values ('func_cdr_update_amount', '1', '0', 'SQL-DEBUG'); -- 调试所有租户
*   insert into tgeneral_data (dataName, dataValue, dataCode, exTags) values ('func_cdr_update_amount', '2', '[siteID]', 'SQL-DEBUG'); -- 调试指定租户
*   insert into tgeneral_data (dataName, dataValue, dataCode, exTags) values ('func_cdr_update_amount-2', '2', '[siteID]', 'SQL-DEBUG'); -- 调试指定租户存储过程内部流程标识2的消息
*   update tgeneral_data set dataValue='2', dataCode='1' where dataName='func_cdr_update_amount'; -- 打开指定存储过程的调试
*   update tgeneral_data set dataValue='0', dataCode='0' where dataName='func_cdr_update_amount'; -- 关闭指定存储过程的调试
*   update tgeneral_data set dataValue='0', dataCode='0' where exTags='SQL-DEBUG'; -- 关闭所有调试    
* @example call proc_debug('func_cdr_update_amount',concat('[正常计费] [',now(),'] cdrid=',ifnull(vint_cdrid,'null'),', siteID=',ifnull(vint_siteID,'null'),', billsec=',ifnull(vint_billsec,'null'),', direction=',ifnull(vint_direction,'null')), vint_siteID);
*/
DELIMITER ;;
drop procedure if exists proc_debug;;
create procedure proc_debug(vstr_procName varchar(32), vstr_msg varchar(2048), vint_siteID int)
begin
    if func_isdebug(vstr_procName, vint_siteID)=1 then
        insert into tuser_event_log (eObject, eSource, eLevel, eDesc, siteID) values (vstr_procName, 'PROCEDURE', 'SQL-DEBUG', vstr_msg, vint_siteID);
    end if;
end;;
DELIMITER ;

/**
* 用户充值
* @since 1.0 2014-7-14 by sutroon
*/
DELIMITER ;;
drop procedure if exists proc_recharge;;
create procedure proc_recharge(pflt_amount decimal(12,4), pint_siteID int, pstr_origin varchar(32))
begin
    declare vflt_blance decimal(12,4) default 0;
    declare vflt_allowOverdraftAmount decimal(12,4) default 0;
    declare vint_daybill_id int;
    declare vstr_userName varchar(32);
    declare vint_errno smallint default 0;
    declare continue handler for sqlexception set vint_errno = 1;
    start transaction;

    select userName, blance, allowOverdraftAmount into vstr_userName, vflt_blance, vflt_allowOverdraftAmount from tuser_member where userType='ADMIN' and siteID=pint_siteID;
    set vflt_blance=vflt_blance+pflt_amount;
    update tuser_member set blance=vflt_blance where userType='ADMIN' and siteID=pint_siteID;
    
    insert into tfinance_recharge (userName, amount, blance, origin, createdTime, siteID) values (vstr_userName, pflt_amount, vflt_blance, pstr_origin, now(), pint_siteID);
    if exists(select id from tcti_daybill where billdate=CURDATE() and siteID=pint_siteID) then
        update tcti_daybill set rechargeAmount=rechargeAmount+pflt_amount,blance=blance+pflt_amount where billdate=CURDATE() and siteID=pint_siteID;
    else
        insert into tcti_daybill (billdate,rechargeAmount,blance, allowOverdraftAmount,createdTime,siteID) values (CURDATE(),pflt_amount,vflt_blance, vflt_allowOverdraftAmount,now(),pint_siteID);
    end if;

    if vint_errno <> 0 then
        rollback;
    else        
        commit;
    end if;
    SELECT vint_errno;
end;;
DELIMITER ;

/**
* 在线充值申请
* @since 1.0 2014-9-17 by sutroon
 */
DELIMITER ;;
drop procedure if exists proc_recharge_submit;;
create procedure proc_recharge_submit(pstr_orderNo varchar(128), pflt_amount decimal(12,4), pint_siteID int, pstr_origin varchar(32), pstr_remark varchar(255))
begin
    insert into tfinance_recharge (userName, orderNo, amount, origin, remark, createdTime, siteID, state) values ('', pstr_orderNo, pflt_amount, pstr_origin, pstr_remark, now(), pint_siteID, 0);
end;;
DELIMITER ;

/**
* 在线充值审核
* @since 1.0 2014-9-17 by sutroon
 */
DELIMITER ;;
drop procedure if exists proc_recharge_confirm;;
create procedure proc_recharge_confirm(pstr_orderNo varchar(128),pstr_tradeNo varchar(128), pint_state int)
begin
    declare vflt_blance decimal(12,4) default 0;
    declare vflt_allowOverdraftAmount decimal(12,4);
    declare vint_daybill_id int;
    declare vstr_userName varchar(32);
    declare pint_siteID int;
    declare pflt_amount decimal(12,4);
    declare vint_state int;

    if pint_state=1 then
        select siteID, amount, state into pint_siteID, pflt_amount, vint_state from tfinance_recharge where orderNo=pstr_orderNo;
        if vint_state <> 2 then
            select userName, blance, allowOverdraftAmount into vstr_userName, vflt_blance, vflt_allowOverdraftAmount from tuser_member where userType='ADMIN' and siteID=pint_siteID;
            set vflt_blance=vflt_blance+pflt_amount;
            update tuser_member set blance=vflt_blance where userType='ADMIN' and siteID=pint_siteID;
            update tfinance_recharge set state=pint_state, userName=vstr_userName, blance=vflt_blance where orderNo=pstr_orderNo;
            if exists(select id from tcti_daybill where billdate=CURDATE() and siteID=pint_siteID) then
                update tcti_daybill set rechargeAmount=rechargeAmount+pflt_amount,blance=blance+pflt_amount where billdate=CURDATE() and siteID=pint_siteID;
            else
                insert into tcti_daybill (billdate,rechargeAmount,blance, allowOverdraftAmount,createdTime,siteID) values (CURDATE(),pflt_amount,vflt_blance, vflt_allowOverdraftAmount,now(),pint_siteID);
            end if;
        end if;
    elseif pint_state=2 then
        select siteID, amount, state into pint_siteID, pflt_amount, vint_state from tfinance_recharge where orderNo=pstr_orderNo;
        select userName, blance, allowOverdraftAmount into vstr_userName, vflt_blance, vflt_allowOverdraftAmount from tuser_member where userType='ADMIN' and siteID=pint_siteID;
        set vflt_blance=vflt_blance+pflt_amount;
        update tuser_member set blance=vflt_blance where userType='ADMIN' and siteID=pint_siteID;
        update tfinance_recharge set state=pint_state, userName=vstr_userName, blance=vflt_blance where orderNo=pstr_orderNo;
        if exists(select id from tcti_daybill where billdate=CURDATE() and siteID=pint_siteID) then
            update tcti_daybill set rechargeAmount=rechargeAmount+pflt_amount,blance=blance+pflt_amount where billdate=CURDATE() and siteID=pint_siteID;
        else
            insert into tcti_daybill (billdate,rechargeAmount,blance, allowOverdraftAmount,createdTime,siteID) values (CURDATE(),pflt_amount,vflt_blance, vflt_allowOverdraftAmount,now(),pint_siteID);
        end if;
    else
        update tfinance_recharge set state=pint_state where orderNo=pstr_orderNo;
    end if;
end;;
DELIMITER ;

DELIMITER ;;
drop procedure if exists proc_recharge_confirm;;
create procedure proc_recharge_confirm(pstr_orderNo varchar(128),pstr_tradeNo varchar(128), pint_state int)
begin
    declare vflt_blance decimal(12,4) default 0;
    declare vflt_allowOverdraftAmount decimal(12,4);
    declare vint_daybill_id int;
    declare vstr_userName varchar(32);
    declare pint_siteID int;
    declare pflt_amount decimal(12,4);
    declare vint_state int;
    declare vint_errno smallint default 0;
    declare continue handler for sqlexception set vint_errno = 4000;
    start transaction;
    -- 1=确认收货,交易完成; 2=已付款,未发货; 3=已发货,未签收;

    select siteID, amount, state into pint_siteID, pflt_amount, vint_state from tfinance_recharge where orderNo=pstr_orderNo;
    if isnull(vint_state) then
        set vint_errno=4400; -- 记录不存在
    end if;
    if vint_state=1 or vint_state=pint_state then
        set vint_errno=4200; -- 重复确认
    end if;
    if pint_state=1 then -- 确认收货,交易完成
        select userName, blance, allowOverdraftAmount into vstr_userName, vflt_blance, vflt_allowOverdraftAmount from tuser_member where userType='ADMIN' and siteID=pint_siteID;
        set vflt_blance=vflt_blance+pflt_amount;
        update tuser_member set blance=vflt_blance where userType='ADMIN' and siteID=pint_siteID;
        update tfinance_recharge set state=pint_state, userName=vstr_userName, blance=vflt_blance where orderNo=pstr_orderNo;
        if exists(select id from tcti_daybill where billdate=CURDATE() and siteID=pint_siteID) then
            update tcti_daybill set rechargeAmount=rechargeAmount+pflt_amount,blance=blance+pflt_amount where billdate=CURDATE() and siteID=pint_siteID;
        else
            insert into tcti_daybill (billdate,rechargeAmount,blance, allowOverdraftAmount,createdTime,siteID) values (CURDATE(),pflt_amount,vflt_blance, vflt_allowOverdraftAmount,now(),pint_siteID);
        end if;
    else
        update tfinance_recharge set state=pint_state where orderNo=pstr_orderNo; -- 标记状态
    end if;

    if vint_errno <> 0 then
        rollback;
    else        
        commit;
    end if;
    SELECT vint_errno;
end;;
DELIMITER ;

/**
* 客户调度
* @since 1.0 2014-12-26 by sutroon
 */
DELIMITER ;;
drop procedure if exists proc_customer_dispatch;;
create procedure proc_customer_dispatch(pstr_name varchar(32),out oint_result int)
begin
    declare vint_errno smallint default 0;
    declare continue handler for sqlexception set vint_errno = 4000; -- 系统错误
    start transaction;
    
    if pstr_name='' then
        set vint_errno=4100; -- 参数有误
    end if;

    update tcustomer c left join tcustomer_dispatch d on c.id=d.customerID set c.oldUserID=d.oldUserID, c.userID=d.userID where d.name=pstr_name;
    update tcustomer_dispatch set state=1 where `name`=pstr_name;
    
    if vint_errno <> 0 then
        set oint_result=vint_errno;
        rollback;
    else   
        set oint_result=1; -- 操作成功
        commit;
    end if;
end;;
DELIMITER ;