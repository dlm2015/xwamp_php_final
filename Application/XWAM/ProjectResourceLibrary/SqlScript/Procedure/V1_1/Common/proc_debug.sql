/**
* 调试存储过程
* 用于存储过程调试 call proc_debug('', , vint_siteID);
* @since 1.0 2015-1-6 by sutroon
* @remark 必需确认以下表的存在：tgeneral_data, tuser_event_log
*   说明：dataName=存储过程名称, dataValue=0|1(是否开启调试), dataCode=siteID
*   insert into tgeneral_data (dataName, dataValue, dataCode, exTags) values ('func_cdr_update_amount', '1', '0', 'SQL-DEBUG'); -- 调试所有租户
*   insert into tgeneral_data (dataName, dataValue, dataCode, exTags) values ('func_cdr_update_amount', '2', '[siteID]', 'SQL-DEBUG'); -- 调试指定租户
*   insert into tgeneral_data (dataName, dataValue, dataCode, exTags) values ('func_cdr_update_amount-2', '2', '[siteID]', 'SQL-DEBUG'); -- 调试指定租户存储过程内部流程标识2的消息
*   update tgeneral_data set dataValue='2', dataCode='1' where dataName='func_cdr_update_amount'; -- 打开指定存储过程的调试
*   update tgeneral_data set dataValue='0', dataCode='0' where dataName='func_cdr_update_amount'; -- 关闭指定存储过程的调试
*   update tgeneral_data set dataValue='0', dataCode='0' where exTags='SQL-DEBUG'; -- 关闭所有调试    
* @example call proc_debug('func_cdr_update_amount',concat('[正常计费] [',now(),'] cdrid=',ifnull(vint_cdrid,'null'),', siteID=',ifnull(vint_siteID,'null'),', billsec=',ifnull(vint_billsec,'null'),', direction=',ifnull(vint_direction,'null')), vint_siteID);
*/
DELIMITER ;;
drop procedure if exists proc_debug;;
create procedure proc_debug(vstr_procName varchar(32), vstr_msg varchar(2048), vint_siteID int)
begin
    if func_isdebug(vstr_procName, vint_siteID)=1 then
        insert into tuser_event_log (eObject, eSource, eLevel, eDesc, siteID) values (vstr_procName, 'PROCEDURE', 'SQL-DEBUG', vstr_msg, vint_siteID);
    end if;
end;;
DELIMITER ;
