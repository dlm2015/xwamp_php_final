/**
* 客户调度
* @since 1.0 2014-12-26 by sutroon
 */
DELIMITER ;;
drop procedure if exists proc_customer_dispatch;;
create procedure proc_customer_dispatch(pstr_name varchar(32),out oint_result int)
begin
    declare vint_errno smallint default 0;
    declare continue handler for sqlexception set vint_errno = 4000; -- 系统错误
    start transaction;
    
    if pstr_name='' then
        set vint_errno=4100; -- 参数有误
    end if;

    update tcustomer c left join tcustomer_dispatch d on c.id=d.customerID set c.oldUserID=d.oldUserID, c.userID=d.userID where d.name=pstr_name;
    update tcustomer_dispatch set state=1 where `name`=pstr_name;
    
    if vint_errno <> 0 then
        set oint_result=vint_errno;
        rollback;
    else   
        set oint_result=1; -- 操作成功
        commit;
    end if;
end;;
DELIMITER ;
