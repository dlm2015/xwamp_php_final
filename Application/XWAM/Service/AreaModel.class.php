<?php

namespace XWAM\Service;

/**
 * 中国省市区数据类
 *
 * @since VER:1.0; DATE:2016-1-22; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class AreaModel extends \XWAM\Model\AppbaseModel {

    // 原:tgeneral_region, 2016-1-22重命名为 t_porg_area
    protected $tableName = 't_porg_area';

    /**
     * 获取地区列表
     * @param type $province
     * @param type $area
     * @return type
     * @since VER:1.0; DATE:2016-1-22; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
     */
    function get_areas($province, $city) {
        $cache_key = md5('areamdl_get_areas' . ($province ? $province : '') . ($city ? $city : ''));
        $cache_data = F($cache_key);
        if ($cache_data) {
            return $cache_data;
        }
        $pid = 0;
        if ($province) {
            $pid = $this->where(array('area_name' => $province, 'pid' => 0))->getField('id');
            if ($city) {
                $pid = $this->where(array('area_name' => $city, 'pid' => $pid))->getField('id');
            }
        }
        $val = $this->where(array('pid' => $pid))->getField('area_name', true);
        F($cache_key, $val);
        return $val;
    }

    /**
     * 清空缓存
     * @return int
     * @version 1.0 2016-5-10 SoChishun Added.
     */
    function clear_cache() {
        $i = 0;
        $prefix = 'areamdl_';
        $dir = opendir(DATA_PATH);
        while (false !== ($file = readdir($dir))) {
            if (0 === strpos($file, $prefix)) {
                unlink(DATA_PATH . $filename);
                $i++;
            }
        }
        closedir($dir);
        return $i;
    }

    public function resetdb2() {
        $provinces = array('省份', '北京市', '天津市', '上海市', '重庆市', '河北省', '山西省', '内蒙古 ', '辽宁省', '吉林省', '黑龙江省', '江苏省', '浙江省', '安徽省', '福建省', '江西省', '山东省', '河南省', '湖北省', '湖南省', '广东省', '广西 ', '海南省', '四川省', '贵州省', '云南省', '西藏 ', '陕西省', '甘肃省', '青海省', '宁夏 ', '新疆 ', '香港 ', '澳门 ', '台湾省');
        $codes = file_get_contents('G:/PDC/ProjectEnv/XCall1206/Public/Data/cnregion.txt');
        $arr_codes = explode(PHP_EOL, $codes);
        $sqls = false;
        $n = 0;
        foreach ($provinces as $i => $province) {
            if ($i == 0) {
                $n++;
                continue;
            }
            $sqls[] = "($n, '$province', 0)";
            $parent_city = $n;
            foreach ($arr_codes as $line_city) {
                $arr_line_city = explode('=', $line_city);
                if ($arr_line_city[0] == "0_$i") {
                    $citys = explode('|', $arr_line_city[1]);
                    foreach ($citys as $i2 => $city) {
                        $n++;
                        $sqls[] = "($n, '$city', $parent_city)";
                        $parent_area = $n;
                        foreach ($arr_codes as $line_area) {
                            $arr_line_area = explode('=', $line_area);
                            if ($arr_line_area[0] == '0_' . $i . '_' . $i2) {
                                $areas = explode('|', $arr_line_area[1]);
                                foreach ($areas as $i3 => $area) {
                                    $n++;
                                    $sqls[] = "($n, '$area', $parent_area)";
                                }
                            }
                        }
                    }
                }
            }
            $n++;
        }
        $str = "insert into db_xcall_service.tgeneral_region (id, regionName, parentID) values " . implode(',', $sqls);
        echo $str;
    }

    /**
     * 从文件复制数据到数据库
     * @since 1.0 2014-12-9 by sutroon
     */
    public function resetdb() {
        $table = 'db_xcall_service.tgeneral_region';
        $M = M($table);
        $result = $M->delete('1=1');
        if (false === $result) {
            die('删除失败!');
        }
        $str = file_get_contents(UPLOAD_ROOT . 'cnregion.txt');
        $arr = explode("\r\n", $str);
        foreach ($arr as $item) {
            $item = trim($item);
            if (false !== ($pos = strpos($item, ' '))) {
                $code = substr($item, 0, $pos);
                $name = trim(substr($item, $pos + 1));
            }
            $values[] = "($code,'$name')";
        }
        $values = array_chunk($values, 300);
        foreach ($values as $value) {
            $sql = implode(',', $value);
            if ($sql) {
                $result = $M->execute("insert into db_xcall_service.tgeneral_region (id, regionName) values $sql");
                if (false === $result) {
                    die('Error: ' . $M->getDbError());
                }
            }
        }
        // update
        //整理 市级地区 和 县级地区的父级id
        $prave = $M->where('id LIKE "%____00%"')->getField('id', true);
        foreach ($prave as $value) {
            $M->where('id LIKE "%' . substr($value, 0, 4) . '__%"')->save(array('parentID' => $value));
        }
        //整理 市辖区 和 县的父级id
        $prave = $M->where('id LIKE "%__0000%"')->getField('id', true);
        foreach ($prave as $value) {
            $M->where('id LIKE "%' . substr($value, 0, 2) . '__00%"')->save(array('parentID' => $value));
        }

        //还原省级父id为0
        $prave = $M->where('id LIKE "%__0000%"')->setField('parentID', 0);
        echo 'ok';
    }

}
