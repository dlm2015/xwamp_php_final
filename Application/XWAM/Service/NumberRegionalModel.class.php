<?php

namespace XWAM\Service;

/**
 * 号码归属地模型类
 *
 * @since VER:1.0; DATE:2016-1-22; AUTHOR:SoChishun; EMAIL:14507247@qq.com; DESC:Added.
 */
class NumberRegionalModel extends \XWAM\Model\AppbaseModel {

    // 原：tcti_number_start, 2016-1-22重命名为 t_porg_number_regional
    protected $tableName = 't_porg_number_regional';

    /**
     * 获取指定号码的号码归属地
     * @param type $tel
     * @return string
     * @since VER:1.0; DATE:2016-1-22; AUTHOR:SoChishun; DESC:Added.
     */
    function get_regional($tel) {
        $cache_key = 'ngmdl_get_regional_' . $tel;
        $cache_data = F($cache_key);
        if ($cache_data) {
            return $cache_data;
        }

        $tel = trim($tel);
        if (!$tel) {
            return '';
        }
        $len = strlen($tel);
        if ($len < 8) { // 不是固话也不是手机
            return '';
        }
        if ($len > 10) { // 手机号过滤前缀0
            $tel = ltrim($tel, '0');
            if (!$tel) {
                return '';
            }
            $len = strlen($tel);
        }
        $aispstart = array(
            'yidong' => "134,135,136,137,138,139,147,150,151,152,157,158,159,182,183,184,187,188",
            'liantong' => "130,131,132,145,155,156,185,186",
            'dianxin' => "133,153,180,181,189",
        );
        $start = substr($tel, 0, 3);
        foreach ($aispstart as $key => $item) {
            if (false !== strpos(",$item,", ",$start,")) {
                $isp = $key;
                break;
            }
        }
        $is_mobile = true;
        if ($isp) {
            $starts[] = substr($tel, 0, 7); // 手机
        } else {
            $is_mobile = false;
            $starts[] = substr($tel, 0, 5); // 固话
            $starts[] = substr($tel, 0, 4);
            $starts[] = substr($tel, 0, 3);
        }
        $val = '';
        foreach ($starts as $start) {
            $region = $this->where("segment_number= '$start'")->getField('area_name');
            if ($region) {
                $val = $region;
                break;
            }
        }
        // 空格隔开 2016-4-18 SoChishun Added.
        if ($is_mobile && $val) {
            $aval = explode('省', $val);
            if (count($aval) > 1) {
                $aval[0].='省';
                $val = implode(' ', $aval);
            }
        }
        F($cache_key, $val);
        return $val;
    }

    /**
     * 清空缓存
     * @return int
     * @version 1.0 2016-5-10 SoChishun Added.
     */
    function clear_cache() {
        $i = 0;
        $prefix = 'ngmdl_';
        $dir = opendir(DATA_PATH);
        while (false !== ($file = readdir($dir))) {
            if (0 === strpos($file, $prefix)) {
                unlink(DATA_PATH . $filename);
                $i++;
            }
        }
        closedir($dir);
        return $i;
    }

    /**
     * 获取指定地区的号码段列表
     * <br />原get_startNo_list,重构为 get_segment_numbers
     * @since VER:1.0; DATE:2016-1-22; AUTHOR:SoChishun; DESC:Added.
     */
    public function get_segment_numbers($areaname, $phonegear, $isp) {
        if (!$areaname) {
            return '';
        }
        if (!$phonegear) {
            $phonegear = "手机";
        }

        $cache_key = md5('ngmdl_get_segment_numbers_' . ($areaname ? $areaname : '') . $phonegear . ($isp ? $isp : ''));
        $cache_data = F($cache_key);
        if ($cache_data) {
            return $cache_data;
        }

        $sql = "area_name like '%$areaname%'";
        $fields = "area_code as id, concat(replace(area_name,'(*)',''),' >> ',area_code) as text";
        switch ($phoneGear) {
            case "手机":
                $sql .= "  and segment_number_length >5";
                break;
            default:
                $sql .= " and segment_number_length <5";
                $fields = "segment_number as id, concat(cast(number_length as varchar(6)), '位 >> 固话位数：',segment_number ) as name";
                break;
        }
// isp 移动、联通、电信
        $yidong = "134,135,136,137,138,139,147,150,151,152,157,158,159,182,183,184,187,188";
        $liantong = "130,131,132,145,155,156,185,186";
        $dianxin = "133,153,180,181,189";
        switch ($isp) {
            case "移动":
                $sql .= sprintf(" and segment_number_len3 in ('%s') ", str_replace(',', "','", $yidong));
                break;
            case "联通":
                $sql .= sprintf(" and segment_number_len3 in ('%s') ", str_replace(',', "','", $liantong));
                break;
            case "电信":
                $sql .= sprintf(" and segment_number_len3 in ('%s') ", str_replace(',', "','", $dianxin));
                break;
        }
        $sql = 'select ' . $fields . ' from ' . $this->tableName . ' where ' . $sql . ' order by segment_number';
        $list = $this->query($sql);
        F($cache_key, $list);
        return $list;
    }

    /**
     * 获取省份列表
     * <br />用于获取号码段列表
     * @return type
     */
    public function get_provinces() {
        $cache_key = 'ngmdl_get_provinces';
        $cache_data = F($cache_key);
        if ($cache_data) {
            return $cache_data;
        }

        $data = array_keys($this->get_data());
        $out = array();
        foreach ($data as $item) {
            $out[] = array('id' => $item, 'text' => $item);
        }
        F($cache_key, $out);
        return $out;
    }

    /**
     * 获取城市列表
     * <br />用于获取号码段列表
     * @param type $province
     * @return string
     */
    public function get_citys($province = '') {
        if (!$province) {
            return '';
        }
        $cache_key = md5('ngmdl_get_citys_' . $province);
        $cache_data = F($cache_key);
        if ($cache_data) {
            return $cache_data;
        }

        $data = $this->get_data($province);
        $out = array();
        foreach ($data as $item) {
            $out[] = array('id' => $item, 'text' => $item);
        }
        F($cache_key, $out);
        return $out;
    }

    function get_data($province = '') {
        $cache_key = md5('ngmdl_get_data_' . $province);
        $cache_data = F($cache_key);
        if ($cache_data) {
            return $cache_data;
        }

        $arr['北京'] = array('北京市');
        $arr['上海'] = array('上海市');
        $arr['天津'] = array('天津市');
        $arr['重庆'] = array('重庆市');
        $arr['河北'] = array('石家庄', '邯郸', '邢台', '保定', '张家口', '承德', '廊坊', '唐山', '秦皇岛', '沧州', '衡水');
        $arr['山西'] = array('太原', '大同', '阳泉', '长治', '晋城', '朔州', '吕梁', '忻州', '晋中', '临汾', '运城');
        $arr['内蒙古'] = array('呼和浩特', '包头', '乌海', '赤峰', '呼伦贝尔盟', '阿拉善盟', '哲里木盟', '兴安盟', '乌兰察布盟', '锡林郭勒盟', '巴彦淖尔盟', '伊克昭盟');
        $arr['新疆'] = array('博乐', '喀什', '和田', '乌鲁木齐', '库尔勒', '阿勒泰', '石河子', '阿克苏', '昌吉', '伊犁', '克拉玛依', '奎屯', '吐鲁番', '克州', '塔城', '哈密', '阿图什', '博尔塔拉', '巴州', '克孜勒苏', '托里', '皮山', '富蕴', '精河', '乌苏', '库车', '巴楚', '伊宁');
        $arr['辽宁'] = array('沈阳', '大连', '鞍山', '抚顺', '本溪', '丹东', '锦州', '营口', '阜新', '辽阳', '盘锦', '铁岭', '朝阳', '葫芦岛');
        $arr['吉林'] = array('长春', '吉林', '四平', '辽源', '通化', '白山', '松原', '白城', '延边');
        $arr['黑龙江'] = array('哈尔滨', '齐齐哈尔', '牡丹江', '佳木斯', '大庆', '绥化', '鹤岗', '鸡西', '黑河', '双鸭山', '伊春', '七台河', '大兴安岭');
        $arr['江苏'] = array('南京', '镇江', '苏州', '南通', '扬州', '盐城', '徐州', '连云港', '常州', '无锡', '宿迁', '泰州', '淮安');
        $arr['浙江'] = array('杭州', '宁波', '温州', '嘉兴', '湖州', '绍兴', '金华', '衢州', '舟山', '台州', '丽水');
        $arr['安徽'] = array('合肥', '芜湖', '蚌埠', '马鞍山', '淮北', '铜陵', '安庆', '黄山', '滁州', '宿州', '池州', '淮南', '巢湖', '阜阳', '六安', '宣城', '亳州');
        $arr['福建'] = array('福州', '厦门', '莆田', '三明', '泉州', '漳州', '南平', '龙岩', '宁德');
        $arr['江西'] = array('南昌市', '景德镇', '九江', '鹰潭', '萍乡', '新馀', '赣州', '吉安', '宜春', '抚州', '上饶');
        $arr['山东'] = array('济南', '青岛', '淄博', '枣庄', '东营', '烟台', '潍坊', '济宁', '泰安', '威海', '日照', '莱芜', '临沂', '德州', '聊城', '滨州', '菏泽');
        $arr['河南'] = array('郑州', '开封', '洛阳', '平顶山', '安阳', '鹤壁', '新乡', '焦作', '濮阳', '许昌', '漯河', '三门峡', '南阳', '商丘', '信阳', '周口', '驻马店', '济源');
        $arr['湖北'] = array('武汉', '宜昌', '荆州', '襄樊', '黄石', '荆门', '黄冈', '十堰', '恩施', '潜江', '天门', '仙桃', '随州', '咸宁', '孝感', '鄂州');
        $arr['湖南'] = array('长沙', '常德', '株洲', '湘潭', '衡阳', '岳阳', '邵阳', '益阳', '娄底', '怀化', '郴州', '永州', '湘西', '张家界');
        $arr['广东'] = array('广州', '深圳', '珠海', '汕头', '东莞', '中山', '佛山', '韶关', '江门', '湛江', '茂名', '肇庆', '惠州', '梅州', '汕尾', '河源', '阳江', '清远', '潮州', '揭阳', '云浮');
        $arr['广西'] = array('南宁', '柳州', '桂林', '梧州', '北海', '防城港', '钦州', '贵港', '贺州', '百色', '河池');
        $arr['海南'] = array('海口', '三亚');
        $arr['四川'] = array('成都', '绵阳', '德阳', '自贡', '攀枝花', '广元', '内江', '乐山', '南充', '宜宾', '广安', '达川', '雅安', '眉山', '甘孜', '凉山', '泸州');
        $arr['贵州'] = array('贵阳', '六盘水', '遵义', '安顺', '铜仁', '黔西南', '毕节', '黔东南', '黔南');
        $arr['云南'] = array('昆明', '大理', '曲靖', '玉溪', '昭通', '楚雄', '红河', '文山', '思茅', '西双版纳', '保山', '德宏', '丽江', '怒江', '迪庆', '临沧');
        $arr['西藏'] = array('拉萨', '日喀则', '山南', '林芝', '昌都', '阿里', '那曲');
        $arr['陕西'] = array('西安', '宝鸡', '咸阳', '铜川', '渭南', '延安', '榆林', '汉中', '安康', '商洛');
        $arr['甘肃'] = array('兰州', '嘉峪关', '金昌', '白银', '天水', '酒泉', '张掖', '武威', '定西', '陇南', '平凉', '庆阳', '临夏', '甘南');
        $arr['宁夏'] = array('银川', '石嘴山', '吴忠', '固原');
        $arr['青海'] = array('西宁', '海东', '海南州', '海北', '黄南', '玉树', '果洛', '海西', '格尔木', '同仁', '门源');
        $out = $province && array_key_exists($province, $arr) ? $arr[$province] : $arr;
        F($cache_key, $out);
        return $out;
    }

}
