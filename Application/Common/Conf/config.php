<?php
/**
 * 应用配置文件
 * <br />XWAM
 * @since 1.0 <2015-8-25> SoChishun <14507247@qq.com> Added.
 */
return array(

    /* 模块设置 */
    'DEFAULT_MODULE' => 'XWAM', // 默认模块
    
    /* 效能设置 */
    'DB_SQL_BUILD_CACHE' => true, // 开启SQL解析缓存以减少SQL解析提高性能
    'DB_SQL_BUILD_LENGTH' => 50, // SQL缓存的队列长度

    /* 调试设置 */
    'SHOW_PAGE_TRACE' => false, // 开启页面调试

    /* 标签库设置 */
    'TAGLIB_LOAD' => true, //加载标签库
    'TAGLIB_BUILD_IN' => 'Cx,Common\TagLib\Sohtml', // Cx为核心标签库名称，Lists为自定义标签库名称,不能弄错
    'TAGLIB_PRE_LOAD' => 'Common\TagLib\Sohtml', // 全局预先加载的标签库

    /* 模板内容替换设置(ThinkPHP/Library/Behavior/ContentReplaceBehavior.class.php) */
);