<?php
namespace Common\Controller;
/**
 * Description of IController
 *
 * @since 1.0 <2015-3-18> SoChishun Added.
 */
interface IController {
    public function getsearch();
    // 显示列表,包含搜索和导出功能
    public function index();
    public function view();
    public function edit();
    public function edit_save();
    public function delete();
    public function implode();
    public function implode_save();
    public function export();
    public function export_save();
    public function list_json();
    public function item_json();
    public function exec_json();
}
