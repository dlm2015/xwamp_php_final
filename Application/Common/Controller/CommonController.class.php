<?php

namespace Common\Controller;

/**
 * Description of CommonController
 *
 * @since 1.0 <2015-3-18> SoChishun Added.
 */
class CommonController extends \Think\Controller {

    /**
     * 用户权限规则
     * @var string
     */
    protected $user_permission_rules;

    /**
     * 用户登录数据
     * @var array
     */
    protected $user_login_data;

    /**
     * 页面权限规则
     * @var array
     */
    protected $page_permission_rule;

    /**
     * fetch内容填充到模板变量中
     * @param string $var 模板变量名称
     * @param string $tpl 模板名称
     * @param boolean $disalbe_layout 是否临时禁用布局
     * @since 1.0 <2015-6-9> SoChishun Added.
     * @example fetch_assign('dialog_edit','item_edit');
     */
    protected function fetch_assign($var, $tpl, $disalbe_layout = true) {
        if ($disalbe_layout) {
            layout(false);
        }
        $this->assign($var, $this->fetch($tpl));
        if ($disalbe_layout) {
            layout(true);
        }
    }

    /**
     * 显示模板内容
     * <br />区分是否Ajax请求
     * @param string $part_tpl 部分模版名称
     * @param string $container_name 容器变量名称
     * @since 1.0 2014-11-18 by sutroon; 
     * @since 1.1 2014-12-25 by sutroon 新增$tpl参数
     */
    protected function display_cpp($part_tpl = '',$tpl='', $container_name = 'content') {
        if (!$part_tpl) {
            $part_tpl = ACTION_NAME . '_partial';
        }
        if(!$tpl){
            $tpl=ACTION_NAME;
        }
        if (IS_AJAX) {
            $this->display($part_tpl);
        } else {
            $this->assign($container_name, $this->fetch($part_tpl));
            $this->display();
        }
    }

    /**
     * 格式化ajaxReturn的内容
     * @param boolean $status 状态
     * @param string $info 信息,占位符{%}会自动转换为"成功"或"失败"
     * @param string jsonpcb jsonp回调方法名称
     * @since 1.0 2014-11-27 by sutroon; 
     * @since 1.1 2014-12-16 by sutroon 新增jsonp支持
     * @since 2.0 <2015-6-10> SoChishun act_ajaxReturn()重构为 ajaxMsg().
     */
    protected function ajaxMsg($status, $info = '', $jsonpcb = '') {
        $msg = array('status' => (false === $status || is_null($status)) ? false : true, 'info' => (false === strpos($info, '{%}') ? $info : str_replace('{%}', $status ? '成功' : '失败', $info)));
        if ($jsonpcb) {
            echo $jsonpcb . '(' . json_encode($msg) . ')';
            exit;
        }
        $this->ajaxReturn($msg);
    }

}
