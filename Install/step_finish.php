<?php
/**
 * 完成安装
 * 后话：
 * 整个Install模块开发周期：2015-5-25 -> 2015-5-27, 共历时2天, 整个模块从无到有,从零到完整.
 * 主要攻克难点：
 *      1.ajax动态提交表单内容并回显到页面(主要用途：动态创建数据库对象或写入数据并回显)
 *      2.加载sql文件并将内容逐语句拆分成数组(完美解析存储过程和方法，完美过滤SQL注释);
 * 特点：
 *      1.架构上基于配置文件和外置资源文件,可灵活扩展需求
 *      2.页面无刷新脚本执行,用户体验好
 *      3.代码自主版权,方便修改,作者SoChishun <14507247@qq.com>.
 * 不足：
 *      1.一些细节有待加强,如人性化的错误提示.
 * 鸣谢：
 *      1.自己,没有自己的坚持,就没有这个作品的产生, 安装模块自己在以前就搞过，后来因为代码遗失，后面新开发的Web应用都没有一个好的安装向导模块, 这次借着为XCall专项开发的Install模块, 终于弥补了自己内心一直想开发一个安装模块的夙愿.
 *      2.公司,没有公司提供的需求, 自己也没有时间和精力去开发这个模块.
 *      3.感谢政府、感谢领导(小陈、老板)、感谢关心我的每一个好人，没有这些环境支持，也不可能成就这个模块.
 * @since 1.0 <2015-5-27> SoChishun <14507247@qq.com> Added.
 */
include 'function.php';
$config = isset($_SESSION['suinstall-admin']) ? $_SESSION['suinstall-admin'] : false;
if (!$config) {
    header('Location:./step_data.php');
    exit;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>完成安装-INSTALL</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="//libs.useso.com/js/yui/3.17.2/cssreset/cssreset-min.css" rel="stylesheet" />
        <link href="./Skin/theme-blue.css" rel="stylesheet" />
        <script src="//libs.useso.com/js/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>   
        <style type="text/css">
            h1 { font-size:24px; font-weight:bold;}
            .bodier { width:300px; margin:100px auto 0 auto;}
            .bodier h1 { margin:20px 0;}
            .bodier table { width:auto; margin:10px auto;}
            .bodier p { line-height: 28px;}
            .bodier p strong { font-weight: bold;}
        </style>
    </head>
    <body class="page-step-setting">
        <div class="header">
            <div class="frame-style">
                <div class="logo">INSTALL安装向导</div>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="./install.php" target="_blank">欢迎界面</a></li>
                        <li><a href="./step_env.php">环境检测</a></li>
                        <li><a href="./step_setting.php">系统配置</a></li>
                        <li><a href="./step_data.php">初始化数据</a></li>
                        <li class="cur">完成安装</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bodier">
            <h1 class="ok">恭喜您, 系统安装完成</h1>
            <p>
                <strong>请记住您的管理员帐号：</strong><br />
                用户名：<?php echo $config['adm_user'] ?><br />
                密码：<?php echo $config['adm_pwd'] ?>
            </p>
            <p>
                <strong>您可以：</strong><br />
                <a href="../index.php" target="_blank">[前往网站]</a>
            </p>
        </div>
    </body>
</html>
<?php

// 获取post数据
function query($name) {
    return isset($_POST[$name]) ? $_POST[$name] : '';
}

function create_table() {
    $id = query('id');
    $path = query('path');
    if (!is_file($path)) {
        return array('status' => false, 'id' => $id, 'info' => $path . ' 不存在');
    }
    $sql = file_get_contents($path);
    if (!$sql) {
        return array('status' => false, 'id' => $id, 'info' => $path . ' 无内容');
    }
    $config = array('db_host' => query('db_host'), 'db_name' => query('db_name'), 'db_user' => query('db_user'), 'db_pwd' => query('db_pwd'), 'db_prefix' => query('db_prefix'));
    $link = @mysql_connect($config['db_host'], $config['db_user'], $config['db_pwd']);
    if (false === $link) {
        return array('status' => false, 'id' => $id, 'info' => mysql_error());
    }
    $res = @mysql_query('CREATE DATABASE IF NOT EXISTS ' . $config['db_name'] . ' DEFAULT CHARSET utf8 COLLATE utf8_general_ci;');
    if (false === $res) {
        return array('status' => false, 'id' => $id, 'info' => mysql_error());
    }
    $res = @mysql_select_db($config['db_name']);
    if (false === $res) {
        return array('status' => false, 'id' => $id, 'info' => mysql_error());
    }
    $res = @mysql_query('SET NAMES UTF8');
    if (false === $res) {
        return array('status' => false, 'id' => $id, 'info' => mysql_error());
    }
    $res = @mysql_query($sql);
    if (false === $res) {
        return array('status' => false, 'id' => $id, 'info' => mysql_error());
    }
    session_start();
    $_SESSION['suinstall-db-config'] = $config;
    return array('status' => true, 'id' => $id, 'info' => $id);
}
?>
    