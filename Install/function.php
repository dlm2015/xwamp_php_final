<?php

/*
 * 公用函数库
 * @since 1.0 <2015-5-27> SoChishun Added.
 */
/* 页面头部输出 */
error_reporting(0); //抑制所有错误信息
@header("content-Type: text/html; charset=utf-8"); //语言强制
ob_start();
date_default_timezone_set('Asia/Shanghai'); //此句用于消除时间差
set_time_limit(0); // PHP运行超时限制
session_start(); // 开启session
/* /页面头部输出 */

/**
 * 加载配置文件
 * @return false|array 成功返回array,失败返回false
 * @since 1.0 <2015-5-27> SoChishun Added.
 */
function load_config() {
    $config = include './Conf/config.php'; // 读取全局配置文件
    $cur_mod = $config['MODULE']; // 读取全局配置当前模块名称
    $config = include './Conf/' . $cur_mod . '.php'; // 读取全局配置当前模块配置
    return $config;
}

/**
 * 层级加载配置中的参数值
 * @param type $config
 * @param type $name
 * @param type $defv
 * @return type
 * @since 1.0 2016-4-1 SoChishun Added.
 */
function c($config, $name, $defv = '') {
    if (!$config) {
        return $defv;
    }
    $anames = explode('.', $name);
    $n = count($anames);
    switch ($n) {
        case 1:
            return isset($config[$name]) ? $config[$name] : $defv;
        case 2:
            list($k1, $k2) = $anames;
            return isset($config[$k1]) && isset($config[$k1][$k2]) ? $config[$k1][$k2] : $defv;
        case 3:
            list($k1, $k2, $k3) = $anames;
            return isset($config[$k1]) && isset($config[$k1][$k2]) && isset($config[$k1][$k2][$k3]) ? $config[$k1][$k2][$k3] : $defv;
            break;
    }
    return $defv;
}

// 获取post数据 2016-3-16
function I($name, $defv = '') {
    if (isset($_POST[$name])) {
        return $_POST[$name] ? $_POST[$name] : $defv;
    } else if (isset($_GET[$name])) {
        return $_GET[$name] ? $_GET[$name] : $defv;
    }
    return $defv;
}
