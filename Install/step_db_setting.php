<?php
/**
 * 系统设置
 * @since 1.0 <2015-5-26> SoChishun <14507247@qq.com> Added.
 */
include 'function.php';
$config = load_config();
if ('testconn' == I('action')) {
    $result = test_conn();
    exit($result);
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>系统设置-INSTALL</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="//libs.useso.com/js/yui/3.17.2/cssreset/cssreset-min.css" rel="stylesheet" />
        <link href="./Skin/theme-blue.css" rel="stylesheet" />
        <script src="//libs.useso.com/js/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>   
    </head>
    <body class="page-step-setting">
        <div class="header">
            <div class="frame-style">
                <div class="logo"><?php echo $config['APP_NAME'] ?> INSTALL安装向导</div>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="./install.php">欢迎界面</a></li>
                        <li><a href="./step_env.php">环境检测</a></li>
                        <li class="cur">数据库配置</li>
                        <li>初始化数据</li>
                        <li>完成安装</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bodier">
            <form method="post" action="step_db_install.php">
                <h3>数据库信息</h3>
                <table cellspacing="0" class="table-form">
                    <tr><th>数据库服务器</th><td><input type="text" name="db_host" placeholder="数据库服务器" required="required" size="30" value="<?php echo c($config, 'default_config.db_host', '127.0.0.1') ?>" /><em>(一般为localhost或ip地址)</em></td></tr>
                    <tr><th>数据库名</th><td><input type="text" name="db_name" placeholder="数据库名" required="required" size="30" value="<?php echo c($config, 'default_config.db_name', 'db_xwam_v1') ?>" /></td></tr>
                    <tr><th>数据库用户名</th><td><input type="text" name="db_user" placeholder="数据库用户名" required="required" size="30" value="<?php echo c($config, 'default_config.db_user', 'root') ?>" /></td></tr>
                    <tr><th>数据库密码</th><td><input type="password" name="db_pwd" placeholder="数据库密码" required="required" size="30" value="<?php echo c($config, 'default_config.db_pwd') ?>" /><a href="#" id="lnk-show-pwd">[显示]</a><span></span></td></tr>
                    <tr><th>数据表前缀</th><td><input type="text" name="db_prefix" placeholder="数据表前缀" size="30" value="<?php echo c($config, 'default_config.table_prefix') ?>" /></td></tr>
                </table>
                <div class="footer">
                    <a href="./step_env.php">上一步</a><button type="submit">安装数据库</button><button type="reset">重置</button>
                    <a href="?action=testconn" id="lnk-test-conn">测试连接</a>
                </div>
                <input type="hidden" name="action" value="do" />
            </form>
        </div>
        <script type="text/javascript">
            // 2016-3-16
            $('#lnk-test-conn').click(function () {
                $.get($(this).attr('href'), $('form').serialize().replace('action=do', 'action=testconn'), function (data) {
                    alert(data);
                })
                return false;
            })
            $('#lnk-show-pwd').click(function () {
                var $a = $(this);
                if ($a.text() == '[隐藏]') {
                    $a.text('[显示]');
                    $a.prev().attr('type', 'password');
                } else {
                    $a.text('[隐藏]');
                    $a.prev().attr('type', 'text');
                }
                return false;
            })
        </script>
    </body>
</html>
<?php

// 2016-3-16
function test_conn() {
    $link = @mysql_connect(I('db_host'), I('db_user'), I('db_pwd'));
    if (false === $link) {
        return '连接失败：' . mysql_error();
    }
    return '连接成功!';
}
?>
    