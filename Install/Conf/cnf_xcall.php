<?php

/**
 * XCall配置文件
 * @since 1.0 <2015-5-25> SoChishun Added.
 */
return array(
    // 第一步:安装数据库子目录,如有依赖次序不可颠倒(:array)
    'db_object_dir' => array(
        './Res/XCall/Database/',
        './Res/XCall/Table/',
        './Res/XCall/View/',
        './Res/XCall/Function/',
        './Res/XCall/Procedure/'
    ),
    // 第二步:安装附加数据,如有依赖次序不可颠倒(:array)
    'db_data_dir' => array(
        './Res/XCall/Data/tgeneral_region.sql' => '完整地区数据',
        './Res/XCall/Data/tuser_permission.sql' => '权限菜单数据',
        './Res/XCall/Data/tcti_number_start.sql' => '电话号码头数据',
    ),
    // 第三步：安装补丁,如有依赖次序不可颠倒(:array)
    'db_patch_dir' => array(
        './Res/XCall/Patch/data_transfer.sql',
        './Res/XCall/Patch/drop_table.sql'
    ),
    // 第四步：管理员创建脚本模板
    'db_create_admin_sql' => "insert into tuser_member (userName, password) values ('%s','%s');",
);

