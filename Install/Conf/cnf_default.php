<?php

/**
 * Crm配置文件
 * @since 1.0 <2015-7-10> SoChishun Added.
 */
return array(
    /* 应用程序名称 */
    'APP_NAME' => 'XCRM',
    /* 预设默认值 */
    'default_config' => array(
        'db_name' => 'db_xcrm_v1',
        'db_host' => '127.0.0.1',
        'db_user' => 'xcall',
        'db_pwd' => 'root!@#xcall098)(*Admin%^&',
        'table_prefix' => '',
    ),
    /* 第一步:安装数据库子目录,如有依赖次序不可颠倒(:array) */
    'db_object_dir' => array(
        './Res/Default/Table/',
    ),
    /* 第二步:安装附加数据,如有依赖次序不可颠倒(:array) */
    'db_data_dir' => array(
        './Res/Default/Data/base.sql' => '基础数据',
        './Res/Default/Data/t_porg_area.sql' => '完整地区数据',
        //'./Res/Default/Data/tuser_permission.sql' => '权限菜单数据',
        //'./Res/Default/Data/t_porg_number_regional.sql' => '号码归属地数据',
    ),
    /* 第三步：安装补丁,如有依赖次序不可颠倒(:array) */
    'db_patch_dir' => array(
    //'./Res/XCall/Patch/data_transfer.sql',
    //'./Res/XCall/Patch/drop_table.sql'
    ),
    /* 第四步：管理员创建脚本模板 */
    'db_create_admin_sql' => "insert into t_porg_user (user_name, password, type_name, status) values ('%s','%s','ADMIN',1);",
);

