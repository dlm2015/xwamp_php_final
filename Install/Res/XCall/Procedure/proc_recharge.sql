DELIMITER $$
drop procedure if exists proc_recharge;$$
create procedure proc_recharge(pflt_amount decimal(12,4), pint_siteID int, pstr_origin varchar(32), pstr_remark varchar(64))
begin
    declare vflt_blance decimal(12,4) default 0;
    declare vflt_allowOverdraftAmount decimal(12,4) default 0;
    declare vint_daybill_id int;
    declare vstr_userName varchar(32);
    declare vint_errno smallint default 0;
    declare continue handler for sqlexception set vint_errno = 1;
    start transaction;

    select userName, blance, allowOverdraftAmount into vstr_userName, vflt_blance, vflt_allowOverdraftAmount from tuser_member where userType='ADMIN' and siteID=pint_siteID;
    set vflt_blance=vflt_blance+pflt_amount;
    update tuser_member set blance=vflt_blance where userType='ADMIN' and siteID=pint_siteID;
    
    insert into tfinance_recharge (userName, amount, blance, origin, remark, createdTime, siteID) values (vstr_userName, pflt_amount, vflt_blance, pstr_origin, pstr_remark, now(), pint_siteID);
    if exists(select id from tcti_daybill where billdate=CURDATE() and siteID=pint_siteID) then
        update tcti_daybill set rechargeAmount=rechargeAmount+pflt_amount,blance=blance+pflt_amount where billdate=CURDATE() and siteID=pint_siteID;
    else
        insert into tcti_daybill (billdate,rechargeAmount,blance, allowOverdraftAmount,createdTime,siteID) values (CURDATE(),pflt_amount,vflt_blance, vflt_allowOverdraftAmount,now(),pint_siteID);
    end if;

    if vint_errno <> 0 then
        rollback;
    else        
        commit;
    end if;
    SELECT vint_errno;
end$$
DELIMITER ;
