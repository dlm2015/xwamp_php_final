/**
* 在线充值申请
* @since 1.0 2014-9-17 by sutroon
 */
DELIMITER $$
drop procedure if exists proc_recharge_submit;$$
create procedure proc_recharge_submit(pstr_orderNo varchar(128), pflt_amount decimal(12,4), pint_siteID int, pstr_origin varchar(32), pstr_remark varchar(255))
begin
    insert into tfinance_recharge (userName, orderNo, amount, origin, remark, createdTime, siteID, state) values ('', pstr_orderNo, pflt_amount, pstr_origin, pstr_remark, now(), pint_siteID, 0);
end$$
DELIMITER ;
