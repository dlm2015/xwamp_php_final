-- 日结账单 2014-10-11 by sutroon
create or replace view vcti_daybill as
    select b.*, u.userName from tcti_daybill b left join tuser_member u on b.siteID=u.siteID where u.userType='ADMIN' order by b.billdate desc, b.siteID
