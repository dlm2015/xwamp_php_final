/**
* 删除租户
* @since 1.0 2014-10-23 by sutroon
 */
create function func_delete_admin(iint_id int)
returns int
begin
    declare vint_siteID int;
    declare exit handler for SQLEXCEPTION
    begin
        return 4100;
    end;
    select siteID into vint_siteID from tuser_member where id=iint_id;

    if vint_siteID is null then
        return 4400;
    end if;
    
    -- 分机
    if exists(select id from tuser_member where siteID=vint_siteID and userType='SEAT') then
        return 4301;
    end if;
    -- 队列
    if exists(select id from tcti_acd_queue where siteID=vint_siteID) then
        return 4302;
    end if;
    -- 中继
    if exists(select id from tcti_trunk where siteID=vint_siteID) then
        return 4303;
    end if;
    -- 语音
    if exists(select id from tgeneral_file where siteID=vint_siteID and exTags in ('sounds','moh')) then
        return 4304;
    end if;
    -- 语音菜单
    if exists(select id from tcti_voice_menu where siteID=vint_siteID) then
        return 4305;
    end if;
    -- 策略
    if exists(select id from tcti_task_strategy where siteID=vint_siteID) then
        return 4306;
    end if;
    -- 自动登录
    -- if exists(select id from tcti_auto_duty where siteID=vint_siteID) then
    --     return 4307;
    -- end if;
    -- 来话导航
    if exists(select id from tcti_income_nav where siteID=vint_siteID) then
        return 4308;
    end if;
    -- 删除租户
    delete from tuser_member where id=iint_id;
    -- 删除报表
    delete from tstat_seat_call where siteID=vint_siteID;
    delete from tstat_seat_work where siteID=vint_siteID;
    delete from tstat_extension_call where siteID=vint_siteID;
    delete from tstat_extension_work where siteID=vint_siteID;
    delete from tstat_queue_work where siteID=vint_siteID;
    -- 删除录音、充值记录、日结账单
    delete from cdr where siteID=vint_siteID;
    delete from tfinance_recharge  where siteID=vint_siteID;
    delete from tcti_daybill where siteID=vint_siteID;
    delete from tcti_meeting_room where siteID=vint_siteID;
    delete from tcti_workday where siteID=vint_siteID;
    delete from tcti_task where siteID=vint_siteID;
    delete from tcti_task_history where siteID=vint_siteID;
    return 1;
end;
