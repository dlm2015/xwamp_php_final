/**
* 更新cdr表的计费金额
* 用于存储过程proc_cdr_after_insert调用
* @since 1.0 2014-8-26 by sutroon
*/
create procedure func_cdr_update_amount(vint_cdrid int,vint_siteID int,vint_billsec int,vint_direction int)
proc:begin
    declare vflt_amount decimal(12,4) default 0.00;
    declare vflt_allowOverdraftAmount decimal(12,4)default 0.00;
    declare vint_firstSec int;
    declare vflt_firstRate decimal(12,4);
    declare vint_perSec int;
    declare vflt_perRate decimal(12,4);
    declare vint_inFeeRate int;
    declare vint_outFeeRate int;
    declare vflt_blance decimal(12,4);
    declare vflt_newblance decimal(12,4);
    declare vdat_date date;
    
    select DATE_FORMAT(end,'%Y-%m-%d') into vdat_date from cdr1 where id=vint_cdrid;
    call proc_debug('func_cdr_update_amount', concat('[正常计费] [',now(),'] cdrid=',ifnull(vint_cdrid,'null'),', siteID=',ifnull(vint_siteID,'null'),', billsec=',ifnull(vint_billsec,'null'),', direction=',ifnull(vint_direction,'null')), vint_siteID);
    if (vint_siteID is not null) and (vint_siteID < 100) and (vint_billsec > 0) then
        select outFeeRate, inFeeRate, allowOverdraftAmount, ifnull(blance,0.00) into vint_outFeeRate, vint_inFeeRate, vflt_allowOverdraftAmount, vflt_blance from tuser_member where siteID=vint_siteID and userType='ADMIN';

        -- 生成日结账单
        if not exists(select id from tcti_daybill where billdate=vdat_date and siteID=vint_siteID) then
            insert into tcti_daybill (billdate,createdTime,siteID,allowOverdraftAmount, blance) values (vdat_date,now(),vint_siteID,vflt_allowOverdraftAmount, vflt_blance);
        end if;
        
        -- 更新计费金额
        if vint_direction=1 then
            set vflt_amount=func_cdr_get_billsec_amount(vint_inFeeRate, vint_billsec);
            select blance into @daybillblance from tcti_daybill where siteID=vint_siteID and billdate=vdat_date;
            set vflt_newblance=@daybillblance-vflt_amount;
            update tcti_daybill set inAmount=inAmount+vflt_amount,subtotal=subtotal+vflt_amount,blance=blance-vflt_amount where siteID=vint_siteID and billdate=vdat_date;
            -- update tcti_daybill set inAmount=inAmount+vflt_amount,subtotal=subtotal+vflt_amount,blance=vflt_newblance where siteID=vint_siteID and billdate=vdat_date;
            update tuser_member set blance=vflt_newblance where siteID=vint_siteID and userType='ADMIN';
            update cdr set amount=vflt_amount where id=vint_cdrid;
            -- 调试
            call proc_debug('func_cdr_update_amount_1', concat('[正常计费] [',now(),'] cdrid=',ifnull(vint_cdrid,'null'),', siteID=',ifnull(vint_siteID,'null'),', billsec=',ifnull(vint_billsec,'null'),', amount=',ifnull(vflt_amount,'null'),', blance',ifnull(vflt_newblance,'null'),', direction=',ifnull(vint_direction,'null')), vint_siteID);
        elseif vint_direction=2 or vint_direction=5 then
            select firstSec, firstRate, perSec, perRate into vint_firstSec, vflt_firstRate, vint_perSec, vflt_perRate from tcti_payfeerate where payType=vint_outFeeRate;
            set vflt_amount=func_cdr_get_billsec_amount(vint_outFeeRate, vint_billsec);
            select blance into @daybillblance from tcti_daybill where siteID=vint_siteID and billdate=vdat_date;
            set vflt_newblance=@daybillblance-vflt_amount;
            update tcti_daybill set outAmount=outAmount+vflt_amount,subtotal=subtotal+vflt_amount,blance=blance-vflt_amount where siteID=vint_siteID and billdate=vdat_date;
            -- update tcti_daybill set outAmount=outAmount+vflt_amount,subtotal=subtotal+vflt_amount,blance=vflt_newblance where siteID=vint_siteID and billdate=vdat_date;
            update tuser_member set blance=vflt_newblance where siteID=vint_siteID and userType='ADMIN';
            update cdr set amount=vflt_amount where id=vint_cdrid;
            -- 调试
            call proc_debug('func_cdr_update_amount_2_5', concat('[正常计费] [',now(),'] cdrid=',ifnull(vint_cdrid,'null'),', siteID=',ifnull(vint_siteID,'null'),', billsec=',ifnull(vint_billsec,'null'),', amount=',ifnull(vflt_amount,'null'),', blance',ifnull(vflt_newblance,'null'),', 原daybill-blance=',ifnull(@daybillblance,'null'),', direction=',ifnull(vint_direction,'null')), vint_siteID);
        end if;
    end if;
end;
