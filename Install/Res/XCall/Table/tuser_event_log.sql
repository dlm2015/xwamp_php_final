create table if not exists tuser_event_log(
    id int auto_increment primary key comment '主键编号',
    userID int not null comment '用户编号',
    ip varchar(32) not null default '' comment 'IP',
    eAction varchar(16) not null default '' comment '事件动作(如:充值、查询、登录、新增、编辑、删除)',
    eObject varchar(32) not null default '' comment '事件对象(如：存储过程、表名、视图名、页面)',
    eSource varchar(255) not null default '' comment '事件来源(如：网址)',
    eLevel varchar(12) comment '事件级别(如：信息、错误、警告、攻击)',
    eDesc varchar(20480) comment '描述(如：充值100元)',
    exTags varchar(16) not null comment '扩展标签',
    `state` smallint not null default 0 comment '事件结果(1=成功,4=失败)',
    createdTime timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    siteID int not null default 0 comment '公司编号(公司管理员的uid)'
) comment '用户事件日志表 1.0 2014-6-27 by sutroon' ENGINE=MyISAM  default CHARSET=utf8;
