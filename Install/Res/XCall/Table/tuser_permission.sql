create table if not exists tuser_permission(
    id int auto_increment primary key comment '主键编号',
    `name` varchar(32) not null comment '权限名称',
    parentID int not null default 0 comment '上级权限编号',
    code varchar(32) not null default '' comment '权限代码',
    ordinal smallint not null default 999 comment '排序号码',
    `state` smallint not null default 1 comment '权限状态(0=禁用,1=正常)',
    createdTime datetime not null comment '创建时间',
    linkUrl varchar(255) not null default '' comment '菜单的URL',
    isMenu smallint not null default 0 comment '是否显示为菜单'
) comment '权限表 1.1 2014-6-16 by sutroon' ENGINE=MyISAM DEFAULT CHARSET=utf8;
