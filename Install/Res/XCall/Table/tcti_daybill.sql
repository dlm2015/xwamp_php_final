create table if not exists tcti_daybill(
    id int auto_increment primary key comment '主键编号',
    billdate date not null comment '计费日期(月日)',
    outAmount decimal(12,4) not null default 0.0000 comment '外呼金额',
    inAmount decimal(12,4) not null default 0.0000 comment '转座席金额',
    smsAmount decimal(12,4) not null default 0.0000 comment '短信金额',
    subtotal decimal(12,4) not null default 0.0000 comment '消费小计',
    rechargeAmount decimal(12,4) not null default 0.0000 comment '充值金额',
    allowOverdraftAmount decimal(12,4) not null default 0.0000 comment '可透支金额',
    blance decimal(12,4) not null default 0.0000 comment'余额',
    `state` smallint not null default 0 comment '状态',
    createdTime datetime comment '创建时间',
    siteID int comment '公司编号(公司管理员的uid)'
) comment '日结账单表 1.0 2014-7-9 by sutroon' ENGINE=InnoDB DEFAULT CHARSET=utf8;
