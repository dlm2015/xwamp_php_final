CREATE TABLE if not exists `tgeneral_region` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键编号',
  `regionName` varchar(32) DEFAULT NULL COMMENT '地区名称',
  `parentID` int(11) NOT NULL DEFAULT '0' COMMENT '上级地址编号',
  `zipCode` varchar(8) DEFAULT NULL COMMENT '邮政编码',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='中国地区表 1.0 2014-12-8 by sutroon';
