create table if not exists tcti_number_start(
    id int auto_increment primary key comment '主键编号',
    areaName varchar(32) not null comment '地区名称',
    numberPart varchar(32) not null default '' comment '号码前段部分',
    phoneLength varchar(32) not null comment '号码长度',
    ordinal int not null default 0 comment '排列次序',
    `state` smallint not null default 0 comment '号码状态(0=未呼,1已呼)'
) comment '号码范围表 1.0 2014-6-27 by sutroon' ENGINE=MyISAM DEFAULT CHARSET=utf8;
