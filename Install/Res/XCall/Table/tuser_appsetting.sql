create table if not exists tuser_appsetting(
    id int auto_increment primary key comment '主键编号',
    userID int not null comment '用户表主键编号',
    pageSize smallint not null default 25 comment '列表分页尺寸,默认25条',
    theme varchar(32) not null default '' comment '网站外观主题',
    lang varchar(32) not null default '' comment '语言'
) comment '用户-系统配置表 1.0 2014-9-25 by sutroon' ENGINE=MyISAM DEFAULT CHARSET=utf8;
