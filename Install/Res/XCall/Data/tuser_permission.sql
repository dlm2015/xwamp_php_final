/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50536
Source Host           : localhost:3306
Source Database       : db_xcall

Target Server Type    : MYSQL
Target Server Version : 50536
File Encoding         : 65001

Date: 2015-05-27 16:19:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tuser_permission
-- ----------------------------
DROP TABLE IF EXISTS `tuser_permission`;
CREATE TABLE `tuser_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键编号',
  `name` varchar(32) NOT NULL COMMENT '权限名称',
  `parentID` int(11) NOT NULL DEFAULT '0' COMMENT '上级权限编号',
  `code` varchar(32) NOT NULL DEFAULT '',
  `ordinal` smallint(6) NOT NULL DEFAULT '999' COMMENT '排序号码',
  `state` smallint(6) NOT NULL DEFAULT '1' COMMENT '权限状态(0=禁用,1=正常)',
  `createdTime` datetime NOT NULL COMMENT '创建时间',
  `linkUrl` varchar(255) DEFAULT NULL COMMENT '菜单的URL',
  `isMenu` smallint(6) NOT NULL DEFAULT '0' COMMENT '是否显示为菜单',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COMMENT='权限表 1.1 2014-6-16 by sutroon';

-- ----------------------------
-- Records of tuser_permission
-- ----------------------------
INSERT INTO `tuser_permission` VALUES ('1', '系统管理', '0', 'XT_MK', '1', '1', '2015-04-23 12:01:33', '', '1');
INSERT INTO `tuser_permission` VALUES ('2', '系统管理', '1', '', '2', '1', '2015-04-23 12:01:33', '', '1');
INSERT INTO `tuser_permission` VALUES ('3', '操作员管理', '2', '', '3', '1', '2015-04-23 12:01:33', '/index.php/XCall/Admin/admin_list.html', '1');
INSERT INTO `tuser_permission` VALUES ('4', '费率管理', '2', '', '4', '1', '2015-04-23 12:01:33', '/index.php/XCall/PayFee/', '1');
INSERT INTO `tuser_permission` VALUES ('5', '日结账单', '2', '', '5', '1', '2015-04-23 12:01:33', '/index.php/XCall/Statics/day_bill.html', '1');
INSERT INTO `tuser_permission` VALUES ('6', '充值查询', '2', '', '6', '1', '2015-04-23 12:01:33', '/index.php/XCall/Recharge/', '1');
INSERT INTO `tuser_permission` VALUES ('7', '系统配置', '1', '', '7', '1', '2015-04-23 12:01:34', '', '1');
INSERT INTO `tuser_permission` VALUES ('8', '分机管理', '7', '', '8', '1', '2015-04-23 12:01:34', '/index.php/XCall/Extension/extension_list.html', '1');
INSERT INTO `tuser_permission` VALUES ('9', '编辑分机', '8', '', '9', '1', '2015-04-23 12:01:34', '', '0');
INSERT INTO `tuser_permission` VALUES ('10', '中继管理', '7', '', '10', '1', '2015-04-23 12:01:34', '/index.php/XCall/Trunk/', '1');
INSERT INTO `tuser_permission` VALUES ('11', '语音管理', '7', '', '11', '1', '2015-04-23 12:01:34', '/index.php/XCall/Music/', '1');
INSERT INTO `tuser_permission` VALUES ('12', '上传语音', '11', '', '12', '1', '2015-04-23 12:01:34', '', '0');
INSERT INTO `tuser_permission` VALUES ('13', '删除语音', '11', '', '13', '1', '2015-04-23 12:01:34', '', '0');
INSERT INTO `tuser_permission` VALUES ('14', '语音菜单', '7', '', '14', '1', '2015-04-23 12:01:34', '/index.php/XCall/MusicMenu', '1');
INSERT INTO `tuser_permission` VALUES ('15', '新增语音菜单', '14', '', '15', '1', '2015-04-23 12:01:34', '', '0');
INSERT INTO `tuser_permission` VALUES ('16', '编辑语音菜单', '14', '', '16', '1', '2015-04-23 12:01:34', '', '0');
INSERT INTO `tuser_permission` VALUES ('17', '删除语音菜单', '14', '', '17', '1', '2015-04-23 12:01:34', '', '0');
INSERT INTO `tuser_permission` VALUES ('18', '队列管理', '7', '', '18', '1', '2015-04-23 12:01:34', '/index.php/XCall/Queue/', '1');
INSERT INTO `tuser_permission` VALUES ('19', '编辑队列', '18', '', '19', '1', '2015-04-23 12:01:34', '', '0');
INSERT INTO `tuser_permission` VALUES ('20', '工号管理', '7', '', '20', '1', '2015-04-23 12:01:34', '/index.php/XCall/Extension/seat_list.html', '1');
INSERT INTO `tuser_permission` VALUES ('21', '编辑工号', '20', '', '21', '1', '2015-04-23 12:01:34', '', '0');
INSERT INTO `tuser_permission` VALUES ('22', '重置工号密码', '20', '', '22', '1', '2015-04-23 12:01:34', '', '0');
INSERT INTO `tuser_permission` VALUES ('23', '会议室', '7', '', '23', '1', '2015-04-23 12:01:34', '/index.php/XCall/MeetingRoom/', '1');
INSERT INTO `tuser_permission` VALUES ('24', '工作日设置', '7', '', '24', '1', '2015-04-23 12:01:35', '/index.php/XCall/WorkDay/', '1');
INSERT INTO `tuser_permission` VALUES ('25', '编辑工作日', '24', '', '25', '1', '2015-04-23 12:01:35', '', '0');
INSERT INTO `tuser_permission` VALUES ('26', '短信服务器', '7', '', '26', '1', '2015-04-23 12:01:35', '/index.php/XCall/SMS/server_list.html', '1');
INSERT INTO `tuser_permission` VALUES ('27', '备份恢复', '1', '', '27', '1', '2015-04-23 12:01:35', '', '1');
INSERT INTO `tuser_permission` VALUES ('28', '数据备份', '27', '', '28', '1', '2015-04-23 12:01:35', '/index.php/XCall/XTool/data_backup.html', '1');
INSERT INTO `tuser_permission` VALUES ('29', '数据清理', '27', '', '29', '1', '2015-04-23 12:01:35', '/index.php/XCall/XTool/data_clear.html', '1');
INSERT INTO `tuser_permission` VALUES ('30', '外呼管理', '0', 'RW_MK', '30', '1', '2015-04-23 12:01:35', '', '1');
INSERT INTO `tuser_permission` VALUES ('31', '外呼任务', '30', '', '31', '1', '2015-04-23 12:01:35', '', '1');
INSERT INTO `tuser_permission` VALUES ('32', '外呼模式', '31', '', '32', '1', '2015-04-23 12:01:35', '/index.php/XCall/TaskStrategy/', '1');
INSERT INTO `tuser_permission` VALUES ('33', '编辑外呼模式', '32', '', '33', '1', '2015-04-23 12:01:35', '', '0');
INSERT INTO `tuser_permission` VALUES ('34', '删除任务', '32', '', '34', '1', '2015-04-23 12:01:35', '', '0');
INSERT INTO `tuser_permission` VALUES ('35', '回收任务', '32', '', '35', '1', '2015-04-23 12:01:35', '', '0');
INSERT INTO `tuser_permission` VALUES ('36', '批量删除任务', '32', '', '36', '1', '2015-04-23 12:01:35', '', '0');
INSERT INTO `tuser_permission` VALUES ('37', '添加号码', '31', '', '37', '1', '2015-04-23 12:01:35', '/index.php/XCall/Task/add_number.html', '0');
INSERT INTO `tuser_permission` VALUES ('38', '导入号码', '31', '', '38', '1', '2015-04-23 12:01:35', '/index.php/XCall/Task/import_number.html', '0');
INSERT INTO `tuser_permission` VALUES ('39', '生成号码', '31', '', '39', '1', '2015-04-23 12:01:35', '/index.php/XCall/Task/generate_number.html', '0');
INSERT INTO `tuser_permission` VALUES ('40', '号码生成历史', '31', '', '40', '1', '2015-04-23 12:01:35', '/index.php/XCall/Task/task_history_of_generate.html', '1');
INSERT INTO `tuser_permission` VALUES ('41', '统计分析', '0', 'TJ_MK', '41', '1', '2015-04-23 12:01:35', '', '1');
INSERT INTO `tuser_permission` VALUES ('42', '话务统计分析', '41', '', '42', '1', '2015-04-23 12:01:35', '', '1');
INSERT INTO `tuser_permission` VALUES ('43', '通话详单', '42', '', '43', '1', '2015-04-23 12:01:35', '/index.php/XCall/Statics/call_history.html', '1');
INSERT INTO `tuser_permission` VALUES ('44', '策略外呼分析', '42', '', '44', '1', '2015-04-23 12:01:35', '/index.php/XCall/Statics/seat_call_stat.html', '1');
INSERT INTO `tuser_permission` VALUES ('45', '分机外呼分析', '42', '', '45', '1', '2015-04-23 12:01:36', '/index.php/XCall/Statics/extension_call_stat.html', '1');
INSERT INTO `tuser_permission` VALUES ('46', '座席工作统计', '42', '', '46', '1', '2015-04-23 12:01:36', '/index.php/XCall/Statics/seat_work_stat.html', '1');
INSERT INTO `tuser_permission` VALUES ('47', '分队列通话时长统计', '42', '', '47', '1', '2015-04-23 12:01:36', '/index.php/XCall/Statics/queue_calltime_stat.html', '1');
INSERT INTO `tuser_permission` VALUES ('48', '分座席通话时长统计', '42', '', '48', '1', '2015-04-23 12:01:36', '/index.php/XCall/Statics/seat_calltime_stat.html', '1');
INSERT INTO `tuser_permission` VALUES ('49', '系统话务统计', '42', '', '49', '1', '2015-04-23 12:01:36', '/index.php/XCall/Statics/system_call_stat.html', '1');
INSERT INTO `tuser_permission` VALUES ('50', '系统呼损统计', '42', '', '50', '1', '2015-04-23 12:01:36', '/index.php/XCall/Statics/system_call_loss_stat.html', '1');
INSERT INTO `tuser_permission` VALUES ('51', '录音查询', '41', '', '51', '1', '2015-04-23 12:01:36', '', '1');
INSERT INTO `tuser_permission` VALUES ('52', '录音查询', '51', '', '52', '1', '2015-04-23 12:01:36', '/index.php/XCall/Statics/record_list.html', '1');
INSERT INTO `tuser_permission` VALUES ('53', '留言查询', '51', '', '53', '1', '2015-04-23 12:01:36', '/index.php/XCall/Statics/message_list.html', '1');
INSERT INTO `tuser_permission` VALUES ('54', '满意度查询', '51', '', '54', '1', '2015-04-23 12:01:36', '/index.php/XCall/Statics/evaluation_list.html', '1');
INSERT INTO `tuser_permission` VALUES ('55', '短信管理', '0', 'DX_MK', '55', '1', '2015-04-23 12:01:36', '', '1');
INSERT INTO `tuser_permission` VALUES ('56', '短信管理', '55', '', '56', '1', '2015-04-23 12:01:36', '', '1');
INSERT INTO `tuser_permission` VALUES ('57', '策略外呼短信管理', '56', '', '57', '1', '2015-04-23 12:01:36', '/index.php/XCall/SMS/strategycall_sms_list.html', '1');
INSERT INTO `tuser_permission` VALUES ('58', '新增策略外呼短信', '57', '', '58', '1', '2015-04-23 12:01:36', '', '0');
INSERT INTO `tuser_permission` VALUES ('59', '分机外呼短信管理', '56', '', '59', '1', '2015-04-23 12:01:36', '/index.php/XCall/SMS/extensioncall_sms_list.html', '1');
INSERT INTO `tuser_permission` VALUES ('60', '新增分机外呼短信', '59', '', '60', '1', '2015-04-23 12:01:36', '', '0');
INSERT INTO `tuser_permission` VALUES ('61', '导入发送', '56', '', '61', '1', '2015-04-23 12:01:36', '/index.php/XCall/SMS/sms_import.html', '1');
INSERT INTO `tuser_permission` VALUES ('62', '短信详单', '56', '', '62', '1', '2015-04-23 12:01:36', '/index.php/XCall/SMS/send_history.html', '1');
INSERT INTO `tuser_permission` VALUES ('63', '来话服务', '0', 'LH_MK', '63', '1', '2015-04-23 12:01:37', '', '1');
INSERT INTO `tuser_permission` VALUES ('64', '服务管理', '63', '', '64', '1', '2015-04-23 12:01:37', '', '1');
INSERT INTO `tuser_permission` VALUES ('65', '来话导航', '64', '', '65', '1', '2015-04-23 12:01:37', '/index.php/XCall/IncomingCall/rule_list.html', '1');
INSERT INTO `tuser_permission` VALUES ('66', '新增来话导航', '65', '', '66', '1', '2015-04-23 12:01:37', '', '0');
INSERT INTO `tuser_permission` VALUES ('67', '编辑来话导航', '65', '', '67', '1', '2015-04-23 12:01:37', '', '0');
INSERT INTO `tuser_permission` VALUES ('68', '黑名单', '64', '', '68', '1', '2015-04-23 12:01:37', '/index.php/XCall/IncomingCall/black_list.html', '1');
INSERT INTO `tuser_permission` VALUES ('69', '新增黑名单', '68', '', '69', '1', '2015-04-23 12:01:37', '', '0');
INSERT INTO `tuser_permission` VALUES ('70', '编辑黑名单', '68', '', '70', '1', '2015-04-23 12:01:37', '', '0');
INSERT INTO `tuser_permission` VALUES ('71', '删除黑名单', '68', '', '71', '1', '2015-04-23 12:01:37', '', '0');
INSERT INTO `tuser_permission` VALUES ('72', '运行状态', '0', 'ZT_MK', '72', '1', '2015-04-23 12:01:37', '', '1');
INSERT INTO `tuser_permission` VALUES ('73', '运行状态', '72', '', '73', '1', '2015-04-23 12:01:37', '', '1');
INSERT INTO `tuser_permission` VALUES ('74', '当前通话', '73', '', '74', '1', '2015-04-23 12:01:37', '/index.php/XCall/Status/trunk_status.html', '1');
INSERT INTO `tuser_permission` VALUES ('75', '分机在线状态', '73', '', '75', '1', '2015-04-23 12:01:37', '/index.php/XCall/Status/extension_status.html', '1');
INSERT INTO `tuser_permission` VALUES ('76', '分机通话状态', '73', '', '76', '1', '2015-04-23 12:01:37', '/index.php/XCall/Status/extension_talk_status.html', '1');
INSERT INTO `tuser_permission` VALUES ('77', '队列状态', '73', '', '77', '1', '2015-04-23 12:01:37', '/index.php/XCall/Status/acdno_status.html', '1');
INSERT INTO `tuser_permission` VALUES ('78', '服务器状态', '73', '', '78', '1', '2015-04-23 12:01:37', '/index.php/XCall/Status/sip_server_status.html', '1');
INSERT INTO `tuser_permission` VALUES ('79', '系统测试', '73', '', '79', '1', '2015-04-23 12:01:37', '/index.php/XCall/CtiTester/', '1');
INSERT INTO `tuser_permission` VALUES ('80', '其他管理', '0', '', '80', '1', '2015-04-24 15:16:12', '', '1');
INSERT INTO `tuser_permission` VALUES ('81', '客资管理', '80', '', '81', '1', '2015-04-24 15:16:12', '', '1');
INSERT INTO `tuser_permission` VALUES ('82', '客资管理', '81', '', '82', '1', '2015-04-24 15:16:12', '?s=ApiCustomer/index.html', '1');
INSERT INTO `tuser_permission` VALUES ('83', '学习交流', '81', '', '83', '1', '2015-04-24 15:16:12', '?s=Article/notice_list.html', '1');
INSERT INTO `tuser_permission` VALUES ('84', '财务报表', '81', '', '84', '1', '2015-04-24 15:16:12', '?s=FinanceStat/financestat_list.html', '1');
INSERT INTO `tuser_permission` VALUES ('85', '跑马灯管理', '81', '', '85', '1', '2015-04-24 15:16:12', '?s=Notice/notice_list.html', '1');
