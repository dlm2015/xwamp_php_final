drop table if exists t_porg_document_category;
create table if not exists t_porg_document_category (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null comment '名称',
    pid int not null default 0 comment '父级编号',
    type_id int not null default 0 comment '类型编号(1:单独页面,会生成一个全新的页面;2:文章栏目,用于在此栏目下添加文章;3:分类链接,可以链接到指定模块分类;4:链接地址,增加外链，如论坛地址;5:文件栏目,用户在此栏目下添加文件)',
    item_count int not null default 0 comment '子项目数量(如相册显示相片数量等)',
    visit_count int not null default 0 comment '浏览次数',
    picture_url varchar(255) not null default '' comment '图片路径',
    link_url varchar(255) not null default '' comment '链接地址',
    user_name varchar(32) not null default '' comment '创建人用户名',
    access varchar(255) not null default '' comment '访问权限(如相册中FRIEND=朋友,ALL=公开,PRIVATE=私密)',
    remark varchar(32) not null default '' comment '备注',
    sort smallint not null default 0 comment '排列次序',
    `status` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX idx_pid (pid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '文档类别表\r\n@since 1.0 <2015-4-8> sutroon <14507247@qq.com> Added.';