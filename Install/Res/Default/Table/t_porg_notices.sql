/**
通用文档表
1.如果数据比较多，可以平行拆表，如tarticle_notice,tarticle_news
2.商品知识库也用这个表结构,表名称为tproduct_article,商品ID对应item_id
*/
drop table if exists t_porg_notices;
create table if not exists t_porg_notices (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null comment '标题',
    category_id int not null default 0 comment '类别编号',
    meta_keywords varchar(32) not null default '' comment 'meta关键词',
    meta_description varchar(128) not null default '' comment 'meta描述',
    picture_url varchar(255) not null default '' comment '图片路径',
    visit_count int not null default 0 comment '浏览次数',
    content varchar(20480) not null default '' comment '文档内容',
    user_name varchar(32) not null default '' comment '创建人用户名',
    content_tags varchar(16) not null default '' comment '内容标签',
    sort smallint not null default 0 comment '排列次序',
    `status` smallint not null default 0 comment '状态(0=隐藏,1=显示,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    update_time datetime not null comment '更新时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX idx_title (title),
    INDEX idx_category_id (category_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '公告表\r\n@since 1.0 <2016-2-3> sutroon<14507427@qq.com> Added.';
