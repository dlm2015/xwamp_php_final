drop table if exists t_porg_customer_attr;
create table t_porg_customer_attr (
    id int auto_increment primary key comment '主键编号',
    type_id int not null default 0 comment '客户类型编号', 
    group_name varchar(16) not null default '' comment '属性分组(基本资料;联系方式)',
    is_system char(1) not null default 'Y' comment '是否系统字段(系统字段来自customer主表,自定义字段存放在customer_attr_n属性扩展表',
    is_editable char(1) not null default 'Y' comment '是否允许编辑(有些系统字段只能展示,禁止编辑,如工号)',
    title varchar(16) not null default '' comment '属性标题(姓名,手机号码,联系地址,性别,QQ,安全问题,回答等)',
    help_text varchar(16) not null default '' comment '属性帮助(如有填写则界面上的鼠标划过会显示帮助说明)',
    -- 字段属性 --
    field_name varchar(16) not null default '' comment '字段名称(英文)',
    field_type varchar(16) not null default 'varchar' comment '字段数据类型',
    field_length varchar(8) not null default '' comment '字段数据长度',
    field_decimals char(1) not null default '' comment '小数长度',
    field_default varchar(32) not null default '' comment '默认值',
    field_comment varchar(128) not null default '' comment '字段注释',
    -- /字段属性 --
    -- 字段控制 --
    is_required char(1) not null default 'N' comment '是否必填',
    is_listable char(1) not null default 'N' comment '是否列表显示',
    is_searchable char(1) not null default 'N' comment '是否搜索',
    is_importable char(1) not null default 'N' comment '是否导入',
    is_exportable char(1) not null default 'N' comment '是否导出',
    is_popup char(1) not null default 'N' comment '是否来电弹屏显示',
    -- /字段控制 --
    -- 选项属性 --
    options_content varchar(128) not null default '' comment '配置内容(json)',
    options_input_type varchar(16) not null default '' comment '输入控件类型(text:字符输入框;int:数字输入框;decimal:小数输入框;email:邮件输入框;mobile:手机号码输入框;telphone:电话号码输入框;date:日期选择框(xxxx-xx-xx);datetime:日期和时间选择框(xxxx-xx-xx xx:xx:xx);time:时间选择框(xx:xx);select:下拉选择框;radio:单选框;checkbox:复选框;textarea:多行文本框;editor:可视化编辑器;file:文件上传框)',
    -- options_value varchar(255) not null default '' comment '配置值',
    -- options_content_type varchar(16) not null default '' comment '配置内容数据类型(CHAR:字符;NUMBER:数字;TEXT:文本;ARRAY:数组;ENUM:枚举,EMAIL,URL,MOBILE,TELPHONE)值类型',
    -- options_validate_type varchar(20) not null default '' comment '输入验证类型(varchar=输入项(输入内容不限制);int=仅限输入整数;decimal=仅限输入小数;chars=仅限输入字符;chars2num=仅限输入数字和字符;date=仅限输入日期(年月日);email=仅限输入电子邮件;url=仅限输入超链接地址;mobile=仅限输入手机号码;telphone=仅限输入电话号码)',
    -- /选项属性 --
    sort smallint not null default 0 comment '排序',
    status smallint not null default 1 comment '是否可用(0:禁用;1:可用)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号,在此表功能相当于ParentID,超级管理员可以给每个公司分配一个顶级管理员(siteID=0),顶级管理员可以再给本公司分配多个普通管理员,siteID=该顶级管理员的uid',
    INDEX idx_site_id (site_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '用户属性表\r\n@since 1.0 <2015-10-28> SoChishun <14507247@qq.com> Added.\r\n@since 2.0 <2016-1-15> SoChishun <14507247@qq.com> 重构.\r\n';

-- insert into t_porg_user_attr (id, title) value ('客户编号',);
-- insert into t_porg_user_attr (id, title) value ('购买意向',);
-- insert into t_porg_user_attr (id, title) value ('营销结果',);
-- insert into t_porg_user_attr (id, title) value ('自定义标签',);
-- insert into t_porg_user_attr (id, title) value ('消费次数',);
-- insert into t_porg_user_attr (id, title) value ('营销结果',);
