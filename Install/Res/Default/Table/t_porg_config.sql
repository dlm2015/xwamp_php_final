drop table if exists t_porg_config;
create table if not exists t_porg_config (
    id int auto_increment primary key comment '主键编号',
    `name` varchar(32) not null comment '配置名称',
    title varchar(32) not null default '' comment '配置标题',
    `type` char(1) not null default 'C' comment '配置类型(C:字符;N:数字;T:文本;A:数组;E:枚举)',
    `group` char(1) not null default '' comment '配置分组(B=基本,C=内容,U=用户,S=系统)',
    prop char(1) not null default 'C' comment '配置归属(O:厂商预置,不可删除;C:用户自定义)',
    content varchar(128) not null default '' comment '配置内容',
    `value` varchar(512) not null default '' comment '配置值',
    remark varchar(32) not null default '' comment '配置说明',
    status smallint not null default 0 comment '状态',
    sort smallint not null default 0 comment '排序',
    create_time timestamp not null default current_timestamp comment '创建时间',
    update_time datetime comment '更新时间',
    UNIQUE INDEX idxu_name (`name`),
    INDEX `idx_group` (`group`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '系统配置表\r\n@since 1.0 <2015-10-19> SoChishun <14507247@qq.com> Added.';

