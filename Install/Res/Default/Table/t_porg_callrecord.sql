drop table if exists t_porg_callrecord;
create table if not exists t_porg_callrecord(
    id int auto_increment primary key comment '主键编号',
    line varchar(32) not null default '' comment '电话号码',
    exten varchar(16) not null default '' comment '分机号',
    customer_no varchar(32) not null default '' comment '客户编号',
    customer_name varchar(16) not null default '' comment '客户姓名',
    caller_no varchar(16) not null default '' comment '主叫号码',
    called_no varchar(16) not null default '' comment '被叫号码',
    start_time datetime not null comment '开始呼叫时间',
    end_time datetime not null comment '交谈结束时间',
    duration int not null default 0 comment '振铃时间+通话时间',
    billsec int not null default 0 comment '通话时长(计费时长)',
    direction smallint not null default 0 comment '呼叫方向(1=接入(策略外呼后转分机)[呼入费率组],2=呼出(策略外呼)[呼出费率组],3=网内(分机互打),4=来话(系统被叫来话),5=外呼(分机直接呼出)[呼出费率组],6=签入(分机签入队列),7=签出(分机签出队列),8=未接(策略外呼转队列未接或来话未接))',
    recording_file varchar(255) not null default '' comment '录音文件地址',
    is_recording_shared char(1) not null default 'N' comment '是否分享录音',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    INDEX idx_exten (exten),
    INDEX idx_line (line)
) comment '通话记录表(来电弹屏表) 1.0 2014-10-23 by sutroon\r\n2.0 2016-2-1 SoChishun 重构' ENGINE=MyISAM DEFAULT CHARSET=utf8;