drop table if exists t_porg_customer_dispatch;
create table if not exists t_porg_customer_dispatch(
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null default '' comment '批次名称(默认时间戳)',
    customer_id int not null comment '客户表主键编号',
    old_user_name varchar(16) not null comment '原营销员用户名称',
    new_user_name varchar(16) not null comment '新营销员用户名称',
    admin_name varchar(16) not null comment '操作员用户名称',
    dispatch_remark varchar(32) not null default '' comment '备注',
    status smallint not null default 0 comment '状态',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX idx_customer_id (customer_id),
    INDEX idx_title (title),
    INDEX idx_new_user_name (new_user_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '客户调度表\r\n@since 1.0 <2014-12-24> sutroon <14507247@qq.com> Added.';
