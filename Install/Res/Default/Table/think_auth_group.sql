/**
 * 用户组(角色)表
 * Author:  SoChishun
 * Created: 2016-4-5
 */
drop table if exists think_auth_group;
create table if not exists think_auth_group (
    id int auto_increment primary key comment '主键编号',
    title varchar(30) not null default '',
    display_title varchar(30) not null default '',
    `status` smallint not null default 1,
    rules varchar(800) not null default '',
    code varchar(32) not null default '' comment '角色代码',
    remark varchar(32) not null default '' comment '备注',
    sort smallint not null default 0 comment '排列次序',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用户组(角色)表';