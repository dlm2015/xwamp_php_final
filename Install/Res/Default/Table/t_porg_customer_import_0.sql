drop table if exists t_porg_customer_import_0;
create table if not exists t_porg_customer_import_0 (
    serial_no varchar(32) not null default '',
    `name` varchar(16) not null,
    sex varchar(6) not null default '未知',
    id_no varchar(32) not null default '',
    telphone varchar(32) not null default '',
    address varchar(200) not null default '',
    zip varchar(8) not null default '',
    INDEX idx_telphone (telphone)
) comment '客户导入临时表\r\n1.0 2016-2-25 SoChishun Added.' ENGINE=MEMORY  default CHARSET=utf8;