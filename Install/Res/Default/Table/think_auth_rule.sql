/**
 * 权限规则表
 * Author:  SoChishun
 * Created: 2016-4-5
 */
drop table if exists think_auth_rule;
create table if not exists think_auth_rule (
    id int auto_increment primary key comment '主键编号',
    `name` varchar(64) not null default '' comment '权限名称(模块、控制器或操作的名称)',
    title varchar(50) not null comment '权限标题',
    code varchar(32) not null default '' comment '菜单代码(必须加厂商代码前缀,如：P10000_M1_YH,P10000_YH_M2_YHGL,P10000_YH_M3_GLYGL,P10000_YH_GLYGL_XZGLY)',
    pid smallint(6) unsigned not null comment '父级编号(模块pid默认是0)',
    addon varchar(32) not null default '' comment '插件名称(应用模块名称,用于卸载应用模块时索引)',
    `level` tinyint(1) unsigned not null comment '节点等级(1:模块,2:控制器,3:操作)',
    `type` varchar(16) not null default '' comment '类型(M(Menu)=菜单,O(Operate)=操作,F(File)=文件,E(Element)=页面元素)',
    url varchar(64) not null default '' comment '链接URL',
    `condition` varchar(128) not null default '' comment '规则附件条件,满足附加条件的规则,才认为是有效的规则',
    remark varchar(32) not null default '' comment '备注',
    sort smallint not null default 0 comment '排列次序',
    status smallint not null default '0' comment '状态(0=禁用,1=可用)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    INDEX ix_code (code),
    INDEX ix_pid (pid),
    INDEX ix_addon (addon)
    ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='权限规则(节点)表\r\n
    @since 1.0 <2014-6-28> SoChishun <14507247@qq.com> Added.\r\n
    @since 1.1 <2015-8-28> SoChishun 修改:parent_id 改为 parent_code.\r\n
    @since 1.2 <2015-8-29> SoChishun 新增:vendor_code,appmodule_name.\r\n
    @since 2.0 <2015-9-19> SoChishun 重构以适合RBAC的自动拦截';