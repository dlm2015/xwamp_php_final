drop table if exists t_porg_knowledge_category;
create table if not exists t_porg_knowledge_category (
    id int auto_increment primary key comment '主键编号',
    title varchar(32) not null comment '名称',
    pid int not null default 0 comment '父级编号',
    user_name varchar(32) not null default '' comment '创建人用户名',
    remark varchar(32) not null default '' comment '备注',
    sort smallint not null default 0 comment '排列次序',
    `status` smallint not null default 0 comment '状态(0=未审核,1=正常,4=关闭)',
    create_time timestamp not null default CURRENT_TIMESTAMP comment '创建时间',
    site_id int not null default 0 comment '公司编号(公司管理员的uid)',
    INDEX idx_pid (pid),
    INDEX idx_user_name (user_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 comment '知识库类别表\r\n@since 1.0 <2016-2-3> sutroon <14507247@qq.com> Added.';