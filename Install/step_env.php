<?php
/**
 * 环境检测
 * @since 1.0 <2015-5-25> SoChishun <14507247@qq.com> Added.
 */
include 'function.php';
$config = load_config();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>环境检测-INSTALL</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="//libs.useso.com/js/yui/3.17.2/cssreset/cssreset-min.css" rel="stylesheet" />
        <link href="./Skin/theme-blue.css" rel="stylesheet" />
        <script src="//libs.useso.com/js/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>        
    </head>
    <body class="page-step-env">
        <div class="header">
            <div class="frame-style">
                <div class="logo"><?php echo $config['APP_NAME'] ?> INSTALL安装向导</div>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="./install.php">欢迎界面</a></li>
                        <li class="cur">环境检测</li>
                        <li>系统配置</li>
                        <li>初始化数据</li>
                        <li>完成安装</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bodier">
            <section>
                <h3>环境检查</h3>
                <table cellspacing="0" class="table-grid">
                    <tr><th>检查项目</th><th>最低需求</th><th>最佳配置</th><th>当前服务器</th></tr>
                    <tr><td>PHP版本</td><td>5.3</td><td>5.3以上,6.0以下</td><td><?php echo PHP_VERSION; ?></td></tr>
                    <tr><td>附件上传</td><td>不限</td><td>2M</td><td><?php echo show("upload_max_filesize"); ?></td></tr>
                    <tr><td>GD库</td><td>1.0</td><td>2.0以上</td><td><?php
                            if (function_exists(gd_info)) {
                                $gd_info = @gd_info();
                                echo $gd_info["GD Version"];
                            } else {
                                echo '<span class="no">否</span>';
                            }
                            ?></td></tr>
                    <tr><td>磁盘空间</td><td>50MB</td><td>300MB</td><td><?php echo round(disk_free_space(realpath('../')) / 1024 / 1024, 2) . 'MB'; ?></td></tr>
                </table>
            </section>
            <section>
                <h3>目录/文件权限检测</h3>
                <table cellspacing="0" class="table-grid">
                    <tr><th>目录文件</th><th>所需状态</th><th>当前状态</th></tr>
                    <tr><td>./Uploads</td><td>可写</td><td><?php echo check_writeable('../Uploads/') ?></td></tr>
                    <tr><td>./Application/Runtime</td><td>可写</td><td><?php echo check_writeable('../Application/Runtime/') ?></td></tr>
                </table>
            </section>
            <section>
                <h3>PHP相关参数</h3>
                <table cellspacing="0" class="table-grid">
                    <tr><td>PHP运行方式</td><td><?php echo strtoupper(php_sapi_name()); ?></td><td>PHP版本</td><td><?php echo PHP_VERSION; ?></td></tr>
                    <tr><td>PHP安全模式（safe_mode）</td><td><?php echo show("safe_mode"); ?></td><td>脚本占用最大内存（memory_limit）</td><td><?php echo show("memory_limit"); ?></td></tr>
                    <tr><td>POST方法提交最大限制（post_max_size）</td><td><?php echo show("post_max_size"); ?></td><td>上传文件最大限制（upload_max_filesize）</td><td><?php echo show("upload_max_filesize"); ?></td></tr>
                    <tr><td>脚本超时时间（max_execution_time）</td><td><?php echo show("max_execution_time"); ?>秒</td><td>socket超时时间（default_socket_timeout）</td><td><?php echo show("default_socket_timeout"); ?>秒</td></tr>
                    <tr><td>Cookie 支持</td><td><?php echo isset($_COOKIE) ? '<span class="ok">支持</span>' : '<span class="no">不支持</span>'; ?></td><td>Session支持</td><td><?php echo isfun("session_start"); ?></td></tr>
                    <tr><td>PHP已编译模块检测</td><td colspan="3" class="break-line"><?php echo implode(', ', get_loaded_extensions()); ?></td></tr>
                </table>
            </section>
            <section>
                <h3>组件支持</h3>
                <table cellspacing="0" class="table-grid">
                    <tr><td>GD库支持</td><td>
                            <?php
                            if (function_exists(gd_info)) {
                                $gd_info = @gd_info();
                                echo $gd_info["GD Version"];
                            } else {
                                echo '<span class="no">否</span>';
                            }
                            ?></td><td>压缩文件支持(Zlib)</td><td><?php echo isfun("gzclose"); ?></td></tr>
                    <tr><td>Iconv编码转换</td><td><?php echo isfun("iconv"); ?></td><td>mbstring</td><td><?php echo isfun("mb_eregi"); ?></td></tr>
                    <tr><td>Zend版本</td><td><?php
                            $zend_version = zend_version();
                            echo (empty($zend_version)) ? '<span class="no">否</span>' : $zend_version;
                            ?></td><td><?php
                            $PHP_VERSION = PHP_VERSION;
                            $PHP_VERSION = substr($PHP_VERSION, 2, 1);
                            echo ($PHP_VERSION > 2) ? "ZendGuardLoader[启用]" : "Zend Optimizer";
                            ?></td><td><?php
                                if ($PHP_VERSION > 2) {
                                    echo (get_cfg_var("zend_loader.enable")) ? '<span class="ok">支持</span>' : '<span class="no">不支持</span>';
                                } else {
                                    if (function_exists('zend_optimizer_version')) {
                                        echo zend_optimizer_version();
                                    } else {
                                        echo (get_cfg_var("zend_optimizer.optimization_level") || get_cfg_var("zend_extension_manager.optimizer_ts") || get_cfg_var("zend.ze1_compatibility_mode") || get_cfg_var("zend_extension_ts")) ? '<span class="ok">支持</span>' : '<span class="no">不支持</span>';
                                    }
                                }
                                ?></td></tr>
                </table>
            </section>
            <section>
                <h3>数据库支持</h3>
                <table cellspacing="0" class="table-grid">
                    <tr><td>MySQL 数据库</td><td><?php
                            echo isfun("mysql_close");
                            if (function_exists("mysql_get_server_info")) {
                                $s = @mysql_get_server_info();
                                $s = $s ? '&nbsp; mysql_server 版本：' . $s : '';
                                $c = '&nbsp; mysql_client 版本：' . @mysql_get_client_info();
                                echo $s;
                            }
                            ?></td><td>ODBC 数据库</td><td><?php echo isfun("odbc_close"); ?></td></tr>
                    <tr><td>SQLite 数据库</td><td><?php
                            if (extension_loaded('sqlite3')) {
                                $sqliteVer = SQLite3::version();
                                echo '<span class="ok">支持</span>　', "SQLite3　Ver ", $sqliteVer[versionString];
                            } else {
                                echo isfun("sqlite_close");
                                if (isfun("sqlite_close") == '<span class="ok">支持</span>') {
                                    echo "&nbsp; 版本： " . @sqlite_libversion();
                                }
                            }
                            ?></td><td>SQL Server 数据库</td><td><?php echo isfun("mssql_close"); ?></td></tr>
                </table>
            </section>
            <div class="more">更多环境检测信息：<a href="./tz.php" target="_blank">[探针]</a><a href="tz.php?act=phpinfo" target="_blank">[phpinfo]</a></div>
        </div>
        <div class="footer"><a href="./install.php">上一步</a><a href="./step_db_setting.php">下一步</a></div>
    </body>
</html>
<?php

//检测PHP设置参数
function show($varName) {
    switch ($result = get_cfg_var($varName)) {
        case 0:
            return '<span class="no">否</span>';
        case 1:
            return '<span class="ok">是</span>';
        default:
            return $result;
    }
}

// 检测函数支持
function isfun($funName = '') {
    if (!$funName || trim($funName) == '' || preg_match('~[^a-z0-9\_]+~i', $funName, $tmp))
        return '错误';
    return (false !== function_exists($funName)) ? '<span class="ok">支持</span>' : '<span class="no">不支持</span>';
}

// 检测文件或目录是否可写 2015-5-26 SoChishun Added.
function check_writeable($path) {
    if (!is_dir($path) && !is_file($path)) {
        return '<span class="no">不存在</span>';
    }
    return is_writeable($path) ? '<span class="ok">可写</span>' : '<span class="no">不可写</span>';
}
?>
    