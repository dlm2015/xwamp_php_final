<?php
/**
 * 安装向导
 * @since 1.0 <2015-5-25> SoChishun <14507247@qq.com> Added.
 */
include 'function.php';
$config = load_config();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>安装向导-INSTALL</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style type="text/css">
            body { background-color:#007FAC; color:#FFF; margin:0px; font-size:12px;}
            a { color:#FFF; text-decoration: none;}
            #head { position: absolute; top:43%; left:0; text-align: center; width:100%; }
            h1 { font-size:30px;}
        </style>
    </head>
    <body>
        <div id="head">
            <h1><?php echo $config['APP_NAME'] ?> Install安装向导</h1>
            <div><a href="step_env.php">[开始安装]</a></div>
        </div>
    </body>
</html>
